TTitan.UI = {}
TTitan.UI.Data = {}
TTitan.UI.DataLoaded = false
TTitan.UI.DataSource = {}
TTitan.UI.Type = TTitan.Type.BESTIARY

TTitan.UI.ListBoxData = {}
TTitan.UI.OrderTable = {}
TTitan.UI.RowToEntryMap = {}
TTitan.UI.NumTotalRows = 0
TTitan.UI.LastPressedButtonId = 0
TTitan.UI.AlphaValue = 1
-- Setup local variables for animated text label
local TitleBarDelay = 0.1 -- letter display rate in seconds
local TitleBarDelayCheck = TitleBarDelay
local TitleBarAnimatedStage = 0 -- just declareing it a number
local titletext = L"Tome Titan - Скрытые задания истинных искателей."

function TTitan.UI.OnUpdate(timePassed)
	if WindowGetShowing("TTitanUI") == true then
		TitleBarDelayCheck = TitleBarDelayCheck - timePassed
		if TitleBarDelayCheck < 0 then
			TitleBarDelayCheck = TitleBarDelay 
			TTitan.UI.TitleBarAnimated()
		end
	else
		TitleBarAnimatedStage = 0 -- reset the titlebar 
	end
end

function TTitan.UI.Init()
	TTmodules.OnInitialize()
	TTitan.UI.TomeClearNewEntriesCheckForNew()
end

function TTitan.UI.Initialize()
	-- Misc. Stuff
	LabelSetText("TTitanUITitleBarText", towstring(titletext))
	DynamicImageSetTexture("TTitanUIFAQButtonIconBase", "icon020084", 0, 0) -- "T" button in bottom right corner of TomeTitan window
	
	-- Set the About this Add-on page as the default
	TTitan.UI.SetDataSource(TTitan.Data.Credits)
	TTitan.UI.DisplayRow(1)
	TTitan.UI.DisplayRow(2)

	-- Create button that appears by the 'New Entries' list in the ToK
	CreateWindowFromTemplate("TTitanClearNewTomeList", "TTitanClearNewTomeList", "TomeWindowTitlePagePageWindowContentsChild")
	ButtonSetTextColor("TTitanClearNewTomeList", 0, 0, 100, 0 )
	ButtonSetText("TTitanClearNewTomeList", L"Удалить\n  список")


	-- Setup check box for toggling hiding completed unlocks.
	LabelSetText("TTitanUICheckBoxLabel", L"Прозрачность и Видемость")
	ButtonSetPressedFlag("TTitanUICheckBoxButton", TTitan.Settings["hidecompleted"])

	-- Splash text and button on ToK Title page and Bestiary Pages
	-- Create buttons/labels
	CreateWindowFromTemplate("TTitanSplashTextTitle", "TTitanSplashTextTitle", "TomeWindowTitlePagePageWindowContentsChild")
	CreateWindowFromTemplate("TTitanSplashTextTitleClick", "TTitanSplashTextTitleClick", "TomeWindowTitlePagePageWindowContentsChild")
	CreateWindowFromTemplate("TTitanSplashTextBestiary", "TTitanSplashTextBestiary", "BestiaryTOCPageWindowContentsChild")
	CreateWindowFromTemplate("TTitanSplashTextBestiaryClick", "TTitanSplashTextBestiaryClick", "BestiaryTOCPageWindowContentsChild")
	CreateWindowFromTemplate("TTitanSplashTextNoteworthy", "TTitanSplashTextNoteworthy", "NoteworthyPersonsTOC")
	CreateWindowFromTemplate("TTitanSplashTextNoteworthyClick", "TTitanSplashTextNoteworthyClick", "NoteworthyPersonsTOC")
	CreateWindowFromTemplate("TTitanSplashTextHistory", "TTitanSplashTextHistory", "HistoryAndLoreTOC")
	CreateWindowFromTemplate("TTitanSplashTextHistoryClick", "TTitanSplashTextHistoryClick", "HistoryAndLoreTOC")

	----Set text/color of label 1 and button 1 (appears on ToK Title page)
	LabelSetText("TTitanSplashTextTitle", L"Смотреть ")
	ButtonSetText("TTitanSplashTextTitleClick", L" Tome Titan")
	ButtonSetTextColor("TTitanSplashTextTitleClick", 0, 0, 100, 0 )
	---- Set text/color of label 2 and button 2 (appears on ToK Bestiary page)
	LabelSetText("TTitanSplashTextBestiary", L"Смотреть ")
	ButtonSetText("TTitanSplashTextBestiaryClick", L" Tome Titan")
	ButtonSetTextColor("TTitanSplashTextBestiaryClick", 0, 0, 100, 0 )
	---- Set text/color of label 3 and button 3 (appears on ToK Noteworthy page)
	LabelSetText("TTitanSplashTextNoteworthy", L"Смотреть ")
	ButtonSetText("TTitanSplashTextNoteworthyClick", L" Tome Titan")
	ButtonSetTextColor("TTitanSplashTextNoteworthyClick", 0, 0, 100, 0 )
	---- Set text/color of label 4 and button 4 (appears on ToK History and Lore page)
	LabelSetText("TTitanSplashTextHistory", L"Смотреть ")
	ButtonSetText("TTitanSplashTextHistoryClick", L" Tome Titan")
	ButtonSetTextColor("TTitanSplashTextHistoryClick", 0, 0, 100, 0 )
end

function TTitan.UI.Show()
    WindowSetShowing("TTitanUI", true)
end

function TTitan.UI.Hide()
    WindowSetShowing("TTitanUI", false)
end

function TTitan.UI.ToggleShowing()
	WindowUtils.ToggleShowing("TTitanUI")
end

function TTitan.UI.OnShown()
	WindowUtils.OnShown()
end

function TTitan.UI.OnHidden()
	WindowUtils.OnHidden()
end

function TTitan.UI.Shutdown()
end

function TTitan.UI.MouseOverRow()
    local row = WindowGetId(SystemData.ActiveWindow.name)
    local targetRowWindow = "TTitanUIEntryListRow"..row
    DefaultColor.SetLabelColor(targetRowWindow.."Name", DefaultColor.MAGENTA)
end

function TTitan.UI.UpdateTTitanUIRow()
end

function TTitan.UI.OnLButtonUpEntryRow()
    local row = WindowGetId(SystemData.ActiveWindow.name)
    TTitan.UI.DisplayRow(ListBoxGetDataIndex("TTitanUIEntryList", row))
	TTitan.UI.UpdateColorsAndSigns()
end

function TTitan.UI.OnRButtonUpEntryRow()
end

function TTitan.UI.SetDataSource(source)
    TTitan.UI.DataSource = source
	TTitan.UI.Data = TTitan.UI.DataSource.GetData()

    TTitan.UI.PrepareData()
    TTitan.UI.UpdateTTitanUIRow()

    LabelSetText( "TTitanUIHeaderText", TTitan.UI.Data.header)
    LabelSetText( "TTitanUITopicLabel", L"Добро пожаловать")
    LabelSetText( "TTitanUIMainText", L"Выберите категорию слева.")

    TTitan.UI.SetListRowTints()
end

function TTitan.UI.PrepareData()
    TTitan.UI.OrderTable = {}
    TTitan.UI.ListBoxData = {}
    TTitan.UI.NumTotalRows = 1
    TTitan.UI.RowToEntryMap = {}

    if not TTitan.UI.Data then
		return
	end

    for categoryIndex, category in ipairs(TTitan.UI.Data.categories) do
		local categoryName = category.name
		if category.completed and not category.divider then
			categoryName = categoryName .. L" <•НЕТ•>"
		end

		local categoryTable = { name = categoryName, isCategory = true, id = category.id }
        table.insert(TTitan.UI.ListBoxData, TTitan.UI.NumTotalRows, categoryTable)
        table.insert(TTitan.UI.OrderTable, TTitan.UI.NumTotalRows)
        local indexStruct = { index = categoryIndex, isCategory = true }
        table.insert(TTitan.UI.RowToEntryMap, TTitan.UI.NumTotalRows, indexStruct)
        TTitan.UI.NumTotalRows = TTitan.UI.NumTotalRows + 1

        if category.expanded and not category.divider then
            for entryIndex, entry in ipairs(category.entries) do
				if (TTitan.Settings["hidecompleted"] and entry.completed) then continue end

				local entryName = entry.name
				if entry.completed then entryName = L"[X] "..entryName end

                local entryTable = { name = L"   " .. towstring(entryName), isCategory = false, id = entry.id }
                table.insert(TTitan.UI.ListBoxData, TTitan.UI.NumTotalRows, entryTable)
                table.insert(TTitan.UI.OrderTable, TTitan.UI.NumTotalRows)
                local indexStruct = { categoryIndex = categoryIndex, index = entryIndex, isCategory = false }
                table.insert(TTitan.UI.RowToEntryMap, TTitan.UI.NumTotalRows, indexStruct)
                TTitan.UI.NumTotalRows = TTitan.UI.NumTotalRows + 1
            end
        end
    end

    ListBoxSetDisplayOrder("TTitanUIEntryList", TTitan.UI.OrderTable )
    TTitan.UI.SetListRowTints()
	TTitan.UI.UpdateColorsAndSigns()
    TTitan.UI.ResetAllButtons()
end

function TTitan.UI.ExpandCurrentZone()
	local zoneId = GameData.Player.zone

	-- all the zones from 0 - 99 are Dwarfs vs Greenskins
	if zoneId > 0 and zoneId < 100	then
		if TTitan.UI.Type == TTitan.Type.BESTIARY then TTitan.UI.SetDataSource(TTitan.Data.BestiaryDvG) end
		if TTitan.UI.Type == TTitan.Type.HISTORY then TTitan.UI.SetDataSource(TTitan.Data.HistoryDvG) end
		if TTitan.UI.Type == TTitan.Type.NOTEWORTHY then TTitan.UI.SetDataSource(TTitan.Data.NoteworthyDvG) end

	-- all the zones from 100-199 are Empire vs Chaos
	elseif zoneId > 99 and zoneId < 200 then
		if TTitan.UI.Type == TTitan.Type.BESTIARY then TTitan.UI.SetDataSource(TTitan.Data.BestiaryEvC) end
		if TTitan.UI.Type == TTitan.Type.HISTORY then TTitan.UI.SetDataSource(TTitan.Data.HistoryEvC) end
		if TTitan.UI.Type == TTitan.Type.NOTEWORTHY then TTitan.UI.SetDataSource(TTitan.Data.NoteworthyEvC) end

	-- all the zones from 200-299 are High Elves vs Dark Elves
	elseif zoneId > 199 and zoneId < 300 then
		if TTitan.UI.Type == TTitan.Type.BESTIARY then TTitan.UI.SetDataSource(TTitan.Data.BestiaryHEvDE) end
		if TTitan.UI.Type == TTitan.Type.HISTORY then TTitan.UI.SetDataSource(TTitan.Data.HistoryHEvDE) end
		if TTitan.UI.Type == TTitan.Type.NOTEWORTHY then TTitan.UI.SetDataSource(TTitan.Data.NoteworthyHEvDE) end

	else
		TTitan.UI.SetDataSource(TTitan.Data.Credits)
		return
	end

	-- collapse all categories
	for _, category in ipairs(TTitan.UI.Data.categories) do
		category.expanded = false
	end

	TTitan.UI.PrepareData()

	-- strip grammar metadata, thanks to Aiiane for the pattern
	local zoneName = GetZoneName(zoneId):match(L"([^^]+)^?.*")

	local row = 0
	for categoryIndex, category in ipairs(TTitan.UI.Data.categories) do
		row = row + 1

		if category.name == zoneName then
			TTitan.UI.DisplayRow(row)
			break
		else
			local hide = TTitan.Settings["hidecompleted"] and category.completed
			if category.expanded and not hide then
				row = row + table.getn(category.entries)
			end
		end
	end
end

function TTitan.UI.DisplayRow(row)
	local index = TTitan.UI.RowToEntryMap[row].index

    if TTitan.UI.RowToEntryMap[row].isCategory then
		if TTitan.UI.Data.categories[index].divider then return end

		if TTitan.UI.Data.categories[index].expanded then
            TTitan.UI.Data.categories[index].expanded = false
			LabelSetText("TTitanUITopicLabel", L"Добро пожаловать")
			LabelSetText("TTitanUIMainText", L"Выберите категорию слева.")
        else
			LabelSetText("TTitanUITopicLabel", TTitan.UI.Data.categories[index].name)

			if TTitan.Settings["hidecompleted"] and TTitan.UI.Data.categories[index].completed then
				TTitan.UI.Data.categories[index].expanded = false
				LabelSetText("TTitanUIMainText", L"Записи в этой категории не найдены.")
			else
				TTitan.UI.Data.categories[index].expanded = true
				LabelSetText("TTitanUIMainText", L"Выберите элемент слева.")
			end
        end

	else
        TTitan.UI.ResetPressedButton()
        local categoryIndex = TTitan.UI.RowToEntryMap[row].categoryIndex
        TTitan.UI.LastPressedButtonId = TTitan.UI.Data.categories[categoryIndex].entries[index].id
        ButtonSetPressedFlag("TTitanUIEntryListRow"..row.."Name", true)
        TTitan.UI.DisplayManualEntry(TTitan.UI.Data.categories[categoryIndex].entries[index].name, TTitan.UI.Data.categories[categoryIndex].entries[index].text)
    end

    TTitan.UI.PrepareData()
end

function TTitan.UI.DisplayManualEntry(name, text)
    LabelSetText("TTitanUITopicLabel", towstring(name))
    LabelSetText("TTitanUIMainText", towstring(text))
    ScrollWindowSetOffset("TTitanUIMain", 0)
    ScrollWindowUpdateScrollRect("TTitanUIMain")
end

function TTitan.UI.ResetPressedButton()
    TTitan.UI.SetEntrySelectedById( TTitan.UI.LastPressedButtonId, false )
end

function TTitan.UI.ResetAllButtons()
    for index, data in ipairs(TTitan.UI.ListBoxData) do
		-- 23 once again from visiblerows in the TTitan.UIEntryList ListBox
		if (index > 23) then break end

        if( TTitan.UI.LastPressedButtonId == data.id and data.isCategory == false ) then
            ButtonSetPressedFlag("TTitanUIEntryListRow"..index.."Name", true )
        else
            ButtonSetPressedFlag("TTitanUIEntryListRow"..index.."Name", false )
        end
    end
end

function TTitan.UI.SetEntrySelectedById(id, selected)
    for index, data in pairs(TTitan.UI.ListBoxData) do
        if (data.isCategory == false and data.id == TTitan.UI.LastPressedButtonId) then
            ButtonSetPressedFlag("TTitanUIEntryListRow"..index.."Name", selected)
        end
    end
end

function TTitan.UI.SetListRowTints()
	-- 23 is from visiblerows in the TTitan.UIEntryList ListBox
    for row = 1, math.min(23, TTitan.UI.NumTotalRows) do
        local row_mod = math.mod(row, 2)
        color = DataUtils.GetAlternatingRowColor(row_mod)

        local targetRowWindow = "TTitanUIEntryListRow"..row
		WindowSetTintColor(targetRowWindow.."RowBackground", color.r, color.g, color.b )
		WindowSetAlpha(targetRowWindow.."RowBackground", color.a )
    end
end

function TTitan.UI.UpdateColorsAndSigns()
	-- 23 is from visiblerows in the TTitan.UIEntryList ListBox
    for index = 1, math.min(23, TTitan.UI.NumTotalRows) do
        WindowSetShowing( "TTitanUIEntryListRow"..index.."MinusButton", false )
        WindowSetShowing( "TTitanUIEntryListRow"..index.."PlusButton", false )
    end
end

function TTitan.UI.OnLButtonUpBestiaryDvGButton()
	TTitan.UI.Type = TTitan.Type.BESTIARY
	TTitan.UI.SetDataSource(TTitan.Data.BestiaryDvG)
end

function TTitan.UI.OnLButtonUpHistoryDvGButton()
	TTitan.UI.Type = TTitan.Type.HISTORY
	TTitan.UI.SetDataSource(TTitan.Data.HistoryDvG)
end

function TTitan.UI.OnLButtonUpNoteworthyDvGButton()
	TTitan.UI.Type = TTitan.Type.NOTEWORTHY
	TTitan.UI.SetDataSource(TTitan.Data.NoteworthyDvG)
end

function TTitan.UI.OnLButtonUpBestiaryEvCButton()
	TTitan.UI.Type = TTitan.Type.BESTIARY
	TTitan.UI.SetDataSource(TTitan.Data.BestiaryEvC)
end

function TTitan.UI.OnLButtonUpHistoryEvCButton()
	TTitan.UI.Type = TTitan.Type.HISTORY
	TTitan.UI.SetDataSource(TTitan.Data.HistoryEvC)
end

function TTitan.UI.OnLButtonUpNoteworthyEvCButton()
	TTitan.UI.Type = TTitan.Type.NOTEWORTHY
	TTitan.UI.SetDataSource(TTitan.Data.NoteworthyEvC)
end

function TTitan.UI.OnLButtonUpBestiaryHEvDEButton()
	TTitan.UI.Type = TTitan.Type.BESTIARY
	TTitan.UI.SetDataSource(TTitan.Data.BestiaryHEvDE)
end

function TTitan.UI.OnLButtonUpHistoryHEvDEButton()
	TTitan.UI.Type = TTitan.Type.HISTORY
	TTitan.UI.SetDataSource(TTitan.Data.HistoryHEvDE)
end

function TTitan.UI.OnLButtonUpNoteworthyHEvDEButton()
	TTitan.UI.Type = TTitan.Type.NOTEWORTHY
	TTitan.UI.SetDataSource(TTitan.Data.NoteworthyHEvDE)
end

function TTitan.UI.OnLButtonUpKillersButton()
	TTitan.UI.SetDataSource(TTitan.Data.Killers)
end

function TTitan.UI.OnLButtonUpPlantsButton()
	TTitan.UI.SetDataSource(TTitan.Data.Plants)
end

function TTitan.UI.OnLButtonUpTalismansButton()
	TTitan.UI.SetDataSource(TTitan.Data.Talismans)
end

function TTitan.UI.OnLButtonUpMinusButton()
	if TTitan.UI.AlphaValue > 0 then
		alpha = TTitan.UI.AlphaValue - 0.2
		WindowSetAlpha("TTitanUIBackground", alpha)
		TTitan.UI.AlphaValue = alpha
	end
end

function TTitan.UI.OnLButtonUpPlusButton()
	if TTitan.UI.AlphaValue < 1 then
		alpha = TTitan.UI.AlphaValue + 0.2
		WindowSetAlpha("TTitanUIBackground", alpha)
		TTitan.UI.AlphaValue = alpha
	end
end

function TTitan.UI.OnLButtonUpFAQButton() -- The "T" button that appears on the lower-right corner of the TomeTitan
	for _, category in ipairs(TTitan.UI.Data.categories) do
		category.expanded = false
	end
	TTitan.UI.SetDataSource(TTitan.Data.Credits)
	TTitan.UI.DisplayRow(1) 
	TTitan.UI.DisplayRow(2) 
end

function TTitan.UI.OnMouseOverFAQButton() -- The tooltip for the FAQ Button
Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )
Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_BOTTOM )
Tooltips.SetTooltipColor( 1, 1, 130, 180, 255 )
Tooltips.SetTooltipText( 1, 1, L"             Tome Titan" )
Tooltips.SetTooltipText( 3, 1, L"Часто задаваемые вопросы" )
Tooltips.SetTooltipText( 4, 1, L"     и Описание работы" )
Tooltips.SetTooltipText( 5, 1, L" " )
Tooltips.Finalize()
end

function TTitan.UI.TomeClearNewEntriesCheckForNew() -- Check if there's any new entries, hide/show button
	if TomeWindow.NewEntries.unreadEntryWindowCount == 0 then
		WindowSetShowing("TTitanClearNewTomeList", false)
	else
		WindowSetShowing("TTitanClearNewTomeList", true)
	end
end

function TTitan.UI.TomeClearNewEntriesLClick()
	TomeWindow.OnClearListEntries() -- Clear the list
	WindowSetShowing("TTitanClearNewTomeList", false) -- List is clear, now hide the button
end

function TTitan.UI.TomeClearNewEntriesMOver()
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_BOTTOM )
	Tooltips.SetTooltipColor( 5, 1, 255, 125, 125 )
	Tooltips.SetTooltipText( 2, 1, L"Нажмите левой кнопкой чтобы")
	Tooltips.SetTooltipText( 3, 1, L"    очистить все записи снизу.")
	Tooltips.SetTooltipText( 4, 1, L"--------------------------------------")
	Tooltips.SetTooltipText( 5, 1, L" Предоставлено от Tome Titan!")
	Tooltips.SetTooltipText( 6, 1, L" ")
	Tooltips.Finalize()
end

function TTitan.UI.SplashTextLClick()
	local button = string.sub(string.sub(SystemData.MouseOverWindow.name, 17),0,-6)	-- strip the button label down
	if button == "Title" then
		TTitan.UI.SetDataSource(TTitan.Data.Credits)
		TTitan.UI.Show()
	elseif button == "Bestiary" then
		TTitan.UI.Type = TTitan.Type.BESTIARY
		TTitan.UI.ExpandCurrentZone()
		TTitan.UI.Show()
	elseif button == "Noteworthy" then
		TTitan.UI.Type = TTitan.Type.NOTEWORTHY
		TTitan.UI.ExpandCurrentZone()
		TTitan.UI.Show()
	elseif button == "History" then
		TTitan.UI.Type = TTitan.Type.HISTORY
		TTitan.UI.ExpandCurrentZone()
		TTitan.UI.Show()
	else	--just incase I add more labels later and don't give them a proper entry in the function, default to FAQ/Credits
		TTitan.UI.SetDataSource(TTitan.Data.Credits)
		TTitan.UI.Show()
	end
end

function TTitan.UI.ToggleButton()
	if WindowGetShowing("TTitanTTButtonWindow") == true then
		WindowSetShowing("TTitanTTButtonWindow", false)
		TTitan.Settings["showbutton"] = false
		TTitan.Print([[Turning the TT button off, use "/ttitan button" to turn it back on.]])
	elseif WindowGetShowing("TTitanTTButtonWindow") == false then
		WindowSetShowing("TTitanTTButtonWindow", true)
		TTitan.Settings["showbutton"] = true
		TTitan.Print([[Turning the TT button on, use "/ttitan button" to turn it back off.]])
	end
end

-- This function is called from the slash command AND from the checkbox button
function TTitan.UI.ToggleHideCompleted()
	if TTitan.Settings["hidecompleted"] then
		TTitan.Settings["hidecompleted"] = false
		ButtonSetPressedFlag("TTitanUICheckBoxButton", false)
		TTitan.UI.PrepareData()
		TTitan.Print([[Tome Titan will now show completed unlocks. Completed unlocks will be marked with an X.]])
	else
		TTitan.Settings["hidecompleted"] = true
		ButtonSetPressedFlag("TTitanUICheckBoxButton", true)
		TTitan.UI.PrepareData()
		TTitan.Print([[Tome Titan will now hide entries for completed unlocks.]])
	end
end

function TTitan.UI.TitleBarAnimated()
	local step = TitleBarAnimatedStage
	if (step <= string.len(titletext)) and (step >= 0) then -- we'll stop when we get to the end instead of resetting
		TitleBarAnimatedStage = TitleBarAnimatedStage + 1
		LabelSetText("TTitanUITitleBarText", (towstring(string.sub(titletext, 1, step))))
	end
end


