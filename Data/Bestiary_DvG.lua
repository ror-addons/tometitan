TTitan.Data.BestiaryDvG = {}

function TTitan.Data.BestiaryDvG.GetData()
	return TTitan.Data.BestiaryDvG.RawData
end

function TTitan.Data.BestiaryDvG.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.BestiaryDvG.RawData)
end

TTitan.Data.BestiaryDvG.RawData =
{
	header = L"Бестиарий - Гномы против Зеленокожих",
	categories =
	{
		{
			name = L"Классовый свиток с РвР зоны (Race/Class scrolls (RVR LAKES))",
			entries =
			{
				
				{	
					name = L"• Dwarf (Title)",
					text = L"Гномы\n- Долина Кадрин (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: История гномов.\n- КООРДИНАТЫ: 34к, 7к.\n- ОПИСАНИЕ: Взять Спрятанный свиток на пригорке, за камнями (лежит под ёлкой).\n- НАГРАДА: титул Крушитель кузней.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nDwarf - The History of the Dwarfs\nForgebreaker\n\nKADRIN VALLEY @ 34.5, 7. Click on the Hidden Scroll NE of the northernmost BO, behind some rocks.\n\nBy Loupgaroo, Socran & Risingashes\n20081216",
				},
				
				{
					name = L"• Rune Priest (Pocket)",
					text = L"Жрец рун\n- Барак Варр (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 31к, 36к.\n- ОПИСАНИЕ: Взять Спрятанный свиток справа от задней двери замка.\n- НАГРАДА: Справочник по рунам для простолюдинов (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nDwarf, Rune Priest - What do We Have Here ?\nPOCKET ITEM\n\nBARAK VARR @ 31.5, 36. Click on the 'Hidden Scroll' -- 'Hidden Scroll' is located against the keep and near the postern door.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBe Sane Malk",
				},
				
				{	
					name = L"• Ironbreaker (Pocket)",
					text = L"Бронелом\n- Экрунд (тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 49к, 9к.\n- ОПИСАНИЕ: Взять Спрятанный свиток рядом с большими воротами.\n- НАГРАДА: Бесконечный список обид (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nDwarf, Ironbreaker - What do We Have Here ?\n\nEKRUND @ 50, 8.5. Use the 'Hidden Scroll' just outside a dwarf tower due S of the Stonemine Tower BO. Unlocks: Endless List of Grudges.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy Gorfist, Benji & Fadira\n20081216",
				},
				
				{
					name = L"?• Slayer (Pocket) - не найден",
					text = L"Мститель",
				},
				
				{	
					name = L"• Engineer (Pocket)",
					text = L"Инжинер\n- Ущелье Чёрного Огня (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 13к, 14к.\n- ОПИСАНИЕ: Взять Спрятанный свиток справа от подъёма к замку, рядом со сломанной катапультой.\n- НАГРАДА: Блестящая металлическая штука (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nDwarf, Engineer - What do We Have Here ?\nPOCKET ITEM\n\nBlack fire pass @ 13,6k, 14,5k. Click on the 'Hidden Scroll' -- Scroll is located east of Gnol baraz keep between a broken siege weapon and a bush\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy Urgiz",
				},
				
				
				{   
					name = L"• Goblin (Title)",
					text = L"Зеленокожие\n- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: История гоблинов.\n- КООРДИНАТЫ: 36к, 60к (пещера) 39к, 61к (свиток).\n- ОПИСАНИЕ: в РвР зоне найдите пещеру, ведущую к замку Нора Дурной Луны, и пройдя внутрь ко 2-м координатам найдите и прочитайте Спрятанный свиток.\n- ПРИМЕЧАНИЕ: возле пещеры бродит Пепельная дриада.\n- НАГРАДА: Титул Погибель Грота.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGoblin - The History of the Goblins\nGrot's Doom\n\nBLACK CRAG @ 36, 60. Click on the 'Hidden Scroll' located in a cave. Enter the cave and follow the path to 39.5, 61. Go to the base of the big rock column coming down from the ceiling and find the scroll between 2 mushroom stocks.\n\nBy rouko\n20081216",
				},
				
				{   
					name = L"?• Choppa (Pocket) - не найден",
					text = L"Рубака",
				},

				{   
					name = L"• Squig Herder (Pocket)",
					text = L"Пастух сквигов\n- ГОРА КРОВАВЫЙ РОГ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 25к, 4к.\n- ОПИСАНИЕ: Найти в глубине пещеры Спрятанный свиток.\n- НАГРАДА: Шо-то вкуснющее (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGoblin, Squig Herder - What do We Have Here ?\nPOCKET ITEM\n\nMOUNT BLOODHORN @ 25.2k, 4.3k. In the very back of the cave SE of the Ironmane outpost in the RvR lake there is a Hidden Scroll on the ground (between the orc bodies at the cave-in and the large stone column).\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)",
				},
				
				{   
					name = L"• Black Orc (Pocket)",
					text = L"Черный орк\n- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это тут у нас?\n- КООРДИНАТЫ: 22к, 56к.\n- ОПИСАНИЕ: под большим скелетом в куче мусора у бочки найти спрятанный свиток.\n- НАГРАДА: Счастливая лапа чела (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nOrc, Black Orc - What do We Have Here ?\nPOCKET ITEM\n\nTHE BADLANDS @ 22, 55. Click on the 'Hidden Scroll' located within the giant bone area, right beside a barrel that's on a pile of rubble. Unlocks: Lucky Humie's Hand.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy rouko\n20081217",
				},

				{   
					name = L"• Shaman (Pocket)",
					text = L"Шаман\n- ТОПИ БЕЗУМИЯ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это тут у нас?\n- КООРДИНАТЫ: 29к, 29к.\n- ОПИСАНИЕ: Найти Спрятанный свиток на груде камней.\n- НАГРАДА: Аварийный гриб (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGoblin, Shaman - What do We Have Here ?\nPOCKET ITEM\n\nMARSHES OF MADNESS @ 29.09, 29.65. NW of the keep there is a Hidden Scroll sitting on top of a pile.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy Fredegar\n20081217",
				},

				
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 1 •••",
			divider = true,
			entries = {},
		},
		
		{
			name = L"ЭКРУНД • Ekrund",
			entries =
			{
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Plaguebearer (Title)",
					text = L"- Экрунд (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Байки у костра.\n- КООРДИНАТЫ: 60к, 39,2к.\n- ОПИСАНИЕ: Поговорить с фанатиком Отверженный служитель у горы в лагере зеленокожих 4-й главы.\n- НАГРАДА: титул Охотник на Чумоносцев.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPlaguebearer - A Tale by the Fire \nThe Plaguebearer Hunter (Blightseeker)\n\nEKRUND. (D) Talk to a Zealot named Forsaken Acolyte behind some huts in Gorgor's Smash. Mob is currently untargetable.",
				},
				
				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,	
					name = L"• Plaguebearer (Title)",
					text = L"- Экрунд (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Байки у костра.\n- КООРДИНАТЫ: 44.5к, 30к.\n- ОПИСАНИЕ: Поговорить с Адепт-Охотник на ведьм в лагере гномов 2-й главы.\n- НАГРАДА: титул Охотник на Чумоносцев.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPlaguebearer - A Tale by the Fire \nThe Plaguebearer Hunter (Blightseeker)\n\nEKRUND. (O) Talk to a Witch Hunter Adept in Dwarf Ch 2. Mob is currently untargetable.",
				},
				
				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Squig (Title)",
					text = L"- Экрунд (Тир 1)\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Другое, другое свежее мясо.\n- КООРДИНАТЫ: 21к, 38к.\n- ОПИСАНИЕ: Поговорить в таверне с Корминой Фалинсдоттир, получив сквига, съесть его.\n- НАГРАДА: титул Лакомка.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nSquig - The Other, Other Red Meat\nThe Gourmand\n\nEKRUND. Head into Pick and Goggles tavern in the dwarf starting area and talk to waitress for Roasted squig then eat it.\n\nBy Kovacs, OdaNova, & burfo\n20081007",
				},

			},
		},



		{
			name = L"ГОРА КРОВАВЫЙ РОГ • Mount Bloodhorn",
			entries =
			{
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Boar (Tactic)",
					text = L"- ГОРА КРОВАВЫЙ РОГ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Избежать жестокой смерти.\n- КООРДИНАТЫ: 15к, 40к.\n- ОПИСАНИЕ: Убить несколько Последователей псов после чего поговорить в лагере зеленокожих 2-й главы с Регистратором убийств Шныроклыком.\n- НАГРАДА: Фрагмент тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBoar (Dog Soldier/Waylayer/Firecaller) - Avoid a Gory End\nBESTIAL TOME TACTIC\n\nMOUNT BLOODHORN. Kill enough Dog mobs to max out the kill collector @ Da War Maker.\n\nBy notalltogether, Demonspawn & Cahyr\n20081103",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Night Goblin (Title)",
					text = L"- ГОРА КРОВАВЫЙ РОГ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Хороший ночной гоблин.\n- КООРДИНАТЫ: 35к, 47к.\n- ОПИСАНИЕ: Поговорить с гоблином Боевым подровником кровавого солнца у пешеры.\n- АЛЬТЕРНАТИВА: Задание в БЕСПЛОДНЫЕ ЗЕМЛИ.\n- НАГРАДА: Титул Ужасный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGoblin, Night Goblin (Bloody Sun War Blasta) - Good Night Goblin\nThe Frightful\n\nMOUNT BLOODHORN @ 35, 47. Talking to this NPC gets the unlock.\n\nBy Tananthalas\n20081021",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Spite (Pocket)",
					text = L"- ГОРА КРОВАВЫЙ РОГ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Обезвреживание.\n- КООРДИНАТЫ: 4к, 61к.\n- ОПИСАНИЕ: Найти и убить Мейгда (8 ур.), бродящего недалеко от Паблики 3-й главы.\n- НАГРАДА: Эль вредня (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpite (Maegda) (Tarestil) - De-spite\nPOCKET ITEM\n\nMOUNT BLOODHORN @ 3.5, 61. Run SW to HERE. Near the PQ Sacred Grove, so if you see that you know you're in the right area. Kill this L5-8 mob to unlock Spite Ale.\n\nBy NicWester, Wykk, Finalheaven & ThePedant\n20081118",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Spite (Pocket)",
					text = L"- Гора Кровавый Рог (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Обезвреживание.\n- КООРДИНАТЫ: 3к, 61к (Мэйгда), 54к, 28к (Тарестил).\n- ОПИСАНИЕ: убить моба Мэйгда (8 ур.), либо убить моба Тарестил (10 ур.).\n- НАГРАДА: Эль вредня (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nSpite (Maegda) (Tarestil) - De-spite\nPOCKET ITEM\n\nMOUNT BLOODHORN @ 3.5, 61. Run SW to HERE. Near the PQ Sacred Grove, so if you see that you know you're in the right area. Kill this L5-8 mob to unlock Spite Ale.\nAlso @ 54.5, 28. Killing the named spite Tarestil gives the unlock as well.\n\nBy NicWester, Wykk, Finalheaven & ThePedant and by India & kazetenzan\n20081118",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Spite (Tactic)",
					text = L"- ГОРА КРОВАВЫЙ РОГ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Оскверненная сила жизни.\n- КООРДИНАТЫ: 8к, 62к.\n- ОПИСАНИЕ: Недалеко от паблика 3-й главы найти Нечистого лесного вредня (герой 7 ур.). Убить его и получить Сущьность нечистого вредня, которое нужно отнести в лагерь 3-й главы зеленокожих Поджигателю кровавого солнца, который стоит в хижине рядом с торговцем.\n- НАГРАДА: Фрагмент тактики Мифических существ (2/10).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpite (Tainted Grove Spite) - A Defiled Life-Force\nMYTHICAL TOME TACTIC\n\nMOUNT BLOODHORN @ 9, 61. Kill this L7 Hero and loot Tainted Spite Essence. Take it to the Bloody Sun Burner in a hut at the Ch3 camp. NOTE: Mob does NOT drop the loot every time, 10m repop.\n\nBy raicarter & Tananthalas\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Spite (Tactic)",
					text = L"- ГОРА КРОВАВЫЙ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Оскверненная сила жизни.\n- КООРДИНАТЫ: 9к, 61к.\n- ОПИСАНИЕ: недалеко от паблика 3-й главы найти героя 7-го уровня Нечистого лесного вредня. Убить его и получить Сущьность нечистого вредня, которое нужно отнести в Кром Комар, и поговорить с Рубильщиком молотобойцев, который стоит рядом с торговцем.\n- НАГРАДА: Фрагмент тактики Мифических существ (2/10).\n\nTEZARIUS, A2ex © 2009\n\n————————————————————————————————\n\nSpite (Tainted Grove Spite) - A Defiled Life-Force\nMYTHICAL TOME TACTIC\n\nMOUNT BLOODHORN @ 9, 61. Kill this L7 Hero and loot Tainted Spite Essence. Speaking to Hammerstriker Hasher beside the Krom Komar merchant gives the unlock.  NOTE: Mob does NOT drop the loot every time, 10m repop.\n\nBy Torgan\n20081124",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Squig (Title)",
					text = L"- ГОРА КРОВАВЫЙ РОГ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Другое, другое свежее мясо.\n- КООРДИНАТЫ: 16к, 54к.\n- ОПИСАНИЕ: Поговорить c Точез Швыряка в лагере Забияка, зеленокожие глава 2-я возле костра. Воспользоваться полученным Сквигом-на-палочке.\n- НАГРАДА: Титул Лакомка.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSquig - The Other, Other Red Meat\nThe Gourmand\n\nMOUNT BLOODHORN. Talk to Tochez da Slop Slinger by Da War Maker. Get the Squig-on-a Stick & enjoy your meal.\n\nBy Ulgwar\n20081103",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Vulture (Tactic)",
					text = L"- ГОРА КРОВАВЫЙ РОГ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пойло дня.\n- КООРДИНАТЫ: 2.5к, 26к (поджигатель), 23к, 24к или 25к, 25к (лагерь).\n- ОПИСАНИЕ: Поговорить с Поджигателем кровавого солнца в Лагере зеленокожих 3-й главы, который даст 3 Мешка гнилых плодов. Потом у дороги найти лагерь стервятников и положить плоды в Корзины с помоями. После этого вернуться в лагерь и поговорить с Поджигателем кровавого солнца.\n- НАГРАДА: Фрагмент Тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nVulture - Slop-of-the-Day\nBESTIAL TOME TACTIC\n\nMOUNT BLOODHORN @ 2.5, 26 THEN -> 22.5, 24 or 24.5, 25. Talk to Bloody Sun Burner in Komar at the first coords to get a Sack of Rotting Fruit. Travel E to second coords. Among the camp of vultures are slop baskets. Right-click on each one to dispose of the fruit (weak aggressive vultures will spawn).\nReturn to the Bloody Sun Burner inside the hut @ 2.5, 26.\n\nBy Tananthalas & Elmarra\n20081103",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Vulture (Tactic)",
					text = L"- Гора Кровавый Рог (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пойло дня.\n- КООРДИНАТЫ: 2.5к, 26к, затем 22к, 24к.\n- ОПИСАНИЕ: Взять у рубильщика-молотобойца (первые координаты) Мешок гнилых фруктов, затем следовать ко вторым координатам, найти там Помойную корзину (правый клик на ней). Отнести Пустой джутовый мешок обратно рубильщику-молотобойцу.\n- НАГРАДА: фрагмент тактики зверей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nVulture - Slop-of-the-Day\nBESTIAL TOME TACTIC\n\nMOUNT BLOODHORN @ 2.5, 26 THEN ---> 22.5, 24 or 24.5, 25. Talk to Bloody Sun Burner in Komar at the first coords to get a Sack of Rotting Fruit. Travel E to second coords. Among the camp of vultures are slop baskets. Right-click on each one to dispose of the fruit (weak aggressive vultures will spawn).\nReturn the Empty Burlap Sack to the NPC to receive the unlock.\n\nBy Aoibhinn\n20081103",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 2 •••",
			divider = true,
			entries = {},
		},

		{
			name = L"БАРАК ВАРР • Barak Varr",
			entries =
			{
				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Boar (Tactic)",
					text = L"- Барак Варр (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Избежать жестокой смерти.\n- КООРДИНАТЫ: 31к, 49к.\n- ОПИСАНИЕ: убить 60 секачей кровоклыка, затем забрать награду у регистратора убийств Эгалрота Калгрисона в Обороне тана.\n- НАГРАДА: фрагмент тактики зверей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nBoar - Avoid a Gory End\nBEASTIAL TOME TACTIC UNLOCK\n\nBARAK VARR. Max out the kill collector named Elagroth Karginson in Thane's Defense.\n\nBy Lobo6717\n20081007",
				},

				{
					name = L"• Boar (Tactic)",
					text = L"- БАРАК ВАРР (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Избежать жестокой смерти.\n- КООРДИНАТЫ: 20к, 52к.\n- ОПИСАНИЕ: Недалеко от военного лагеря гномов есть мост через ущелье под которым нужно найти кабана Проглота (16 ур.) и убить его.\n- ПРИМЕЧАНИЕ: Также его можно найти на 57, 31 и 41, 52 но реже.\n- НАГРАДА: Фрагмент Тактики зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBoar (Jowler) - Great Tusk\nBESTIAL TOME TACTIC\n\nBARAK VARR @ 56.5, 30.5 or 20, 64 or 40.5, 52 -OR- the location described below:\nVanquish this L16 named mob near the (O) warcamp. There's a trench that runs S, get in and follow the trench till you get to a bridge. The boar lives under the bridge.\n\nBy Deedee, Avale, notalltogether, Ulgwar & Icebird\n20081103",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Dwarf Slayer (Tactic)",
					text = L"- БАРАК ВАРР (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Заканчиваем с этим...\n- КООРДИНАТЫ: 11к, 49к.\n- ОПИСАНИЕ: На берегу убить гномов мстителей и собрать 20 Скальпов убийцы. После чего сходить в лагерь зеленокожих 7-й главы и поговорить с регистратором убийств Ризелем.\n- НАГРАДА: Фрагмент тактики Людей (1/6).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDwarf Slayer (Sea Anvil Slayer) - Off With His ...\nMAN TOME TACTIC\n\nBARAK VARR @ 11, 49. Kill the mob and collect 20 Slayer Scalps. Then go talk to Kill Collector Reezel in Rottoof's Mugz 33.5, 60.\n\nBy NicWester, Rhah & Wykk\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Dwarf Slayer (Tactic)",
					text = L"- Барак Варр (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Заканчиваем с этим...\n- КООРДИНАТЫ: 10к, 45к.\n- ОПИСАНИЕ: Войти внутрь корабля и поговорить с капитаном Эргриссоном.\n- НАГРАДА: фрагмент тактики людей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nDwarf Slayer (Sea Anvil Slayer) - Off With His ...\nMAN TOME TACTIC\n\nBARAK VARR @ 10.5, 45. Go inside The Sea Anvil in Hammerhead Lagoon and talk to the Captain for the unlock.\n\nBy Kellithe, War Woman & Xiani\n20081124",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Dwarf Slayer (Title)",
					text = L"- БАРАК ВАРР (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Первый из многих.\n- КООРДИНАТЫ: 11к, 47к.\n- ОПИСАНИЕ: Забраться на судно, стоящее возле берега, и убить первого помошника Чельсона.\n- НАГРАДА: Титул Вестник рока.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDwarf Slayer (First Mate Kjaelsson) - First of Many\nDoom Bringer\n\nBARAK VARR @ 10.5, 47. Board The Sea's Anvil ship. Kill the mob.\n\nBy NicWester, Wilken & Wykk\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Dwarf Slayer (Title)",
					text = L"- Барак Варр (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Первый из многих.\n- КООРДИНАТЫ: 10к, 47к.\n- ОПИСАНИЕ: Подняться на корабль и поговорить с первым помощником.\n- НАГРАДА: титул Вестник рока.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nDwarf Slayer (First Mate Kjaelsson) - First of Many\nDoom Bringer\n\nBARAK VARR @ 10.5, 47. Just speak with the NPC for the unlock.\n\nBy armorum\n20081124",
				},

				{
					name = L"• Giant Lizard (Tactic)",
					text = L"- БАРАК ВАРР (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Обворованный.\n- КООРДИНАТЫ: 8к, 29к или 5к, 23к.\n- ОПИСАНИЕ: Запись откроеться при посещении Лагеря Ногаза 5-я глава зеленокожих.\n- НАГРАДА: Фрагмент Тактики Зверей (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant Lizard- Poached\nBESTIAL TOME TACTIC\n\nBARAK VARR @ 8, 29 or 5, 23. Habitat unlock, just walk along the beach where the salamanders are.\n\nBy LatukiJoe, burfo, NicWester, Dormammu, Wykk & Puddles404\n20081103",
				},

				{
					name = L"• Giant Lizard (Title)",
					text = L"- БАРАК ВАРР (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Дело в весах (Чешуйчатое дело).\n- КООРДИНАТЫ: 8, 26.\n- ОПИСАНИЕ: Найдите саламандр на берегу и добыть с них Чешуйчатую Кожу.\n- ПРИМЕЧАНИЕ: Шанс выпадения очень мал порой нужно убить от 50 до 300 ящериц чтобы получить кожу.\n- АЛЬТЕРНАТИВА: Задание на Оскверненном острове територии эльфов.\n- НАГРАДА: Титул Охотник на ящериц.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant Lizard - A Matter of Scales\nThe Lizard Hunter\n\nBARAK VARR @ 8, 25.5. Go to the beach and collect the item Scaly Skin from the lizards you see there. NOTE: This site is more (D) friendly.\nAlternate method: See entry in BLIGHTED ISLE below.\n\nBy notalltogether & JodouTR\n20081021",
				},

				{
					name = L"• Giant Lizard (Exp)",
					text = L"- БАРАК ВАРР (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Кровь камня.\n- КООРДИНАТЫ: 19к, 17к.\n- ОПИСАНИЕ: На берегу реки возле камней найти Камнешкурую ящерецу (20 ур.) и убить ее.\n- НАГРАДА: 1800 опыта.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant Lizard (Stonescale) - Blood from a Stone\n\nBARAK VARR @ 19, 17. Kill this L20 mob.\n\nBy Sandkat, Thanat0s & Sildroni\n20081103",
				},

				{
					name = L"• Nurgling (Title)",
					text = L"- БАРАК ВАРР (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Вырвать из груди.\n- КООРДИНАТЫ: 53к, 49к.\n- ОПИСАНИЕ: Недалеко от дороги внизу утеса валяеться Гниющий труп гнома. Активируйте его и убейте появившихся нурлингов.\n- НАГРАДА: Титул Борец с поветрием.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nNurgling (Festering Corpse) - Getting it off Your Chest\nThe Denier of Disease\n\nBARAK VARR @ 53, 49. Go where a patch of road is littered with dead dwarf bodies and vultures eating them. If you are on the N side of the vultures on the road, head just NE and fall down the cliff. Search the body and a couple nurglings pop out along with getting the unlock. NOTE: The corpse is on the ground next to a tree.\n\nBy Muninn, Wilken & Sandkat\n20081103",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Ogre, Bull (Title)",
					text = L"- БАРАК ВАРР (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Толстогубый.\n- КООРДИНАТЫ: 21к, 47к.\n- ОПИСАНИЕ: Чуть ниже военного лагеря гномов найти пристанище орков и поговорить с главарем Рагакк.\n- НАГРАДА: титул Громила.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nOgre, Bull - Loose-Lipped\nThe Crusher\n\nBARAK VARR @ 21, 47. Find an ogre named Ragakk, a little bit S from the (O) warcamp along with some gobbos.\n\nBy Ursus, Thovargh & Blodsalv\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Ogre, Bull (Title)",
					text = L"- Барак Варр (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Толстогубый.\n- КООРДИНАТЫ: 49к, 57к.\n- ОПИСАНИЕ: поговорить в лагере с Главным рыгуном.\n- НАГРАДА: титул Громила.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nOgre, Bull - Loose-Lipped\nThe Crusher\n\nBARAK VARR @ 49, 57. In the SE dwarf camp (Just head a little N of the squiggly path that heads S into the Marsh of Madness) talk to a Lead Belcher Ogre.\n\nBy Ursus, Thovargh & Blodsalv\n20081124",
				},

				{	-- Destruction version
					-- realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Spider, Giant (Title)",
					text = L"- БАРАК ВАРР (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Так себе интерьер.\n- КООРДИНАТЫ: 5к, 28к.\n- ОПИСАНИЕ: Найдите пешеру Мокроклык и зайдите в нее.\n- АЛЬТЕРНАТИВА: Задание в Стране тролей или в Норска.\n- НАГРАДА: Титул Убийца пауков.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpider, Giant - Tacky Decor\nSpider-Slayer\n\nBARAK VARR @ 5, 28. Go to the cave N of the zone entrance. BONUS: Lore unlock by examining the statue nearby.\n\nBy NicWester, burfo & FireOpal\n20081103",
				},
			},
		},

		
		{
			name = L"ТОПИ БЕЗУМИЯ • Marshes of Madness",
			entries =
			{
				{
					name = L"• Banshee (Title)",
					text = L"- ТОПИ БЕЗУМИЯ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Звук в тишине.\n- КООРДИНАТЫ: 56к, 30к.\n- ОПИСАНИЕ: На берегу реки под корнями дерева есть Неглубокая могила, которую нужно исследовать.\n- НАГРАДА: Титул Головорез.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBanshee - A Sound to Silence\nThe Silencer\n\nMARSHES OF MADNESS @ 56, 30. On the shore, behind a tree is a shallow grave hidden between the roots. Examine it to gain unlock.\n\nBy Enli, skyrunner620, Thanat0s & Jorus\n20081104",
				},

				{
					name = L"• Bat, Giant (Title)",
					text = L"- ТОПИ БЕЗУМИЯ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что-то кроме помёта.\n- КООРДИНАТЫ: 41к, 48к или 50к, 46к.\n- ОПИСАНИЕ: Найти и убить летучую мыш Ноберхест (19 ур.).\n- АЛЬТЕРНАТИВА: Задание в Стране тролей.\n- НАГРАДА: Титул Охотник на нетопырей.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBat, Giant (Neborhest Bat) - For Once It Isn't Guano\nEcho Hunter\n\nMARSHES OF MADNESS @ 41, 48. Perform killing blow on this regular L19 mob just S of Dwarf Ch9.\n\nBy Squarkk, burfo& Pinafore\n20081020",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Ghoul (Title)",
					text = L"- ТОПИ БЕЗУМИЯ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Вурдалаки - прочь.\n- КООРДИНАТЫ: 31к, 11к.\n- ОПИСАНИЕ: Под корнями дерева найдите гоблина Ноглика и поговорите с ним.\n- НАГРАДА: Титул Гроза каннибалов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGhoul - Ghoul to be Gone\nFoe of the Cannibal\n\nMARSHES OF MADNESS @ 31, 10.5. First kill a ghoul. Second, talk to the goblin Noglik.\n\nBy Nerror, Wykk & Deymus\n20081117",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Ghoul (Title)",
					text = L"- Топи Безумия (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Вурдалаки-прочь.\n- КООРДИНАТЫ: 50к, 40к.\n- ОПИСАНИЕ: Поговорить с учеником Шерером. Он сидит справа от пещеры, в руинах.\n- НАГРАДА: титул Гроза каннибалов.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nGhoul - Ghoul to be Gone\nFoe of the Cannibal\n\nMARSHES OF MADNESS @ 7.5, 61 OR 49.5, 40.5 by the entrance to the Fellowing Cave. Talk to Acolyte Scherer. Inspecting him grants you the title.\nNOTE: You may need to kill ghouls before the unlock will trigger.\n\nBy Ursus, Dormammu, Wilken, Puddles404 & Zmiinyi\n20081117",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Gnoblar (Tactic)",
					text = L"- ТОПИ БЕЗУМИЯ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Естественный отбор.\n- КООРДИНАТЫ: 56к, 7к.\n- ОПИСАНИЕ: Найти Жабистых болотников и добыть 5 Ушей болотника.\n- НАГРАДА: Фрагмент Тактики Зеленокожих (1/5).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGnoblar - Cull the Weak\nGREENSKIN TOME TACTIC\n\nMARSHES OF MADNESS @ 56, 7. Fight Boglars and loot their Boglar Ears.\nCollect 5, once you have 5 then it auto-completes and you receive the unlock.\n\nBy OneWingedAngelo1, Dormammu, Risingashes, Ibeefus, Rhumble & Icebird\n20081022",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Gnoblar (Tactic)",
					text = L"- Топи Безумия (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Естественный отбор.\n- КООРДИНАТЫ: 56к, 7к.\n- ОПИСАНИЕ: собрать 5 ушей болотников.\n- НАГРАДА: фрагмент тактики зеленокожих.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nGnoblar - Cull the Weak\nGREENSKIN TOME TACTIC\n\nMARSHES OF MADNESS @ 56, 7. Fight Boglars and loot their Boglar Ears.\nCollect 5, once you have 5 then talk to the Kill Collector. NOTE: Bug of this works by turning in 1 ear (may be fixed).\n\nBy OneWingedAngelo1, Dormammu, Risingashes, Ibeefus, Rhumble & Icebird\n20081022",
				},

				{
					name = L"• Gnoblar (Tactic)",
					text = L"- ТОПИ БЕЗУМИЯ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Маленький тиран.\n- КООРДИНАТЫ: 35к, 60к.\n- ОПИСАНИЕ: Найти на болоте Головастого болотника (21 ур.) и убить его.\n- НАГРАДА: Фрагмент Тактики зеленокожих (1/5).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGnoblar (Honcho Bogwash) - Tiny Tyrant\nGREENSKIN TOME\n\nMARSHES OF MADNESS @ 35, 60. Kill the L20 mob. Doing this unlock plus Cull the Weak will completely unlock the first Greenskin Tactic called Fungal Fetish.\n\nBy Unpeftable, Kellithe & Wilken\n20081022",
				},

				{
					name = L"• Giant (Title)",
					text = L"- ТОПИ БЕЗУМИЯ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Отражение удара.\n- КООРДИНАТЫ: 55к, 60к.\n- ОПИСАНИЕ: На вершине горы найти великана Трагз (герой 15 ур.).\n- ПРИМЕЧАНИЕ: Чтобы активировать запись вы должны получить критический урон до убийста героя.\n- НАГРАДА: Титул Сотрясатель земли.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant (Thrags) - Bare the Brunt\nThe Earth Shaker\n\nMARSHES OF MADNESS @ 55, 60. Engage and battle AND get critically hit by this L15 Hero (you don't have to defeat him to get the unlock). TIP: Scrap your initiative gear before you engage him.\nNOTE: Repop time may be lengthy.\nBONUS: Hidden quest for a golden nugget when traveling up the mt toward the coords.\n\nBy Unpeftable, Thanat0s, Dormammu, notalltogether, Pinafore, India& Puddles404\n20081021",
				},

				{
					name = L"• Vampire (Title)",
					text = L"- ТОПИ БЕЗУМИЯ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Приветствуя ужас.\n- КООРДИНАТЫ: 50к, 53к.\n- ОПИСАНИЕ: Возле горы найти Изваяние вампира и активировать его.\n- НАГРАДА: Титул Рассветоносец.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nVampire - Hail to the Horror\nThe Dawnbringer\n\nMARSHES OF MADNESS @ 50, 53. Near where all the plague priests are look for a clickable object called a Vampire Effigy. Right click it to get the title.\n\nBy Thrid, Jschmuck2 & BlackNova169\n20081022",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 3 •••",
			divider = true,
			entries = {},
		},
		

		{
			name = L"БЕСПЛОДНЫЕ ЗЕМЛИ • The Badlands",
			entries =
			{

				{
					name = L"• Ghoul (Tactic)",
					text = L"- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Волк в овечьей шкуре.\n- КООРДИНАТЫ: 19к, 10к.\n- ОПИСАНИЕ: Найти в проходе, где обитают птицы и зомби Вурдалака (22 ур.) и убить его.\n- НАГРАДА: Фрагмент тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGhoul (Rauklus) - Foul Amongst the Fowl\nBESTIAL TOME TACTIC\n\nBADLANDS @ 19, 10. Kill the mob in the valley with the other zombies and birds.\n\nBy Blodsalv, Ghenghiz & Teran\n20081124",
				},
				
				{
					name = L"• Gnoblar (Acc) - Сет Великие камни клятвы",
					text = L"- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: В конечном итоге. (Sum of the Whole).\n- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- КООРДИНАТЫ: 37.1к, 31.7к.\n- ОПИСАНИЕ: На небольшой скале найти Гноблер Кровавой Пасти (25 ур.), убив его получить Нос гнобблера Кровавой пасти (Bloodmaw Gnoblar's Nose). Он являеться элементом большой коллекции.\n- ПЕПЛОПАД (Тир 4).\n- КООРДИНАТЫ: 51, 53.\n- ОПИСАНИЕ: В районе паблика Крепость Кишкобоя найти гноблеров Зверолов Кишкобоя (35-36 ур.) с которых выбить Куски-ловушки гнобблера-зверолова и Зубастый гноблер Кишкобоя (35 ур.) с него получить Зуб зубастого гноблара.\n- ЧЕРНЫЙ УТЕС (Тир 4).\n- КООРДИНАТЫ: 40, 11.\n- ОПИСАНИЕ: Около Башни Паленого Брюха среди огров найти Гноблеров Паленого Брюха (38 ур.) и добыть у них Блестючки гнобблера Паленого Брюха (Burnmaw Gnoblar's Shiny Bits).\nПри активации одной из частей получаем Анлок и Ограниченный выпуск коллекционных фигур гнобблера.\n- ПРИМЕЧАНИЕ: Шанс выпадения предметов: нос - 7%, зуб и ловушка - 0,1%, блестючка - 3%.\n- НАГРАДА: Камень клятвы Восьми Вершин Карака (Сет Великие камни клятвы 1/2).\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{
					name = L"• Night Goblin (Title)",
					text = L"- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Хороший ночной гоблин.\n- КООРДИНАТЫ: 6к, 8к или 4к, 8к.\n- ОПИСАНИЕ: В одной из палаток найти шамана Узфанка (22 ур.) и убить его.\n- АЛЬТЕРНАТИВА: Задание в ГОРА КРОВАВЫЙ РОГ.\n- НАГРАДА: Титул Ужасный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGoblin, Night Goblin (Uzfang) - Good Night Goblin\nThe Frightful\n\nBADLANDS @ 5.5, 8 or 4, 8. Kill it. He's inside one of the huts. BONUS: There's a lore unlock by the bonfire, too.\n\nBy Kellithe & Teran\n20081030",
				},

				{
					name = L"• Ogre Tyrant (Title)",
					text = L"- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Не то, чем кажется.\n- КООРДИНАТЫ: 63к, 36к или 60к, 27к.\n- ОПИСАНИЕ: В Пещере Кровавой Пасти найти огра Грау Мудробрюх (побед. 29 ур.) и убить его, чтобы получить Потускневшую медаль и активировать ее.\n- НАГРАДА: Титул Освободитель.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nOgre Tyrant (Graw Leadgut) - Not What it Appears\nThe Liberator\n\nBADLANDS @ 63, 36k or 60, 27k. Enter the cave. Kill this L29 Champ and inspect the medal he drops. NOTE: If you kill him in a group, he only drops 1 medal, so you'll have to kill him several times.\n\nBy Thovargh, notalltogether & Teran\n20081013",
				},

				{
					name = L"• Savage Orc (Token)",
					text = L"- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: До смерти испуган.\n- КООРДИНАТЫ: 5к, 55к или 4к, 35к.\n- ОПИСАНИЕ: На вершине горы выше пешеры Гейзерная Купель найти Шраам Видаш (герой 21 ур.), который стоит возле дерева, и убить его.\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSavage Orc (Skaar Vidash) - Scar'd for Life\nBEASTIAL TOKEN UNLOCK\n\nBADLANDS @ 5, 55k or 4, 35k. Kill this L21 Hero. He stands next to a tree in the mts above the Geyser Den. NOTE: Respawn timer is lengthy.\n\nBy cintra, JodouTR, Blodsalv & poll12\n20081016",
				},

				{
					name = L"• Savage Orc (Title)",
					text = L"- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Грибы среди нас!\n- КООРДИНАТЫ: 15к, 21к (троль), 6к, 17к или 15к, 18к (гриб).\n- ОПИСАНИЕ: Убить речного троля на островке и получить Осколок яркого камня. На самом острове покопаться по кустам и найти Странный Гриб. Потом найти рядом с Пешерой Кровошипа орков Синего племени, которые бродят возле мусорных куч, и убить их.\n- НАГРАДА: Тиитул Культурный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSavage Orc (Tribal Blue Face) - Fungus Amoung Us\nThe Civil\n\nBADLANDS @ 14.5, 20.5 THEN -> 6, 17 or 15, 18. First, loot the Strange Mushroom from a Thornberry Bush on a small island at the first set of coords. Then, have it in your inventory when you kill the mobs at next coords. NOTE: Make sure to kill the orcs that are hanging around the piles of rubbish and not the ones on the cliff itself.\n\nBy Arienh, Thovargh, Houyoko, Gabriel Stern, notalltogether & Pinafore\n20081103",
				},

				{
					name = L"• Skaven (Title)",
					text = L"- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Веяния времени.\n- КООРДИНАТЫ: 8к, 53к.\n- ОПИСАНИЕ: В пещере Убежище Злокогтя найти ловушку для крыс Людоловка и активировать ее.\n- НАГРАДА: Титул Убийца Крысолюдов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSkaven (Man Trap) - Sign of the Times\nSkaven Splitter\n\nBADLANDS. S of the first T3 Dwarf town there is a cave called Warpclaw Hideout. It's full of Skavens amidst a bunch of Geiser Squigs. Head into the cave, and click on a corpse in a Man Trap (Obviously a Skaven take on the rat trap) to get the unlock.\n\nBy Ursus, Dormammu & notalltogether\n20081029",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Snotling (Pocket)",
					text = L"- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Скользкие воспоминания.\n- КООРДИНАТЫ: 24к, 27к.\n- ОПИСАНИЕ: В низине водопадов на маленьком острове поговорить с гоблином Фойгак.\n- НАГРАДА: Знак паровоза (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSnotling (Foigak) - Miscreant Memories\nPOCKET ITEM\n\nBADLANDS @ 24, 26.5. Talk to this snotling on a tiny island in the river, close to a waterfall and surrounded by trolls. Unlocks Steamtrain Token.\n\nBy Amagorr, Banehowl, nny76 & Pinafore\n20081030",
				},
				
				{	-- Order version
					-- realm = TTitan.Data.Realm.ORDER,
					name = L"• Snotling (Pocket)",
					text = L"- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Скользкие воспоминания.\n- КООРДИНАТЫ: 38.6к, 19.4к.\n- ОПИСАНИЕ: Поговорить с одинокой женьщиной.\n- НАГРАДА: Знак паровоза (карман).\n\nTEZARIUS, MeanLizard © 2009\n\n————————————————————————————————\n\nSnotling - Miscreant Memories\nPOCKET ITEM\n\nBADLANDS @ Talk to Elke Raub in The Badlands (found around 38600, 19400).",
				},
				
				{	-- Order version
					-- realm = TTitan.Data.Realm.ORDER,
					name = L"?• Troll (Pocket) - не найден", -- проверить
					text = L"- БЕСПЛОДНЫЕ ЗЕМЛИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Буть готов.\n- КООРДИНАТЫ: 10к, 20к.\n- ОПИСАНИЕ: Найти тролей и добыть с них Обед Тролля.\n- АЛЬТЕРНАТИВА: Задание в Остланде в РвР зоне.\n- НАГРАДА: Домашнее животное Котенок Тролля (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll (Heathgut Troll) - Who Is Up for Seconds\nPOCKET ITEM UNLOCK\n\nBADLANDS @35.5, 33.5 and along the road S of the C11 Dwarf camp. Mudbelly Trolls drop Troll's Dinner. Examine the item to get the unlock. Drop rate is low. http://www.wardb.com/npc.aspx?id=4693.\n\nBy Impatiens, Archaides & Nerror\n20081009",
				},

				{	-- Order version
					-- realm = TTitan.Data.Realm.ORDER,
					name = L"?• Vulture (Title) - не найден", -- проверить
					text = L"- Бесплодные земли (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пора обедать...\n- КООРДИНАТЫ: 35к, 27к.\n- ОПИСАНИЕ: Найти и использовать Части пищи стервятников.\n- ПРИМЕЧАНИЕ: (для Дестракта)Возможно до активации предмета нужно выпольнить другое поручение.\n- НАГРАДА: титул Главное блюдо.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nVulture (Vulture Feeder Parts) - Feeding Time...\nThe Main Course\n\nBADLANDS @ 35, 27.5. Destroy the Vulture Feeder for the unlock.\n\nBy FnClassy, Gabriel Stern, Houyoko, Kriegzwerg & Kutzum\n20081124",
				},
			},
		},


		{
			name = L"УЩЕЛЬЕ ЧЕРНОГО ОГНЯ • Black Fire Pass",
			entries =
			{
				{-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Bandit (Tactic)", -- для дестра анлок несрабатывает из за того что герой союзный.
					text = L"- УЩЕЛЬЕ ЧЕРНОГО ОГНЯ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Ограбление.\n- КООРДИНАТЫ: 57к, 45к.\n- ОПИСАНИЕ: Недалеко от пещеры Темная низина каждые 15 минут проходит отряд из 3-х гномов Рунные мечники Колаз Умгала, которых пытаеться ограбить выбигающий бандит Хаалот Кровавый Налетчик (герой 40 ур.). После того как он расправиться с гномами убейте грабителя.\n- НАГРАДА: фрагмент Тактика людей (1/6).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBandit - Robbery\nMan Tactic Fragment\n\nAt 57k, 45k Black Fire Pass kill Haaloth Bloodreaver (40 hero).\n\nApproximately every fifteen minutes there will be a group of three Kolaz Umgal Runebrand dwarves that walk by this location one after the other. They stop and group up and then Haaloth Bloodreaver will come out to rob and kill them. Allow Haaloth to kill all three and then kill Haaloth to recieve the unlock.\n\nMawgrim",
				},
				
				{
					name = L"• Squig (Tactic)",
					text = L"- УЩЕЛЬЕ ЧЕРНОГО ОГНЯ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Голод и забытье.\n- КООРДИНАТЫ: 38к, 60к.\n- ОПИСАНИЕ: Убить Оголодавший сквиг (побед. 25 ур.).\n- НАГРАДА: фрагмент Тактика зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSquig (Famished Squig) - Forgotten and Famished\nBESTIAL TOME TACTIC\n\nBLACK FIRE PASS @ 37.5, 59.5. Kill this champ hiding in the mts, eating goats, yum!\n\nBy Ulgwar, Sandkat & JodouTR\n20081103",
				},
			},
		},
		
		{
			name = L"<данж> ГОРА ГУНБАД • Mount Gunbad",
			entries =
			{
				{
					name = L"• Night Goblin (Pocket)",
					text = L"- ГОРА ГУНБАД (данж).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Ночь - правильное время.\n- ОПИСАНИЕ: В центральном проходе, в зоне первого паблика Топтальня красноглазов, найти гоблина Моргляк Ваагхный (27 ур. побед.) и убить его.\n- НАГРАДА: Незримый свет (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nNight Goblin (Morgit da Waaagher) - Night-Time is the Right Time\nPOCKET ITEM\n\nMOUNT GUNBAD (Middle Wing). Kill the champ Night Goblin in the first PQ area in the middle wing of Mt Gunbad (Straight down the ramp from the entrance, the PQ with the giants). The mob is hiding in a little alcove on the left near the start of the long hallway leading to the next area. Unlocks: Darklight.\nNOTE: May or may not be a killing blow unlock. Confirmed reports of both instances.\n\nBy Houyoko, JodouTR & Kellithe\n20081110",
				},

				{
					name = L"• Snotling (Tactic)",
					text = L"- ГОРА ГУНБАД (данж).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Зеленая молния.\n- ОПИСАНИЕ: Найти маленького соплинга Кеццен, который бегает через мирную зону с НПСами от правого хода до левого. Если его ударить то он побежит в свою темную дырку которая находиться правее ночного гоблина красноглазов.\n- НАГРАДА: Фрагмент Тактики Зеленокожих (1/5).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSnotling (Kezzen) - Green Lightning\nGREENSKIN TOME TACTIC\n\nMOUNT GUNBAD. The mob spawns in a hidey hole in the right wing before the first PQ. Kezzen immediately paths from the right wing, through the NPC area (he passes by the crock pot there) and on to the left wing. The mob will continue on, following the wall to the right and enter the hidey hole near a bunch of wyvern eggs. There he despawns. Best advice is to wait for him in the NPC area, he'll pass right on by. NOTE: Sometimes he can take up to 3 hours to spawn. Also he can be bugged and ghost through walls.\n\nBy Kellithe\n20081110",
				},

				{
					name = L"• Squig (Title)",
					text = L"- ГОРА ГУНБАД (данж).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пора обедать.\n- ОПИСАНИЕ: В конце центрального прохода за последним пабликом справой стороны есть вход к 2-х главному сквигу.\n- ПРИМЕЧАНИЕ: Чтобы попасть в комнату сквига нужно набрать полное влияние зоны и закончить цепочку из 4-х квестов:\n      Устранить главного мешалу\n      Устранить Гломпа\n      Устранить Солитекса\n      Устранить Две Глотки\n- НАГРАДА: Титул Попробуй съешь.\n\nTEZARIUS, Беорн © 2009\n\n————————————————————————————————\n\nSquig ('Ard ta Feed) - Feeding Time\nArd ta Eat\n\nMOUNT GUNBAD (Middle Wing). Killing blow the last boss, the giant 2-headed squig. NOTE: Killing blow is confirmed, but needs more evidence to support.\n\nBy Dissent\n20081110\n\n————————————————————————————————\n\nMake sure when you make a trip to Gunbad to get all the quests. One quest chain is specifically necessary to do the Giant Squig boss. You have to have completed:\nThe Threat from the Masta Mixa - http://www.wardb.com/quest.aspx?id=762\nThe Threat from Glomp - http://www.wardb.com/quest.aspx?id=763\nThe Threat from Solithex - http://www.wardb.com/quest.aspx?id=778\nin order to both get\nThe Threat from 'Ard ta Feed - http://www.wardb.com/quest.aspx?id=780\nand to gain entrance into the Squigs room.\nYou will find the Masta Mixa inside the instance portal at the end of the right wing, The Alchemy Lab; Glomp da Squig Masta can be found at the instance portal at the end of the left wing, The Squig Nursery; Wight Lord Solithex is in the instance portal to the left after you pass the Redeye fighting the Deathshadow Mourkain; and 'Ard ta Feed is in the instance portal up the ramp from Wight Lord Solithex. The rewards, along with the ability to enter the last boss portal make the quests very much worth the time and effort.\nAlso, as earlier stated, you need 1 bar of Influence to do the Masta Mixa, 2 bars to do Glomp da Squig Masta, and 3 bars of influence to do both Wraith Lord Solithex and 'Ard ta Feed. Hopefully you find this useful, if not enlightening.",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 4 •••",
			divider = true,
			entries = {},
		},

		{
			name = L"ГОРА ГРОМА • Thunder Mountain",
			entries =
			{
				{
					name = L"• Bull Ogre (Acc) - Сет Разделенный",
					text = L"- ГОРА ГРОМА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Ты что, собираешься это съесть?\n- КООРДИНАТЫ: 48к, 54к.\n- ОПИСАНИЕ: Найти котел и нажав на него получить Ломоть сырого мяса. Кликнуть на мясо для открытия записи.\n- НАГРАДА: Роковой покров (Сет Разделенный 1/4).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBull Ogre (Cauldron) - You Going to Eat That?\nJEWELRY ITEM\n\nTHUNDER MOUNTAIN @ 48, 54. Clicking on the Cauldron will give you Fresh Meat. Right-click it to get the unlock: Fated Veil.\n\nBy Sandkat & Teran\n20081027",
				},

				{
					name = L"• Dragon (Title)",
					text = L"- ГОРА ГРОМА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Все что осталось.\n- КООРДИНАТЫ: 2к, 21к.\n- ОПИСАНИЕ: На вершине горы найти сломанный топор Синдри Камнер.\n- ПРИМЕЧАНИЕ: На гору лучше всего взобраться с южной стороны.\n- НАГРАДА: Титул Огнерожденный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDragon - All That Remains\nFireborn\n\nTHUNDER MOUNTAIN @ 1.5, 21. Top of the mt between the two roads crossing into Cinderfall, probably easiest to climb up to this point from the S road. The Broken Axe Cindri Cragborn is a clickable item on ground.\n\nBy fjir, Dissent & m3atwad\n20081103",
				},

				{
					name = L"• Drakk Cultist (Tactic)",
					text = L"- ГОРА ГРОМА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Тяжело быть главным.\n- КООРДИНАТЫ: 17к, 5к.\n- ОПИСАНИЕ: Найти Взрыв-боченок и взорвать 5-6 Драконьих костей разбросанные возле пешеры.\n- НАГРАДА: Фрагмент Тактики Людей (1/6).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDrakk Cultist - Hard To Be a Leader\nMAN TOME TACTIC\nTHUNDER MOUNTAIN @ 17, 5. Click the Drakk Altar inside the cultist cave for the unlock. NOTE: Also accomplished by killing a Drakk Cultist. Any Cultist up there should do. FURTHER: May take multiple kills for the unlock to trigger (confirmed at 5 kills).\n\nBy Sandkat, Teran, JodouTR & xarinth\n20081124",
				},

				{
					name = L"• Drakk Cultist (Title)",
					text = L"- ГОРА ГРОМА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Там где кровь течет рекой.\n- КООРДИНАТЫ: 20к, 8к.\n- ОПИСАНИЕ: Активировать Алтарь Дракка в самой глубене Пешеры Дыханья Дракона.\n- НАГРАДА: титул Разоритель.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDrakk Cultist (Cultist Altar) - Where the Blood Flows\nThe Harrier\n\nTHUNDER MOUNTAIN @ 20, 7.5. Click the altar at the very end of the Dragon Breath Cave.\n\nBy Sandkat, JodouTR & Quiggles\n20081020",
				},

				{
					name = L"• Giant (Pocket)",
					text = L"- ГОРА ГРОМА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Местоотмечено крестом.\n- КООРДИНАТЫ: 12к, 24к.\n- ОПИСАНИЕ: С левой стороны лагеря гномов 18-й главы найти между палатками бочку Багманское Экстра и активировать ее.\n- ПРИМЕЧАНИЕ: Бочку можно достать с крайней палатки, когда патрульный победоносец отойдет в сторону.\n- НАГРАДА: Бочка с элем (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant (Bugman's XXXXXX) - X Marks the Spot\nPOCKET ITEM\n\nTHUNDER MOUNTAIN. Left of the merchant in the Dwarf Ch18 town, click the barrel for the unlock: Barrel of Ale.\n\nBy skyrunner620 & JodouTR\n20081021",
				},

				{
					name = L"• Spider (Tactic)",
					text = L"- ГОРА ГРОМА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Большие проблнмы с самооценкой.\n- КООРДИНАТЫ: 24к, 49к или 22к, 47к или 62к, 22к.\n- ОПИСАНИЕ: Рядом с дорогой убить паука Граелба (35 ур.).\n- НАГРАДА: Фрагмент Тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant Spider (Graelba) - Big Identity Crisis\nBESTIAL TOME TACTIC\n\nTHUNDER MOUNTAIN @ 24, 49 or 22, 47 or 62, 22. Kill this L35 named mob next to the road between Greenskin Ch18 and the Greenskin WC.\n\nBy Supaplex, TorqueVolkmar & Lirion\n20081104",
				},

				{
					name = L"• Spider (Acc) - Сет Ветростойкость",
					text = L"- ГОРА ГРОМА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Грабь награбленное.\n- КООРДИНАТЫ: 43к, 8к.\n- ОПИСАНИЕ: Найти пауков Магмовых мизгирей (38 ур.) и добыть с них 5 Паучьих ядовитых желез.\n- ПРИМЕЧАНИЕ: Шанс выпадение желез примерно 10%.\n- НАГРАДА: Цепь Зовощуго воронов (Сет Ветростойкость 1/3).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant Spider (Magma Crawler) - Sacking for Sacs\nJEWELRY ITEM\n\nTHUNDER MOUNTAIN @ 43, 7.5. Kill the mobs until you get an 5 Arachnid Poison Sac. Unlocks Crow Caller Chain. NOTE: Less than 10% drop rate.\n\nBy Krazed, Kaijin, Draskuul& Venar\n20081103",
				},

				{
					name = L"• H. of Tzeentch (Tactic)",
					text = L"- ГОРА ГРОМА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Чему ужаснется ужас?\n- КООРДИНАТЫ: 16к, 17к или 18к, 13к.\n- ОПИСАНИЕ: На вершине холма возле лавого озера найти Ужас Тзинча и добыть с них Странную куклу.\n- НАГРАДА: Фрагмент Тактики Демона (1/22).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHorror of Tzeentch - What Horrifies Horrors?\nDAEMONIC TOME TACTIC\n\nTHUNDER MOUNTAIN @ 16, 17 or 18, 13. Kill horrors on top of that mt and they drop An Odd Doll. Location is up the mt on the S side, right across the Dwarf Ch18 town. Rt click it in inventory for the unlock. NOTE: Drop rates are very low.\n\nBy armorum, India, JodouTR, poll12 & Quiggles\n20081021",
				},

				{
					name = L"• Rhinox (Title)",
					text = L"- ГОРА ГРОМА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Больше и больше.\n- КООРДИНАТЫ: 50к, 55к.\n- ОПИСАНИЕ: Найдите у берега в лаве Груду риноксовых костей и активируйте ее.\n- НАГРАДА: Титул Разрубатель риноксов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nRhinox (Rhinox Bonepile) - Dense and Denser\nThe Rhinox Rocker\n\nTHUNDER MOUNTAIN @ 50, 55. Click on the bonepile in the lava. This is very close to the cauldron for the Bull Ogre unlock. NOTE: The bone pile can spawn random at every lava lake in the SE part of the map.\n\nBy cintra, TorqueVolkmar & Thovargh\n20081027",
				},
			},
		},

		{
			name = L"ПЕПЛОПАД • Cinderfall",
			entries =
			{
				{
					name = L"• Basilisk (Acc) - The Winds Impervious",
					text = L"- ПЕПЛОПАД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Умение не играет роли.\n- КООРДИНАТЫ: 35к, 34к.\n- ОПИСАНИЕ: У камня найти статую Комнор Бронзовая Кружка и взять с нее Зеркало бронзовой кружки. После чего убить появившуюся ящерицу Дурной Глаз (37 ур.).\n- НАГРАДА: Повязка нарушившего слова (The Winds Impervious сет 1/3).\n\nTEZARIUS © 2009\n\n————————————————————————————————Basilisk (Bale Gaze) - No Reflection of Your Skill\nSET ITEM\n\nCINDERFALL @ 35, 34. Across the lava there is a skeleton Komnor Bronzemug. Click it and read, The statue before you is incredible life-like, and highly detailed. The oddest feature is the cracked mirror that you pry from the statues fingers. Rt clicking on the mirror to make this L37 mob appear. Kill it for: Wordbreaker Band part of The Winds Impervious set.\nTIP: You can climb up the mt @ 46.5, 41 and then work your way through it.\n\nBy JCBN, Almaram & India\n20081021",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Ghoul (Tactic)",
					text = L"- ПЕПЛОПАД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пальчики оближешь.\n- КООРДИНАТЫ: 54к, 43к.\n- ОПИСАНИЕ: На кладбище найти и убить 60 Вурдолаков для Регистратора убийств.\n- НАГРАДА: Фрагмент Тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGhoul (Flamepicked/Deathflame Ghoul) - Finger Lickign Good\nBESTIAL TOME TACTIC\n\nCINDERFALL @ 54, 43. Ghouls are in Cinderfall. Max the Kill Collector @ Da Scrapin Camp for the unlock. NOTE: I killed 80 ghouls and got the unlock. You can probably kill less. As you kill them the ghouls are being counted as zombies but unlock a ghoul tome entry. NOTE: There appears to be a lvl requirement for the kill collector.\n\nBy Sandkat, Grevan, India & victor0219\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Ghoul (Tactic)",
					text = L"- ПЕПЛОПАД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пальчики оближешь.\n- КООРДИНАТЫ: 54к, 43к.\n- ОПИСАНИЕ: Убить 60 зомби и получить награду у регистратора убийств в 18 главе гномов.\n- НАГРАДА: фрагмент тактики зверей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nGhoul (Flamepicked/Deathflame Ghoul) - Finger Lickign Good\nBESTIAL TOME TACTIC\n\nCINDERFALL @ 54, 43. Ghouls are in Cinderfall. Max the Kill Collector @ ch18 Camp for the unlock. NOTE: I killed 80 ghouls and got the unlock. You can probably kill less. As you kill them the ghouls are being counted as zombies but unlock a ghoul tome entry. NOTE: There appears to be a lvl requirement for the kill collector.\nNOTE: Kill Collector says you need to kill Flamepicked Ghouls, but the Deathflame Ghouls on (D) side work.\n\nBy Sandkat, Grevan, India & victor0219\n20081103",
				},

				{
					name = L"• Skeleton (Acc) - The Blood-soaked Sigils",
					text = L"- ПЕПЛОПАД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Разменная монета.\n- КООРДИНАТЫ: 61к, 4к (гном) 57к, 57к (эльф) 64к, 50к (человек).\n- ОПИСАНИЕ: Нужно найти 3 черепа.\n1) Найти в Пешере Обездвиженный скелет гнома и взять с него Череп Гнома-нежети.\n2) Возле пещеры слева найти обездвиженный скелет эльфа и взять Череп эльфа-нежити.\n3) В пешере в первой комнате найти Обездвиженный скелет человека и взять с него Череп человека-нежити.\n- НАГРАДА: Мучение (The Blood-soaked Sigils сет 1/2).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSkeleton (De-Animated Skeleton) - Coin of the Trade\nJEWELRY ITEM\n\nCINDERFALL @ coords below. Examining a Skeleton will give you a skull. Right clicking on the skull unlocks item: Torment. NOTE: You need all three skulls to get the unlock.\n1. @ 59.5, 11k -> Undead Dwarf Skull. In the cave, up the ramp to the left, close to a goblin and some piping.\n2. @ 57, 57k -> Undead Elven Skull @ entrance to cave with 4 vultures in (D) zone.\n3. @ 64, 50k -> Undead Human Skull in first room with ogre and bright wizards inside.\nBUG: vendor refuses to sell the ring despite it's unlock in ToK.\n\nBy Foggye, SmithyPasha, Sandkat, JodouTR, Xiani & Risingashes\n20081110",
				},

				{
					name = L"• Zombie (Tactic)",
					text = L"- ПЕПЛОПАД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Вулгриг.\n- КООРДИНАТЫ: 44к, 3к.\n- ОПИСАНИЕ: В Туннелях Паллик найти Груду камней и убить возле нее толстого Вулгринга (36 ур.).\n- ПРИМЕЧАНИЕ: В туннеле нужно сначало пройти прямо а потом повернуть налево в сторону скелетов.\n- НАГРАДА: Фрагмент тактики нежети (1/12).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nZombie (Wulgrig) - Wulgrig\nUNDEAD TOME TACTIC UNLOCK\n\nCINDERFALL @ 44, 2.5k. This L36 mob spawns in the N cave, that you trigger by interacting with a Pile of Rocks found one lvl below the main lvl of this cave. After killing it I got the unlock Wulgrig, 9th in the list under zombie. Also acquired the first undead tactic after 4 undead tome tactic unlocks called: Undead Chill. NOTE: (D) players cannot interact with the rocks, but after approaching them a few times he spawns.\n\nBy Nilenya, Grevan & JodouTR\n20081017",
				},
			},
		},

		{
			name = L"ПИК СМЕРТИ • Death Peak",
			entries =
			{
				{
					name = L"?• Dragon Ogre (Title) - не найден",
					text = L"- ПИК СМЕРТИ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Страсть к искуству.\n- КООРДИНАТЫ: ...\n- ОПИСАНИЕ: Найти уникального монтсра и убить его.\n- НАГРАДА: Титул ???.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDragon Ogre - Eye for Art",
				},
				
				{
					name = L"• Ghoul (Acc - The Blood-soaked Sigils)",
					text = L"- ПИК СМЕРТИ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Понижение.\n- КООРДИНАТЫ: 7к, 36к.\n- ОПИСАНИЕ: В кустах найти зомби Пайзаха Смрадная (38 ур.) и убить его.\n- НАГРАДА: Борьба (The Blood-soaked Sigils сет 1/2).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGhoul (Pisacha the Rank) - Demoted\nJEWLERY ITEM\n\nDEATH PEAK @ 9.6, 36. Kill it. Unlocks Strife.\n\nBy Zoe9906\n20081021",
				},

				{
					name = L"• Wight (Title)",
					text = L"- ПИК СМЕРТИ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Мертвый груз.\n- КООРДИНАТЫ: 2к, 13к.\n- ОПИСАНИЕ: В Кислотной пещере найти нейтральную крысу Жгучехвостый крысиный король (1 ур.) и убить ее.\n- АЛЬТЕРНАТИВА: Задание в Пустошах Хаоса.\n- НАГРАДА: Титул Убийца Короля.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWight (Seno Cirrus) - Dead Weight\nThe King Slayer\n\nDEATH PEAK @ 3, 10k. Kill the named mob at the end of the tunnel.\n\nBy Zoe9906\n20081013",
				},

				{
					name = L"• Winged N. (Title)",
					text = L"- ПИК СМЕРТИ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Подрезать крылья.\n- КООРДИНАТЫ: 11к, 12к.\n- ОПИСАНИЕ: В ущелье найти и убить Жуткокрыла (побед. 37 ур.).\n- НАГРАДА: Титул Похититель снов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWinged Nightmare (Terrorwing) - Clip Its Wings\nThe Dreamstealer\n\nDEATH PEAK @ 11, 12. Kill it.\n\nBy Sandkat\n20081124",
				},

				{
					name = L"• Wyvern (Title)",
					text = L"- ПИК СМЕРТИ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: На природе.\n- КООРДИНАТЫ: 3к, 45к (пещера) 10к, 45к (яйцо).\n- ОПИСАНИЕ: Внутри пешеры, охраняемую орками, в самой дальней маленькой комнате найти Поврежденное яйцо виверны.\n- НАГРАДА: Титул Смотритель виверн.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWyvern (Damaged Wyvern Egg) - In the Wild\nThe Wyvern Watcher\n\nDEATH PEAK @ 3k, 45k. Find Damaged Wyvern Egg in the back of the cave, in a small room.\n\nBy Sandkat\n20081007",
				},
			},
		},

		{
			name = L"ДОЛИНА КАДРИН • Kadrin Valley",
			entries =
			{
				{
					name = L"?• Bloodbeast of Khorne (???) - не работает",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Сломанные кости.\n- КООРДИНАТЫ: 30.5к, 19к.\n- ОПИСАНИЕ: На скале найдены Кости Кровавой твари. Считаеться что этот предмет должен открывать запись но кости неактивны.\n- НАГРАДА: ???.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBloodbeast of Khorne - Broken Bones\nThere are the Bones of a Bloodbeast in the middle of Kadrin Valley (in the hills dividing the Destruction and Order sections directly above Icehearth Crossing at 30600,19200), but they are currently unusable. This unlock is almost certainly currently broken",
				},
				
				{
					name = L"• Dwarf (Title)",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Завершить сценарий гномов.\n- ОПИСАНИЕ: Принять участие в сценарии Переправа Громрила. Сценарий доступен когда зона открыта для завоевания.\n- НАГРАДА: Титул Гроза Дэви.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDwarf - Complete a Dwarf Scenario\nFoe of the Dawi\n\nKADRIN VALLEY. Win a Gromril Crossing Scenario. Scenario unlocks when the zone becomes contested.\n\nBy Socran\n20081124",
				},
							
				{
					name = L"• Dwarf Slayer (Tactic)",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Смотри вниз.\n- КООРДИНАТЫ: 29к, 40к.\n- ОПИСАНИЕ: На самом верху разрушенного моста найти Припасы мастера камня.\n- НАГРАДА: Фрагмент Тактики Людей (1/6).\n\nTEZARIUS © 2009\n\n————————————————————————————————\nDwarf, Slayer - Look Out Below!\nMAN TOME TACTIC\n\nKADRIN VALLEY @ 29, 40. Up on the bridge there's a barrel of tools, right click for unlock.\n\nBy Itsari & KramerTheWeird\n20090106",
				},

				{
					name = L"• Bear (Tactic)",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Точите ножи.\n- КООРДИНАТЫ: 5к, 15к.\n- ОПИСАНИЕ: Убить бурого медведя Таг Болгар (32 ур.), который спрятался за лагерем фанатиков.\n- НАГРАДА: Фрагмент Тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBear (Thag Bolgar) - Sharpen Your Knives\nBEASTIAL TOME TACTIC\n\nKADRIN VALLEY @ 5, 14.6. Kill the mob. He's at the back of a place in between the mts, behind a camp filled with zealots.\n\nBy Supaplex\n20081021",
				},

				{
					name = L"• Beastman, Gor (Tactic)",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Во имя высшего горха.\n- КООРДИНАТЫ: 27к, 20к.\n- ОПИСАНИЕ: Найти лагерь горхов и убить Йаррган Вражина (32 ур.).\n- НАГРАДА: Фрагмент Тактики Хаоса.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Gor (Yarrgan The Fiendish) - For the Greater Gor\nCHAOS TOME TACTIC\n\nKADRIN VALLEY @ 27, 20 just W of the PVP Objective. Kill it.\n\nBy Iranium & Kraxis\n20081124",
				},
				
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Dwarf Slayer (Acc) - Сет Великие камни клятвы",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Поиски проблем.\n- КООРДИНАТЫ: 58к, 50к.\n- ОПИСАНИЕ: Найти и убить гнома Аргнус Камнечереп, победоносца (38 ур.).\n- НАГРАДА: Камень клятвы Караз-а-Карака (Сет Великие камни клятвы 1/2).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDwarf Slayer (Argnus Rockskull) - Looking for Trouble\nJEWELRY ITEM\n\nKADRIN VALLEY @ 58, 50. Kill the NPC to receive the unlock.\n\nBy cintra & Zerk\n20081021",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Dwarf Slayer (Acc) - Сет Великие камни клятвы",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Поиски проблем.\n- КООРДИНАТЫ: 58к, 50к.\n- ОПИСАНИЕ: Поговорить с Каргнусом Камнечерепом.\n- НАГРАДА: сетовый талисман Камень клятвы Караз-а-Карака (Сет Великие камни клятвы 1/2).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nDwarf Slayer (Argnus Rockskull) - Looking for Trouble\nJEWELRY ITEM\n\nKADRIN VALLEY @ 58, 50. Talk to this NPC to unlock: Oathstone of Karaz-a-Karak.\n\nBy cintra & Zerk\n20081021",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Giant Lizard (Tactic)",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Муки голода.\n- КООРДИНАТЫ: 36к, 40к.\n- ОПИСАНИЕ: Рядом с лагерем зеленокожых 21-й главы найти и убить 60 Снежных ящерец, после чего доложиться регистратору убийств.\n- НАГРАДА: рагмент Тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant Lizard - Hunger Pangs\nBESTIAL TOME TACTIC\n\nKADRIN VALLEY. Max the Kill Collector in Greenskin Ch21 for the tactic. Requires the standard 60 kills.\n\nBy armorum, Houyoko, JodouTR, JCBN & Benji\n20081117",
				},

				{
					name = L"• Giant (Tactic)",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Кольцо в нос.\n- КООРДИНАТЫ: 34к, 28к.\n- ОПИСАНИЕ: Найти на вершине холма троля Вреги Погромщик (39 ур.) и добыть с него Великанское кольцо в нос.\n- АЛЬТЕРНАТИВА: Задание в Рейкланде.\n- НАГРАДА: Фрагмент Тактики Великанов (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant (Vragi the Brute) - Brutal Nose Ring\nGIANT TOME TACTIC\n\nKADRIN VALLEY @ 34, 28. Drops Giant Nosering. Rt clicking object gives the unlock. NOTE: May take multiple kills.\nAlternate method: See REIKLAND.\n\nBy JCBN, JodouTR, skyrunner620 & TorqueVolkmar\n20081031",
				},

				

				{
					name = L"• Nurgling (Tactic)",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: ознакомся с моими маленькими друзьями.\n- КООРДИНАТЫ: 40к, 25к.\n- ОПИСАНИЕ: На самом краю обрыва найти Власий Ипатьев (герой 40 ур.) и убить его.\n- ПРИМЕЧАНИЕ: При попытке атаковать героя прибегут 3 Нурлинга (32 ур.).\n- НАГРАДА: Фрагмент Тактики Демона (1/22).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nNurgling (Vlasi Ipatiev) - Say Hello to My Little Friends\nDAEMONIC TOME TACTIC\nKADRIN VALLEY @ 40.5, 25. Kill this L40 Hero human N of Greenskins Ch22 sitting at the edge of the cliff.\n\nBy MasterM & skyrunner620\n20081103",
				},

				{
					name = L"• Rat Ogre (Title)",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Идущие на смерть.\n- КООРДИНАТЫ: 57к, 39к.\n- ОПИСАНИЕ: В пещере выше паблика 20-й главы Барадум найти и убить Защитник норм (побед. 38 ур.).\n- ПРИМЕЧАНИЕ: Когда вы начнете атаковать его появяться 4 крысолюда которые нападут на вас, когда у победоносца будет меньше 20% жизни.\n- НАГРАДА: Титул Исполосованный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nRat Ogre (Burrow Champion) - To the Death\nThe Scarred\n\nKADRIN VALLEY @ 57, 39k. Kill this L38 champ mob, up a ramp inside the cave. NOTE: He summons 3 skaven at 20% health and is on a 10 minute repop.\n\nBy Sandkat & poll12\n20081017",
				},

				{
					name = L"• Troll, Stone (Acc)",
					text = L"- ДОЛИНА КАДРИН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Необработанный алмаз.\n- КООРДИНАТЫ: 31к, 25к.\n- ОПИСАНИЕ: Добыть из тролей Камнекидал (39 ур.) Расходный необработанный алмаз.\n- НАГРАДА: Окровавленный разлом (сет аксесуаров 1/4).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nStone Troll (Boulder Chukka) - Diamond in the Rough\nJEWELRY ITEM\n\nKADRIN VALLEY @ 31, 25. Kill these mobs to get the drop Consumed Rough Diamond. Rt click it to unlock: Bloodied Fracture.\n\nBy JCBN\n20081021",
				},
			},
		},

		{
			name = L"ЧЕРНЫЙ УТЕС • Black Crag",
			entries =
			{
				{
					name = L"• Basilisk (Title)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пещера камня.\n- КООРДИНАТЫ: 6к, 9к.\n- ОПИСАНИЕ: Исследовать окресности Серной пещеры.\n- НАГРАДА: Титул Камень.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBasilisk - Cave of Stone\nThe Rock\n\nBLACK CRAG. Enter the Sulfur Caves @ NW corner of the map.\n\nBy Supaplex\n20081007",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Basilisk (Tactic)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: В сумке.\n- КООРДИНАТЫ: 9к, 11к.\n- ОПИСАНИЕ: Севернее лагеря зеленокожих (глава 16) найти и убить 5 молодых василисков, после чего поговорить с гоблином Хрюмс в северной части лагеря.\n- НАГРАДА: Фрагмент тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBasilisk - It's in the Bag\nBESTIAL TOME TACTIC\n\nBLACK CRAG. Kill 6 young basilisks N of the Ch16 quest hub. Then go back to the hub and talk to the goblin Gubs. He is hiding by the fence in the N part of the hub.\n\nBy Ravnir, Apophisthecursed, cintra, Yvarma & flushed\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Basilisk (Tactic)",
					text = L"- Чёрный утёс (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: В сумке.\n- КООРДИНАТЫ: 40к, 38к.\n- ОПИСАНИЕ: найти рюкзак Дуренгара в разрушенном лагере. Отнести его Дуренгару Бронзовой Кружке в 21 главу гномов.\n- НАГРАДА: фрагмент тактики зверей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nBasilisk - It's in the Bag\nBESTIAL TOME TACTIC\n\nBLACK CRAG @ 40, 38. Pick-up Duregar's pack located in or around the burnt down huts. Return the bag to Duregar Bronzemug at the Dwarf CH21 camp. NOTE: Bag despawns on pickup and instantly respawns elsewhere. Just keep looking--it's a small bag on the ground.\n\nBy Ravnir, Apophisthecursed, cintra, Yvarma & flushed\n20081124",
				},

				{
					name = L"• Basilisk (Tactic)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пена слез.\n- КООРДИНАТЫ: 2к, 9к.\n- ОПИСАНИЕ: В дальней комнате Серной пещеры найти василиска Вонючка (34 ур.) и убить его.\n- НАГРАДА: Фрагмент тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBasilisk (Stinky) - Scales of Mourning\nBEASTIAL TOME TACTIC UNLOCK\n\nBLACK CRAG. Find this mob in the back of the Sulfur Caves (NW Corner of the map). Also needed for a Destruction quest.\n\nBy Supaplex\n20081007",
				},
				
				{
					name = L"?• Dryad (???) - не найден", -- ненайдена нужная дриада
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Огонь не умерает в одиночку.\n- КООРДИНАТЫ: 21.5к, 36.2к (ящик дестракта), 38.6к, 45.2к (ящик ордера).\n- ОПИСАНИЕ: В лагере взять в старом ящике Факел и воспользоваться им против Пепельной дриады, которых можно найти в южной части РвР зоны.\n- НАГРАДА: ...\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDryad - Fire Seldom Dies Alone",
				},

				{
					name = L"• Bat, Giant (Tactic)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Во тьме дневной.\n- КООРДИНАТЫ: 13к, 33к.\n- ОПИСАНИЕ: Недалеко от лагеря зеленокожих 15-й главы в Лощине летучих мышей убить Норную летучую мышь (31 ур.).\n- НАГРАДА: Фрагмент тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBat, Giant (Pit Bat) - In the Dark of Day\nBEASTIAL TOME TACTIC UNLOCK\n\nBLACK CRAG. Kill a Pit Bat @ the Bat Hollow, W of Greenskin Ch15 (D) camp.\n\nBy Supaplex\n20081007",
				},

				{
					name = L"• Giant (Tactic)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Великан среди великанов.\n- КООРДИНАТЫ: 36к, 53к.\n- ОПИСАНИЕ: Найти возле дороги разрушенную башню и обитающих там трех гигантов Бубор (40 ур.), Гарнток (40 ур.) и Уфгор (40 ур. побед.). Убить всех трех гигантов для получения тактики.\n- НАГРАДА: Фрагмент тактики Великанов (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant (Ufgor) - Giant Amongst Giants\nGIANT TOME TACTIC\n\nBLACK CRAG @ 36, 53. There are 3 named giants, one is a L40+ champ. Kill it.\n\nBy Gorbasch & Doombeard\n20081020",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Gnoblar (Title)",
					text = L"- Чёрный утёс (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Разношёрстная компания.\n- КООРДИНАТЫ: 48к, 12к.\n- ОПИСАНИЕ: пробежать через лагерь гоблинов и огров.\n- НАГРАДА: титул Задира.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nGnoblar - Mixed Company\nThe Bully\n\nBLACK CRAG @ 40, 12k. Burnmaw Tower. Run through the camp with Ogres and Gnoblars.\n\nBy India\n20081016",
				},

				{
					name = L"• Goblin (Title)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Завершить сценарий зеленокожих.\n- ОПИСАНИЕ: Принять участие в сценарии Howling Gorge, который достуен когда зона находиться в спорном конфликте.\n- НАГРАДА: Титул Мстительный пивовар.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGoblin - Complete a Greenskin Scenario\nThe Avenging Brewmaster\n\nBLACK CRAG. Win a Howling Forge scenario. This scenario unlocks when the zone becomes contested.\n\nBy Risingashes\n20081110",
				},

				{
					name = L"• Orc (Title)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Завершить сценарий зеленокожих.\n- ОПИСАНИЕ: Принять участие в сценарии Howling Gorge, который достуен когда зона находиться в спорном конфликте.\n- НАГРАДА: Титул Увечащий.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nOrc - Complete a Greenskin Scenario\nThe Mangler\n\nBLACK CRAG. Win a Howling Forge scenario. This scenario unlocks when the zone becomes contested.\n\nBy Risingashes\n20081110",
				},

				{
					name = L"• Slimehound (Title)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Очень заметный след.\n- КООРДИНАТЫ: 29к, 29к.\n- ОПИСАНИЕ: Правее пещеры Рудник Камнегрива в кустах найти Останки слизепса и осмотреть их.\n- НАГРАДА: Титул Слизеступ.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSlimehound (Slimehound Remains) - An Easy Trail to Follow\nThe Slime-Trodder\n\nBLACK CRAG @ 29, 29k. The slimehound remains are just to the left of the cave entrance behind some bushes. Examine the slime to get the unlock.\n\nBy Thovargh, Archaides& JodouTR\n20081016",
				},

				{
					name = L"• Snotling (Tactic)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Страсть к реликвиям.\n- КООРДИНАТЫ: 63к, 56к (пещера) 62к, 61к (соплинг).\n- ОПИСАНИЕ: Найти пешеру Логово Утесников и в самой дальней комнате отыскать соплинга Кладоискатель (41 ур.) и добыть с него Родовой меч Верены. После чего осмотреть меч.\n- ПРИМЕЧАНИЕ: Он единственный враждебный в этой пещере для Разрушения. Дроп меча всего 10% а релог соплинга 5 минут.\n- НАГРАДА: Фрагмент тактики Зеленокожих (1/5).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSnotling (Hoarding Burrower) - Relish the Relics\nGREENSKIN TOME TACTIC\n\nBLACK CRAG @ 63, 56. In the cave Cliffrunners Den, enter the last big room before the orc champ spawn. There is the L41 named snotling in the corner to your right. Snotling will drop Relic Sword of Verena which must be right clicked for the unlock. NOTE: Drop rate is not %100, 10min repop.\n\nBy Gorbasch, Dissent& Foggye\n20081020",
				},

				{
					name = L"• Troll (Tactic)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Камень в брюхе у тьмы.\n- КООРДИНАТЫ: 7к, 24к (пещера) 1к, 25к (троль).\n- ОПИСАНИЕ: В пещере Яма отчаяния в одной из дальних комнат найти троля Камнебрюх (31 ур. побед.) и убить его.\n- НАГРАДА: Фрагмент тактики Великанов (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll (Rockmaw) - Rock in the Maw of Darkness\nGIANT TOME TACTIC\n\nBLACK CRAG @ 6.5, 24 THEN ---> 1.3, 24.5. L31 mob found outside and later inside the Despair Pits. First, kill it outside the cave and then venture into the cave where he spawns in the back. Kill him there for the unlock. It appears that these two killings are connected. NOTE: Spawn repop between the two mobs is short, if not instant.\n\nBy Lilion, Itsari, JodouTR & Doombeard\n20081020",
				},

				{
					name = L"• Troll (Tactic)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Вышеупомянутая плоть.\n- КООРДИНАТЫ: 2к, 27к.\n- ОПИСАНИЕ: Найти в пещере Яма отчаяния тролей и выбить с них Мясо троля.\n- ПРИМЕЧАНИЕ: Шанс выпадения составляет всего 5%.\n- НАГРАДА: Фрагмент тактики Великанов (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll (Stonegullet Troll) - Prescribed Flesh\nGIANT TOME TACTIC\n\nBLACK CRAG. Kill this mob in the Despair Pits (middle of W border) to receive Troll Flesh. Automatically gave me unlock. NOTE: 5% drop rate.\n\nBy Supaplex & Xerexerex\n20081103",
				},

				{
					name = L"• Troll, Stone (Tactic)",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Несварение желудка.\n- КООРДИНАТЫ: 3к, 27к (пауки) 1к, 20к (камни).\n- ОПИСАНИЕ: в пещере Яма отчаяния найти пауков и выбить Ядовитую железу, после чего пройти в конец пещеры и осмотреть кучу камней Обед Друля.\n- НАГРАДА: Фрагмент тактики Великанов (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll, Stone - Indigestion\nGIANT TOME TACTIC\n\nBLACK CRAG. Enter the Despair Pits (middle of W border). In the N end of the cave, there's something on the ground that you can interact with called Drul's Lunch. Interacting with it gives you a text saying you could poison. Go to the S end of the cave & kill Skittering Spiders to get a Poison Gland. Then interact with Drul's Lunch to get the unlock.\n\nBy Supaplex\n20081021",
				},

				{	
					name = L"• Wyvern (Acc) - Fortune's Baubles",
					text = L"- ЧЕРНЫЙ УТЕС (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Посмотрим, для чего эта штука.\n- КООРДИНАТЫ: 19к, 9к (сквиги) 21к, 9к (ловушка).\n- ОПИСАНИЕ: Убить Ржавошкурых сквигов и доюбыть Свежее мясо. Потом найти Ловушку для Виверн и положить в нее Мясо.\n- НАГРАДА: Счастливая ного толстопуза (Fortune's Baubles сет 1/4).\n\nTEZARIUS, Чейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nWyvern - Let's See What This Does\nJEWELRY ITEM\n\nBLACK CRAG @ 20, 9. Kill a Squig in this area to acquire Fresh Meat, then use it on the Wyvern trap to receive: Fullbelly's Lucky Foot. NOTE: When you use the trap, a L37 Wyvern will appear but it has been confirmed that killing it is not required for the unlock. NOTE: Wyvern trap also spotted @ 44, 13.5. Here, the unlock is unconfirmed.\n\nBy Thovargh, MrPing1000 & Amaranth\n20081027",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• форт •••",
			divider = true,
			entries = {},
		},
		
		{
			name = L"КАМЕННАЯ СТРАЖА",
			entries =
			{
				{-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Dwarf (Title)",
					text = L"- КАМЕННАЯ СТРАЖА (Форт).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Убить властителя крепости гномов.\n- ОПИСАНИЕ: Убить лорда цитадели в форте Гномов.\n- НАГРАДА: Титул Плач Валайи.\n\n————————————————————————————————\n\Dwarf  - Vanquish the Dwarf Fortress Lord\n...",
				},

			},
		},

		{
			name = L"ПЕРЕВАЛ МЯСНИКА",
			entries =
			{
				{-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"?• Greenskins (Title) - не найден",
					text = L"- ПЕРЕВАЛ МЯСНИКА (Форт).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Победить Лорда крепости зеленокожих.\n- ОПИСАНИЕ: Убить лорда в форте Зеленокожих.\n- НАГРАДА: Титул ...\n\n————————————————————————————————\n\Greenskins  - Vanquish the Greenskins Fortress Lord\n...",
				},

			},
		},

		
		

		

	},
}
