TTitan.Data.HistoryEvC = {}

function TTitan.Data.HistoryEvC.GetData()
	return TTitan.Data.HistoryEvC.RawData
end

function TTitan.Data.HistoryEvC.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.HistoryEvC.RawData)
end

TTitan.Data.HistoryEvC.RawData =
{
	header = L"История и знания - Империя против Хаоса", -- History & Lore Unlocks - Empire vs Chaos
	categories =
	{
		{
			name = L"НОРДЛАНД (Nordland)",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Order of the Griffon",
					text =
[[
Just talk to any NPC with the word "griffon" in their name (should get this automatically while questing)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Militia",
					text =
[[
Walk up to the farmer Jakob Hekel in Grimmenhagen Village, his farm's right next to the path leading northward out of the village.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Superstition",
					text =
[[
Just walk into the Grimmenhagen Inn.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Plague",
					text =
[[
There's a house full of plague victims near the center of Grimmenhagen Village.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Sigmar",
					text =
[[
Second floor of the Grimmenhagen inn, there's a book on the floor.May need to do the quest Of Honor and Glory from Ehren Aldemar right by the inn to be able to interact with the book.
]],
				},

				{
					name = L"• Плохие предзнаменования (Ill Omens)",
					text = L"42.1к, 25.5к\nОсмотреть поляну где бродят дикие кабаны.\n\n————————————————————————————————\n\nStraight East of Faewulf's Rest, near some blackened earth and dead birds. (42140, 25494)",
				},

				{
					name = L"• Колледжи магии (The Colleges of Magic)",
					text = L"11.4к, 24.4к\nНа холме найти Пыльный фолиант и осмотреть его.\n\n————————————————————————————————\n\nThis unlock is achieved by examining 'A Dusty Tome' near the ruins of Schloss von Rubendorff. The location is approximately (11360, 24379), it's hidden up on the ledge near some spites under a bush.",
				},

				{
					name = L"• Отдельные поселения (Isolated Settlements)",
					text = L"15.1к, 51.0к\nНайти мертвого Полночного всадника.\n\n————————————————————————————————\n\nIn the Grimmenhagen Village, run through the burning village (the one where you fight your first quest mobs) and stick close to the mountainside. There will be a dead man (identified as \Midnight Rider') and horse along the way. (15130, 51072)",
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Elector Counts",
					text =
[[
This unlock is achieved by examining a dedication plaque south of the Grimmenhagen Farm. The location is approximately (31100, 46400). - appears Order Only, but might be a bug, since destruction can actually examine the plaque, but nothing happens.
]],
				},
			},
		},

		{
			name = L"НОРСКА (Norsca)",
			entries =
			{
				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Портал Хаоса (Chaos Portal)",
					text = L"28.5к, 11.0к\nОсмотреть Портал Хаоса в самом начале карты\n\n————————————————————————————————\n\nApproach the portal north of spawn point in Prologue camp. (28312,10214)",
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Воронова Орда (The Raven Host)",
					text = L"Посетить 1-ю главу\n\n————————————————————————————————\n\nAs you aproach Chapter 1 camp from Herdstone. (Destruction only)",
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Пушка Демона (Hellcannon)",
					text = L"38.5к 10.2к\nОсмотреть боеприпасы рядом с Пушками демона.\n\n————————————————————————————————\n\nInteract with the ammunition next to hellcannon #2 (starting with the northernmost and going clockwise). Hellcannons are located east of the graveyard and Prologue camp. (38470,10158)",
				},

				{   -- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Бог-Ворон (The Raven God)",
					text = L"31.2к 12.7к\nПолучить у Гарик Кровобой квест 'О битвах и кровопролитии' и осмотреть нарисованную на земле руну.\n\n————————————————————————————————\n\nThe glowing blue symbol south of spawn point in Prologue camp, the ridge it is on overlooks the Ch 1 camp. You may need to have the quest 'Of Carnage and Conquest' from nearby NPC Garik Bludfist in order to examine the rune and get the unlock.(Can only be completed by Destruction) (31195,12714)",
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Демоны (Daemons)",
					text = L"39.5к, 20.0к\nОсмотреть територию паблика 1-й главы хаоса и найти синий глаз.\n\n————————————————————————————————\n\nIn the middle of the depression of casters in the Ch 1 PQ. (The eye in the center is also a Pursuit unlock if you interact with it) (39453,20000)",
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Северные племена (Norse Tribes)",
					text = L"35.1к, 11.5к\nВойти в лагерь 1-й главы Хаоса\n\n————————————————————————————————\n\nFrom the Ch 1 camp, head east to the tents and approach a purple mutated tree (do not cross bridge or river.)",
				},

				{
					name = L"• Разорение и Грабеж (Raiding and Pillaging)",
					text = L"37.6к, 22.2к\nСлева у дороги найти Сломанную телегу и осмотреть ее.\n\n————————————————————————————————\n\nAs you leave the Ch 1 PQ heading towards the bridge to Ch 2, there is a collection of wheelbarrows and crates just east of the road. Interact with the Broken Wheelbarrow . (36782,22200)",
				},

				{
					name = L"• Кочевники (Nomads)",
					text = L"30.8к, 10.6к\nВойти в палатку с Бешенными марадерами.\n\n————————————————————————————————\n\nIn the prologue camp, as you exit towards the graveyard to the east, there is a tent on the north side full of marauders. Walk inside it. (30867,10617) Can be completed by Order (suicide mission).",
				},

				{
					name = L"• Чародейство (Sorcery)",
					text = L"61.0к, 48.7к\nНайти на алтаре Книгу и прочесть ее.\n\n————————————————————————————————\n\nIn the Empire Ch 4 area, there's an Altar of Bloodbane PQ in the middle of Gotland Forest (You won't see PQ info as Chaos) and in the center of it (61030, 48742 roughly) is a large open book lying on the ground near some stones. Interact with it.",
				},

				{
					name = L"• Марадеры (Marauders)",
					text = L"31.0к, 47.4к\nНайти на холме мертвую лошадь Тупица и осмотреть ее.\n\n————————————————————————————————\n\nA dead horse (named Logi) on top of a small cliff, east of the Suderholm PQ in Ch 2 (31048,47432 roughly)",
				},
			},
		},

		{
			name = L"Troll Country",
			entries =
			{
				{
					name = L"Cult of the Plaguesworn",
					text =
[[
On the southwest side of the Grave Diggers PQ (Chapter 6). Located by the water's edge (41643, 56698).
]],
				},

				{
					name = L"Scarce Resources",
					text =
[[
On the west side of The Blighted Farm PQ (Chapter 6) is a burning house. Walk in-between the small area where the house and the cart next to it are at. (23150, 52750)
]],
				},

				{
					name = L"Sacred Totems",
					text =
[[
In the southern part of Trovolek (Destruction Chapter 8) are 3 wooden eagle-like totems in the beach, walk near them. (Confirmed)
]],
				},

				{
					name = L"The End Times",
					text =
[[
Located among the Great Eagle Nests (Major Landmark), there is a nest near the ledge to the your right while going up the long hill. The book, which is mostly obscured by the nest, titled "This Will Be Your Doom". Interact with the book. (34790, 32256)
]],
				},

				{
					name = L"The Cult of Sigmar",
					text =
[[
(RvR area) North from Felde (not the castle) is a nearby ruined Sigmar Shrine just walk to it to get the unlock. (10675, 53300)
]],
				},

				{
					name = L"Nobility",
					text =
[[
There are some Fine Tools lying on the ground off the cliff of Suskarg (Chapter 5). Note: It's close to the Order town so, watch for guards if you're Destruction sided. Obscenely hard to get for Destruction. If you stay on the lower edge of the hill, you will find the tools just before reaching the snow hills. Stay right below order town and you'll be just fine.(8314,23218)
]],
				},

				{
					name = L"Nurgle",
					text =
[[
(Destruction side of the map) Middle of Lursa's Blight PQ (Destruction Chapter 8) on top of the hill. Walk up to the Nurgle Symbol to get it. (51605, 30559)(Confirmed)
]],
				},

				{
					name = L"Soulblight Stone",
					text =
[[
This unlock is in-between Slayer's Demise PQ (Chapter 6) and Plague Trolls PQ (Chapter 7) but it's closest to the latter. On the map is a medium sized lake (the only one), there are some little plots of land scattered around, it's in the right-most side of the lake. (looks like a circle on the map) On it beside the large rock are a group of Maurauder corpses, one of the them can be clicked on (Dead Maurauder). (50257, 54451
]],
				},

				{
					name = L"Taal",
					text =
[[
(RvR area) Click on the Wine in a Casket on the ground near a copse of trees it's east from the Ruins of Greystone Keep Battle Objective. It's to the right of the fork in the road on the map. (7603, 52683)
]],
				},

				{
					name = L"Mannslieb and Morrslieb",
					text =
[[
Directly south of the Empire Ch. 6 area Felde Castle, there is a Very Large Telescope up on a partially crumbled section of wall. You will need to look up with your camera to see it. Hug the west side of the wall and stand partially on the rock to just slightly come within range of the telescope to use it. This unlock is exceptionally hard for destruction players to get as approaching closer will cause you to be killed by the town guards. (16435, 55680)
]],
				},
			},
		},

		{
			name = L"Ostland",
			entries =
			{
				{
					name = L"Twin-tailed comets and other Omens",
					text =
[[
(Destruction side of the map) Click on the statue where a camp surrounds it in Wayshrine of Sigmar PQ (Chapter 6) 32k, 13k
]],
				},

				{
					name = L"Magnus the Pious",
					text =
[[
(Order Side of the map) A pile of books can be found lying around 13k , 32k titled as "The Canonization of Magnus the Pious". Its near a small camp's entrance up the slope to the right of the Black Mire Bell Tower hidden behind a small bush.
]],
				},

				{
					name = L"Loathsome Rat Men",
					text =
[[
(Order Side of the map) Click the Dead Skaven located under the bridge behind the grate just east of Empire ch9 52k, 52k.
]],
				},

				{
					name = L"Favored of the Gods",
					text =
[[
(Destruction side of map) There is a corpse of Dagny Betz in the RvR area aprox 31k by 11k near the hills behind a tree with a another corpse hanging from it and a couple of other bodies surrounding it.
]],
				},

				{
					name = L"Dark Rites",
					text =
[[
(Destruction side of map) Approach the altar in the Krul'Gor Herd PQ (Chapter 5)-18k,20k
]],
				},

				{
					name = L"Wilderness",
					text =
[[
(Order side of the map) At about 36k, 26k, along the road from you exit from Bohsenfels there is an Imperial Signpost. Right-click on it to receive the unlock.
]],
				},

				{
					name = L"Khorne",
					text =
[[
(Order side of the map) At the Gore Wood PQ (Chapter 9). The ruined tent where Dieden Bloodmane spawns is a powder barrel, right click on it. (around 6.5k, 58k)
]],
				},

				{
					name = L"Blood for the Blood God",
					text =
[[
(Order side of the map) Another unlock nearby the Gore Wood PQ (Chapter 9) there is an gathering of cultists along with a big symbol of Khorne overlooking the lake. Approach the the area from the lake side so you won't have to go through the trouble wiping the whole group, but just a few after-wards click on the altar by the icon. Another note the cultists are around rank 19-20, so if you're lower ranked, careful.(around 10.5k, 55k)
]],
				},

				{
					name = L"The Howling Vale",
					text =
[[
(Order side of the map) Forest of Shadows (Major Landmark): In the middle of the Howling Vale PQ is a wrecked wagon surrounded by some Gor Raiders, (rank 19-20)go near it to unlock. (32k, 53k)
]],
				},

				{
					name = L"Forest Vipers",
					text =
[[
(Destruction side of the map) East of the Krul'Gor Herd PQ (Chapter 5), walk towards a house and you'll get it. (around 21k, 21k)
]],
				},
			},
		},

		{
			name = L"High Pass",
			entries =
			{
				{
					name = L"Doctor Zumwald",
					text =
[[
Click on a chest Zumwalds Supplies at 31532, 41494.
]],
				},

				{
					name = L"Temple of Change",
					text =
[[
Walk in the Temple of Change at 25990,48609.
]],
				},

				{
					name = L"Ranald",
					text =
[[
???
]],
				},

				{
					name = L"The Luminous Veil",
					text =
[[
62, 37 Run into the pit
]],
				},

				{
					name = L"Empire Steam Tank",
					text =
[[
Go near the steam tank at around 9000, 56000 in Raven's End to get the unlock. It is possible for Destruction to get this, but it is a suicide run.
]],
				},

				{
					name = L"The Sigil of Malice",
					text =
[[
Enter the temple at 11300, 10675 for the update
]],
				},

				{
					name = L"Shallya",
					text =
[[
A corpse (plague victim)right of the door of Hallenfurt Manor between the wall and a rock in 50,3k/55,3k 
]],
				},

				{
					name = L"Ogrund's Tavern",
					text =
[[
Walk near the RvR objective Ogrund's Tavern at 27862,59919.
]],
				},

				{
					name = L"Feiten's Lock",
					text =
[[
In the RvR zone there is a gate (40k,64k). Pass through it and you will get the update.
]],
				},

				{
					name = L"Fort Marvik",
					text =
[[
Enter Stoneclaw Castle at 4569, 56324.
]],
				},

				{
					name = L"Black Powder",
					text =
[[
Behind Ogrund's Tavern is a Black Powder Keg. Click on it to learn about black powder.
]],
				},

				{
					name = L"Myrmidia",
					text =
[[
On the inside of the Outer Wall, against the outer wall in Hallenfurt Manor. A pile of books called On Warfare at 46797, 61747. 
]],
				},

				{
					name = L"The Grand Conclave",
					text =
[[
Click on a scroll named "Letter to the Order of Luck" at 52, 64. A dead horse and rider are nearby. The horse's head points to the scroll. 
]],
				},

				{
					name = L"Verena",
					text =
[[
Click on Dietrich's Letter at 30857, 44146. It's located near the Doctor Zumwald unlock.
]],
				},

				{
					name = L"Exorcism",
					text =
[[
Click on the chest Blair's Raiments at 42k,56k.
]],
				},
			},
		},

		{
			name = L"Talabecland",
			entries =
			{
				{
					name = L"Serpent Fang Bandits",
					text =
[[
Go to 3800, 19000.
]],
				},

				{
					name = L"Slaanesh",
					text =
[[
Click on the corpse Albert Calvert Sr. at 13931, 49508. It's on the northern side of a building next to a path.
]],
				},

				{
					name = L"Corruptor's Crown",
					text =
[[
Enter the cave in RvR zone at 39.5k,1k.
]],
				},

				{
					name = L"Lady Kreuger",
					text =
[[
Enter the big house at 38000, 16690. It should unlock in the entrance hall.
]],
				},

				{
					name = L"Verentane's Keep",
					text =
[[
Enter the keep in the RvR zone of Talabecland. (The Only one thats there) (Uncomfirmed)
]],
				},

				{
					name = L"Dragonships",
					text =
[[
Approach the middle ship at about 51k, 17k.
]],
				},

				{
					name = L"Necromancy",
					text =
[[
Click on the chest labelled "Gosbert Troutman" underneath the Haunted Windmill at 45978, 48845.
]],
				},

				{
					name = L"Witch Hunts",
					text =
[[
Gravestone in suderheim cemetary @24,28.7.Says "Herein lies Hester Prynne"
]],
				},

				{
					name = L"Talabec River",
					text =
[[
Walk on to the boat at 18350, 41718.
]],
				},

				{
					name = L"Woodsmen",
					text =
[[
At about 13k, 8k...click on the Barrel of Fish in the wagon.
]],
				},

				{
					name = L"Talabecland Thunder Water",
					text =
[[
Click on the Tasty Beverage on the ground at 54151, 30109.
]],
				},

				{
					name = L"Longshanks",
					text =
[[
8k, 21,5k Click on the corpse Longshanks
]],
				},

				{
					name = L"Rhya",
					text =
[[
Click on the tombstone Rest in Peace at 40820, 39172.
]],
				},

				{
					name = L"Eye of the Beholder",
					text =
[[
Click the barrel of Stirland Red at about 32k, 23k.
]],
				},

				{
					name = L"Moral Oppression",
					text =
[[
Walk into the graveyard at 40820, 39172 to get the unlock.
]],
				},

								{
					name = L"Agitators",
					text =
[[
In the Chaos Ch. 11 PQ Army of Faith, interact with one of the soapboxes under the Doom Prophets. (Adds an unlock for another zone) (Unconfirmed)
]],
				},

			},
		},

		{
			name = L"Praag",
			entries =
			{
				{
					name = L"Kislev",
					text =
[[
Walk through the area around 38000,26000
]],
				},

				{
					name = L"The Steppes",
					text =
[[
???
]],
				},

				{
					name = L"Ungol Horsemen",
					text =
[[
At 8.5k, 19.5k Praag examine the Target. It is an archery target. ~Lucrin 
]],
				},

				{
					name = L"Tor",
					text =
[[
Examine book on the ground in the middle of the Gates of Praag PQ at 11000,3200
]],
				},

				{
					name = L"Warpwind Clan",
					text =
[[
Examine a banner in the Skaven Tunnels. Entrance is at 8.6k, 64k.
]],
				},

				{
					name = L"Vodka",
					text =
[[
???
]],
				},

				{
					name = L"Dazh",
					text =
[[
Walk around 61k, 49.9K for unlock. 
]],
				},

				{
					name = L"Gospodars",
					text =
[[
Click the corpse of Sascha Makiev around 8.7, 39.1
]],
				},

				{
					name = L"Winged Lancers",
					text =
[[
Walk around 12600,15300 for unlock
]],
				},

				{
					name = L"Ursun",
					text =
[[
Approach a Charred Wizard's Corpse at 50850,37220 for the unlock. The corpse also gives a quest (for Destruction only? unsure). This is directly east of the Ravensworn Warcamp in a large crater.
]],
				},

				{
					name = L"Griffon Legion",
					text =
[[
???
]],
				},

				{
					name = L"The Ice Queen of Kislev",
					text =
[[
???
]],
				},

				{
					name = L"The Great War Against Chaos",
					text =
[[
Walk near the battlefield objective at 29810, 36101.
]],
				},

				{
					name = L"Ice Magic",
					text =
[[
???
]],
				},

				{
					name = L"The Cursed City",
					text =
[[
Walk around 55000,44200 for the Unlock
]],
				},
			},
		},

		{
			name = L"Chaos Wastes",
			entries =
			{
				{
					name = L"Mutation",
					text =
[[
Journal of Teppo Benzinger 33.5k, 16.5k
]],
				},

				{
					name = L"Change Storms",
					text =
[[
Move around the area at 51700,4600 for the unlock.
]],
				},

				{
					name = L"Changing Terrain",
					text =
[[
Examine corpse at 33870, 35100.
]],
				},

				{
					name = L"Tribal Rivalries",
					text =
[[
Unlocked while walking near 47935, 17077.
]],
				},

				{
					name = L"The Lonely Tower",
					text =
[[
Walk toward steps leading to tower door in 'Lonely Tower' pq. (2.27, 22.1k)
]],
				},

				{
					name = L"Monoliths",
					text =
[[
???
]],
				},

				{
					name = L"Grimclan",
					text =
[[
Click on the Banner at 12.5k, 53.5k
]],
				},

				{
					name = L"Grimnir's Journey",
					text =
[[
Click on the Axe at 31k, 3k
]],
				},

				{
					name = L"Soul Gem",
					text =
[[
15k, 58k click the book near the ruined house for the 'Corruption' unlock. (see below)
]],
				},

				{
					name = L"Corruption",
					text =
[[
Location update at 15k, 58k. Killing all the zombies in the ruined house also gives a Tactical Tome Unlock for zombies.
]],
				},

				{
					name = L"Teef",
					text =
[[
???
]],
				},

				{
					name = L"Gifts of the Gods",
					text =
[[
48,5k 42k, in marauder camp, click on a broken desk.
]],
				},

				{
					name = L"Rise of a Champion",
					text =
[[
28k 43k, scout battlefield objective, Statue of Everchosen.
]],
				},

				{
					name = L"The Hill of the Dead",
					text =
[[
In RvR territory, there's a statuary at 25.5k,39.5k that will unlock this achievement.
]],
				},

				{
					name = L"Daemon Weapon",
					text =
[[
???
]],
				},
			},
		},

		{
			name = L"Reikland",
			entries =
			{
				{
					name = L"The Reiksguard",
					text =
[[
26k, 52k - there is a Reiksguard Guide, examine it to get unlock.
]],
				},

				{
					name = L"The River Reik",
					text =
[[
Ride over the bridge at 28986, 48085.
]],
				},

				{
					name = L"Slaves to Fashion",
					text =
[[
It looks like a pile of dirt along side of the road but is called "Empire Culture" 49k 64k. :D Slaves. 
]],
				},

				{
					name = L"Shallya",
					text =
[[
???
]],
				},

				{
					name = L"Reikflies",
					text =
[[
Click on the Barrel of Ale at 53k 51.5k (Just outside (D) PQ Hunting the Hunters)
]],
				},

                {
                    name = L"Agitators",
                    text =
[[
There is also a "Soap Box" at 6800,9350 in Reikland in the Castle Grauenburg area. Need confirmation if this will also trigger the unlock.  (doesn't work ~Lucrin) 
]],
                },

				{
					name = L"Abject Poverty",
					text =
[[
IN TALABECLAND! In the Chaos Ch. 11 PQ Army of Faith, interact with one of the soapboxes under the Doom Prophets.

There is also a "Soap Box" at 6800,9350 in Reikland in the Castle Grauenburg area. Need confirmation if this will also trigger the unlock. (doesn't work ~Lucrin) 
]],
				},

				{
					name = L"Celestial College",
					text =
[[
Examine the Dwarven Refractory Contraption at the Wilhem's Fist Keep (after outer wall) at about 28700,26950.
]],
				},

				{
					name = L"Reik River Bandits",
					text =
[[
Click on the Bag of Gold next at 6350, 27750.
]],
				},

				{
					name = L"Reikland Blackswords",
					text =
[[
Enter one of the tents in the (D) PQ Rain of Fire area (41.5k, 20k)
]],
				},

				{
					name = L"Morr's Maze",
					text =
[[
Approach the entrance to Morr's Maze at 63000,36700. 
]],
				},

				{
					name = L"Hedge Wizards",
					text =
[[
Examine fire outside the Wilhelm's Fist Keep by the wall left of the main gate at 33000,23000. (double unlock with Tilea)
]],
				},

				{
					name = L"Detachments",
					text =
[[
Walk up the road to a cannon battery around 49600,61000. 
]],
				},

				{
					name = L"The Jade College",
					text =
[[
Talk to Davan Stonebach in Praag, Empire Chapter 19 quest hub. Coordinates are aproximately 28k, 24k (this NPC is Order-only). Destruction - Got this by killing Searing Squire in Chaos Wastes - 21800, 38725
]],
				},

				{
					name = L"Tilea",
					text =
[[
Examine fire outside the Wilhelm's Fist Keep by the wall left of the main gate at 33000,23000. (double unlock with hedge wizards)
]],
				},
			},
		},

		{
			name = L"Altdorf",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Altdorf Docks",
					text =
[[
Enter the Docks (28319,18074)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Ol' Knuckles' Ring",
					text =
[[
Approach the two men fighting on the docks, north of the Blowhole Tavern (25578,13898)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Pale-Eye Hideout",
					text =
[[
Talk to Gahela Coppermane on the Docks (28514,18009)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Sewers of Altdorf",
					text =
[[
Explore The Sewers dungeon located in Altdorf (3 wings).
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Blowhole Tavern",
					text =
[[
Enter The Blowhole, located on far north side of the docks (24599,14486)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Cult of the Feathered Coin",
					text =
[[
To the north of the Temple of Sigmar, keep going north until you see a fence gate in front of you and a beggar to the left, turn right and go into the small alley (13530,20212)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The City Guard",
					text =
[[
I received this when I tried to go into the sewers behind the shelves in the basement of The Mastiff's End (19184,21663)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The First Bank of Altdorf",
					text =
[[
Approach the Bank of Altdorf Entrance (14486,24077)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Emperor's Palace",
					text =
[[
Unlocks when you first fly into Altdorf
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Temple of Sigmar",
					text =
[[
Enter the Temple of Sigmar (10179,24143)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Reikland Arms",
					text =
[[
Enter the Reikland Arms, across from First Bank of Altdorf (13376,24077)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Clank",
					text =
[[
Enter The Clank along Lords Row (17161,27796)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Aqshy Fountain",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Slums",
					text =
[[
Enter the Slums (22185,20293)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Lord's Row",
					text =
[[
Enter the Lord's Row (16508,24077)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"War Quarters",
					text =
[[
Enter the War Quarters
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Legacy of the Emperors",
					text =
[[
Click on the Old Empire Standard in front of the Emperor's Palace. It looks like a red standard pinned to the wall on the right side of the 2nd doorway to the Palace from the Marketplace. (19500,16000)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Bright College",
					text =
[[
Enter the Bright College (27796,24143)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Market Square",
					text =
[[
Enter the Market Square (20684,17944)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Goradian's Fall",
					text =
[[
Enter the Sewers at 25k, 22k. Find the room with Goradian The Creator. He's a level 20 Champion but not hostile. There is a well worn book on the floor, click it to get the unlock.
]],
				},
			},
		},

		{
			name = L"The Inevitable City",
			entries =
			{
				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Sacellum",
					text =
[[
Approach The Sacellum (27884,20173)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Holding Pits",
					text =
[[
Kill the Hero in the Sacellum Dungeon. "Which Hero" is being discussed on the talk page.
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Cult of the Plaguesworn",
					text =
[[
Talk to Shargra Glutmire in Fleshrot Alley (npc wanders around some, but usually standing inside at 39451,19760)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Bloodfiends",
					text =
[[
Interact with the Mark of Khorne above the door south of the Sacellum. (It is in the archway) (29382, 25044)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Battle for the Herdstone",
					text =
[[
Stand next to the Herdstone. (24786,20173)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Temple of the Damned",
					text =
[[
Kill Sovak the Merciless during the Public Quest in The Lost Narrows
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Lost Narrows",
					text =
[[
Enter The Lost Narrows (23616,24717)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Apex",
					text =
[[
Approach The Apex (19691,21412)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Soul Vaults",
					text =
[[
Stand near the banker just south of the Lyceum
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Eternal Citadel",
					text =
[[
Approach The Eternal Citadel (19140,12668)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Scrying Vats",
					text =
[[
Unlocked by walking into The Apex (19760,19347)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Journey's End",
					text =
[[
Interact with the skeleton of Agder Rotwing on Dread Way (11085,28504)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Price of Knowledge",
					text =
[[
Interact with the Treatise of Dire Caution in The Lyceum (7160,18865)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Foreboding Lyceum",
					text =
[[
Talk to Disciple Everdamned. He is located in the upper left wing of the Lyceum. You can only open the door there when the city rank is 4. (3.9k,21k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Monolith",
					text =
[[
Approach the Monolith, as you near it you'll receive this unlock (11980,21619)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Sentries",
					text =
[[
Talk to a Dread Sentry
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Fate's Edge",
					text =
[[
Approach Fate's Edge (18108,16731)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Dhar'ek Cores",
					text =
[[
This unlock requires you to be near a Dhar'ek Core, the blue crystals on the ground, when one of the Lost Souls decides to attack and destroy it. This event normally takes place in Journey's End and if you loiter near a core at the entrance to the Undercroft or just NE of Journey's End towards the Temple of the Damned PQ it will happen.
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Imbalance of Power",
					text =
[[
Kill the Blue Speckled Crow outside of The Viper Pit (21619,12393)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Watchers",
					text =
[[
Unlocked by walking towards The Journeys End (20104,27402)
]],
				},
			},
		},
	},
}
--[[
хистори анлок на Прааг - "Степи" 26.8k, 4.45k сломанный плуг
MeanLizard © 2009
--]]