TTitan.Data.Talismans = {}

function TTitan.Data.Talismans.GetData()
	return TTitan.Data.Talismans.RawData
end

function TTitan.Data.Talismans.FilterData()
end

TTitan.Data.Talismans.RawData =
{
	header = L"Производство - Изготовление талисманов", -- Talisman Making
	categories =
	{
		{
			name = L"Basic Information",
			entries =
			{
				{
					name = L"Introduction",
					text =
[[
Talismans are gems that get added to special armor and jewelry with Talisman slots.  They provide one of eleven statistic or resist bonuses to that particular piece of equipment.  The duration of a particular Talisman is variable.  It is based on the quality of the Talisman.  The duration only counts down when the character is logged in and the particular piece of armor with the Talisman in it is equipped.

A Talisman is created by using 5 parts: a relic box, a fragment, a gold dust, a curio, and an essence.  The quality of these ingredients affect the outcome of the combine in a very predictible fashion.  Using a little bit of math, it's easy for a Talisman Maker to know exactly what the statistics on a Talisman will be before it's made.  There are chances at making "something better" or "something awesome" that are rare, but for the most part, it's a very simple skill to understand.
]],
				},
				{
					name = L"Special Thanks",
					text =
[[
I wouldn't have been able to create this section of the Talisman Titan without Karylet of the Monolith server's great guide and thread on the Warhammer Alliance forums.  A huge thanks to Karylet and all the research that has been done and tables compiled.  A lot of this comes directly from that, and by no means am I trying to take credit for it!
]],
				},
			},
		},
		{
			name = L"Components",
			entries =
			{
				{
					name = L"Relic Box",
					text =
[[
The relic box is the container that is used to combine all the other components.  At present, there is only one type relic box in the game--the Journeyman's Relic Box.

Journeyman's Relic Box has 0 value; it does not improve your final fragment at all.  This is different than Apothecary, which has over a dozen unique vials which affect the final product.

Journeyman's Relic Boxes can be purchased at any chapter camp.  At present, they cannot be found in any Warcamps, with the exception of the Destruction Warcamp in Dragonwake.  Journeyman's Relic Boxes CANNOT be purchased in either Altdorf or the Inevitable City.  This is thought to be an oversight by Mythic, and it may be amended in the future.
]],
				},
				{
					name = L"Fragments",
					text =
[[
Coming soon.
]],
				},
				{
					name = L"Gold Dust",
					text =
[[
Gold Dust Name                 Skill    Color  Value
----------------------------------------------------------
Refulgent Gold Dust         - 200 - Green - 34
Flawless Gold Dust            - 200 - White - 33
Lucent Gold Dust              - 175 - Green - 30
Polished Gold Dust           - 175 - White - 29
Lambent Gold Dust          - 150 - Green - 26
Refined Gold Dust            - 150 - White - 25
Radiant Gold Dust             - 125 - Green - 22
Fine Gold Dust                  - 125 - White  - 21
Incandescent Gold Dust - 100 - Green  - 18
Crude Gold Dust                - 100 - White - 17
Glowing Gold Dust            - 75 - Green  - 14
Powdery Gold Dust           - 75  - White  - 13
Dim Gold Dust                   - 50 -  Green  - 10
Granulated Gold Dust       - 50 - White   - 9
Unusual Gold Dust            - 25 - Green   - 6
Coarse Gold Dust              - 25 - White   - 5
Curious Gold Dust             - 1   - Green   - 2
Impure Gold Dust             - 1   - White   - 1
]],
				},
				{
					name = L"Curios",
					text =
[[
Coming soon.
]],
				},
				{
					name = L"Essences",
					text =
[[
Essence Name             Skill    Value
----------------------------------------------
Harmonious Essence - 200  -  35
Singing Essence         - 175  -  31
Clamoring Essence    - 150  -  27
Chattering Essence    - 125  -  23
Humming Essence     - 100  -  19
Murmuring Essence   - 75    -  15
Subdued Essence       - 50    -  11
Dormant Essence       - 25     -  7
Silent Essence             - 1       -  3
]],
				}
			}
		},
		{
			name = L"Crafting Information",
			entries =
			{
				{
					name = L"About Values",
					text =
[[
Each material has a very linear value assigned to it.  The final outcome of your Talisman creation is based primarily upon the total value of the 5 materials used--relic box, fragment, gold dust, curio, and essence.  The value of each individual component is listed in the Components section.

To determine what the outcome of a particular combine will be, simply add up the value of each component in the relic box and establish which minimum tier value you meet.

Except when you have a "critical" combine, such as "you've created something better" and "you've created something awesome," the properties of your resulting talisman will always follow the pattern outlined on that table.

Check out Talisman Tiers for more information on minimum values.
]],
				},
				{
					name = L"Talisman Tiers",
					text =
[[
The following table identifies the minimum required "value" to craft any Talisman of a particular tier.

The values below the Tier # are the statistical bonus on the final Talisman.  The color of the fragment is critical to this portion.  The table below is arranged as follows, regarding fragment color:
 (Purple/Blue/Green/White)
Note: Resistance Talismans have a value that is 10 times what is displayed on this table.  So a Tier 7 green Elemental Resistance Talisman would provide +110 Elemental Resistance.
 
The durations of the Talisman is also based on the color of the fragment.
 - Purple Fragments yield permanent Talismans
 - Blue Fragments yield 5 day duration Talismans
 - Green Fragments yield 2 day duration Talismans
 - White Fragments yield 8 hour duration Talismans
 
Tier 10 - Still gathering information
 (20/17/14/11)
Tier 9 - 120 value required
 (19/16/13/10)
Tier 8 - 104 value required
 (18/15/12/9)
Tier 7 - 88 value required
 (17/14/11/8)
Tier 6 - 72 value required
 (16/13/10/7)
Tier 5 - 56 value required
 (15/12/9/6)
Tier 4 - 40 value required
 (14/11/8/5)
Tier 3 - 24 value required
 (13/10/7/4)
Tier 2 - 8 value required
 (12/9/6/3)
Tier 1 - 5 value required
 (11/8/5/2)
Tier 0 - 4 value required
 (10/7/4/1)
]],
				},
				{
					name = L"Critical Successes",
					text =
[[
Explain how crit'ing on a combine works here.
]],
				},
				{
					name = L"Skill Adjustments",
					text =
[[
Explain how the character's skill affects crafting.
]],
				},
				{
					name = L"Recipes",
					text =
[[
The following is a list of useful combinations that will take add up to a value of 120 utilizing more common materials, allowing you to create Tier 9 Talismans (+16/+19) using a 200 Blue Fragment and a 200 Blue Essence.

200 Blue Fragment (35 Value)
200 Harmonious Essence (35 Value)

And pick any one of the following combinations:

 + 150 White Gold Dust (Refined Gold Dust) (25 Value)
 + 150 White Curio (Steelcap Mushroom) (25 Value)
 
 + 175 White Gold Dust (Polished Gold Dust) (29 Value)
 + 125 White Curio (Pilgrim's Prayer Beads) (21 Value)

 + 125 White Gold Dust (Fine Gold Dust) (21 Value)
 + 175 White Curio (Sabretusk Canine) (29 Value)
 
 + 200 White Gold Dust (Flawless Gold Dust) (33 Value)
 + 100 White Curio (Thaumaturgic Seal) (17 Value)
 
 + 100 White Gold Dust (Crude Gold Dust) (17 Value)
 + 200 White Curio (Bitterstone Arrowhead) (33 Value)
 
 + 100 White Gold Dust (Crude Gold Dust) (17 Value)
 + 200 White Curio (Forgotten Soldier's Insignia) (33 Value)
]],
				},
			},
		}
	},
}
