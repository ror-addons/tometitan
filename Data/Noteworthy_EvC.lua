TTitan.Data.NoteworthyEvC = {}

function TTitan.Data.NoteworthyEvC.GetData()
	return TTitan.Data.NoteworthyEvC.RawData
end

function TTitan.Data.NoteworthyEvC.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.NoteworthyEvC.RawData)
end

TTitan.Data.NoteworthyEvC.RawData =
{
	header = L"Известные личности - Империя против Хаоса", -- Noteworthy Persons Unlocks - Empire vs Chaos
	categories =
	{
		{
			name = L"Nordland",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Eldred Krebs",
					text =
[[
Rally Master, Chapter 1 'Grimmerhagen'. (26,54.3k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Werner Fassbinder",
					text =
[[
Rally Master, Chapter 2 'Grey Lady Coaching Inn'. (21,27.1k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"General Breuer",
					text =
[[
Rally Master, Chapter 3 'Breur's Regiment'. (33, 18.3k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Arnholdt",
					text =
[[
'Arnholdt's Company' Warcamp. (28.7,19.2k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Caemris Sharpspear",
					text =
[[
Kill Collector, Chapter 1 'Grimmerhagen Windmill'. (23.5,40.3k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Edwin Hulsemann",
					text =
[[
Kill Collector, Chapter 2 'Grey Lady Coaching Inn'. (21,27.1k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Lieutenant Boen Scharmdorf",
					text =
[[
Kill Collector, Chapter 3 'Breur's Regiment'. (31.7,20.4k)

Stands by the road.
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Volshehk",
					text =
[[
Rally Master, Chapter 3 'Death's Brink'. (41.5,1.4k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Authun Skandsen",
					text =
[[
Rally Master, Chapter 4 'Authun's Host'. (57.4,20.1k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Orlyg Raud",
					text =
[[
Kill Collector, Chapter 3 'Death's Brink'. (41.5,1.4k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Golen",
					text =
[[
Kill Collector, Chapter 4 'Authun's Host'. (57.4,20.1k)
]],
				},
			},
		},

		{
			name = L"Norsca",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Kleinke",
					text =
[[
Rally Master, Chapter 4 'Gotland Advance'. (54.7,57.4k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Thomas Schmidt ",
					text =
[[
Kill Collector, Chapter 4 'Gotland Advance'. (54.7,57.4k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Eidolon",
					text =
[[
Rally Master, Chapter 1 'Unshackled Host. (35,12.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Haldar",
					text =
[[
Rally Master, Chapter 2 'Sorcerer's Axiom'. (36.6,32.4k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Creve Avdvare",
					text =
[[
'Blessed Gathering' Warcamp. (40.5,4.1k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Vaulkor",
					text =
[[
Kill Collector, Chapter 1 'Unshackled Host'. (35,12.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Vasili Tomarev",
					text =
[[
Kill Collector, Chapter 2 'Sorcerer's Axiom'. (36.6,32.4k)
]],
				},
			},
		},

		{
			name = L"Troll Country",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Siegmund Kraemer",
					text =
[[
Rally Master-Empire Chapter 5-Suskarg. (6.2,23.5k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Oswin Breitenbach",
					text =
[[
Rally Master-Empire Chapter 6-Felde Castle. (17.3,53.7k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Presbyter Gillian",
					text =
[[
Bramble Hollow Warcamp. (40.9,60.6k). NOTE: Unlock received by clicking Presbyter Fromme
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Ruthgar",
					text =
[[
Kill Collector-Empire Chapter 5-Suskarg. (6.2,23.5k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Dieter Stroh",
					text =
[[
Kill Collector-Empire Chapter 6-Felde Castle. (17.3,53.7k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Gunvor",
					text =
[[
Rally Master, Chapter 6 'Felde'. (12.2, 63k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Halza",
					text =
[[
Rally Master, Chapter 8 'Trovolek'. (38, 42.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Yggdren",
					text =
[[
Rally Master, Chapter 9 'Trollhaugen'. (55.4, 17.7k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Gottfried Holz",
					text =
[[
Kill Collector, Chapter 6 'Felde'. (12.2, 63k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Nina",
					text =
[[
Kill Collector, Chapter 8 'Trovolek'. (38, 42.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Lilja Shadowdrinker",
					text =
[[
Kill Collector, Chapter 9 'Trollhaugen'. (55.4, 17.7k)
]],
				},
			},
		},

		{
			name = L"Ostland",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Sergeant Kraussner",
					text =
[[
Rally Master - Empire Chapter 7 - Kraussner's Charge. (53,1.2k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Demona Gaertner",
					text =
[[
Rally Master - Empire Chapter 8 - Bohsenfels. (31.1,26.7k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Liebert Naubhof",
					text =
[[
Rally Master - Empire Chapter 9 - Wolfenburg. (49.1,54.3k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Farmer Nadezhda",
					text =
[[
Kill Collector - Empire Chapter 7 - Kraussner's Charge. (53,1.2k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Droki Greybeard",
					text =
[[
Kill Collector - Empire Chapter 8 - Bohsenfels. (31.1,26.7k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Dieter Totenhosen",
					text =
[[
Kill Collector - Empire Chapter 9 - Wolfenburg. (48.5,50.7k)South of quest hub.
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Turaanos",
					text =
[[
Rally Master, Chapter 5 'Ferlangen'. (7.8, 27.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Kournar",
					text =
[[
Rally Master, Chapter 7 'Kournos' Encampment'. (52.9, 11.7k)

NOTE: KNOWN BUG, WONT REGISTER IN TOME OF KNOWLEDGE
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Zaccur the Despised",
					text =
[[
'Ravens Edge' Warcamp. (28.1, 5.1k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Rangrith",
					text =
[[
Kill Collector, Chapter 5 'Ferlangen'. (7.4, 26.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Tosha Schreiber",
					text =
[[
Kill Collector, Chapter 7 'Kournos' Encampment'. (52.9, 11.7k)
]],
				},
			},
		},

		{
			name = L"High Pass",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Nuhr",
					text =
[[
Rally Master-Empire Chapter 10-Nuhr's Crest. (4.4,28.5k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Grizela Hirtzel",
					text =
[[
Rally Master-Empire Chapter 11-Raven's End. (9.3,58.2k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Konrad Riese",
					text =
[[
Rally Master-Empire Chapter 12-Bitter Woods. (43.5,56.7k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Teodor Hennschel",
					text =
[[
Dogbite Ridge Warcamp. (35.7,56.9k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Rolf Grimwold",
					text =
[[
Kill Collector-Empire Chapter 10-Nuhr's Crest. (4.4,28.5k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Vilmer DonHeisen",
					text =
[[
Kill Collector-Empire Chapter 11-Raven's End. (9.3,58.2k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Archibald Fleck",
					text =
[[
Kill Collector-Empire Chapter 12-Bitter Woods. (43.5,56.7k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Urun",
					text =
[[
Rally Master, Chapter 13 'Bloodmarr'. (36,33k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Albodi the Scarred",
					text =
[[
Rally Master, Chapter 14 'Jaggedspine Ridge'. (49,20.7k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Kaltea Wyrmreaver",
					text =
[[
Kill Collector, Chapter 13 'Bloodmarr'. (36,33k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Norrmar Boneblade",
					text =
[[
Kill Collector, Chapter 14 'Jaggedspine Ridge'. (49,20.7)
]],
				},
			},
		},

		{
			name = L"Talabecland",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Melicent",
					text =
[[
Rally Master-Empire Chapter 13-Hergig Landing. (61.6,9.7k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Emil Trachsel",
					text =
[[
Rally Master-Empire Chapter 14-Unterbaum Cemetary. (44.5,43.4k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Borak Brightaxe",
					text =
[[
Kill Collector-Empire Chapter 13-Hergig Landing. (61.6, 9.7k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Mary Delarue",
					text =
[[
Kill Collector-Empire Chapter 14-Unterbaum Cemetary. (44.5,43.4k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Vul'toren",
					text =
[[
Rally Master, Chapter 10 'Goblin's Head Coaching Inn'. (2.4,29k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Arinbjorn",
					text =
[[
Rally Master, Chapter 11 'Witches' Hollow'. (14.6,2.8k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Ulvarin",
					text =
[[
Rally Master, Chapter 12 'Volgen'. (42,52k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Telva Silverwheel",
					text =
[[
'Hellfang Ridge' Warcamp. (20.9,6.1k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Harkon Steinkell",
					text =
[[
Kill Collector, Chapter 10 'Goblin's Head Coaching Inn'. (2.4,29k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Fnord Bulweis",
					text =
[[
Kill Collector, Chapter 11 'Witches' Hollow'. (14.6,2.8k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Reikl Bloodpike",
					text =
[[
Kill Collector, Chapter 12 'Volgen'. (42,52k)
]],
				},
			},
		},

		{
			name = L"Praag",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Lieberholz",
					text =
[[
Rally Master-Empire Chapter 17-Lieberholz's Command. (12.5,57k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Becker",
					text =
[[
Rally Master-Empire Chapter 18-Death's Cross. (15,49.5k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Krasa Falkenheim",
					text =
[[
Rally Master-Empire Chapter 19-Knight's Watch. (28.8,24.2k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"General Ludwig Kunst",
					text =
[[
Westmark Barricade Warcamp. (16,35.4k) Bugged, wont register.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Hartlieb Roth",
					text =
[[
Kill Collector-Empire Chapter 17-Lieberholz's Command. (12.5,57k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Danil Balk",
					text =
[[
Kill Collector-Empire Chapter 18-Death's Cross. (15,49.5k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Guildmaster Geoff",
					text =
[[
Kill Collector-Empire Chapter 19-Knight's Watch. (28.8,24.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Korzah the Exalted",
					text =
[[
Rally Master, Chapter 17 'Korzah's Assault'. (6,10.8k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Exalted Magus Thurik",
					text =
[[
Rally Master, Chapter 18 'Daemonfire'. (50,6.8k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Lord Xyshrenth",
					text =
[[
Rally Master, Chapter 19 'Southern Breach'. (46,48.5k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Magedon the Omnicient",
					text =
[[
'Ravensworn' Warcamp. (44.5,38k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Aesa Heidrdottir",
					text =
[[
Kill Collector, Chapter 17 'Korzah's Assault'. (6,10.8k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Zalir Shadowtalon",
					text =
[[
Kill Collector, Chapter 18 'Daemonfire'. (50,6.8k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Lahkis Curseblade",
					text =
[[
Kill Collector, Chapter 19 'Southern Breach'. (46,48.5k)
]],
				},
			},
		},

		{
			name = L"Chaos Wastes",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Valdred Kaltenbach",
					text =
[[
Rally Master-Empire Chapter 20-Kaltenbach Expedition. (19.4,62.7k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Sigric Drakenhof",
					text =
[[
Rally Master-Empire Chapter 21-Camp of the Faithful. (54,46k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"General Vogel",
					text =
[[
Rally Master-Empire Chapter 22-Fires of Sigmar. (61,3.9k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Kalder Tannenbach",
					text =
[[
Tannenbach's Doom Warcamp. (47.9,36.8k). Bugged, wont register.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Constable Luidheim",
					text =
[[
Kill Collector-Empire Chapter 20-Kaltenbach Expedition. (19.4,62.7k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Quartermaster Randol",
					text =
[[
Kill Collector-Empire Chapter 21-Camp of the Faithful.( 54,46k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Harald Nacht",
					text =
[[
Kill Collector-Empire Chapter 22-Fires of Sigmar. (61,3.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Larus",
					text =
[[
Rally Master, Chapter 15 'Deathchill'. (3.5, 6.3k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Ydreda",
					text =
[[
Rally Master, Chapter 16 'Awakened Tempest'. (12, 46.8k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Loefret the Hatebringer",
					text =
[[
'Seven Shades Creep' Warcamp. (24, 29.70k)

NOTE: KNOWN BUG, WONT REGISTER IN TOME OF KNOWLEDGE
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Xahiriek",
					text =
[[
Kill Collector, Chapter 15 'Deathchill'. (3.5, 6.3k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Gorfing Headsmasha",
					text =
[[
Kill Collector, Chapter 16 'Awakened Tempest'. (12, 46.8k)
]],
				},
			},
		},

		{
			name = L"Reikland",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Elmeric Oberholzer",
					text =
[[
Rally Master-Empire Chapter 15-Kreuznach. (7.7,33.4k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Greta Machholt",
					text =
[[
Rally Master-Empire Chapter 16-Reik River Observatory. (18.6,15k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"General Gellert",
					text =
[[
Deathwatch Landing Warcamp. (25.1,12.9k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Barnabus Kuhn",
					text =
[[
Kill Collector-Empire Chapter 15-Kreuznach. (7.7,33.4k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Sigrid Widmann",
					text =
[[
Kill Collector-Empire Chapter 16-Reik River Observatory. (18.6,15k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Astryth the Reaver",
					text =
[[
Rally Master, Chapter 20 'Mark of the Reaver'. (49,8.5k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Chieftain Vanik",
					text =
[[
Rally Master, Chapter 21 'Vanik's Horde'. (47,35k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Khyathor",
					text =
[[
Rally Master, Chapter 22 'The Inevitable Fire'. (35,53.5k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Lord Xyshrenth",
					text =
[[
Rally Master, Chapter 19 'Southern Breach'. (46,48.5k)

NOTE: KNOWN BUG, WILL NOT REGISTER FOR REIKLAND. HE IS IN PRAAG.
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Jarl Spearfist",
					text =
[[
Kill Collector, Chapter 20 'Mark of the Reaver'. (49,8.5k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Amundr the Despoiler",
					text =
[[
Kill Collector, Chapter 21 'Vanik's Horde'. (47,35k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Gorfaug Manhewer",
					text =
[[
Kill Collector, Chapter 22 'The Inevitable Fire'. (35,53.5k)
]],
				},
			},
		},

		{
			name = L"Altdorf",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"High King Thorgrim Grudgebearer",
					text =
[[
Just west of the throne room in the Emperor's Palace, in a discussion with Phoenix King Finubar (19000,3000)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Emperor Karl Franz",
					text =
[[
In the throne room of the Emperor's Palace (19300,3000)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Supreme Patriarch Thyrus Gormann",
					text =
[[
Inside the Bright College, floating in the huge pillar of fire.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Gotrek & Felix",
					text =
[[
In the Screaming Cat inn, at the southern end of the docks (31000,26000).
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Phoenix King Finubar",
					text =
[[
Just west of the throne room in the Emperor's Palace, in a discussion with High King Thorgrim (19000,3500)
]],
				},
			},
		},

		{
			name = L"The Inevitable City",
			entries =
			{
				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Grumlok",
					text =
[[
'The Eternal Citadel'. (17,1.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Gazbag",
					text =
[[
'The Eternal Citadel'. (17,1.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Tchar'zanek",
					text =
[[
'The Eternal Citadel'. (20,1.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Engra Deathsword",
					text =
[[
'The Arena'. (30.9,18.7k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Lycithas the Harridan Seer",
					text =
[[
Use the teleport inside 'The Apex'. (11.7,21.5)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Malekith",
					text =
[[
'The Eternal Citadel'. (17,1.2k)
]],
				},
			},
		},
	},
}
