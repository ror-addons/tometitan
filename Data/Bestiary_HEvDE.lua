TTitan.Data.BestiaryHEvDE = {}

function TTitan.Data.BestiaryHEvDE.GetData()
	return TTitan.Data.BestiaryHEvDE.RawData
end

function TTitan.Data.BestiaryHEvDE.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.BestiaryHEvDE.RawData)
end

TTitan.Data.BestiaryHEvDE.RawData =
{
	header = L"Бестиарий - Высшие эльфы против Темных эльфов",
	categories =
	{
		{
			name = L"Классовый свиток с РвР зоны (Race/Class scrolls (RVR LAKES))",
			entries =
			{
				
				{
					name = L"• High Elf (Title)",
					text = L"Высшие эльфы\n- Каледор (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: История высших эльфов.\n- КООРДИНАТЫ: 38к, 30к.\n- ОПИСАНИЕ: найти Спрятанный свиток (лежит рядом с большими костями).\n- НАГРАДА: титул Плач Иши.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nHigh Elf - The History of the High Elves\nIsha's Lament\n\nCALEDOR @ 38,30 . Click on the Hidden Scroll located next to backbone of the dragon skeleton.\n\nBy Scilla\n20081217",
				},
				
				{
					name = L"• Archmage (Pocket)",
					text = L"Верховный маг\n- Сафери (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 16к, 10к.\n- ОПИСАНИЕ: найти Спрятанный свиток (лежит между камнями).\n- НАГРАДА: Испорченная книга (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nHigh Elf, Archmage - What do We Have Here ?\nPOCKET ITEM\n\nSAPHERY @ 15.6k, 10.4k. Click on the Hidden Scroll located behind a rock just to the W of the Well of Qhaysh, very near the (D) side road, barely inside the lake, in fact. Unlocks: Ruined Book.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy Dagaric, Sidac & LeviathanXIII\n20081217",
				},
				
				{
					name = L"• Swordmaster (Pocket) - не найден",
					text = L"Мастер меча\n- Сафери (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 40к, 6к.\n- ОПИСАНИЕ: найти Спрятанный свиток (лежит рядом с большими камнями чуть выше фонтана).\n- НАГРАДА: Сломанный меч (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nEmpire, Swordmaster - What do We Have Here ?\nPOCKET ITEM\n\nSAPHERY @ 40, 6.2. Click on the Hidden Scroll located inside the fountain. Unlocks: Broken Sword.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy greggus\n20081217",
				},
				
				{
					name = L"• Shadow Warrior (Pocket)",
					text = L"Воин теней\n- Земля Теней (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 35к, 54к.\n- ОПИСАНИЕ: найти Спрятанный свиток (лежит между деревьями).\n- НАГРАДА: Лопнувшая тетива (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nHigh Elf, Shadow Warrior - What do We Have Here ?\nPOCKET ITEM\n\nSHADOWLANDS @ 35.5, 54.5. Click on the 'Hidden Scroll' located in the middle of a some trees. Unlocks: Broken Bowstring.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy rouko\n20081217",
				},
				
				{
					name = L"• White Lion (Pocket)",
					text = L"Белый лев\n- Крейс (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 27к, 11к.\n- ОПИСАНИЕ: найти Спрятанный свиток.\n- НАГРАДА: Пойманная львиная сущность (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nHigh Elf, White Lion - What do We Have Here ?\nPOCKET ITEM\n\nCHRACE @ 27.9, 11.2. To the W of the Shard of Grief RvR objective is a Hidden Scroll on the ground. It can be found between some rocks at the W ruined doorway of the Shard of Grief. Unlocks: Captured Lion Essence.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy wahooka, Dagaric & Sidac\n20081217",
				},

				{
					name = L"• Dark Elf (Title)",
					text = L"Темные эльфы\n- ЭАТАН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: История темных эльфов.\n- КООРДИНАТЫ: 30к, 36к.\n- ОПИСАНИЕ: Возле небольшого здания рядом с бочками найти Спрятанный свиток.\n- НАГРАДА: Титул Львиный страж.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDark Elf - The History of the Dark Elves\nLion Guard\n\nEATAINE @ 30, 36. Click on the Hidden Scroll located near a building, next to 3 barrels.\n\nBy rouko\n20081216",
				},
				
				{
					name = L"• Witch Elf (Pocket)",
					text = L"Эльфийская ведьма\n- ОСКВЕРНЕННЫЙ ОСТРОВ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 26к, 62к.\n- ОПИСАНИЕ: Возле здания в куче найти спрятанный свиток.\n- НАГРАДА: Доспехи ведьмы (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDark Elves, Witch Elves - What do We Have Here ?\nPOCKET ITEM\n\nTHE BLIGHTED ISLE @ 26.5, 62. Click on the Hidden Scroll located in front of the N tower by the W BO, in the rubble. Unlocks: Witch Armor.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy rouko\n20081217",
				},
				
				{
					name = L"• Sorcerer (Pocket)",
					text = L"Чародей\n- ПРОБУЖДЕНИЕ ДРАКОНА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 21к, 32к.\n- ОПИСАНИЕ: У горы между камнем и деревом найти Спрятанный свиток.\n- НАГРАДА: Скверный свиток (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDark Elf, Sorcerer - What do We Have Here ?\nPOCKET ITEM\n\nDRAGONWAKE @ 21.4, 32.5. Click on the Hidden Scroll S of the Mournfire's Approach BO, within some rocks. Unlocks: Foul Scroll.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy Risingashes\n20081216",
				},
				
				{
					name = L"• Disciple of Khaine (Pocket)",
					text = L"Последователь Кейна\n- ЭЛЛИРИОН (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 36к, 32к.\n- ОПИСАНИЕ: На берегу между камнями найти Спрятанный свиток.\n- НАГРАДА: Темная полировочная жидкость (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDARKELF, DISCIPLE OF KHAINE - What do We Have Here ?\nPOCKET ITEM\n\nELLYRION@ 36,32. Click on the 'Hidden Scroll' - Near the waterfall, between the stones.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy Fadira",
				},
				
				{
					name = L"?• Black Guard (Pocket) - не найден",
					text = L"Черный страж",
				},
				
				
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 1 •••",
			divider = true,
			entries = {},
		},


		{
			name = L"ОСКВЕРНЕННЫЙ ОСТРОВ • The Blighted Isle",
			entries =
			{
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"?• Bandit (Title) - не работает",
					text = L"- ОСКВЕРНЕННЫЙ ОСТРОВ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Городские сплетни.\n- КООРДИНАТЫ: 13к, 8к.\n- ОПИСАНИЕ: В этой области есть волшебницы окруженные нейтральными феями. Поговорите с одной из них.\n- НАГРАДА: Титул Наемный убийца.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBandit (Uthorin Sorceress) - Talk of the Town\nThe Bounty Hunter\n\nBLIGHTED ISLE @ 13, 8. In the DE prologue area there are multiple Uthorin Sorceress mobs surrounded by groups of neutral sprites. Simply talk to any of them.\n\n**As of the 12/16 patch, this unlock is broken**\n\nBy Shadeviper & Road\n20081104",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Bird, Warhawk (Title)", -- Warhawk 
					text = L"- ОСКВЕРНЕННЫЙ ОСТРОВ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Я же только что постирал этот плащь.\n- КООРДИНАТЫ: 7к, 37к или 8к, 47к.\n- ОПИСАНИЕ: Найти Скального ястреба и исследуйте место его обитания.\n- НАГРАДА: Титул Основательный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBird, Warhawk (Crag Great Wing) - I Just Washed This Cloak\nThe Grounded\n\nBLIGHTED ISLE @ 7, 37 or 8, 47. Kill the warhawk by Volruc Kinslayer to collect Warhawk Feathers. WARNING: This entry is specifically buggy. Confirmed unlocks by killing the bird ONLY.\n\nBy khelben, Demonspawn, FireOpal, Sereca, raicarter, Yvarma, ThePedant & h0tr0d\n20081027",
				},

				{
					name = L"• Giant Lizard (Title)",
					text = L"- ОСКВЕРНЕННЫЙ ОСТРОВ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Дело в весах.\n- КООРДИНАТЫ: 37к, 20к.\n- ОПИСАНИЕ: В пещере Огненных кристалов, недалеко от лагеря высших эльфов 2-й главы, найти огненных ящериц и выбить с них Чешуйчатую кожу.\n- ПРИМЕЧАНИЕ: Шанс выпадения очень мал порой нужно убить от 50 до 300 ящериц чтобы получить кожу.\n- АЛЬТЕРНАТИВА: Задание в Барак-Варре територия зеленокожих.\n- НАГРАДА: Титул Охотник на ящериц.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant Lizard - A Matter of Scales\nThe Lizard Hunter\n\nBLIGHTED ISLE @ Fire Crystal Caverns. Loot Scaly Skin drop off of a Fire Tail (it took me over 130 kills).\nBLIGHTED ISLE. Ch2 High Elves @ the lizard cave near the forest burning PQ. Enter cave for title. EDIT: You have to keep killing giant lizards until they drop a specific item. NOTE: This site is more (O) friendly.\n\nBy Kovacs, Puddles404, FireOpal, Dormammu & JodouTR\n20081029",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Horror (Pocket)",
					text = L"- ОСКВЕРНЕННЫЙ ОСТРОВ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Ну где вы все?\n- КООРДИНАТЫ: 24к, 43к (камни) 20к, 36к (ужас).\n- ОПИСАНИЕ: В зоне паблика Золотая вершина взобраться по камням на пригорок и найти Потерянный ужас победоносец 4-го уровня, который находиться дальше на пригорке возле дерева.\n- НАГРАДА: Бесформенная слизь (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHorror (Lost Horror) - Where Did You All Go?\nPOCKET ITEM\n\nBLIGHTED ISLES climb the mt @ 24, 43. Jump on the rocks near the DE PQ Golden Tor to get onto the mt cliffs. Then run along the top and you should see the Pink Horror after about 10-15 seconds. Kill it.\n\nBy Puddles404, burfo, Doyle, Nozaro, Pinith & Mimer\n20081119",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Horror (Pocket)",
					text = L"- Осквернённый остров (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Ну где вы все?\n- КООРДИНАТЫ: 20к, 38к.\n- ОПИСАНИЕ: убить Потерянного ужаса.\n- НАГРАДА: Бесформенная слизь (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nHorror (Lost Horror) - Where Did You All Go?\nPOCKET ITEM\n\nBLIGHTED ISLES @ 20.5, 38. In the hills W of Lacorith, there is a lone L4 champ Pink Horror. Kill him.\n\nBy Puddles404, burfo, Doyle, Nozaro, Pinith & Mimer\n20081119",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Scorpion, Giant (Tactic)",
					text = L"- Осквернённый остров (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Откусишь с одной стороны - вырастешь.\n- КООРДИНАТЫ: 49к, 36к.\n- ОПИСАНИЕ: убить 60 заражённых скорпионов и получить награду у регистратора убийств во второй главе Высших эльфов.\n- НАГРАДА: фрагмент тактики зверей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nScorpion, Giant - A Pinch to Grow an Inch\nBESTIAL TOME TACTIC\n\nBLIGHTED ISLE @ 49, 36. Kill Blighted Scorpions for the Kill Collector Caethsetir Silverstrand @ 44, 26. 60 kills are required.\n\nBy Spasmbot, Akiu & WarWoman\n20081124",
				},

				{
					name = L"• Scorpion, Giant (Title)",
					text = L"- ОСКВЕРНЕННЫЙ ОСТРОВ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пляжный бродяга.\n- КООРДИНАТЫ: 3к, 63к.\n- ОПИСАНИЕ: На островке недалеко от пляжа найти скорпиона Гибельная Клешня (побед. 7 ур.) и убейте его.\n- НАГРАДА: Титул Крушитель скорпионов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nScorpion, Giant (Bane Claw) - Beach Bum\nThe Scorpion Squisher\n\nBLIGHTED ISLE @ 3, 63. Far SW before you leave the zone for Chrace. There's a champ on the sandbox shores. Kill him.\n\nBy NicWester, reggie, thezenny & Jarathorn\n20081021",
				},

				{
					name = L"• Snotling (Title)",
					text = L"- ОСКВЕРНЕННЫЙ ОСТРОВ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Я же только что почистил ботинки.\n- КООРДИНАТЫ: 48к, 48к.\n- ОПИСАНИЕ: На большом камне рядом с РвР зоной найти Подвижной камень и скинуть его на голову гоблину.\n- НАГРАДА: Титул Причудливый.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSnotling - And I Just Cleaned My Boots\nThe Peculiar\nBLIGHTED ISLE @ 48.5, 48. A snotling sleeps by a rock. Target the boulder above him and push it onto him for the unlock. The rock is buggy, I literally had to jump on top of it before I was able to activate it. NOTE: Snotling area is NOT in the RvR area.\n\nBy Amagorr, OdaNova & Meloc\n20081020",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Spite (Title)",
					text = L"- Осквернённый остров (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: В поисках блестяшек.\n- КООРДИНАТЫ: 54к, 52к.\n- ОПИСАНИЕ: убить тёмных вредней и получить сияющее кольцо.\n- НАГРАДА: титул Вредитель вредней.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nSpite - Looking for Shinnies\nThe Spite Splitter\n\nBLIGHTED ISLE. Near the ruins of Erraneth, there is a waystone with 'anomalies' around it, and Dark Spites. This has been the best place I've found to kill them for the item: Shining Ring. Don't know what you are meant to do with the ring, haven't found anyone interested in it yet.\n\nBy Puddle404\n20081021",
				},

				{-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"?• Warhawk (Tactic) - ненайден",
					text = L"- ОСКВЕРНЕННЫЙ ОСТРОВ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Прикладная орнитология.\n- КООРДИНАТЫ: ...\n- ОПИСАНИЕ: Найти Ястребов и выбить с них 5 Перьев ястреба-весника.\n- НАГРАДА: фрагмент Тактика Зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWarhawk - Bird Watching...Up Close, in Person\nBeastial Tactic Fragment",
				},
				
				{
					name = L"• Wolf (Title)",
					text = L"- ОСКВЕРНЕННЫЙ ОСТРОВ (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: С громким воем.\n- КООРДИНАТЫ: 5к, 49к.\n- ОПИСАНИЕ: Там где обитают волки пройтиде по краю ущелья, чтобы услышать вой волков снизу.\n- НАГРАДА: Титул Бирюк.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWolf - View With a Howl\nThe Howler\n\nBLIGHTED ISLE @ 5, 49. NW of Dreamshade. While hunting wolf matrons, approach the edge of the cliff to hear wolves howling down below. This unlocked after killing him. NOTE: Between the PQ Dreamshade & The Watchtower, walk along the cliffside until you get a message Looking out over the cliffs to the dark waters below, you hear the echo of the wolfs calL and you will get the unlock.\n\nBy Ater, NicWester, thezenny & angral\n20081028",
				},
			},
		},


		{
			name = L"КРЕЙС • Chrace",
			entries =
			{
				
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Scorpion, Giant (Tactic)",
					text = L"- КРЕЙС (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Откусиш с одной стороны - вырастешь (Большое жало).\n- КООРДИНАТЫ: 9к, 2к.\n- ОПИСАНИЕ: Убить больше 60 Гиганских скорпионов для регистратора убийств Крартил Смертесказ.\n- ПРИМЕЧАНИЕ: Скорпионы водятся на границе Оскверненного острова и Крейс.\n- НАГРАДА: Фрагмент тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nScorpion, Giant - A Pinch to Grow an Inch\nBESTIAL TOME TACTIC\n\nBLIGHTED ISLE. Unlocked this in the quest camp near the first PQ near the starting DE area. It was from one of the quest givers in that camp. EDIT: The quest is called Lucrative Intrigue, turn in NPC is Tiritha Venomfire. 2nd EDIT: This quest requires you complete both quests given by Nehmora the Hag before you can get the Lucrative Intrigue quest. Kill the scorpions just outside of Sternbrow's Lament.\n\nBy Rhianni, Arienh, Squarkk & Sereca\n20081124",
				},

				{
					name = L"• Dryad (Title)",
					text = L"- КРЕЙС (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Рощи и могилы.\n- КООРДИНАТЫ: 8к, 34к или 6к, 47к или 7к, 50к.\n- ОПИСАНИЕ: Найти и убить Дриаду гневного леса.\n- НАГРАДА: Титул Лесник.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDryad - Grove and Grave\nForester\n\nCHRACE @ 8, 34k or 6, 47k or 7.3k, 49.8k .. Run to the SW as if going to Shadowlands on foot. Once you're there the sky should change sort of and you should be surrounded by Spites and Bats and other weaksauce punks. Just keep running N until you get the unlock.\n\nBy NicWester, Nogglli, Rakaija & Meep\n20081007",
				},

				{
					name = L"• Harpy (Tactic)",
					text = L"- КРЕЙС (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Спускайся сюда.\n- КООРДИНАТЫ: 45к, 23к.\n- ОПИСАНИЕ: Возле башни найти летающих гарпий и убить несколько штук.\n- НАГРАДА: Фрагмент Тактики Хаоса (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHarpy - Get Down Here!\nCHAOS TOME TACTIC\n\nCHRACE @ 44.7, 22.5. Between Yenlui and Blackwood Hill Garrison there is a ruined tower with swarming Harpies. Keep killing the Harpies to get the unlock. NOTE: From the name of the unlock, one may have to kill the airborne harpies, not the ones on the ground.\n\nBy Puddles404, notalltogether, Shadeviper & TBBle\n20081020",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Human (Pocket)",
					text = L"- КРЕЙС (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Торг за сокровище.\n- КООРДИНАТЫ: 48к, 8к.\n- ОПИСАНИЕ: Возле моста в РвР зону есть спуск к воде. Там найти местного бандита Фей Валь и убить его.\n- НАГРАДА: Почти счастливый амулет (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHuman, Bandit (Fay Val) - Treaty for Treasure\nPOCKET ITEM\n\nCHRACE @ 48, 8. The 3 NPCs are up a ramp from the water right under the bridge connecting to the RvR area, W of Yenlui. The other mobs are L10 Amalyn Weber with her dog L20 Canine. Unlocks: Semi-lucky Charm. NOTE: Confirmed unlock by killing Weber.\n\nBy gaberiel07, Thanat0s, , Kjermzs, Puddles404, ThePedant, Feclump, ultraczar, Sildroni & ThePedant\n20081027",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Spider, Giant (Tactic)",
					text = L"- Крейс (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Лёгкая нажива.\n- КООРДИНАТЫ: 49к, 21к.\n- ОПИСАНИЕ: убить 60 пауков и получить награду у регистратора убийств в 4 главе Высших Эльфов.\n- НАГРАДА: фрагмент тактики зверей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nSpider, Giant (Warpweb Spiders) - Fast Finds\nBESTIAL TOME TACTIC\n\nCHRACE @ 49, 21.7k Max out the Kill Collector Elthossar YoungStarat found by the Cliffs of Ushuru @ 53, 32. 60 kills is generally enough for KC's.\n\nBy FireOpal, Meep & TBBle\n20081021",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 2 •••",
			divider = true,
			entries = {},
		},


		{
			name = L"ЗЕМЛЯ ТЕНЕЙ • The Shadowlands",
			entries =
			{
				{
					name = L"• Bear (Title)",
					text = L"- ЗЕМЛЯ ТЕНЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Они хотябы симпатичные.\n- КООРДИНАТЫ: 57к, 27к.\n- ОПИСАНИЕ: Найти медведей Темного когтя и выбить с них 5 Медвежьих шкур.\n- НАГРАДА: Видевший медведя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBear (Darkclaw Bear/Mauler) - At Least They'll Stay Cool\nPioneer\n\nSHADOWLANDS 55, 25 or NE of Elbisar. Kill for 5 Bear Pelts, then inspect them.\n\nBy Arcton, Dormammu, Blodsalv, CastleBravo & Kadregon\n20081029",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Bird, Warhawk (Title)",
					text = L"- Земля Теней (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Я же только что постирал этот плащ!\n- КООРДИНАТЫ: 24к, 22к.\n- ОПИСАНИЕ: убить ястребов и собрать 5 перьев для получения анлока.\n- НАГРАДА: титул Основательный.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\nBird, Warhawk (Warhawk Mauler) - I Just Washed This Cloak\nThe Grounded\n\nSHADOWLANDS @ 20, 23 or 19, 26 or 11.5, 22.5 or 24, 22. Kill the mob for Warhawk Feathers. Get five for the unlock. Drops are not 100%.\nWARNING: This entry is specifically buggy. There is a mob (D) players can kill in Blighted Isle, but that entry is buggy also. You may need to try several different things. Try clicking on the feathers after getting 5. Try visiting and kill the warhawk in the Blighted Isle. Also try deleting your feathers and recollecting them.\nNOTE: Confirmed that killing the Bird with no feathers also gave the unlock.\nFURTHER: Confirmed unlock by killing Skyherald Warhawk @ 24,22 (both once and multiple times).\n\nBy Wolfed, notalltogether, Ulgwar, Yvarma, Torgan & Ryfar\n20081103",
				},

				{
					name = L"• Cockatrice (Exp)",
					text = L"- ЗЕМЛЯ ТЕНЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Если бы взгляды убивали.\n- КООРДИНАТЫ: 58к, 30к.\n- ОПИСАНИЕ: На верху горы найти окаменевший труп.\n- НАГРАДА: Опыт 334.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nCockatrice (Petrified Corpse) - If Looks Have Killed\nSHADOWLANDS @ 58, 29. NE of Elbisar examine the corpse.\n\nBy khelben\n20081027",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Cold One (Title)",
					text = L"- ЗЕМЛЯ ТЕНЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Равновесие.\n- КООРДИНАТЫ: 37к, 9к.\n- ОПИСАНИЕ: В лагере 5-й главы темных эльфов Руины Анлека найти целителя Тейлк Венец Гнева и поговорить с ним.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nCold One - A Balancing of Scales\nThe Scale Breaker\n\nSHADOWLANDS. Talk to Taelq Ragecrown.\nThis healer is in the DE camp in Ruins of Anlec (the first camp when entering from Chrace).\n\nBy Shiwoen & Nozaro\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Cold One (Title)",
					text = L"- Земля Теней (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Равновесие.\n- КООРДИНАТЫ: 46к, 23к.\n- ОПИСАНИЕ: убить 60 хладозверей и получить награду у регистратора убийств в 6 главе Высших эльфов.\n- НАГРАДА: титул Пронзатель чешуи.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nCold One - A Balancing of Scales\nThe Scale Breaker\n\nSHADOWLANDS. Max out the Cold One Kill Collector for the unlock.\n\nBy Shiwoen & Nozaro\n20081124",
				},

				{
					name = L"• Harpy (Title)",
					text = L"- ЗЕМЛЯ ТЕНЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Дом на холме.\n- КООРДИНАТЫ: 53к, 45к.\n- ОПИСАНИЕ: Найти Гнездо гарпий на вершине хома недалеко от дороги.\n- НАГРАДА: Титул Храбрый.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHarpy - House on the Hill\nThe Bold\n\nSHADOWLANDS @ 53, 45. Climb up and use the harpy roost on top of a slab of rock on the side of the road. Best access is running to the E side of the slab and jumping up, I couldn't get onto the slab from the road.\n\nBy khelben, Blodsalv, Nymph of Tor & Puddles404\n20081020",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Imp (Title)",
					text = L"- ЗЕМЛЯ ТЕНЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что тебя зажигает?\n- КООРДИНАТЫ: 36к, 9к.\n- ОПИСАНИЕ: Поговорить с торговцем Брадас Касание Хлада.\n- НАГРАДА: Титул Борец с бесами.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nImp (Bradas Coldtouch) - What Makes You Sparkle?\nThe Imp Squisher\n\nSHADOWLANDS @ 35.5, 10. Talk to this merchant.\n\nBy NicWester, notalltogether, shadracht, Elli0t & Lessing\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Imp (Title)",
					text = L"- Земля Теней (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что тебя зажигает?\n- КООРДИНАТЫ: 33к, 11к.\n- ОПИСАНИЕ: убить бесов и получить останки. Использовать их из инвентаря для получения анлока.\n- НАГРАДА: титул Борец с бесами.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nImp (Bradas Coldtouch) - What Makes You Sparkle?\nThe Imp Squisher\n\nSHADOWLANDS. Directly SW of Ruins of Anlec (DE Ch5) there are a few sprites with a random Dark Elf sorceress. Kill these to loot Sparkling Sprite Remains and right click them to get the unlock. NOTE: Drops chances are about .31% and mobs respawn slowly--so get ready to bust out that Thank-You card for Mythic.\n\nBy NicWester, notalltogether, shadracht, Elli0t & Lessing\n20081124",
				},

				{
					name = L"• Scorpion (Tactic)",
					text = L"- ЗЕМЛЯ ТЕНЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Мертвые и опасные.\n- КООРДИНАТЫ: 5к, 45к или 3к, 41к.\n- ОПИСАНИЕ: На берегу найти и убить 5 скорпионов Ядохвостый ужальник (побед. 15 ур.).\n- НАГРАДА: Фрагмент Тактика зверей (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nScorpion, Giant (Poisontail Stinger) - Deadly When Dead\nBESTIAL TOME TACTIC\n\nSHADOWLANDS @ 5.6, 45 or 2, 41.5. Kill the scorpions in the area along the coast for the loot. The drop may take a while, but it has been confirmed to drop off both normal and champ mobs.\n\nBy Mabuz, NicWester, Icebird, Emrus, Crucifix & Sildroni\n20081027",
				},

				{	-- Destruction version
					--realm = TTitan.Data.Realm.DESTRUCTION, также доступен и Ордеру
					name = L"• Tree Kin (Title)",
					text = L"- ЗЕМЛЯ ТЕНЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Спираль истории (Древесные кольца).\n- КООРДИНАТЫ: 12к, 58к.\n- ОПИСАНИЕ: Найти и обследовать територию возле Останков корневика.\n- НАГРАДА: Титул Лесоруб.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTree Kin - Rings of History\nThe Lumberjack\n\nSHADOWLANDS @ 9, 60 or 12, 58.5. On the cliff overlooking the final DE Ch7 PQ The Griffon Gate select Rootwalker Remains for the unlock.\n\nBy Malfious\n20081027",
				},
				
				{-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"?• Warhawk (Tactic) - ненайден",
					text = L"- ЗЕМЛЯ ТЕНЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Прикладная орнитология.\n- КООРДИНАТЫ: 20к, 26к или 26к, 20к.\n- ОПИСАНИЕ: Найти Ястребов и выбить с них 5 Перьев ястреба-весника.\n- НАГРАДА: фрагмент Тактика Зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWarhawk - Bird Watching...Up Close, in Person\nBeastial Tactic Fragment",
				},
				
				{
					name = L"• Unicorn (Title)",
					text = L"- ЗЕМЛЯ ТЕНЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Не в своем уме.\n- КООРДИНАТЫ: 16к, 44к.\n- ОПИСАНИЕ: Найти единорога Сайлендорас (побед. 15 ур.) и убив его осмотреть полученный Кусок рога единорога.\n- НАГРАДА: Титул Роголом.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nUnicorn (Sylendoras) - A Little Off the Top\nThe Hornsnapper\n\nSHADOWLANDS @ 16, 44. Kill this L15 champ who hides among the other unicorns. Inspect a piece of a Unicorn's Horn.\n\nBy Blodsalv, Crucifix, Thanat0s, Ulgwar & meth8609\n20081103",
				},
			},
		},


		{
			name = L"ЭЛЛИРИОН • Ellyrion",
			entries =
			{
				{
					name = L"• Great Cat (Tactic)",
					text = L"- ЭЛЛИРИОН (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Истребление теней.\n- КООРДИНАТЫ: 61к, 58к.\n- ОПИСАНИЕ: Между деревьями у дороги найти пантеру Тенепопль (побед. 19 ур.) и убить ее.\n- НАГРАДА: Фрагмент тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGreat Cat (Shadowscream) - Silencing Shadows\nBESTIAL TOME TACTIC UNLOCK\n\nELLYRION @ 61k, 57.5k. Kill the named champ panther for the unlock. NOTE: The original coords posted (60k, 40k) lead to a hero mob named Bloodpaw. Bloodpaw drops an item that gives a quest. However, even tho' the quest is acceptable by Destro', the person you need to talk to is Order.\n\nBy expansionsss, notalltogether, Elmarra & Bastion\n20081007",
				},

				{
					name = L"• F. of Slaanesh (Title)",
					text = L"- ЭЛЛИРИОН (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: О чем лучше не знать.\n- КООРДИНАТЫ: 35к, 48к.\n- ОПИСАНИЕ: Недалеко от дороги которая ведет через ворота на холме лежит Книга невинности которую нужно прочесть.\n- НАГРАДА: Титул Порочный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nFiend of Slaanesh (Book of Innocence) - Things Best Left Unknown\nThe Impure\n\nELLYRION @ 35k ,48k. On the HE side of the mt near the double gates in the middle of the map is a book called The Book of Innocence. A non aggro mob called Kelthera Darklust sits beside it. Click on the book and the mob turns into the lvl 20 Fiend of Slaanesh and attacks you. NOTE: Receiving the unlocks without killing the mob has been confirmed.\n\nBy Nozaro, Ulgwar, CastleBravo & Colloquial\n20081007",
				},

				{
					name = L"• Great Eagle (Title)",
					text = L"- ЭЛЛИРИОН (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пернатые.\n- КООРДИНАТЫ: 27к, 44к.\n- ОПИСАНИЕ: На горе над гнездами найти Эллирийских орлиц (19 ур.) и добыть Перо великого орла после чего активировать перо.\n- НАГРАДА: Титул Бдительный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGreat Eagle (Ellyrian Eagle) - Birds of a Feather\nThe Vigilant\n\nELLYRION @ 27, 44. Kill the Ellyrian Eagles until the Great Eagle Feather drops and examine it. The eagles are found in the middle Ellryion to the N of the two gates with Dark Elves inside. They're in the mt ranges in the vicinity of three large nests.\nNOTE: (For Order players) unlock seems bugged. Confirmed unlock, but the entry does not log into the ToK.\n\nBy Shaia, scootle, Tayshrenn & ThePedant\n20081027",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 3 •••",
			divider = true,
			entries = {},
		},

		{
			name = L"АВЕЛОРН • Avelorn",
			entries =
			{
				{
					name = L"• Beastman (Cloak)",
					text = L"- АВЕЛОРН (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Важная зверушка.\n- КООРДИНАТЫ: 45к, 36к.\n- ОПИСАНИЕ: Рядом с лагерем высших эльфов 12-й главы найти зверолюда Хурраак Могучерог (25 ур.). Он прибегает когда подходиш близко к лагерю с южной стороны дороги.\n- НАГРАДА: Шкура темного молодняка (Плащь).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Bestigor (Khurraak Might-Horns) - Bossy Beastie\nCLOAK ITEM UNLOCK\n\nAVELORN. Spawns on the road, on the S side of the (O) camp. He is with another non-named bestigor and attacks @ the Ch12 town. Kill him and you will get the unlock and the ability to buy the Pelt of the Dark Young cloak.\n\nBy Abriel, Kellithe & notalltogether\n20081007",
				},
				
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Bone Giant (Title)",
					text = L"- АВЕЛОРН (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Куски кусков.\n- КООРДИНАТЫ: 58к, 62к или 47к, 45к.\n- ОПИСАНИЕ: Поговорить со скелетом Брулгурлах Мертвяк (побед. 25 ур.).\n- ПРИМЕЧАНИЕ: Его появление может быть довольно долгим и возможно придеться убить множество скелетов оханников для появления победоносца.\n- НАГРАДА: Титул Костолом.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBone Giant (Brulgulrach the Unliving) - Parts of Parts\nThe Bone Breaker\n\nAVELORN @ 58.5, 62 or 47, 45. He is friendly and talking to him completes the unlock.\n\nBy Kellithe, notalltogether, Tyrlian & halsfield\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Bone Giant (Title)",
					text = L"- Авелорн (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Куски кусков.\n- КООРДИНАТЫ: 58к, 62к или 47к, 45к.\n- ОПИСАНИЕ: убить Брулгурлаха Мертвяка (25 ур., победоносец).\n- НАГРАДА: титул Костолом.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nBone Giant (Brulgulrach the Unliving) - Parts of Parts\nThe Bone Breaker\n\nAVELORN @ 58.5, 62 or 47, 45. Kill this L25 skeleton champ. NOTE: May require a trigger to populate, have a placeholder, or be on a long timer. TIP: The ToK places the mob in the Bone Giant category, but it looks like a normal sized skeleton.\n\nBy Kellithe, notalltogether, Tyrlian & halsfield\n20081124",
				},

				{
					name = L"• Dryad (Tactic)",
					text = L"- АВЕЛОРН (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Весь в старших.\n- КООРДИНАТЫ: 31к, 39к.\n- ОПИСАНИЕ: Найти одиноко стоящего Руперта (26 ур.) и убить его.\n- НАГРАДА: Фрагмент Тактики Мифических существ (1/10).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDryad (Oldbark) - Chip Off the Old...\nMYTHICAL TOME TACTIC\n\nAVELORN @ 31, 39 or 40, 40.: Kill it.\n\nBy Supaplex, notalltogether & OGGleep\n20081029",
				},

				{
					name = L"• H. of Tzeentch (Tactic)",
					text = L"- АВЕЛОРН (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Три товарища.\n- КООРДИНАТЫ: 62к, 14к.\n- ОПИСАНИЕ: Найти на холме у водопада Голмира (побед. 23 ур.) и убить его.\n- НАГРАДА: Фрагмент Тактики демона (1/22).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHorror of Tzeentch (Golmir) - Pack of Three\nDAEMONIC TOME TACTIC UNLOCK\n\nAVELORN @ 62, 14k. He's in a slight depression in the far E wall of the mt and is a l23 champ, also two non-champ L23 spiders come along for the fun.\n\nBy Kellithe & JodouTR\n20081013",
				},

				{
					name = L"• Liche (Title)",
					text = L"- АВЕЛОРН (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Покойся с миром ... снова.\n- КООРДИНАТЫ: 33к, 49к.\n- ОПИСАНИЕ: На камне перед рекой найти и убить Злодеус потерянный (герой 1 ур.).\n- НАГРАДА: Титул Поборник Морра.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nLiche (Tarnolious the Lost) - Final Rest... Again\nChampion of Morr\n\nAVELORN @ 33, 49. Kill this L1 Hero SE of the Tower of Aethwyn PQ on a cliff at the river.\n\nBy Blodsalv & Ulgwar\n20081124",
				},

				{
					name = L"• Nurgling (Pocket)",
					text = L"- АВЕЛОРН (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Больше грязи!\n- КООРДИНАТЫ: 37к, 16к.\n- ОПИСАНИЕ: Найти и убить Нечистень (побед. 21 ур.).\n- НАГРАДА: Извергнутое украшение (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nNurgling (Filthmiser) - More Filth for Me!\nPOCKET ITEM\n\nAVELORN @ 37, 16. A L21 champ gives the unlock. Killing this mob yields the item: Regurgitated Trinket\n\nBy Thovargh, beetfield, notalltogether & Flea\n20081029",
				},

				{
					name = L"• Treemen (Title)",
					text = L"- АВЕЛОРН (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Кто хранит Мирохранителя?\n- КООРДИНАТЫ: 44к, 44к.\n- ОПИСАНИЕ: Найти древолюда Дрегенис, стоящего возле дерева, и поговорить с ним.\n- НАГРАДА: Титул Любитель долин.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTreemen (Dregenis) - Who Bears the Worldbearer?\nThe Vale Walker\n\nAVELORN @ 43.7, 44.5. Approach Dregenis. Talk to him.\n\nBy Blodsalv, Muninn & Nozaro\n20081021",
				},

				{
					name = L"• Troll (Bestial Token)",
					text = L"- АВЕЛОРН (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Исполненное правосудие.\n- КООРДИНАТЫ: 19к, 49к.\n- ОПИСАНИЕ: На холме недалеко от дороги найти троля Кровобрюх (побед. 25 ур.) и убить его.\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll, Chaos (Goremaw) - Dispense Justice\nBEAST TOKEN\n\nAVELORN @ 19, 49. Kill a L25 Chaos Troll Champ. He's just a little ways N of the DE camp in the zone. EDIT Mob has also been confirmed to pop E of the DE camp, between it and the PQ The Watchtower, amongst the trolls there.\n\nBy Draekon, Pinafore, notalltogether & TorqueVolkmar\n20081029",
				},

				{
					name = L"• Zombie (Pocket)",
					text = L"- АВЕЛОРН (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Все сначала.\n- КООРДИНАТЫ: 31к, 39к.\n- ОПИСАНИЕ: Найти дриаду Стародрев (24 ур.) и убить ее.\n- НАГРАДА: Запленевевший кошелек (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nZombie (Rupert) - Loop Back\nPOCKET ITEM\n\nAVELORN @ 40, 40. Kill this named zombie to unlock Moldy Coin Purse.\n\nBy Kellithe & Amagorr\n20081029",
				},
			},
		},

		{
			name = L"САФЕРИ • Saphery",
			entries =
			{
				
				{	
					name = L"?• Beastmen, Bray Shaman (???) - не найден",
					text = L"- САФЕРИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Подходящий набор.\n- КООРДИНАТЫ: ...\n- ОПИСАНИЕ: ...\n- НАГРАДА: Титул ???.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastmen, Bray Shaman - A Matching Set",
				},
				
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Chaos Hound (Acc) - Сет Жубы Ваааагха!",
					text = L"- САФЕРИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Изо рта на шею.\n- КООРДИНАТЫ: 32к, 15к (нпц), 47к, 12к (собаки).\n- ОПИСАНИЕ: Поговорить с Дресировщиком Мезеллиан, чтобы получить Амулет клыков. После  добыть из Мерзосных Собак 10 Клыков собаки хаоса. Вернуться к Дресировщику Мезеллиан.\n- НАГРАДА: Черный жуб (Сет Жубы Ваааагха! 1/4).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Hound - From Mouth to Neck\nJEWELRY ITEM\n\nSAPHERY. Talk to Beastmaster Mezellian @ 31.5, 15 to get the Amulet. Then kill Twisted Hounds and return to Beastmaster Mezellian.\n\nBy Sandkat\n20081023",
				},
				
				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Chaos Hound (Acc) - Сет Жубы Ваааагха!",
					text = L"- Сафери (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Изо рта на шею.\n- КООРДИНАТЫ: 48к, 10к.\n- ОПИСАНИЕ: поговорить с Голграндом Кулакамнем в 13 главе эльфов и получить Амулет клыков. Затем двигаться по координатам и убить 10 мерзостных собак. Получить 10 клыков. Отнести их обратно Голгранду Кулакамню.\n- НАГРАДА: сетовый талисман Чёрный жуб.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nChaos Hound - From Mouth to Neck\nJEWELRY ITEM\n\nSAPHERY @ 59.5, 20.5. First speak to Golgrund Stonefist @ the Ch13 camp to get Amulet of Fangs. Then kill Chaos Hounds near 49.5, 13 until you have 10 Chaos Hound Fangs. Speak to Golgrund again for: Black Toof.\n\nBy Risingashes, Ulgwar & TorqueVolkmar\n20081023",
				},



				{
					name = L"• Cockatrice (Cloak)",
					text = L"- САФЕРИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Очарованная жизнь.\n- КООРДИНАТЫ: 59к, 28к.\n- ОПИСАНИЕ: За небольшим камнем у горы найти Окаменевший труп и осмотрев его получить Оберег кокатрикса. После чего убить Остроклюва (побед. 27 ур.).\n- НАГРАДА: Перья убивающего взглдом (плащь).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nCockatrice (Petrified Corpse) (Slashbeak)- A Charmed Life\nCLOAK ITEM\n\nSAPHERY @ 59, 27.5. Head E and cross the road from the Ithilmar Tower PQ. You should see a small camp of cockatrice type mobs and toward the wall there will be a corpse that's clickable. Click it to receive an item and then kill the L27 champ to receive the unlock for: Plumes of the Eye Killer.\n\nBy Gabriel Stern, hamsterz, Buzzbomb & benji\n20081021",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Cold One (Token)",
					text = L"- САФЕРИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Большой мороз.\n- КООРДИНАТЫ: 9к, 35к.\n- ОПИСАНИЕ: В лагере 14 главы темных эльфов поговорить с Мадлайна Кровавое Вино.\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nCold One (Coldlash) - Big Freeze\nBEAST TOKEN\n\nSAPHERY @ 9, 35. Talk to Maedlaine Bloodwine.. Don't have to kill anything.\n\nBy Nozaro\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Cold One (Token)",
					text = L"- Сафери (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Большой мороз.\n- КООРДИНАТЫ: 54к, 10к.\n- ОПИСАНИЕ: убить Хладохлеста (27 ур., победоносец).\n- НАГРАДА: знак зверя.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nCold One (Coldlash) - Big Freeze\nBEAST TOKEN\n\nSAPHERY @ 54, 10. Kill this L27 named champ.\n\nO by Risingashes & dremmor\n20081124",
				},

				{
					name = L"• Manticore (Еxp)",
					text = L"- САФЕРИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Не спрашивай, где это было.\n- КООРДИНАТЫ: 54к, 20к.\n- ОПИСАНИЕ: На вершине холма возле водопада найти Мантикора без всадника (27 ур.) и убить его. Активировать полученное Полупереваренное руководство.\n- НАГРАДА: 336 опыта.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nManticore (Unmounted Manticore) - Won't Ask Where it Kept That\n\nSAPHERY @ 53.5, 20. Mob drops A Partially-digested Manual. Use it for unlock. NOTE: Killing the Manticore mobs at the lake near 53.5, 15.5 DO NOT drop the Manual.\n\nBy Dissent, cintra & Foggye\n20081103",
				},

				{
					name = L"• Pegasus (Title)",
					text = L"- САФЕРИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Знаешь дорогу?\n- КООРДИНАТЫ: 14к, 1к.\n- ОПИСАНИЕ: На маленькой березе найти и активировать Рог древнего всадника.\n- НАГРАДА: Титул ???.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPegasus - Do You Know the Way\n???\n\nSAPHERY @ 14, 1 There is an Ancient Rider's Horn hanging from a tree that unlocks this. BUG: The title it gives seems to be bugged and doesn't show up.\n\nBy Dissent, Expance & skyrunner620\n2008110",
				},

				{
					name = L"• Squig (Pocket)",
					text = L"- САФЕРИ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Перерыв.\n- КООРДИНАТЫ: 58к, 61к.\n- ОПИСАНИЕ: Внутри пещеры Платиновая Башня пройти по длинному тунели до поворота где дорога расходиться вверх и вниз. Внизу найти Мясистый гриб.\n- НАГРАДА: Ожерелье из сквиговых зубов (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSquig - Take a Break\nPOCKET ITEM UNLOCK\n\nSAPHERY @ 60.3,61.5. In the Southern Stonewind Cavern, follow the tunnel first South, then East. The tunnel splits into two before continuing South. The left fork slopes slightly upwards and the right one slopes down; take the right one. The Fleshy Mushroom is along the left wall just down the slope.\n\nBy Xururuca & Wilken\n20081007",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 4 •••",
			divider = true,
			entries = {},
		},

		{
			name = L"ПРОБУЖДЕНИЕ ДРАКОНА • Dragonwake",
			entries =
			{
				{
					name = L"• Beastmen (Title)",
					text = L"- ПРОБУЖДЕНИЕ ДРАКОНА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Хватай быка за рога.\n- КООРДИНАТЫ: 65к, 30к.\n- ОПИСАНИЕ: На границе возле маленького водопада у камней найти Кучу костей чтобы вызвать Бешенного быка (34 ур.), которого нужно убить.\n- ПРИМЕЧАНИЕ: Убить быка нужно очень быстро, до того как он убежит назад.\n- НАГРАДА: Титул Крушитель пирамид.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastmen, Doombull - Grab the Bull By the Horn\nCairn Breaker\n\nDRAGONWAKE @ 65, 30. Near the border to Eataine there's a small waterfall about 20 yards from 3 rocks, on big one and 2 small ones on the one side. Next to the small rocks are a pile of bones. Looting it to spawn a L34 Doombull. NOTE: Must kill the bull quickly. The unlock relies on a timer--take too long and he despawns.\n\nBy Tyrlian, poll12 & Draegnar\n20081027",
				},

				{
					name = L"• D. of Slaanesh (Acc)",
					text = L"- ПРОБУЖДЕНИЕ ДРАКОНА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Избыточная жертва.\n- КООРДИНАТЫ: 15к, 9к (книга) 54к, 16.5к (алтарь).\n- ОПИСАНИЕ: Найти под деревом книгу Второй дневник Ллианы. Появиться демон (37 ур.) Иезебет убив которого нужно достать Ледяное сердце. Отнести серде к Алтарю Растерзанной плоти в Пустошах Хаоса @ 54, 16.5.\n- ПРИМЕЧАНИЕ: Сердце падает примерно после 10-15 убитых демонов (15%).\n- НАГРАДА: Лунный клык (сет аксесуаров 1/4).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDaemonette of Slaanesh - Sacrifice Over Excess\nJEWELRY ITEM\n\nDRAGONWAKE @ 15, 9. Find a book to the W of the road along the mtside, near a group of trees. Click it and a daemonette will spawn. After I clicked the book 10 times and killed her 10 times she dropped an Icy Heart. Take the item to the Altar of Broken Flesh in CHAOS WASTES @ 54, 16.5. Unlocks Moon Fang.\nNOTE: Summoning horse on top of her book seems to trigger her arrival.\n\nBy Ravnir, Nukeitall, Zoe9906, Crucifix & sakkath\n20081027",
				},

				{
					name = L"• Boar (Acc) - Сет Нарушенный завет",
					text = L"- ПРОБУЖДЕНИЕ ДРАКОНА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Свиной бивштекс.\n- КООРДИНАТЫ: 60к, 18к.\n- ОПИСАНИЕ: Севернее лагеря высших эльфов 16-й главы найти нейтральных кабанов и добыть с них Постное кабанье мясо.\n- АЛЬТЕРНАТИВА: Задание в Каледоре.\n- НАГРАДА: Клятвы Глубин (Сет Нарушенный завет 1/2).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSavage Charger - Pork Chop\nSET ITEM\n\nDRAGONWAKE @ 60, 18. N of the HE Ch16 camp kill this neutral mob which gives the unlock Pork Chop via the item Lean Boar Meat. Unlocks jewelry item The Deep Oaths which is part of a 2-set called The Broken Covenant. NOTE: Drop rate is low, confirmed past 14 kills.\nNOTE: See entry for Boar in CALEDOR for alternate approach.\n\nBy wrajjt & lasmrah\n20081021",
				},
			},
		},

		{
			name = L"ОСТРОВ МЕРТВЫХ • Isle of the Dead",
			entries =
			{
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Chaos Fury (Token)",
					text = L"- ОСТРОВ МЕРТВЫХ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: С крыльями, но не ангелы.\n- КООРДИНАТЫ: от 36к, 5к до 48к, 55к.\n- ОПИСАНИЕ: Найти демона Илегзор Сумасшедший (побед. 35 ур.) и выбить с него Концентрированное зло, который отнести любому торговцу и продать.\n- ПРИМЕЧАНИЕ: Демон бродит сверху вниз и назад. Время релога составляет 5 минут, а шанс дропа 15%.\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Fury (Ylger'oz the Demented) - On Wings, but Not Quite Angels\nBEASTIAL TOKEN\n\nISLE OF THE DEAD @ 36, 5. Kill this L35 champ that spawns @ 36, 5 and can pat all the way down to 48, 55. He is on a 5 min respawn. The item you are looking for is called Concentrated Evil. Kill him until it drops and go sell it to a merchant for the unlock.\n\nBy Sandkat & JodouTR\n20081021",
				},

				{
					name = L"• Living Armor (Title)",
					text = L"- ОСТРОВ МЕРТВЫХ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: У кого есть консервный нож?\n- КООРДИНАТЫ: 14к, 27к.\n- ОПИСАНИЕ: В зоне паблика Ритуал Металла найти Живых доспехов (36 ур.) и убить примерно 20-30 доспехов до получение титула.\n- НАГРАДА: Титул Крушитель доспехов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nConstruct (Living Armor) - Anyone Have a Can Opener?\nThe Armor-Breaker\n\nISLE OF THE DEAD. Kill this L36mob @ the Ritual of Metal till you get the unlock. NOTE: Some people have done it in 10 kills, others 36. Keep trying.\n\nBy Supaplex, Sandkat, Iranium & Societal\n20081028",
				},
			},
		},

		{
			name = L"КАЛЕДОР • Caledor",
			entries =
			{
				{
					name = L"• Dark Elf (Title)",
					text = L"- КАЛЕДОР (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Завершить сценарий темных эльфов.\n- ОПИСАНИЕ: Принять участие в сценарии Леса Каледора. Сценарий доступен когда територия вляеться спорной для обоих сторон.\n- НАГРАДА: Мститель Ултунана.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDark Elf - Complete a Dark Elf Scenario\nUlthuan's Avenger\n\nCALEDOR. Win a Caledor Woods Scenario. Scenario unlocks when the zone becomes contested.",
				},

				{
					name = L"• Boar (Acc) - Сет Нарушенный завет",
					text = L"- КАЛЕДОР (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Свиной бивштекс.\n- КООРДИНАТЫ: 26к, 5к.\n- ОПИСАНИЕ: Найти и убить Боевых кабанов змеекамня 33-го уровня юго-западнее Лагеря Кельсиана, и добудте Постное кабанье мясо.\n- АЛЬТЕРНАТИВА: Задание в Пробуждение Дракона.\n- НАГРАДА: Клятвы глубин (Сет Нарушенный завет 1/2).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBoar (Wyrmstone Charger) - Pork Chop\nSET ITEM\n\nCALEDOR. Kill Wyrmstone Chargers outside the DE camp in the SW, and loot the item Lean Boar Meat. Inspect this to get the unlock for the set item:The Deep Oaths.\nNOTE: See entry for Savage Charger in DRAGONWAKE for alternate approach.\n\nBy Blodsalv, Sandkat, Thanat0s & Zerk\n20081021",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Scoprion (Acc) - The Winds Impervious",
					text = L"- КАЛЕДОР (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пронзи пронзателя.\n- КООРДИНАТЫ: 44к, 5к (скорпион), 50к, 45к (гиганский скорпион).\n- ОПИСАНИЕ: Возле лагеря темных эльфов главы 16 найти и убить скорпиона Огнежала (побед. 33 ур.) чтобы добыть из него Жало гиганского скорпиона. Отправиться на територию ордера  возле Лагеря главы 21 найдите и убейте скорпиона Дробящая клешня (побед. 38 ур.).\n- ПРИМЕЧАНИЕ: Попасть на ту сторону можно через Пробуждение Дракона в координатах @ 15, 45 спрыгнуть в другую зону и пройти по тунелю @ 2, 60.\n- НАГРАДА: Печать непреклонности (The Winds Impervious сет 1/3).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant Scorpion (Crushclaw) (Firestrike) - Stick the Sticker\nSET ITEM\n\nCALEDOR @ 44, 5. Kill Firestrike to get Giant Scorpion Stinger. Then go to (O) side of Caledor @ 50, 45 (via Dragonwake @ 15, 45 and jump to ally side zone. Then go to Dragonwake 2, 60 to take the little secret stage) and kill the champ scorpion mob you find there.\n\nBy Sandkat, JodouTR, Boldag, Kellithe, TorqueVolkmar & prx\n20081027",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Scoprion (Acc) - The Winds Impervious",
					text = L"- Каледор (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пронзи пронзателя.\n- КООРДИНАТЫ: 51к, 46к и 44к, 6к.\n- ОПИСАНИЕ: убить Дробящую клешню (первые координаты) и взять Жало гигантского скрпиона. Затем убить Огнежала (вторые координаты). Если анлок не сработает, попробуйте убить мобов в обратном порядке.\n- НАГРАДА: сетовый талисман Печать Непреклонности.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nGiant Scorpion (Crushclaw) (Firestrike) - Stick the Sticker\nSET ITEM\n\nCALEDOR @ 51, 46. Brightmoon Forest N of (O) ch21. First kill Crushclaw to get: Giant Scorpion Stinger. Then go to 44, 6 and kill Firestrike. The scorpion stinger will disappear and you will get the The Obdurate Seal.\nNOTE: If this doesn't work, try reversing the process. Kill Firestrike first, then Crushclaw.\n\nBy Sandkat, JodouTR, Boldag, Kellithe, TorqueVolkmar & prx\n20081027",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Hydra (Title)",
					text = L"- КАЛЕДОР (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Кто выпустил эту тварь?\n- КООРДИНАТЫ: 12к, 37к.\n- ОПИСАНИЕ: Найти на дороге бродячую Гидру Когтя Смерти (побед. 40 ур.) с охраной из 2-х темных эльфов, и убить ее.\n- НАГРАДА: Титул Укротитель гидр.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHydra (Deathclaw) - Who Let That Thing Out?\nHydra Tamer\n\nDIRECTIONS: Mob patrols with 2 DE guards that are neutral to (D). Start in Dragonwake warcamp and go W to enter Caledor through a small pass in the SW area of Dragonwake. Then, go W following the road until you come to a tunnel. Go through the tunnel and head down the hill/road until you come to the (O) camp. Hug the outside wall of the (O) Ch22 camp and go to the back and jump down to the road. Now mount up and head N to 10k, 37k. If the hydra isn't there, just wait. It will be.\n\nBy Mippo & Sandkat\n20081009",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Hydra (Title)",
					text = L"- Каледор (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Кто выпустил эту тварь?\n- КООРДИНАТЫ: 10к, 37к.\n- ОПИСАНИЕ: убить гидру-победоносца 40 ур.\n- НАГРАДА: титул Укротитель гидр.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nHydra (Deathclaw) - Who Let That Thing Out?\nHydra Tamer\n\nCALEDOR @ 10k, 37k. Lvl 40 Champ. Kill it.\n\nBy Mippo & Sandkat\n20081009",
				},

				{
					name = L"• Liche (Token)",
					text = L"- КАЛЕДОР (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Хитрость против силы (Обещание могущества).\n- КООРДИНАТЫ: 32к, 22к.\n- ОПИСАНИЕ: Между двумя водопадами у горы за деревом найти Кучу грязи. Активировать полученный Скипетр личинок после чего снова активировать кучу грязи.\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nLiche (Mound of Dirt/Scepter of Maggots) - For the Promise of Power\nBEAST TOKEN\n\nCALEDOR @ 32, 22. E of the (D) Warcamp against a hill between two waterfalls is a Mound of Dirt. Click on it to get the Scepter of Maggots, then click on the scepter in your inventory. Finally, click the mound again for the unlock. NOTE: For (O) you'll need to come from the RvR lake, hug the hill to avoid the hero packs and drop down.\n\nBy Foggye\n20081103",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Manticore (Token)",
					text = L"- КАЛЕДОР (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Не смотри вверх.\n- КООРДИНАТЫ: 16к, 40к.\n- ОПИСАНИЕ: Найти и Убить Мартихораса (побед. 41 ур.), который летает над лесом и иногда спускаеться в разных его точках.\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nManticore (Martikhoras) - Don't Look Up\nBEASTIAL TOKEN UNLOCK\n\nCALEDOR. Kill this Lvl40 Champ and it's flying so DO look up!\nDIRECTIONS: See above entry for Hydra (Deathclaw). The Manticore is flying, but occasionally he will land on the side of the mt and you can then pull him.\n\nBy Mippo & Itsari & Sandkat\n20081009",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Manticore (Token)",
					text = L"- Каледор (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Не смотри вверх!\n- КООРДИНАТЫ: 17к, 37к.\n- ОПИСАНИЕ: убить Мантихораса (летает над лесом).\n- НАГРАДА: знак зверя.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nManticore (Martikhoras) - Don't Look Up\nBEASTIAL TOKEN UNLOCK\n\nCALEDOR @ 17k, 37k. Kill this Lvl40 Champ and it's flying so DO look up!\n\nBy Mippo & Itsari & Sandkat\n20081009",
				},
			},
		},

		{
			name = L"ЭАТАН • Eataine",
			entries =
			{
				{
					name = L"• High Elf (Title)",
					text = L"- ЭАТАН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Завершить сценарий высших эльфов.\n- ОПИСАНИЕ: Учавствовать в сценарии Черная пирамида. ПРИМЕЧАНИЕ: Сценарий доступен когда зона являеться спорной для обеих сторон.\n- НАГРАДА: Титул Бич Асуров.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHigh Elf - Complete a High Elf Scenario\nScourge of the Asur\n\nEATAINE. Win a Blood of the Black Cairn Scenario. Scenario unlocks when the zone becomes contested.\n\nBy Gharik_Gallant & Risingashes",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"?• Tuskgor (Tactic) - не найден",
					text = L"- ЭАТАН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Сохраняя лицо.\n- КООРДИНАТЫ: ...\n- ОПИСАНИЕ: Найти Бивнегорхов и добыть 5 бивней.\n- ПРИМЕЧАНИЕ: Шанс выпадения не больше 5%.\n- НАГРАДА: Фрагмент тактики Хаоса (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTuskgor - Saving Face\nChaos Tome Tactic\n\n(O) EATAINE. Gather Unchipped Tusks by killing Ravaging Tuskgors.\n\nBy Mourngrym, Socran, Karylet & Grishnar\n20090107",
				},

				{
					name = L"• Great Eagle (Acc) - Сет Безделушки Удачи",
					text = L"- ЭАТАН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Орлиный глаз.\n- КООРДИНАТЫ: 46к, 60к.\n- ОПИСАНИЕ: Левее Святилища Лепит на вершине горы на камне найти и убить орла Железнопер (побед. 40 ур.).\n- НАГРАДА: Горкаморк (Сет Безделушки Удачи 1/4).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGreat Eagle (Ironfeather) - Eagle-Eye\nITEM UNLOCK\n\nEATAINE @ 46, 60k. Kill a champ eagle called located in the SE, S of Shrine of Lileath, on a little rock. Gives an accessory called Gorkamork from the Fortune's Baubles set. NOTE: Slow spawn.\n\nBy Supaplex\n20081007",
				},


				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• F. of Tzeentch (Title)",
					text = L"- Пустоши Хаоса/Эатан (Тир 3, 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Невозможно отказаться.\n- КООРДИНАТЫ: ...\n- ОПИСАНИЕ: доставить Частицу Перемен Линселлии в 16 главу эльфов. Частицу можно получить, убивая огневиков в Пустошах хаоса.\n- НАГРАДА: титул Экзорцист.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nFlamer of Tzeentch (Aialye Lynsellia) - Hot Deal\nThe Banisher\n\nEATAINE. Return the Mote of Change to Aialye Lynsellia @ the HE Ch16 Hub. The Mote of Change has to chance to drop from any Flamer, but I received it from an Unbound Flamer in the Chaos Wastes.\n\nBy Foggye\n20081013",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Griffon (Title)",
					text = L"- ЭАТАН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Сильнейший.\n- КООРДИНАТЫ: 52к, 63к.\n- ОПИСАНИЕ: Среди грифонов найти лидера Мрачнокрыла (побед. 39 ур.) и убить его.\n- НАГРАДА: Титул Величественный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGriffon (Grimwing) - The Mightiest\nThe Magestic\n\nEATAINE @ 52, 63 or 53.5, 63.5. Kill this champ in the SE, S of Shrine of Lileath.\n\nBy Supaplex, JodouTR & TorqueVolkmar\n20081027",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Griffon (Title)",
					text = L"- Эатан (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Сильнейший.\n- КООРДИНАТЫ: 52к, 63к.\n- ОПИСАНИЕ: поговорить с Мрачнокрылом.\n- НАГРАДА: титул Величественный.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nGriffon (Grimwing) - The Mightiest\nThe Magestic\n\nEATAINE @ 52, 63 or 53.5, 63.5. Just talk to the NPC.\n\nBy Supaplex, JodouTR & TorqueVolkmar\n20081027",
				},

				{
					name = L"• Harpy (Tactic)",
					text = L"- ЭАТАН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Безмолвная звезда.\n- КООРДИНАТЫ: 37к, 13к.\n- ОПИСАНИЕ: Возле палаток на поле убивать летающих гарпий до тех пор пока не появиться главная гарпия Халиграр Кровокрылая (побед. 31 ур.), которую нужно также убить.\n- НАГРАДА: Фрагмент тактики Хаоса (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHarpy (Khaligrar Bloodwing) - Silent Star\nCHAOS TOME TACTIC\n\nEATAINE @ 37, 13 or 32, 13. Kill it.\nMust pull while the mob is airborne for the reward.\nNOTE: Twice confirmed report that killing the flying harpies ONLY cause the named mob to pop.\nFurther, the named may pop in flight even as the other named remains sitting on the ground.\n\nBy Scubasteve79, Dissent, Hoof, Daht, JCBN, Vauhs, Vandiego & Oxdottir\n20081124",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Harpy (Acc) - Сет Безделушки Удачи",
					text = L"- ЭАТАН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Их сущность пока не в этом.\n- КООРДИНАТЫ: 22к, 63к или 30.5к, 59к.\n- ОПИСАНИЕ: Убивать Страдающих гарпий (39-41 ур.), пока не получите Сердце гарпии. Отнести его целителю Аэла Шкуродерка в 21 главу.\n- ПРИМЕЧАНИЕ: Шанс выпадения сердца примерно 3%.\n- НАГРАДА: Ветер Перемен (Сет Безделушки Удачи 1/4).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHarpy (Tormented Harpy) - Their Heart's Not in it, Yet.\nJEWELRY ITEM\n\nEATAINE @ 22, 63 or 30.5, 59. Kill these mobs until they drop a Harpy Heart. Deliver this to the Healer NPC in the DE Ch21 camp. Grants the unlock: Wind of Change. NOTE: The drop might take a while, I killed about 50 of them before the drop. BONUS: Click the Shrine of Asuryan NW of these mobs for an unlock!\n\nBy Supaplex, JodouTR & Benji\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Harpy (Acc) - Сет Безделушки Удачи",
					text = L"- Эатан (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Их сущность пока не в этом.\n- КООРДИНАТЫ: 31к, 61к.\n- ОПИСАНИЕ: убивать сломленных гарпий, пока не получите сердце гарпии. Отнести его целителю в 21 главу эльфов (Каледор).\n- НАГРАДА: сетовый талисман Ветер Перемен.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nHarpy (Tormented Harpy) - Their Heart's Not in it, Yet.\nJEWELRY ITEM\n\nEATAINE @ 22, 63 or 30.5, 59. Kill these mobs until they drop a Harpy Heart. Deliver heart to the healer located in Caledor Ch21--49, 21. Grants the unlock: Wind of Change. NOTE: The drop might take a while, I killed about 50 of them before the drop. BONUS: Click the Shrine of Asuryan NW of these mobs for an unlock!\n\nBy Supaplex, JodouTR & Benji\n20081124",
				},

				{
					name = L"• Treekin (Token)",
					text = L"- ЭАТАН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Древесина\n- КООРДИНАТЫ: от 49к, 63к до 50к, 48к.\n- ОПИСАНИЕ: Найти патрулирующего територию Гневного моховика 40-го героя и убить его.\n- НАГРАДА: Знак зверя\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTreekin (Irebough Mossroot) - Timber!\nBEAST TOKEN\n\nEATAINE @ 49, 63 to 50, 48. Vanquish this L40 Hero that patrols S of Lilithorn Estate.\n\nBy Vert, Supaplex & poll12\n20081023",
				},

				{
					name = L"• Tuskgor (Tactic)",
					text = L"- ЭАТАН (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Клак за клык.\n- КООРДИНАТЫ: 14к, 11к.\n- ОПИСАНИЕ: Убить кабана Остроклык (побед. 33 ур.).\n- НАГРАДА: Фрагмент Тактики Хаоса (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTuskgor (Riptusk) - Tusk for Tusk\nCHAOS TOME TACTIC\n\nEATAINE @ 14, 10.5. Kill the champ.\n\nBy Risingashes & Drunklight\n20081124",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• форт •••",
			divider = true,
			entries = {},
		},

		{
			name = L"КРУШЕНИЕ • Fell Landing",
			entries =
			{
				{-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Dark Elf (Title)",
					text = L"- КРУШЕНИЕ (Форт).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Победить Лорда в крепости темных эльфов.\n- ОПИСАНИЕ: убить владыку форта Тёмных эльфов.\n- НАГРАДА: титул Хранитель пламени.\n\nЧейз Рэйнхолд © 2009\n\n———————————————————————————————\n\nDark Elf - Vanquish the Dark Elf\nFortress Lord\nKeeper of the Flame\n\nBy Socran",
				},
			},
		},

		{
			name = L"СИЯЮЩИЙ ПУТЬ • Shining Way",
			entries =
			{
				{-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"?• High Elf (Title) - не найден",
					text = L"- СИЯЮЩИЙ ПУТЬ (Форт).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Победить Лорда в крепости высших эльфов.\n- ОПИСАНИЕ: Убить лорда в форте Высших эльфов.\n- НАГРАДА: Титул ...\n\n————————————————————————————————\n\High Elf  - Vanquish the High Elf Fortress Lord\n...",
				},

			},
		},
		
		
		
	},
}
