TTitan.Data.NoteworthyHEvDE = {}

function TTitan.Data.NoteworthyHEvDE.GetData()
	return TTitan.Data.NoteworthyHEvDE.RawData
end

function TTitan.Data.NoteworthyHEvDE.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.NoteworthyHEvDE.RawData)
end

TTitan.Data.NoteworthyHEvDE.RawData =
{
	header = L"Известные личности - Высшие эльфы против Темных эльфов", -- Noteworthy Persons Unlocks - High Elves vs Dark Elves
	categories =
	{
		{
			name = L"The Blighted Isle",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Talorith",
					text =
[[
Rally Master of Azurewood Glade (48000,14000)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Kyrinn Silversea",
					text =
[[
Rally Master of Adunei (45000,26500)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Morithwen Nightwhisper",
					text =
[[
Rally Master of Blightsward. This is in Chrace at 53k,1k even though it is listed under The Blighted Isle.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Riandys Arrowlore",
					text =
[[
Tor Aendris War Camp (44000,48000)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Aen Windsong",
					text =
[[
Kill Collector of Azurewood Glade (48000,14000)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Caethsetir Silverstrand ",
					text =
[[
Kill Collector of Adunei (45000,26500)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Aeldith Tearsong ",
					text =
[[
Kill Collector of Blightsward (northern Chrace 53300,1500)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Master Kaltarn",
					text =
[[
Dark Elf Rally master located in Isha's Garden. (10224,19202)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Akrana",
					text =
[[
Dark Elf Rally Master located in the Mistwood Dark Elf encampment. (13173,31261)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Kulvorath Bloodhand",
					text =
[[
Dark Elf Rally Master in Dreamshade encampment. (12911,51642)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Kolith Blackblade",
					text =
[[
Kill Collector in Chapter 2, stands off nearby the rally master. (Akrana's Storm)(13131, 31261) Edit: This is actually the Kill Collector in the Dark Elf Warcamp (26127, 56973)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Saril Zaen",
					text =
[[
Kill Collector in Chapter 1, he's standing to the right-side of the holding pit. (8716,14570)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Riakahd Flameblade",
					text =
[[
Dark Elf Kill Collector located in the Mistwood Dark Elf encampment. (13369,31523)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Krartil Deathsong",
					text =
[[
Kill Collector in Chapter 3, but can only found when you first enter Chrace, he's stands by the ship on the shore along with a quest giver. (9300, 2600)
]],
				},
			},
		},

		{
			name = L"Chrace",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Caduin Goldbough ",
					text =
[[
Rally Master - High Elf Chapter 4
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Elthossar Youngstar",
					text =
[[
???
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"General Malagurn",
					text =
[[
Rally Master, Chapter 4 'Malagurn's Charge'. (19, 25k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Hakazin Felwind",
					text =
[[
Kill Collector, Chapter 4 'Malagurn's Charge'. (19, 25k)
]],
				},
			},
		},

		{
			name = L"The Shadowlands",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Mathrin Leafwind",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Nenthuil",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Liradean Swiftarm",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Lasira Swiftbow",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Celorn Slenderwand",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Shenassa Blesswind",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Cirroniol Sureshot",
					text =
[[
???
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Lord Yldrian",
					text =
[[
Rally Master, Chapter 5 'Ruins of Anlec'. (36.4, 8.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Harodir",
					text =
[[
Rally Master, Chapter 6 'Sundered Strand'. (5.5, 16.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Ralkuth",
					text =
[[
Rally Master, Chapter 7 'Ralkuth's Return'. (18.1, 37.5k).
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Kathandris Skysiege",
					text =
[[
Standing near a rock on the western side of the 'Oath's End Warcamp'.
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Beastmaster Zakira",
					text =
[[
Kill Collector, Chapter 5 'Ruins of Anlec'. (35.4, 9.3k).
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Maefyr Fellwhip",
					text =
[[
Kill Collector, Chapter 6, 'Sundered Strand'. (5.5, 16.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Torrath Ullen",
					text =
[[
Kill Collector, Chapter 7 'Ralkuth's Return'. (17.1, 37.6k)
]],
				},
			},
		},

		{
			name = L"Ellyrion",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Andalian Windrider",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Ithiloen Truedale",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Quartermaster Aenil",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Seer Jhoril",
					text =
[[
???
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Baromad Drakanth",
					text =
[[
???
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Kesida Garalond",
					text =
[[
???
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Tracker Drazek Kar",
					text =
[[
???
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Taraj Kirak",
					text =
[[
???
]],
				},
			},
		},

		{
			name = L"Avelorn",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Cirribhir Strongwing ",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Aeddriel Swiftspear",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Sethril Stillwater",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Mesilhin Darkeagle",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Elithaen Leafwind",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Nalrothel Forestshadow",
					text =
[[
???
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Ilries Stillfury",
					text =
[[
Rally Master-Dark Elves Chapter 10-Burnwood. (5.2,12.1k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Sarava Scornblade",
					text =
[[
Rally Master-Dark Elves Chapter 11-Scornblade's Seige. (23.5,22.5k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Devak Bloodbane",
					text =
[[
Rally Master-Dark Elves Chapter 12-Jade Coast. (6.5,45k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Uzoe Ra'an",
					text =
[[
Kill Collector-Dark Elves Chapter 10-Burnwood. (5.5,12.1k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Garak Zornil",
					text =
[[
Kill Collector-Dark Elves Chapter 11-Scornblade's Seige. (23.5,22.5k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Seleis Soronil",
					text =
[[
Kill Collector-Dark Elves Chapter 12-Jade Coast. (6.5,45k)
]],
				},
			},
		},

		{
			name = L"Saphery",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Berliol Shadowseer",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Menuthil Fargaze",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Eliren Stormshade",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Dekindes Tovliryn",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Ilfuwyr Cetaine",
					text =
[[
???
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Morithia Shadowblight",
					text =
[[
Rally Master-Dark Elves Chapter 13-Knife's Edge. (10,5.6k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Kyaran Noth",
					text =
[[
Rally Master-Dark Elves Chapter 14-Kyaran's Advance. (8.9,34k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Lord Vilkareth",
					text =
[[
Isha's Fall Warcamp-Avelorn. (17.6,54.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Nyethan Blackblade",
					text =
[[
Kill Collector-Dark Elves Chapter 13-Knife's Edge. (10,5.6k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Chorane",
					text =
[[
Kill Collector-Dark Elves Chapter 14-Kyaran's Advance. (8.9,34k)
]],
				},
			},
		},

		{
			name = L"Dragonwake",
			entries =
			{
			},
		},

		{
			name = L"Caledor",
			entries =
			{
			},
		},

		{
			name = L"Eataine",
			entries =
			{
			},
		},
	},
}
