TTitan.Data.NoteworthyDvG = {}

function TTitan.Data.NoteworthyDvG.GetData()
	return TTitan.Data.NoteworthyDvG.RawData
end

function TTitan.Data.NoteworthyDvG.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.NoteworthyDvG.RawData)
end

TTitan.Data.NoteworthyDvG.RawData =
{
	header = L"Известные личности - Гномы против Зеленокожих", -- Noteworthy Persons Unlocks - Dwarfs vs Greenskins
	categories =
	{
		{
			name = L"Ekrund",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Grimilda Mughammer",
					text =
[[
Dwarven Kill Collector in Goldbrow's Lament. (19.2k, 46.1k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Rordin Ironmane",
					text =
[[
Rally Master in the Battle of Bitterstone, Dwarf Chapter 1. (35.1k,37.0k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Skolm Goldskaar",
					text =
[[
Kill Collector in Redhammer Station, Dwarf Chapter 2. (46.8k,30.1k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Krogan Redhammer",
					text =
[[
Rally Master at Redhammer Station, Dwarf Chapter 2. (46.7k,30.0k)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Moranir Grudgekeg",
					text =
[[
NPC at Grudgekeg's Guard Warcamp. (55.5k,17.7k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Gorgor",
					text =
[[
Rally Master - Greenskins Chapter 4 - Gorgor's Smash. (58k,28k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Gorsh Smashmouf",
					text =
[[
???
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Xobz Madgut",
					text =
[[
Kill Collector - Greenskins Chapter 4 - Gorgor's Smash. (58k,28k)
]],
				},
			},
		},

		{
			name = L"Mount Bloodhorn",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Odarik Rorkisson",
					text =
[[
Rally Master of Komar
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Captain Skalfson",
					text =
[[
Rally Master of Skalfson's Watch
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Zamgund Roarhammer",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Dodrum Ironsplitter",
					text =
[[
Kill Collector of Komar
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Barin Grimbeard",
					text =
[[
Kill Collector of Skalfson's Watch
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Maggut",
					text =
[[
Rally Master of Snouts' Pens (33k, 54k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Grokk",
					text =
[[
Rally Master of Da War Maker
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Splinta",
					text =
[[
Rally Master of Kron Komar (3,8k, 27,5k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Screeb",
					text =
[[
Screeb's Warcamp
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Wobna Slipsquig",
					text =
[[
Kill Collector of Lobber Hill (40k, 54k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Sneakfang",
					text =
[[
Kill Collector of Da War Maker
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Virrik",
					text =
[[
Kill Collector of Kron Komar (1,7k, 27k)
]],
				},
			},
		},

		{
			name = L"Barak Varr",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Thane Karrasteel",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Farnir Goldpeak",
					text =
[[
Rally Master - Dwarf Chapter 7
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Elgaroth Karginson",
					text =
[[
Kill Collector - Dwarf Chapter 7
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Nogaz",
					text =
[[
Rally Master - Greenskins Chapter 5 - Nogaz's Boyz. (9.1k,31k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Rottoof",
					text =
[[
Rally Master - Greenskins Chapter 7 - Rottoof's Mugz. (32.8,60.3k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Malgrog",
					text =
[[
Rally Master - Greenskins Chapter 8 - Port of Barak Varr. (43,24.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Mobash",
					text =
[[
Rally Master - Greenskins Chapter 9 - Mobash's Place. (28.1, 18k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Faduk Foultooth",
					text =
[[
Foultooth's Warcamp. (41, 42.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Runtskull",
					text =
[[
Kill Collector - Greenskins Chapter 5 - Nogaz's Boyz. (9.1k,31k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Reezel",
					text =
[[
Kill Collector - Greenskins Chapter 7 - Rottoof's Mugz. (32.8,60.3k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Sneaks",
					text =
[[
Kill Collector - Greenskins Chapter 8 - Port of Barak Varr. (43,24.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Moozle",
					text =
[[
Kill Collector - Greenkins Chapter 9 - Mobash's Place. (28.1,18k)
]],
				},
			}
		},

		{
			name = L"Marshes of Madness",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Durrig Olfrinson",
					text =
[[
Rally Master - Dwarf Chapter 5
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Murdogh Kadrikson",
					text =
[[
Rally Master - Dwarf Chapter 6
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Barin Oathsmiter",
					text =
[[
Rally Master - Chapter 8
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Droki Redbeard",
					text =
[[
Rally Master - Chapter 9
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Balabin Thurarikson",
					text =
[[
Questgiver - Found at (61.97, 65.55)
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Kurgan Ironfist",
					text =
[[
Kill Collector - 5
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Estelle Meyer",
					text =
[[
Kill Collector - 6
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Grunlok Steelspade",
					text =
[[
Kill Collector - 8
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Lokki Redbeard",
					text =
[[
Kill Collector - 9
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Bonerender",
					text =
[[
Rally Master - Greenskins Chapter 6-Bonerender's Bash. (20.7,6.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Morth",
					text =
[[
Morth's Mire Warcamp. (26.7,26.8k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Thudfist",
					text =
[[
Kill Collector - Greenskins Chapter 6-Bonerender's Bash. (20.7,6.9k)
]],
				},
			}
		},

		{
			name = L"The Badlands",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Alethewn Stoutgut",
					text =
[[
Rally Master - Dwarf Chapter 10
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Skaf Copperstone",
					text =
[[
Rally Master - Dwarf Chapter 11
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Dunhilda Grodrikson",
					text =
[[
Rally Master - Dwarf Chapter 13
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Athran Ironbeard",
					text =
[[
Rally Master - Dwarf Chapter 14
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Lungni the Dour",
					text =
[[
Questgiver - Unknown
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Bragni Tunnelcut",
					text =
[[
Kill Collector - Dwarf Chapter 10
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Doragrum Stouthammer",
					text =
[[
Kill Collector - Dwarf Chapter 11
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Hlom Hrnghamson",
					text =
[[
Kill Collector - Dwarf Chapter 13
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Mak Kleintrek",
					text =
[[
Kill Collector - Dwarf Chapter 14
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Trug",
					text =
[[
Rally Master-Greenskins Chapter 11-Trug's Hut. (17,54k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Muggar",
					text =
[[
Muggar's Choppaz Warcamp. (47, 45k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Shaman Greyfrobba",
					text =
[[
Kill Collector-Greenskins Chapter 11-Trug's Hut. (17,54k)
]],
				},
			}
		},

		{
			name = L"Black Fire Pass",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Varmir Proudrock",
					text =
[[
Rally Master - Dwarf Chapter 12
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Gamira Odinadotr",
					text =
[[
Questgiver - Unknown
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Thurn Goldbrewer",
					text =
[[
Kill Collector - Dwarf Chapter 12
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Moogz",
					text =
[[
Rally Master - Greenskins Chapter 10 - Moogz's Brawl. (4.0,51.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Drok Gunash",
					text =
[[
Rally Master - Greenskins Chapter 12 - Drok's Fist. (31.1,51.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Gorthug",
					text =
[[
Rally Master - Greenskins Chapter 13 - Gorthug's Chew. (32.5,37)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Skargor",
					text =
[[
Rally Master - Greenskins Chapter 14 - Da Big Burn. (46.9,27.2k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Torka",
					text =
[[
Torka's Boyz Warcamp. (32.6,8.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Gremgob",
					text =
[[
Kill Collector - Greenskins Chapter 10 - Moogz' Brawl. (4.0,51.9k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Smashmur",
					text =
[[
Kill Collector - Greenskins Chapter 12 - Drok's Fist. (31.1,58.7k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Grik",
					text =
[[
Kill Collector - Greenskins Chapter 13 - Gorthug's Chew. (32.5,37k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Tog Nailclub",
					text =
[[
Kill Collector - Greenskins Chapter 14 - Da Big Burn. (46.9,27.2k)
]],
				},
			}
		},

		{
			name = L"Thunder Mountain",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Zamgrund Lorkison",
					text =
[[
Rally Master - Dwarf Chapter 17
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Bari Hlavengr",
					text =
[[
Rally Master - Dwarf Chapter 18
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Dolgr Grinarson",
					text =
[[
Rally Master - Dwarf Chapter 19
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Bodor Greymere",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Leo Heller",
					text =
[[
Kill Collector - Dwarf Chapter 17
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Rori Mailcutter",
					text =
[[
Kill Collector - Dwarf Chapter 18
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Rofi Throstsson",
					text =
[[
Kill Collector - Dwarf Chapter 19
]],
				},
			}
		},

		{
			name = L"Kadrin Valley",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Skoragrin Drokison",
					text =
[[
Rally Master - Dwarf Chapter 15
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Furen Brazenbrow",
					text =
[[
Rally Master - Dwarf Chapter 16
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Grumir Orcslayer",
					text =
[[
NPC - Location 27.4k - 6.9k
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Karon Vigridson",
					text =
[[
Kill Collector - Dwarf Chapter 15
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Olfgrond Hammerson",
					text =
[[
Kill Collector - Dwarf Chapter 16
]],
				},
			},
		},

		{
			name = L"Black Crag",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Ellamina Craghelm",
					text =
[[
Rally Master - Dwarf Chapter 20
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Gargrim Oathgrund",
					text =
[[
Rally Master - Dwarf Chapter 21
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Gav Hallmundr",
					text =
[[
Rally Master - Dwarf Chapter 22
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Rogrin Weatheredmane",
					text =
[[
???
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Slayer Bolgun",
					text =
[[
Kill Collector - Dwarf Chapter 20
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Bogni Goblingrinder",
					text =
[[
Kill Collector - Dwarf Chapter 21
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Hanri Raudson",
					text =
[[
Kill Collector - Dwarf Chapter 22
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Grimgork",
					text =
[[
Rally Master-Greenskins Chapter 15- Red Dust Camp. (22,36.5k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Mugash",
					text =
[[
Rally Master-Greenskins Chapter 16-Da Great Big Food Pot. (9.7, 13.7k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Shaz da Bleeda",
					text =
[[
Gudmud's Strong-huts. (19.8,61.8k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Kark",
					text =
[[
Kill Collector-Greenskins Chapter 15-Red Dust Camp. (22,36.5k)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Bloodtoof",
					text =
[[
Kill Collector-Greenskins Chapter 16-Da Great Big Food Pot. (9.7,13.7k)
]],
				},
			}
		},
	},
}
