TTitan.Data.BestiaryEvC = {}

function TTitan.Data.BestiaryEvC.GetData()
	return TTitan.Data.BestiaryEvC.RawData
end

function TTitan.Data.BestiaryEvC.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.BestiaryEvC.RawData)
end

TTitan.Data.BestiaryEvC.RawData =
{
	header = L"Бестиарий - Империя против Хаоса",
	categories =
	{
		{
			name = L"Классовый свиток с РвР зоны (Race/Class scrolls (RVR LAKES))",
			entries =
			{
				{
					name = L"• Empire (Title)",
					text = L"Империя\n- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: История Империи.\n- КООРДИНАТЫ: 23к, 9к.\n- ОПИСАНИЕ: Взять Спрятанный свиток в кустах за одним из обелисков.\n- НАГРАДА: титул Анархист.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nEmpire - The History of the Empire\nThe Anarchist\n\nCHAOS WASTES @ 23, 10. Click on the Hidden Scroll located behind the NE stone pillar right by the BO Flag.\n\nBy rouko\n20081217",
				},
				
				{
					name = L"• Witch Hunter (Pocket)",
					text = L"Охотник на ведьм\n- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 3к, 54к.\n- ОПИСАНИЕ: Взять Спрятанный свиток в кустах, слева от замка.\n- НАГРАДА: Страх в бутылке (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nEmpire, Witch Hunter - What do We Have Here ?\nPOCKET ITEM\n\nTROLL COUNTRY @ 3.5, 54. The Hidden Scroll for this can be found slightly N of Stonetroll Keep in the ORvR lake. It is located in some bushes next to a tree N of the NE corner of Stonetroll Keep, near the stone tunnel that leads to the back door of the keep. This will unlock Fear in a bottle.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy wahooka\n20081216",
				},
				
				{
					name = L"• Bright Wizard (Pocket)",
					text = L"Огненный маг\n- НОРЛАНД (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 24к, 12к.\n- ОПИСАНИЕ: Возле берега между камнями и кустом найти Спрятанный свиток.\n- НАГРАДА: Кусок кремня (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nEmpire, Bright Wizard - What do We Have Here ?\nPOCKET ITEM\n\nNORDLAND @ 24, 11. Click on the Hidden Scroll behind a bush against the rocks. Unlocks: Flint Stone.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy Socran\n20081217",
				},
				
				{
					name = L"• Warrior Priest (Pocket)",
					text = L"Воинствующий жрец\n- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 34к, 57к.\n- ОПИСАНИЕ: Взять Спрятанный свиток в фонтане.\n- НАГРАДА: Мазь для пальца (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nEmpire, Warrior Priest - What do We Have Here ?\nPOCKET ITEM\n\nPRAAG @ 35, 58. Click on the 'Hidden Scroll' to unlock: Thumb Unguent.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy rouko\n20081216",
				},
				
				{
					name = L"?• Knight of the Blazing Sun (Pocket) - не найден",
					text = L"Рыцарь сияющего солнца",
				},
				
				
				
				{
					name = L"• Chaos (Title)",
					text = L"Хаос\n- РЕЙКЛАНД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: История слуг Хаоса.\n- КООРДИНАТЫ: 17к, 64к.\n- ОПИСАНИЕ: На холме между досками найти Спрятанный свиток\n- НАГРАДА: Титул Гроза темных богов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos - The History of the servants of Chaos\nFoe of the Dark Gods\n\nREIKLAND @ 17, 64. Click on the Hidden Scroll located between 2 piles of wood.\n\nBy rouko\n20081216",
				},
				
				{
					name = L"• Chosen (Pocket)",
					text = L"Избранный\n- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это тут у нас?\n- КООРДИНАТЫ: 30к, 17к.\n- ОПИСАНИЕ: Рядом с замком Гарнизон Черепов найти раззрушенный дом и под доской осмотреть Спрятанный свиток.\n- НАГРАДА: Знак Тзинча (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos, Chosen - What do We Have Here ?\nPOCKET ITEM\n\nPRAAG @ 30, 16 in the ruined house, under a plank. Click on the 'Hidden Scroll' to unlock: Mark of Tzeench.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy Amaranth\n20090318",
				

				},

				{
					name = L"• Marauder (Pocket)",
					text = L"Марадер\n- ОСТЛАНД (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 35к, 8к.\n- ОПИСАНИЕ: Рядом со входом в Крипту Оружия возле обломка стены найти Спрятанный свиток.\n- НАГРАДА: Мутировавший коготь (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos, Marauder - What do We Have Here ?\nPOCKET ITEM\n\nOSTLAND @ 34.8, 8.3. At the Crypt of Weapons, near the Tainted Earth for the quest, by one of the fallen pillars is a scroll. Unlocks: Mutated Claw.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy Ionass\n20081217",
				},

				{
					name = L"• Zealot (Pocket)",
					text = L"Фанатик\n- НОРСКА (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 42к, 63к.\n- ОПИСАНИЕ: На границе ПвЕ и ПвП зоны у разбитой лочуги найти Спрятанный свиток.\n- НАГРАДА: Закопченный череп (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos, Zealot - What do We Have Here ?\nPOCKET ITEM\n\nNORSCA @ 42, 63. Click on the Hidden Scroll behind the burned house.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy Minusa\n20081217",
				},

				{
					name = L"• Magus (Pocket)",
					text = L"Волхв\n- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 15к, 63к.\n- ОПИСАНИЕ: Найти Спрятанный свиток рядом с разрушенной стеной.\n- НАГРАДА: Лапа Гибельной утробы (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos, Magus - What do We Have Here ?\nPOCKET ITEM\n\nHIGH PASS @ 15, 63. Click on the 'Hidden Scroll' located beside a broken section of the wall, the scroll is semi buried in the snow...not that hard to see. Unlocks: Discus Tech.\n\n(NOTE: This will not mark as complete, as it currently marks as complete while incomplete if notated correctly.)\n\nBy rouko\n20081217",
				},
			
			},
		},

		{	-- Divider, won't expand when clicked
			name = L"••• тир 1 •••",
			divider = true,
			entries = {},
		},
		
		{
			name = L"НОРСКА • Norsca",
			entries =
			{
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Beastman, Gor (Title)",
					text = L"- НОРСКА (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Убийственный лес.\n- КООРДИНАТЫ: 29к, 17к.\n- ОПИСАНИЕ: Найти скалу Родовой камень и изучить ее.\n- НАГРАДА: Титул Страж дорог.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Gor - The Murder Wood\nRoad Warden\n\nNORSCA @ 29k, 17k. Click on the herdstone.\n\nBy Maehan, Puddles404, LatukiJoe & Wykk\n20081007",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Beastman, Ungor (Title)",
					text = L"- НОРСКА (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Кто прибъет?\n- КООРДИНАТЫ: 25, 16.\n- ОПИСАНИЕ: За деревом у камня найти труп Несчастной жертвы и осмотреть его.\n- НАГРАДА: Титул Скотина.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Ungor (An Unfortunate Victim) - Who Wud Murder\nThe Brute\n\nNORSCA @ 24.5, 15.5k. Head toward the first Ungor camp from spawn area and check behind rocks for human corpse.\n\nBy Kovacs, Nozaro, Puddles404 & JodouTR\n20081014",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Plague Victim (Tactic)",
					text = L"- НОРСКА (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Эпическая эпидемия.\n- КООРДИНАТЫ: 19к, 49к.\n- ОПИСАНИЕ: Найдите на скале сидящего Матис Риттенхаус и поговорите с ним.\n- НАГРАДА: Фрагмент тактики Хаоса (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPlague Victim - Epic Epidemic\nCHAOS TOME TACTIC\n\nNORSCA @ 18.5, 48.5. Go to Mathis Rittenhouse sitting on a rock W of Suderholm PQ and near the Power Hungry Villager.\n\nBy Shadeviper, Riggswolfe & Sildroni\n20081124",
				},
				
				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Plague Victim (Tactic)",
					text = L"- Норска (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Эпическая эпидемия.\n- КООРДИНАТЫ: 51к, 57к.\n- ОПИСАНИЕ: Поговорить с сестрой Рафаэллой. Она сидит рядом с камнем, на холме.\n- НАГРАДА: фрагмент тактики хаоса.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nPlague Victim - Epic Epidemic\nCHAOS TOME TACTIC\n\nNORSCA @ 51.5, 57. Look for Sister Raffaela in the plagued village. She's sitting in the back of the village on the ground near the hills next to a bush.\n\nBy FireOpal, Aoibhinn & Sereca\n20081124",
				},
				
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Skeleton (Title)",
					text = L"- НОРСКА (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что лежит впереди.\n- КООРДИНАТЫ: 33к, 7к.\n- ОПИСАНИЕ: На кладбище найти Могильный камень и осмотреть его.\n- НАГРАДА: Титул охотник на скелетов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSkeleton - What Lay Ahead\nThe Skeleton Hunter\n\nNORSCA @ 33, 7. Near the Chaos starting area there is a cemetery with a load of ghouls. In that cemetery is a gravestone, it's tall and there are a bunch of skulls stacked on top of each other on it. Simply click it to get the unlock.\n\nBy Kovacs, Impatiens & Elmarra\n20081110",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Spider, Giant (Title)",
					text = L"- НОРСКА (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Так себе интерьер\n- КООРДИНАТЫ: 61к, 36к.\n- ОПИСАНИЕ: Найти в пещере Спиндекракен главарей пауков и убить их.\n- АЛЬТЕРНАТИВА: Задание в Стране тролей или в Барак Варр.\n- НАГРАДА: Титул Убийца пауков.\n\nTEZARIUS © 2009\n————————————————————————————————\n\nSpider, Giant - Tacky Decor\nSpider-Slayer\n\nNORSCA. Also found in the Spider Cave (N of the bandit camp). NOTE: If you don't get the unlock here, see the entry in TROLL COUNTRY, below, and try that.\n\nBy NicWester, burfo, FireOpal & Wykk...whew.\n20081007",
				},

				{
					name = L"• Spirit Host (Title)",
					text = L"- НОРСКА (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Обитель мертвых.\n- КООРДИНАТЫ: 48к, 45к.\n- ОПИСАНИЕ: Найти и исследовать склеп Амунгов курган.\n- НАГРАДА: Титул Бесстрашный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpirit Host - Place of the Dead\nThe Fearless\n\nNORSCA @ 48k, 45k. It's a habitat unlock on the (D) side. Go into the crypt Amund's Barrow. I got it just a few steps into the crypt.\n\nBy Aoibhinn, Riggswolfe, papalauthority, notalltogether & raicarter\n20081007",
				},
			},
		},

		{
			name = L"НОРДЛАНД • Nordland",
			entries =
			{
				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Beastman (Title)",
					text = L"- Нордланд (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что это у нас тут?\n- КООРДИНАТЫ: 23к, 10к.\n- ОПИСАНИЕ: Взять Спрятанный свиток в кустах рядом с костром.\n- НАГРАДА: Кусок кремня (карман).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nBeastman, Gor - The Murder Wood\nRoad Warden\n\nNORDLAND @ 40.5, 36.5. Click the Beastman Herdstone in the N part of the Murder Wood (it's in the secluded valley with the lake on the empire side).\n\nBy Maehan, Puddles404, LatukiJoe & Wykk\n20081029",
				},

				{   -- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Beastman, Gor (Title)",
					text = L"- Нордланд (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Убийственный лес.\n- КООРДИНАТЫ: 40к, 36к.\n- ОПИСАНИЕ: использовать родовой камень, стоящий неподалёку от озера.\n- НАГРАДА: титул Страж дорог.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nBeastman, Ungor (An Unfortunate Victim) - Who Wud Murder\nThe Brute\n\nNORDLAND @ 37k, 35k. In Murder Wood target corpse of An Unfortunate Victim for the unlock.\n\nBy Kovacs, Nozaro& xepherys\n20081007",
				},

				{
					name = L"• Boar (Title)",
					text = L"- НОРДЛАНД (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Кругом одни свиньи.\n- КООРДИНАТЫ: 52к, 2к или 56к, 2к или 48к, 5к.\n- ОПИСАНИЕ: Исследовать место обитание кабанов.\n- НАГРАДА: Титул Охотник.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBoar - Surrounded by Swine\nHuntsman\n\nNORDLAND @ 52.5k, 2k or 56k, 2k or 48k, 5k. Habitat reward @ an encampment between Ravenraid & Gotland. Just walk through the camp to activate it.\n\nBy LatukiJoe, Nazaro, burfo, NicWester, Puddles404 & Quiggles\n20081007",
				},

				{
					name = L"• Hound (Title)",
					text = L"- НОРДЛАНД (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Очень голодные гончии.\n- КООРДИНАТЫ: 57к, 42к.\n- ОПИСАНИЕ: Возле небольшого лагеря охотников с собаками, возле дерева найти сумку Мусор и шлак.\n- НАГРАДА: Титул Охотник на псов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHound - Hungry, Hungry Hounds\nThe Hound Hunter\n\nNORDLAND @ 57, 42k. Received from a bag of meat by the mt. ridge N of Salzenmund (on the N side). The bag called Kibbles n Scraps is in a camp consisting of one imperial and a few hounds, sitting by a camp fire.\n\nBy Shiwoen, Nozaro & Shawen\n20081013",
				},

				{
					name = L"• Plague Victim (Title)",
					text = L"- НОРДЛАНД (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Люди собираються.\n- КООРДИНАТЫ: 59к, 12к.\n- ОПИСАНИЕ: Возле дерева найти валяющегося крестьянина, когда он подниметься убить его.\n- НАГРАДА: Титул Заразный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPlague Victim - People Pop Up\nThe Infected\n\nNORDLAND @ 59,12k. There is a Plagued Farmer mob lying behind a tree killing him will unlock People Pop Up and the title.\n\nBy Shadeviper, Puddles404 & Shawen\n20081013",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Skeleton (Title)",
					text = L"- Нордланд (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что лежит впереди.\n- КООРДИНАТЫ: 19к, 39к.\n- ОПИСАНИЕ: использовать могильный камень, расположенный слева от входа в склеп.\n- НАГРАДА: титул Охотник на скелеты.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nSkeleton - What Lay Ahead\nSkeleton Hunter\n\n(O) NORDLAND @ 19k, 39k. Click on a grave found in Grimmenhagen just left of the entrance to the Grimmenhagen barrows.\n\nBy Mortiferous\n20081007",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Spider (Tactic)",
					text = L"- НОРДЛАНД (Тир 1).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Легкая нажива.\n- КООРДИНАТЫ: 62к, 25к или 56к, 25к или 56к, 38к или 48к, 41к или 52к, 46к.\n- ОПИСАНИЕ: Убить 60 Серых волкопауков или Алых тарантулов, после чего доложиться Регистратору убийств Голену в 4-й главе хаоса.\n- НАГРАДА: Фрагмент тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpider, Giant (Grey/Scarlet Wolf Spider) - Fast Finds\nBESTIAL TOME TACTIC\n\n(D) NORDLAND. Kill spiders till you complete the kill collector quest in Authun's Host. By complete I mean till you do so many turn ins you can't do any more. NOTE: Requires 60 spider kills.\n\nBy FireOpal, Nerror & xentrix\n20081029",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 2 •••",
			divider = true,
			entries = {},
		},

		{
			name = L"ОСТЛАНД • Ostland",
			entries =
			{
				{
					name = L"• Bandit (Tactic)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Разыскиваеться.\n- КООРДИНАТЫ: 56к, 24к.\n- ОПИСАНИЕ: Найти на дорроге бандита Людвиг Фалькенхайм (побед. 17 ур.) и победить его.\n- НАГРАДА: Фрагмент тактики Людей (1/6).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBandit (Lodwig Falkenheim) - Wanted!\nMAN TOME TACTIC\n\nOSTLAND @ 56, 24. Kill this L17 champ. NOTE: Shadow Bandits may be placeholders.\n\nBy reggie & Dormammu & Kriegzwerg\n20081022",
				},

				{
					name = L"• Bat, Giant (Tactic)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Летучии твари.\n- КООРДИНАТЫ: 21к, 29к или 31к, 46к или 39к, 40к.\n- ОПИСАНИЕ: Найти между деревьями летучую мышь Чуткач (побед. 17 ур.) и убить ее.\n- НАГРАДА: Фрагмент тактики зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBat, Giant (Heartraker)- Things with Wings...\nBESTIAL TOME TACTIC UNLOCK\n\nOSTLAND @ 21.5, 29k or 31, 46k or 39, 40k. Kill this L17 Champ. In the Empire Ch8 town, follow the mts to a champ bat in the trees. It was sort of in the middle of the road between the town and the tower W of it. NOTE: Mob wanders, but will rest at the above locations.\n\nBy Xigence, Wykk, thezenny, Thanat0s, Avale, Getout & Emrus\n20081013",
				},

				{
					name = L"• Beastmen (Title)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Корм для зверей.\n- КООРДИНАТЫ: 21к, 36к.\n- ОПИСАНИЕ: Возле реки у дерева найти Труп охотника на ведьм и осмотреть его.\n- НАГРАДА: Титул Грубый.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Bestigor - Meat for the Beast\nThe Raw\n\nOSTLAND @ 21, 36k. By the Gor PQ for (O), towards the W of the Map. Downhill from the Gor PQ, towards the NW is another small group of Gors and a named NPC who you have to kill for an (O) quest. You'll be near when you see Gashthorn Beastmen instead of Blackmire Beastmen. Just S of them by a tree is a Witchhunter corpse lying between two trees just at the river. Right clicking on it gives you the title.\n\nBy Riggswolfe, Blodsalv, Wykk & scootle\n20081007",
				},

				{   -- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Beastmen (Tactic)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Уменьшения поголовья.\n- КООРДИНАТЫ: 17к, 11к.\n- ОПИСАНИЕ: Найти лагерь Горхов и убить около 60, после чего доложить Регистратору убийств в 6-ю главу Хаоса.\n- НАГРАДА: Фрагмент тактики Хаоса (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastmen, Gor (Darkhorn Gors) - Thin the Herd\nCHAOS TOME TACTIC UNLOCK\n\nOSTLAND @ 17, 11k. Kill Darkhorn Gors. Then head to Felde and talk to the Kill Collector named Gottfried Holz. When you kill enough, you get the unlock.\n\nBy Blodsalv & GreenElite\n20081007",
				},

				{   -- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Beastmen (Tactic)",
					text = L"- Остланд (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Уменьшение поголовья.\n- КООРДИНАТЫ: 20к, 37к.\n- ОПИСАНИЕ: убить 60 горхов и получить награду у регистратора убийств Дроки Седоборода в Босенфельце\n- НАГРАДА: фрагмент тактики хаоса.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nBeastmen, Gor (Darkhorn Gors) - Thin the Herd\nCHAOS TOME TACTIC UNLOCK\n\nOSTLAND @ 20, 37.5k. Go to the Gasthorn Gor camp. Talk to the Kill Collector named Droki Greybeard in Bohsenfels (Ch8 camp). He talks about his friend killing 50 of them, daring you to beat that number, I killed 52 and received the unlock after going back to him.\n\nBy Kellithe\n20081007",
				},

				{
					name = L"• Dryad (Cloak)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Смерть в зеленом и коричневом.\n- КООРДИНАТЫ: 7к, 18к.\n- ОПИСАНИЕ: Найти одиноко бродящую дриаду Шен Мрачнолист среди деревьев и убить ее.\n- НАГРАДА: Шаль весны (Плащь).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDryad (Sheen Gloomleaf)- Death in Green and Brown\n\nCLOAK ITEM UNLOCK\n\nOSTLAND @ 7, 18k. Everyone knows this one. Go to the lake, find the named Dryad, kill it. It's how you get the Shawl of Spring.\n\nBy NicWester, Nazaro & Nerror\n20081007",
				},

				{
					name = L"• Plague Feaster (Tactic)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Мойте руки.\n- КООРДИНАТЫ: 48к, 32к.\n- ОПИСАНИЕ: Недалеко от корзины с рыбой найти бродячего человека Чумная трапеза (побед. 19 ур.) и убить его.\n- НАГРАДА: Фрагмент тактики Хаоса (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPlague Feaster - Wash Your Hands\nCHAOS TOME TACTIC UNLOCK\n\nOSTLAND @ 48.5, 32k. Kill a champ named Plagued Feaster. There's a Basket of Fish that needs to be clicked to spawn the NPC if he isn't there. He's about straight up N of Wolfenburg past the bridge you see NW of Wolvenburg. He wanders along the water in an open area. Kill him for the third Plague Victim unlock.\n\nBy Blodsalv, reggie, scootle & Jschmuch2\n20081007",
				},
				
				{
					name = L"?• Plaguebeast of Nurgle (Title) - не найден",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: След чистого зла.\n- КООРДИНАТЫ: ...\n- ОПИСАНИЕ: ...\n- НАГРАДА: Титул ???.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPlaguebeast of Nurgle - A Trail of Pure Vile Evil",
				},

				{
					name = L"• H. of Tzeentch (Title)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Цвет ужаса.\n- КООРДИНАТЫ: 43к, 52к.\n- ОПИСАНИЕ: В развалинах крепости найти и убить 20 Ужасов тзинча (розовый и синий).\n- НАГРАДА: Титул Страж Этира.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHorror of Tzeentch (Purple Horror) - The Colors of Horror\nAethyr Guard\n\nOSTLAND @ 43, 52. Kill the R20 mob near Wolfenburg in the court with all the horrors. The mobs name is Purple Horror, but the color was blue. NOTE: Frustratingly long spawn timer.\nNOTE: Confirmed reports of Purple Horror spawning when pulling 1 pink and 1 blue horror and killing them at the exact same moment.\n\nBy Blodsalv, Sandkat, notalltogether, ThePedant, Mourngrym, Dguras & Sildroni\n20081124",
				},

				{
					name = L"• Skeleton (Tactic)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Где есть воля ...\n- КООРДИНАТЫ: 31к, 19к или 11к, 29к.\n- ОПИСАНИЕ: Войти в склеп Крипта Герстмана или в склеп Ферлангенское Кладбище и выбить с Костлявых приспешников Выцветший свиток, после чего осмотреть свиток.\n- НАГРАДА: Фрагмент тактики Нежети (1/12).\n\nTEZARIUS, Daris © 2009\n\n————————————————————————————————\n\nSkeleton (Boneshard Minion) - When There's a Will\nUNDEAD TOME TACTIC\n\nOSTLAND @ 30, 18. Go to Gerstman's Crypt and slaughter Boneshard Minions. One of them will drop a Faded Scroll. Right click it and you've got the unlock.\n\nBy NicWester & ThePedant\n20081027",
				},

				{
					name = L"• Skeleton (Tactic)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: У костей тоже есть имена.\n- КООРДИНАТЫ: 49к, 18к.\n- ОПИСАНИЕ: Возле небольшого кладбища найти скелета Хальман Мбюсман (16 ур.) и убить его.\n- НАГРАДА: Фрагмент тактики Нежити (1/12).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSkeleton - Bones Have Names Too\nUNDEAD TOME TACTIC UNLOCK\n\nOSTLAND @ 48, 18k. Kill Halman Meusmann. Best description I can give is follow the road S from Kaussner's Garrison till you see the first bridge. Cross it and close by should be a small camp with skeletons and 2 champ undead, near a camp with bandits which connects to the water. At the skeleton camp is a named normal mob. Kill it.\n\nBy reggie & scootle\n20081007",
				},

				{   -- Order version
					-- realm = TTitan.Data.Realm.ORDER,
					name = L"?• Spirit Host (Pocket) - не исследовано за Дестр",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Время дорого.\n- КООРДИНАТЫ: 48к, 57к (кольцо) и 61к, 60к (могила).\n- ОПИСАНИЕ: Осмотреть труп Джекиля за домом возле реки и взять обручальное кольцо Джекиля, затем отправиться на кладбище и, осмотрев могилу Хильды Бенн, поговорить с появившимся духом с Хильдой Бенн.\n- ПРИМЕЧАНИЕ: Дух появляеться после полудня по серверу и находиться некоторое время.\n- НАГРАДА: Могильная пыль (карман).\n\nTEZARIUS, Чейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nSpirit Host - Time is of the Essence\nPOCKET ITEM\n\n(O) OSTLAND @ 48.5, 57.5. Start by getting a Jekil's Wedding Band off of the Corpse of Jekil Behn in Wolfenberg, behind a house near where the land slopes down to the river. The ring goes in your regular, not quest inventory. Then, travel QUICKLY SE to the graves near the catacombs. Go quickly to Hilde Behn's gravestone at 60k, 60k where she will spawn very briefly. Click on her as you would a quest mob to complete the task and unlock the item: Grave Dust.\nNOTE: If you miss Hilde, she also spawns @ 12:00 p.m. (mid-day) SERVER TIME (type /time) NOTE: Confirmed complete @ 12:45pm GMT, so some wiggle room is given time-wise.\n(D) WARNING: Currently broken.\n\nBy reggie, Riggswolfe, Sique, Avale, Bigskyn, cc2233bb & Xiani\n20081117",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Spite (Title)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: В поисках блестяшек.\n- КООРДИНАТЫ: 14к, 14к.\n- ОПИСАНИЕ: Найти Вредней возле паблика Духи Тени и выбить с них Сияющее кольцо.\n- НАГРАДА: Титул Вредитель вредней.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpite - Looking for Shinnies\nThe Spite Splitter\n\nOSTLAND @ 14,14. Kill Spites for the Shining Ring. Mostly can be found at the PQ Spirits of the Shadow.\n\nBy thezenny & xarinth\n20081124",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"?• Troll (Pocket) - RvR - не найден",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Буть готов.\n- КООРДИНАТЫ: 22.5к, 3.5к или 32.5к, 1.5к или 36.5к, 3.5к.\n- ОПИСАНИЕ: В РвР зоне найти Тролей и добыть с них Обед Тролля.\n- ПРИМЕЧАНИЕ: Выполение возможно только до 21-го уровня. Шанс дропа примерно 5%.\n- АЛЬТЕРНАТИВА: Задание в Бесплодных землях.\n- НАГРАДА: Домашнее животное Котенок Тролля (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll (Heathgut Troll) - Who Is Up for Seconds\nPOCKET ITEM UNLOCK\n\nOSTLAND @ 22.5, 3.5k or 32.5, 1.5k or 36.5, 3.5k. These trolls drop Troll's Dinner. Examine the item to get an unlock for the pocket item: A Troll's Pet Kitty. Drop rate has been nerfed, so get ready for a lot of farming. NOTE: Drop also found off Mudbelly Trolls in BADLANDS: http://www.wardb.com/npc.aspx?id=4693.\n\nBy Impatiens, Archaides & Nerror\n20081009",
				},

				{   -- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Wolf (Cloak)",
					text = L"- ОСТЛАНД (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Собирая куски.\n- КООРДИНАТЫ: 62к, 7к.\n- ОПИСАНИЕ: Найти Волков кровавого тумана и убить 60 штук, после чего доложить Регистратору убийств в 7-й главе Хаоса.\n- ПРИМЕЧАНИЕ: волки бродят у дороги на границе в Страну тролей а также их многовозле паблика Чумная чаща.\n- НАГРАДА: Шкура мастера-охотника (плащь).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWolf (Bloodfog Wolf) - Picking Up The Pieces\nCLOAK ITEM\n\nOSTLAND. Complete the Chaos Ch7 Kill Collector quest in Kournos' Encampment. NOTE: Most are located in Troll Country. Unlocks: Hunter Master's Pelt.\n\nBy Nerror\n20081029",
				},
			},
		},

		{
			name = L"СТРАНА ТРОЛЕЙ • Troll Country",
			entries =
			{
				{	--  Destro version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Bat, Giant (Title)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что-то кроме помета.\n- КООРДИНАТЫ: 11к, 60к.\n- ОПИСАНИЕ: Убить любого нетопыря возле 6-й главы Хаоса и получить нетопырское крыло. Отнести его к Воксана Преобразовательница.\n- АЛЬТЕРНАТИВА: Задание в Топях безумия.\n- НАГРАДА: Титул Охотник на нетопырей.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBat, Giant - For Once It Isn't Guano\nEcho Hunter\n\nTROLL COUNTRY: Kill any bat in Troll Country to net a Bat Wing.\nDeliver to Voxanna the Transmuter in Felde @ 11, 60.\n\nBy NicWester & Elmarra\n20081029",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Bat, Giant (Title)",
					text = L"- Страна Троллей (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Что-то кроме помёта.\n- КООРДИНАТЫ: 27к, 82к.\n- ОПИСАНИЕ: убить любого нетопыря в Стране Троллей и получить нетопырское крыло. Отнести его Бренарду Алхимику в Замок Фельде.\n- АЛЬТЕРНАТИВА: Задание в Топях безумия.\n- НАГРАДА: титул Охотник на нетопырей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nBat, Giant - For Once It Isn't Guano\nEcho Hunter\n\nTROLL COUNTRY: Kill any bat in Troll Country to net a Bat Wing.\nDeliver to Brenard the Alchemist in Felde Castle @ 27, 82.\n\nBy Colloquial & Maehan\n20081029",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Lizard (Tactic)",
					text = L"- Страна Троллей (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Муки голода.\n- КООРДИНАТЫ: 6 глава.\n- ОПИСАНИЕ: убить 60 саламандр-синешеек и получить награду у регистратора убийств в замке Фельде.\n- НАГРАДА: фрагмент тактики зверей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nGiant Lizard - Hunger Pangs\nBESTIAL TOME TACTIC UNLOCK\n\nTROLL COUNTRY. Max out Dieter Stroh (Kill Collector for Ch6) by killing 50 Bluespine Salamanders: http://www.wardb.com/npc.aspx?id=5443 or Blueridge Salamander: http://www.wardb.com/npc.aspx?id=12836\n\nBy armorum\n20081007",
				},

				{
					name = L"• Gorger (Title)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Сомнительная мода.\n- КООРДИНАТЫ: 25к, 11к или 8к, 8к.\n- ОПИСАНИЕ: Найти Косторубов прожор (19 ур.) и выбеть с них Костяное украшение прожоры.\n- НАГРАДА: Титул Безрассудный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGorger - Questionable Fashion\nThe Foolhardy\n\nTROLL COUNTRY. Kill any gorger in Troll Country to loot aGorger Bone Decoration. The Gorgers are mostly on the N and NW mt ranges. Once you have the item use it from your inventory for the unlock.\n\nBy Maehan, Wilken & Elmarra\n20081021",
				},

				{
					name = L"• Plaguebearer (Tactic)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: В поисках нечистой плоти.\n- КООРДИНАТЫ: 55к, 27к.\n- ОПИСАНИЕ: Рядом с пабликом Застава ордена грифона найти гниющий труп сидящий возле дерева за камнем.\n- НАГРАДА: Фрагмент Тактики Демона (1/22).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPlaguebearer (Festering Corpse) - Finding Foul Flesh\nDAEMONIC TOME TACTIC UNLOCK\n\nTROLL COUNTRY @ 55, 27k. Hiding behind a tree NW of the Griffon Outpost PQ for (D). Corpse is half bent over between a rock and a tree; click on it for the unlock.\n\nBy Kargraz & aldrayk\n20081016",
				},

				{
					name = L"• Spider (Title)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Так себе интерьер.\n- КООРДИНАТЫ: 2к, 35к.\n- ОПИСАНИЕ: Осмотреть Пешеры Сускарга.\n- АЛЬТЕРНАТИВА: Задание в Барак Варр или в Норска.\n- НАГРАДА: Титул Убийца Пауков.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpider, Giant - Tacky Decor\nSpider-Slayer\n\nTROLL COUNTRY @ 2, 35k. Habitat unlock when you enter the Suskarg Caves.\n\nBy LatukiJoe\n20081007",
				},

				{
					name = L"• Spite (Tactic)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Бюро находок.\n- КООРДИНАТЫ: 9к, 46к.\n- ОПИСАНИЕ: Найти вредня Элориан (16 ур.) и убить его.\n- АЛЬТЕРНАТИВА: Задание в Высоком перевале.\n- НАГРАДА: Фрагмент тактики Магических существ (1/10).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpite (Elorian) - Lost and Found\nMYTHICAL TOME TACTIC UNLOCK\n\nTROLL COUNTRY @ 8.5, 46k. SW around the Wart Trolls there's a single Spite. Kill it.\n\nBy NicWester, Blodsalv, Royalflush & FireOpal\n20081007",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,	
					name = L"• Troll (Title)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Тролльи байки.\n- КООРДИНАТЫ: 62к, 29к.\n- ОПИСАНИЕ: Найти среди скал Авальдера и поговорить с ним.\n- НАГРАДА: Титул Троллист.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll - Troll Tales\nTrollwise\n\n(D) TROLL COUNTRY @ 61.5, 29. Talk to Afvaldr.\n(O) SAME @ 26, 41. Talk to Walbrecht Veit. Out of sight, behind the rocks just to the right of the entrance to the top camp at Griffon Outpost PQ. NOTE: Also confirmed near the deathzone quarry with all the furys/horrors/flamers.\n\nBy Insubstantial, Culland, dragonbait & Kriegzwerg\n20081021",
				},
				
				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,	
					name = L"• Troll (Title)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Тролльи байки.\n- КООРДИНАТЫ: 26к, 41к.\n- ОПИСАНИЕ: Найти среди скал Вальбрехт и поговорить с ним.\n- НАГРАДА: Титул Троллист.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll - Troll Tales\nTrollwise\n\n(D) TROLL COUNTRY @ 61.5, 29. Talk to Afvaldr.\n(O) SAME @ 26, 41. Talk to Walbrecht Veit. Out of sight, behind the rocks just to the right of the entrance to the top camp at Griffon Outpost PQ. NOTE: Also confirmed near the deathzone quarry with all the furys/horrors/flamers.\n\nBy Insubstantial, Culland, dragonbait & Kriegzwerg\n20081021",
				},

				{
					name = L"• Troll (Tactic)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Попался!\n- КООРДИНАТЫ: 9к, 5к.\n- ОПИСАНИЕ: На самом верху ледяного покрова найти тайную пешеру возле которой бродит троль Хладокус (побед. 19 ур.). Убить троля.\n- НАГРАДА: Фрагмент тактики Великанов (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll, Stone (Coldsnap) - It's a Snap\nGIANT TOME TACTIC\n\nTROLL COUNTRY @ 9.5, 5. Kill the champ troll. NOTE: Need killing blow for the unlock.\n\nBy NicWester, Supaplex, Kellithe, Burgerderby & Shadyjane\n20081027",
				},

				{
					name = L"• Troll (Title)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Пропал, но не забыт.\n- КООРДИНАТЫ: 3к, 44к.\n- ОПИСАНИЕ: Найти  Бородавочных тролей возле пещеры Троллья Яма и добыть с у них Потерянный медальон.\n- ПРИМЕЧАНИЕ: шанс дропа всего 5%.\n- НАГРАДА: Титул Неустрошимый.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll, Chaos - Gone But Not Forgotten\nThe Undaunted\n\nTROLL COUNTRY. While killing Wart Trolls near The Troll Pit cave, one dropped a piece of loot titled Lost Locket. Examining it yields this unlock. NOTE: May take a long time for the drop. Scavenging seems to yield it faster.\n\nBy burfo, Wykk & Avale\n20081021",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Troll (Tactic)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Поехали!\n- КООРДИНАТЫ: 50к, 47к или 50к, 50к или 55к, 48к.\n- ОПИСАНИЕ: Найти на болоте кучку Троллий помет и вытащить Помет речного тролля. Отнести его к Воксана Преобразовательница в 6-ю главу Хаоса.\n- НАГРАДА: Фрегмент тактики Великанов (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll, River - Gotta Go\nGIANT TOME TACTIC\n\nTROLL COUNTRY @ 50, 47 or 50.5, 50.5 or 55, 48. Pick up River Troll Droppings.\nDeliver to Voxanna the Transmuter in Felde @ 11, 60.\n\nBy khelben, Sandkat, Mulesh, ThePedant & Xiani\n20081106",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Troll (Tactic)",
					text = L"- Страна Троллей (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Поехали!\n- КООРДИНАТЫ: 50к, 47к.\n- ОПИСАНИЕ: Взять троллиный помёт и отнести Бренарду Алхимику в замок Фельде (6 глава).\n- НАГРАДА: фрагмент тактики великанов.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nTroll, River - Gotta Go\nGIANT TOME TACTIC\n\nTROLL COUNTRY @ 50, 47 or 50.5, 50.5 or 55, 48. Pick up River Troll Droppings.\nDeliver to Brenard the Alchemist in Felde Castle @ 27, 82.\n\nBy khelben, Sandkat, Mulesh, ThePedant & Xiani\n20081106",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Troll (Title) проверить",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Повсеместное протухание.\n- КООРДИНАТЫ: 57к, 36к.\n- ОПИСАНИЕ: Исследовать територию.\n- НАГРАДА: Титул Истребитель речных троллей.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll, River  - Mass Exstinktion\nRiver Troll Ravager\n\nTROLL COUNTRY @ 57, 35.5. Walk into the entry of the Griffon Outpost PQ.\n\nBy NicWester, Squarkk, scootle & xarinth\n20081110",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Troll (Title)",
					text = L"- Страна Троллей (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Повсеместное протухание.\n- КООРДИНАТЫ: 52к, 55к.\n- ОПИСАНИЕ: исследовать эту зону для получения анлока.\n- НАГРАДА: титул Истребитель речных троллей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nTroll, River  - Mass Exstinktion\nRiver Troll Ravager\n\nTROLL COUNTRY. Go N of the monster habitat to the Griffon Camp PQ. Wandering here should get it for you. If it doesn't, then near The Blighted Farm PQ (on a cliff above the Griffon Camp) I fell off the SE face of the cliff, into the camp. While falling I got the unlock.\n\nBy NicWester, Squarkk, scootle & xarinth\n20081110",
				},

				{
					name = L"• Troll (Pocket)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Стоит того.\n- КООРДИНАТЫ: 54к, 37к или 56к, 23к.\n- ОПИСАНИЕ: Найти и убить под мостом Речного троля.\n- НАГРАДА: Мерзкий котелок (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll, River (Bridge Troll) - Takes its Toll\nPOCKET ITEM\n\nTROLL COUNTRY @ 54, 37 or 56, 23. The mob spawns under one of the two bridges. Kill it for the item: Revolting Crock.\n\nBy Nogglli, NicWester & Squarkk\n20081103",
				},

				{
					name = L"• Troll (Tactic)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Туалетная вода.\n- КООРДИНАТЫ: 56к, 47к (бочка) 56к, 44к (тролль).\n- ОПИСАНИЕ: В лагере гоблинов в болотах найти Бочку духов. После чего найти троля Круши зуб (побед. 15 ур.) и убить его.\n- НАГРАДА: Франмент Тактика великанов (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll, River  (Brackmaw) - O'de Toilet\nGIANT TOME TACTIC\n\nTROLL COUNTRY @ 56, 47. First, near the Troll hunting goblin camp pick up a Barrel of Perfume. Then kill the mob @ 56, 44k.\n\nBy Kellithe, vahnkiljoy, Wykk, Squarkk & Spasmbot\n20081103",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Troll (Title)",
					text = L"- СТРАНА ТРОЛЕЙ (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Бурчит в животе.\n- КООРДИНАТЫ: 12к, 31к (троли) 11к, 60к (нпс).\n- ОПИСАНИЕ: Найти и убить тролей 11-го уровня и добыть с них 5 Нетронутых тролленых желудков. После вернуться в город Фельде 6-я глава хаоса и поговорить с Воксана Преобразовательница.\n- НАГРАДА: итул Гроза камней.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTroll, Stone  (Stormblight Dolt/Brute) - Uneasy Stomach\nThe Boulderbane\n\nTROLL COUNTRY @ 12, 31. Kill this mob. Collect multiple Intact Troll Stomachs. Confirmed return at 5 stomachs, official number may be less.\n\nDeliver to Voxanna the Transmuter in Felde @ 11, 60.\nBy NicWester, Nozaro, Spasmbot & Xiani\n20081106",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Troll (Title)",
					text = L"- Страна Троллей (Тир 2).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Бурчит в животе.\n- КООРДИНАТЫ: 13к, 31к.\n- ОПИСАНИЕ: убить 5 троллей поветрия, собрать 5 желудков (автоматически помещаються в инвентарь при убийстве тролля). Затем отнести желудки Бренарду Алхимику в Замок Фельде.\n- НАГРАДА: титул Гроза камней\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nTroll, Stone  (Stormblight Dolt/Brute) - Uneasy Stomach\nThe Boulderbane\n\nTROLL COUNTRY @ 12, 31. Kill this mob. Collect multiple Intact Troll Stomachs. Confirmed return at 5 stomachs, official number may be less.\nDeliver to Brenard the Alchemist in Felde Castle @ 27, 82.\n\nBy NicWester, Nozaro, Spasmbot & Xiani\n20081106",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 3 •••",
			divider = true,
			entries = {},
		},

		{
			name = L"ТАЛАБЕК • Talabecland",
			entries =
			{
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Beastman (Tactic)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Отвратительная мода.\n- КООРДИНАТЫ: 48к, 42к.\n- ОПИСАНИЕ: Найти и убить Адепт-охотник на ведьм (побед. 9 ур.) и осмотреть полученное Ожерелье свирепости.\nили\n- КООРДИНАТЫ: 55, 35.\n- ОПИСАНИЕ: поговорить с Казимир пор.\n- НАГРАДА: Фрагмент тактике Хаоса (фрагмент 1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Bestigor - Appalling Fashion\nCHAOS TOME TACTIC\n\nTALABECLAND @ 55, 35 or 48, 42. Confirmed at 1st coords by right clicking Casimir Por in the Beastman area E of the Herdstone. At 2nd coords, killing blow the L9 champ Witch Hunter Adept and he will give you the necklace that you click on for the unlock.\n\nBy Blodsalv, armorum, Archaides, Kellithe & Shaz\n20081124",
				},
				
				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Beastman (Tactic)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Отвратительная мода.\n- КООРДИНАТЫ: 49к, 39к.\n- ОПИСАНИЕ: Выбить из Унгорх Черного Рога Унгорхский талисман и активируйте его в инвентаре.\n- НАГРАДА: Фрагмент Тактики Хаоса (фрагмент 1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Bestigor - Appalling Fashion\nCHAOS TOME TACTIC\n\nTALABECLAND @ 48.5, 39. Head to the Blackhorn Heardstone. Kill them to receive a Grisly Necklace. Beastman Bestigors have the best droprate. Right click it for the unlock.\n\nBy Blodsalv, armorum, Archaides, Kellithe & Shaz\n20081124",
				},

				{
					name = L"• Beastman (Tactic)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Отвратительная мода.\n- КООРДИНАТЫ: 49к, 39к.\n- ОПИСАНИЕ: Найти и убить Унгорх Черного Рога (27 ур.), чтобы получить 10шт Унгорхский талисман, которые нужно продать любому торговцу.\n- НАГРАДА: Фрагмент Тактики Хаоса (фрагмент 1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Ungor - Trinkets for Treasure\nCHAOS TOME TACTIC\n\nTALABECLAND @ 48.5, 39. Kill L27 Blackhorn Ungors near the Blackhorn Herdstone. Gives an Ungor Talisman. Sell it to a merchant. Works for both.\n\nBy Blodsalv & armorum\n20081021",
				},

				{
					name = L"• Chaos Mutant (Title)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Угадайка.\n- КООРДИНАТЫ: 23к, 32к.\n- ОПИСАНИЕ: Добыть Руки мутантов в количестве 10 шт падающую с Могильных мутантов. Територия возле 10-й глава хаос и Поместья штайнбрюка.\n- НАГРАДА: НАГРАДА: титул Незапятнанный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Mutant - Handy Dandy\nThe Unsullied\n\nTALABECLAND @ 23k, 32k. Kill 10 Deathbed or Grave Mutants for 10 of their Mutant Hands. ADDENDUM: The drop rate for hands is not 100%, but my friend and I still got them soon enough.\n\nBy Blodsalv, ThePedant & TorqueVolkmar\n20081023",
				},

				{
					name = L"• Plaguebearer (Tactic)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Лети и умри.\n- КООРДИНАТЫ: 36к, 51к.\n- ОПИСАНИЕ: найти возле реки Сеятель болезни (побед. 29 ур.) и убить его.\n- НАГРАДА: фрагмент Тактика демонов (1/22).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPlaguebearer (Blight Spew) - Let It Fly or Let It Die\nDAEMONIC TOME TACTIC UNLOCK\n\nTALABECLAND @ 36, 51k. Where the river curves into a (U) shape kill this L29 champ.\n\nBy Kellithe, armorum & Crucifix\n20081014",
				},

				{
					name = L"• Plague Victim (Pocket)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Побочные эффекты: мутация и смерть.\n- КООРДИНАТЫ: 5к, 43к.\n- ОПИСАНИЕ: Убить или отвлеч 2-х Чумных стражей (побед. 29 ур.), чтобы открыть Сундук Мора.\n- НАГРАДА: Бочка с элем (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPlague Victim - Side Effects Include Mutation and Death\nPOCKET ITEM\n\nTALABECLAND @ 5, 43. A Chest is guarded by 2 L29 champ guards. Opening the chest unlocks item: Barrel of Ale. NOTE: Acquiring item from a librarian may be bugged. You receive the item Filthy Hanky instead.\n\nBy Thovargh, armorum, JodouTR & Vaginitus\n20081022",
				},

				{
					name = L"• Skaven (Tactic)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Какое уютное крысиное гнездышко.\n- КООРДИНАТЫ: 30к, 16к.\n- ОПИСАНИЕ: На самом верху рогатой башни на крыше активировать Крысючее знамя.\n- НАГРАДА: Фрагмент тактике Крысолюдов (1/3).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSkaven - What a Rat's Nest\nSKAVEN TOME TACTIC\n\nTALABECLAND @ 30, 16. In the Horned Tower are mostly L23-34 Skaven. Go all the way to the very top and circle around the balcony. There is a flag/banner, click it and bam, you have the unlock: Queek Reek\n\nBy Kellithe, Ningiliath & Sandkat\n20081029",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Vulture (Pocket)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Заряженное зерно.\n- КООРДИНАТЫ: 4к, 44к.\n- ОПИСАНИЕ: Найти Гнилоклювых стервятников (29 ур.) и добыть с них Клюв гнилоклюва. Поговорить с целителем в лагере 11-й главы хаоса.\n- НАГРАДА: Перышко падали (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nVulture (Blightbeak Vultures/Blightbeak Carrion) - Diseased Pecking Order\nPOCKET ITEM\n\nTALABECLAND @ 4, 44. Near the S side of the river these L29 mobs drop a Blightbeak Beak. Unlocks: Carrion Tickler. Turn in to the healer NPC in chaos Ch11. Requires More Than Just a Foul Feather\n\nBy Thovargh, Mabuz, Shattered, Caliber & Almaram\n20081021",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Vulture (Pocket)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Заряженное зерно.\n- КООРДИНАТЫ: 4к, 44к.\n- ОПИСАНИЕ: Найти Гнилоклювых стервятников (29 ур.) и добыть с них Клюв гнилоклюва. Поговорить с целителем в лагере 14-й главы империи.\n- НАГРАДА: Перышко падали (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nVulture (Blightbeak Vultures/Blightbeak Carrion) - Diseased Pecking Order\nPOCKET ITEM\n\nTALABECLAND @ 4, 44. Near the S side of the river these L29 mobs drop a Blightbeak Beak. Unlocks: Carrion Tickler. Turn in to the healer in (O) Ch14.\n\nBy Thovargh, Mabuz, Shattered, Caliber & Almaram\n20081021",
				},

				{
					name = L"• Vulture (Tactic)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Больше чем просто ложка дегтя.\n- КООРДИНАТЫ: 4к, 44к.\n- ОПИСАНИЕ: Найти  Чумнокрыл (побед. 29 ур.) и убить его.\n- НАГРАДА: фрагмент Тактика зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nVulture (Plaguefeather) - More Than Just a Foul Feather\nBESTIAL TOME TACTIC\n\nTALABECLAND @ 4, 44. Near the W map border, near the S side of the river. Killing blow this Lvl29 champ.\n\nBy Thovargh & Cahyr\n20081105",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Wolf (Tactic)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Растерзанный вожак.\n- КООРДИНАТЫ: 45к, 8к.\n- ОПИСАНИЕ: Возле Фольгена 12-й главы хаоса найти и убить волка Соренна (26 ур.).\n- НАГРАДА: фрагмент Тактики зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWolf (Sorenna) (Lishka) - Alpha in Bits\nBEASTIAL TOME TACTIC UNLOCK\n\nTALABECLAND. Kill this L26 wolf just SE of Volgen, the Chaos Ch12 camp.\n\nBy Supaplex & Kellithe\n20081013",
				},

				{
					name = L"• Zombie (Title)",
					text = L"- ТАЛАБЕК (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Прогулки с нежитью.\n- КООРДИНАТЫ: 38к, 38к.\n- ОПИСАНИЕ: Исследовать територию обитания мутантов.\n- НАГРАДА: Титул Окровавленный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nZombie - Dare to Tread With the Undead\nThe Gory\n\nTALABECLAND @ 38.5, 38k. Explore the graveyard.\n\nBy Blodsalv\n20081007",
				},
			},
		},

		{
			name = L"ВЫСОКИЙ ПЕРЕВАЛ • High Pass",
			entries =
			{
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"?• Tuskgor (Tactic) - не найден", -- Beastman, Bestigor
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Сохраняя лицо.\n- КООРДИНАТЫ: 41к, 46к.\n- ОПИСАНИЕ: Найти Бивнегорхов и добыть 5 бивней.\n- ПРИМЕЧАНИЕ: Шанс выпадения не больше 5%.\n- НАГРАДА: Фрагмент тактики Хаоса (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTuskgor - Saving Face\nCHAOS TOME TACTIC\n\n(D) HIGH PASS @ 41, 46. Gather Unchipped Tusks by killing Bloodsnout Tuskgors in HIGH PASS.\n\nBy Mourngrym, Socran, Karylet & Grishnar\n20090107",
				},

				{
					name = L"• Beastman (Tactic)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Заноза.\n- КООРДИНАТЫ: 43к, 47к.\n- ОПИСАНИЕ: Убить зверолюда Гуррвек Короткорог (побед. 25 ур.).\n- НАГРАДА: Фрагмент тактика Хаоса (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Ungor (Gurrvek Shorthorns) - Thorn in My Side\nCHAOS TOME TACTIC\n\nHIGHPASS @ 46, 50. Kill this L25 champ. Confirmed to spawn due N of Lishka from the wolf entry. NOTE: May be on a long timer, or have multiple spawn points.\n\nBy Alesinde, Crucifix & Coriolis\n20081030",
				},

				{
					name = L"• B. of Khorne (Title)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Поистине жуткие новости.\n- КООРДИНАТЫ: 38к, 41к.\n- ОПИСАНИЕ: Поговорить с Трупом имперского гонца.\n- НАГРАДА: титул Наследник войны.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBloodthirster of Khorne (Empire Runner Corpse) - Dire News Indeed\nHeritor of War\n\nHIGH PASS @ 38, 41k. Near the bottom of the cliffs is a corpse. Interact with it to get the unlock.\n\nBy Wilken & Shawen\n20081007",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Chaos Fury (Title)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Мерзкая мощь.\n- КООРДИНАТЫ: 52к, 50к.\n- ОПИСАНИЕ: Найти гневника Чекил Подлый (герой 25 ур.) и поговорить с ним.\n- НАГРАДА: Титул Губитель Искажения.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Fury (Zchekil the Vile) - Nefarious Force\nThe Warpbinder\n\nHIGH PASS: Simply talk to Zchekil the Vile by right-clicking him.\n\nBy Foggye & Sandkat\n20081104",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Chaos Fury (Title)",
					text = L"- Высокий перевал (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Мерзкая мощь.\n- КООРДИНАТЫ: 51к, 50к.\n- ОПИСАНИЕ: убить Чекила Подлого (герой, 25 ур.).\n- НАГРАДА: титул Губитель Искажения.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nChaos Fury (Zchekil the Vile) - Nefarious Force\nThe Warpbinder\n\nHIGH PASS @ 51, 50. Kill this L25 Hero Chaos Fury. Appears to be a slow or rare spawn. NOTE: May have multiple spawn locations.\n\nBy Foggye & Sandkat\n20081104",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Chaos Hound (Title)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Искатель в снегу.\n- КООРДИНАТЫ: 35к, 33к.\n- ОПИСАНИЕ: Поговорить с регистратором убийств Калтеа Змиекрад в лагере Хаоса глава 13.\n- НАГРАДА: Титул Покусанный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Hound (Bradiclaw the Seeker) - Seeker in the Snow\nThe Gnawed\n\nHIGH PASS: Talk to Karl Oromson in Bloodmarr.\n\nBy Blodsalv & Mabuz\n20081029",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Chaos Hound (Title)",
					text = L"- Высокий перевал (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Искатель в снегу.\n- КООРДИНАТЫ: 56к, 50к.\n- ОПИСАНИЕ: убить Широколапа Ищейку (герой, 25 ур.).\n- НАГРАДА: титул Покусанный.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nChaos Hound (Bradiclaw the Seeker) - Seeker in the Snow\nThe Gnawed\n\nHIGH PASS @ 56, 50. Kill this L25 hero E of Vladesaul.\n\nBy Smilliam, Thanat0s & Akiu\n20081029",
				},

				{
					name = L"• Chaos Spawn (Title)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: За гранью понимания.\n- КООРДИНАТЫ: 16к, 30к или 15к, 42к.\n- ОПИСАНИЕ: На горах найти Шестой дневник Ллиана.\n- НАГРАДА: Титул Безмятежный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Spawn - Beyond Definition\nThe Serene\n\nHIGH PASS @ 16, 30k or 28, 30k or 14.5, 42k. There is a red and black book on the mt titled The 6th Journal of Llian. On pop spot is between a stand of 3 trees, and there's a horn hanging from a rope off one of the branches of the tree. NOTE: Book de-spawns on pickup and can respawn at any of the above locations.\n\nBy vassago1488, Medea, Xururuca, Toodlepips & Greggar\n20081007",
				},

				{
					name = L"• Spite (Tactic)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Бюро находок.\n- КООРДИНАТЫ: 57к, 12к.\n- ОПИСАНИЕ: найти в пещере Вредня Элориан (16 ур.) и убить его.\n- АЛЬТЕРНАТИВА: Задание в Стране Тролей.\n- НАГРАДА: Фрагмент тактики Магических существ (1/10).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpite (Elorian) - Lost and Found\nMYTHICAL TOME TACTIC\n\nHIGH PASS @ 58, 16. Kill this L16 mob.\n\nBy Drunklight, Sandkat, vasil & Josemite\n20081023",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Flayerkin (Title)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Одна часть агонии, три части холодного железа.\n- КООРДИНАТЫ: 5к, 39к (склеп 1.66к, 41,52к (живодер)), 12к, 49к (пещера), 47к, 33к (крепость), 6к, 10к (нпс).\n- ОПИСАНИЕ:\n    1) Войти в Гробницу предателей и найти там тело Мертвого живодера, обыскав его получите Живодерные преобразовательные цепи.\n    2) На вершине холма войти в пещеру и найти там Мертвого живодера, обыскав которого получите Живодерные преобразовательные крючья.\n    3) Внутри расколотой крепости найти Мертвого живодера, обыскав которого получите Живодерский железный шлем. После чего сходить в Прааг в Штурм Корзах 17 глава хаоса и доложиться крысолюду Крилик из клана Литейщика.\n- НАГРАДА: Титул Живодер.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nFlayerkin - One Part Agony, Three Parts Cold Wought Iron\nThe Flayer\n\nHIGH PASS.\mPART 1: 3, 38 is where the entrance to the Tomb for the chain is. Find the item on a corpse called Dead Flayerkin inside the Tomb of the Traitor PQ. The loot was Flayer-Kin's Scaling Chains.\nPART 2: 10, 49 is where to find Flayer-Kin's Scaling Hooks. Go around the mt and swoop back when coming from the S. Inside a cave is a Flayerkin corpse has the hooks.\nPART 3: 47, 33 in WEST PRAAG. Find the Flayer-Kin's Iron Helm on a flayerkin corpse.\nTurn in to Kreelik of Clan Moulder @ 6, 10 in PRAAG, Chaos Ch17 camp. (Blodsalv)\n\nBy Kellithe, Blodsalv, Wilken & Thovargh\n20081022",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Flayerkin (Title)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Одна часть агонии, три части холодного железа.\n- КООРДИНАТЫ: 5к, 39к (склеп 1.66к, 41,52к (живодер)), 12к, 49к (пещера), 47к, 33к (крепость), 13к, 57к (нпс).\n- ОПИСАНИЕ:\n    1) Войти в Гробницу предателей и найти там тело Мертвого живодера, обыскав его получите Живодерные преобразовательные цепи.\n    2) На вершине холма войти в пещеру и найти там Мертвого живодера, обыскав которого получите Живодерные преобразовательные крючья.\n    3) Внутри расколотой крепости найти Мертвого живодера, обыскав которого получите Живодерский железный шлем. После чего сходить в Прааг в 17 глава Империи и доложиться Мабусу.\n- НАГРАДА: Титул Живодер.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nFlayerkin - One Part Agony, Three Parts Cold Wought Iron\nThe Flayer\n\nHIGH PASS.\nPART 1: 3, 38 is where the entrance to the Tomb for the chain is. Find the item on a corpse called Dead Flayerkin inside the Tomb of the Traitor PQ. The loot was Flayer-Kin's Scaling Chains.\nPART 2: 10, 49 is where to find Flayer-Kin's Scaling Hooks. Go around the mt and swoop back when coming from the S. Inside a cave is a Flayerkin corpse has the hooks.\nPART 3: 47, 33 in WEST PRAAG. Find the Flayer-Kin's Iron Helm on a flayerkin corpse.\nTurn this chain in to Breitenhoff the Faithful in Ch17 Empire, in Praag @ 17, 45. NOTE: The coords are 13, 57 for the turn in. (Mabuz)\nNOTE: Confirmed Unlock with only having the chain from Part1 and then talking to the healer. (by Thunderblaze)\n\nBy Kellithe, Blodsalv, Wilken & Thovargh\n20081022",
				},

				{
					-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Gnoblar (Title)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Разношерстная компания.\n- КООРДИНАТЫ: 18к, 4к.\n- ОПИСАНИЕ: Исследовать територию обитания Гноблеров.\n- НАГРАДА: Титул Задира.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGnoblar - Mixed Company\nThe Bully\n\nHIGH PASS @ 18, 3.8 to 23, 3. Habitat unlock by running through several tents with Ogres and Gnoblars.\n\nBy Blodsalv, Avale & Yvarma\n20081029",
				},

				{
					name = L"• Gorger (Token)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Атаковать прожору.\n- КООРДИНАТЫ: 18к, 8к.\n- ОПИСАНИЕ: Найти и убить снежного йети Кровосмрад (побед. 29 ур.).\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGorger (Bloodscent) - Engage the Gorger\nBESTIAL TOKEN UNLOCK\n\nHIGH PASS @ 18, 7.5k. Kill this lvl29 Champ.\n\nBy Sandkat, Thanat0s & Medea\n20081007",
				},

				{	-- Order entry
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Great Cat (Title)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: В погоне за Рольфом.\n- КООРДИНАТЫ: 4к, 28к.\n- ОПИСАНИЕ: Убить 60 больших кошек и доложиться Регистратору убийств.\n- НАГРАДА: Титул Поцарапанный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGreat Cat (Snarling Razortooth) - A Run for Rolf\nThe Clawed\n\nHIGH PASS. Complete Rolf Grimwold's Kill Quest quota for the unlock. Mobs can be found along the pass between Nuhr's Crest and Raven's End.\n\nBy Kutzum & TorqueVolkmar\n20081023",
				},

				{
					name = L"• Great Unclean (Title)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Нечистые.\n- КООРДИНАТЫ: 26к, 41к.\n- ОПИСАНИЕ: Около маленького замерзшего озера найти прохожего Старый Хенрик который будет собирать хворост. Осмотрите котел Горшок Хенрика.\n- ПРИМЕЧАНИЕ: прохожий появляеться ненадолго и в определенные части времени.\n- НАГРАДА: Титул Глашатый Жизни.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGreat Unclean One (Old Hienrich) - The Unclean\nHerald of the Vital\n\nHIGH PASS @ 26, 41. Near a small frozen lake SW of the Chaos Ch13 camp Bloodmarr an NPC directs you to his Hienrich's Stew pot which grants the unlock when used. NOTE: NPC is not always there, so this may be time or circumstance dependent, but once he's up he is confirmed to stay up for quite a while.\nSIGHTINGS:\nSaturday @ 4:34 EST, in-game: 20:10\nSaturday @ Noon CST, in-game: 20:34\n3:20 PM GMT, in-game: 16:20\n8:30 PM CST; in-game 7:00\n\nBy Houyoko, Zarlemagne, Lorem Ipsum, koan, JodouTR & Avale\n20081028",
				},

				{
					name = L"• Ogre, Bull (Tactic)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Снаружи безопаснее.\n- КООРДИНАТЫ: 5к, 35к.\n- ОПИСАНИЕ: Исследовать место обидание огров.\n- НАГРАДА: Фрагмент тактики Великанов (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nOgre, Bull - It's Safer Outside\nGIANT TOME TACTIC UNLOCK\n\nHIGH PASS @ 5, 34.5k. Habitat unlock N of the PQ Tomb of the Traitor near a camp full of Ogre Bulls.\n\nBy carbonara & xentrix\n20081013",
				},

				{
					name = L"• Ogre, Bull (Tactic)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Они больше.\n- КООРДИНАТЫ: от 9к, 44к до 5к, 45к.\n- ОПИСАНИЕ: Найти гуляющего в доль дороги огра Рухляк Людоед (побед. 23 ур.) и убить его.\n- НАГРАДА: Фрагмент тактики Великанов (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nOgre, Bull (Marl the Maneater) - Bigger They Are...\nGIANT TOME TACTIC\n\nHIGH PASS @ 9, 44 or @ 5k, 45. Kill this L23 champ. He patrols the road.\n\nBy Kellithe, Xigence & Pingping\n20081124",
				},

				{
					name = L"• Warhawk (Pocket)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Сварить, взболтать или поджарить?\n- КООРДИНАТЫ: 7к, 45к (вход), 9к, 44к и 7к, 45к и 5к, 43к и 4к, 45к и 8к, 55к (яйца).\n- ОПИСАНИЕ: Взобраться на гору и найти 5 яиц боевого ястреба.\n- ПРИМЕЧАНИЕ: Одно яйцо может быть защитано 1 раз. Для поиска используйте орлов там где они кружаться и можно найти яйца.\n- НАГРАДА: Птичье семя (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWarhawk - Poached, Scrambled, or Fried\nPOCKET ITEM\n\nHIGH PASS @ 9, 44 or 4.5, 42 or 6, 53.5. Gather 5 Warhawk eggs near the trolls, and select the items in your inventory after. Unlocks item: Bird Seed. NOTE: Only 1 egg will spawn at a time and always will spawn near a moltclaw eagle. All the warhawks are on a range of mts accessible at the mt that you get up via a ledge @ 6.5, 44.5 and jumping across to near the Yhetee. NOTE: There are more spawn points than listed just look for the hawks to find the eggs.\n\nBy Amisted, Pingping, Wilken & Dracool\n20081022",
				},

				{
					name = L"• Wight (Cloak)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Сломанные люди, сломанные клинки.\n- КООРДИНАТЫ: 35к, 25к.\n- ОПИСАНИЕ: Найти в разрушенном замке скелетов воинов и выбить с них 5 Сломанных клинков умертвия.\n- ПРИМЕЧАНИЕ: Шанс выпадения кинжалов всего 5-10%.\n- НАГРАДА: Накидка смертного савана (плащь).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWight (Kulian Bonewarriors/Bonearchers) - Broken Men, Broken Blades\nCLOAK ITEM\n\nHIGH PASS @ 34, 25. Collect 5 Broken Wight Blade from these skeletons in the Keep of Asavar Kul to unlock the Death Shroud Cape.\n\nBy Blodsalv & Wykk\n20081022",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Wolf (Tactic)",
					text = L"- Высокий перевал (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Растерзанный вожак.\n- КООРДИНАТЫ: 46к, 51к.\n- ОПИСАНИЕ: убить волка Лишку.\n- НАГРАДА: фрагмент тактики зверей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nWolf (Lishka)- Alpha in Bits\nBESTIAL TOME TACTIC\n\nHIGH PASS @ 46, 51. Kill this grey named that roams the cliffs overlooking the PQ grisley herd.\n\nBy Nilenya, Wilken, Crucifix & Kellithe\n20081029",
				},
				
				{
					name = L"• Yhetee (Title)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Воин, твоя жизненная сила на исходе.\n- КООРДИНАТЫ: 25.7к, 27.5к.\n- ОПИСАНИЕ: Найти в озера Тело утопленника и осмотреть его. Лучше всего он виден в дневное время суток.\n- НАГРАДА: Титул Оттаявший.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nYhetee - Warrior, Your Life Force is Running Out\n25.6, 27.4. Blue drowned body under the water, closer to the frozen side. To find it, stand on the true S side of the black hills and face the frozen side (N), on your mini map you will see the part of the ice flow that bulges outwardly into the lake. Exactly E of the S tip (of the bulge) is the body. Another landmark would be referenced by the bulge as well. The first little 'crack' in the ice that is E of the bulge. Go roughly S of there. Slowly click (do not move your mouse while clicking or you'll just rotate the camera and war wont register the click) NOTE: Daytime may be a factor in finding the body and 'thawing' him out.\nReward\nTitle - The Defrosted",
				},


				{
					name = L"• Yhetee (Cloak)",
					text = L"- ВЫСОКИЙ ПЕРЕВАЛ (Тир 3).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Названный правильно.\n- КООРДИНАТЫ: 6.5к, 45к (вход), 8к, 44к (монстр).\n- ОПИСАНИЕ: Найти на вершине холма снежного йети Лютый мороз (побед. 21 ур.) и убить его.\n- ПРИМЕЧАНИЕ: Взобраться на гору можно нелалеко от холма.\n- НАГРАДА: Кожа лютого мороза (плащь).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nYhetee (Killing Frost) - Appropriately Named\nCLOAK ITEM\n\nHIGH PASS @ 8, 44 . Near T3 (O) Ch10, kill a yeti champ on a mt along the road S of the (O) Ch10 town. It's on top of the first snowy mountain when the ground starts to change. It'll be on your left, if you're heading S and eagles are visible on the ledges. It unlocks Hide of Killing Frost.\nNOTE: The mts are accessible via a ledge at 6.5, 44.5 and jumping across left near the Yhetee.\n\nBy Colloquial, Impatiens, Nilenya & Wilken\n20081029",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• тир 4 •••",
			divider = true,
			entries = {},
		},
		

		{
			name = L"ПРААГ • Praag",
			entries =
			{
				{
					name = L"• Bear (Pocket)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Продвигаясь вперед.\n- КООРДИНАТЫ: 58к, 18к.\n- ОПИСАНИЕ: Возле гор в маленькой пещере убить медведя Смертобой (побед. 36 ур.).\n- НАГРАДА: Лапа гибельной утробы (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBear (Deadmaul) - Getting Ahead\nPOCKET ITEM\n\nPRAAG @ 58, 18. Kill this L36 champ bear inside a cave. Unlocks Deathmaw Paw.\n\nBy Thanat0s & furnaps\n20081106",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Bear (Tactic)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: В направлении наживы.\n- КООРДИНАТЫ: 58к, 18к.\n- ОПИСАНИЕ: Возле маленькой пещеры найти Медведей-гризли (37 ур.) и выбить с них Лапа Смертобоя. Лучше всего она выбиваеться с самого Смертобоя живущего в пещере. Для активации анлока продайте лапу любому торговцу.\n- НАГРАДА: Фрагмент Тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBear (Ursun Bear/Deadmaul) - Heading for Profit\nBESTIAL TOME TACTIC\n\nPRAAG. Kill these mobs at the Ursun's Den (middle of E map border) to get a Deadmaul's Paw. Sell this to a merchant for unlock.\n\nBy Supaplex, Josemite & skyrunner620\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Bear (Tactic)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Продвигаясь вперёд.\n- КООРДИНАТЫ: 59к, 19к.\n- ОПИСАНИЕ: Убить Смертобоя (37 ур., победоносец) в пещере.\n- НАГРАДА: Фрагмент Тактики Зверя (1/30).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nBear (Ursun Bear/Deadmaul) - Heading for Profit\nBESTIAL TOME TACTIC\n\nPRAAG @ 59, 19. Ursun Bears are friendly to (O). Loot the paw from the source: Deadmaul, inside his cave. He doesn't drop it very frequently (WarDB lists 8% to the Ursun Bear's 33%).\n\nBy Supaplex, Josemite & skyrunner620\n20081124",
				},

				{
					name = L"• Beastman (Tactic)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Во имя высшего горха.\n- КООРДИНАТЫ: 7к, 18к.\n- ОПИСАНИЕ: Найти и убить Драхамеш (герй 40 ур.).\n- ПРИМЕЧАНИЕ: Герой не очень сильный.\n- НАГРАДА: НАГРАДА: Фрагмент Тактики Хаоса (1/15).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Gor (Drahamesh) - For the Greater Gor\nCHAOS TOME TACTIC\n\nPRAAG @ 7, 18. Kill this L40 Hero. NOTE: Mob does not hit hard.\n\nBy Alesinde\n20081021",
				},

				{
					name = L"• Beastman (Acc) - Сет Разделенный",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Тупоголовый.\n- КООРДИНАТЫ: 0.7к, 28к.\n- ОПИСАНИЕ: На границе Прага и Западного Прага найти Грентик Дуболом (37 ур.) и убить его.\n- НАГРАДА: Скрученные оковы (Сет Разделенный 1/4).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Ungor (Grentic the Thick-Headed) - Thick-Headed\nJEWELRY ITEM\n\nPRAAG @ 0.7, 28. Kill this named mob @ the W border of Praag. Exact loc might vary a bit in that area. Grants the Writhing Shackles accessory, part of The Divided Jewelry set.\n\nBy Supaplex & Benji\n20081117",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Great Cat (Title)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: В погоне за Рольфом.\n- КООРДИНАТЫ: 52к, 47к.\n- ОПИСАНИЕ: Возле лагеря 19-й главы Хаоса найти и убить 60 Рысей.\n- НАГРАДА: Титул Поцарапанный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBig Cat - A Run for Rolf\nThe Clawed\n\nPRAAG. Southern Breach, Ch19, max the Kill Collector.\n\nBy Sandka",
				},
				
				{
					name = L"• Bird (Tactic)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Слабаки под надежной охраной.\n- КООРДИНАТЫ: 58к, 50к.\n- ОПИСАНИЕ: На вершине горы найти гнездо орла и убить Самку снежного орла (герой 36 ур.).\n- ПРИМЕЧАНИЕ: В гнезде сидят четыре Птенца снежного орла если Самки нету то убив одного из них она появиться. Также на гору лучше всего взобраться с правой стороны.\n- НАГРАДА: Фрагмент Тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBird, Warhawk (Snowperch Matriarch) - Well Watched Weaklings\nBESTIAL TOME TACTIC\nPRAAG @ 58, 50. Kill this L37 Hero. There are 4 L36 Snowperch Fledglings at this spot. Kill 1 of them and the Hero will spawn.\n\nBy Brail & JCBN\n20081106",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Chaos Giant (Title)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: То что внутри.\n- КООРДИНАТЫ: 59к, 11к (великан) 47к, 44к (шаман).\n- ОПИСАНИЕ: На берегу возле реки найти сидящего великана 37-го уровня Джераль Недалекий и убить его чтобы получить Частицу Заангал-нес Наланеля, которую нужно отнести в ЗАПАДНЫЙ ПРААГ  и отдать его шаману Краглорну Созидателю.\n- ПРИМЕЧАНИЕ: Гигант после смерти востанавливаеться примерно через 20-30 минут.\n- НАГРАДА: Титул Стойкий.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Giant (Geral the Dim) - What's Inside is What Counts\nThe Unflinching\n\nPRAAG @ 58, 10. Kill the giant and take the item to Kraglorn the Maker @ 46.5, 44 in WEST PRAAG.\n\nBy Drunklight & Tyrilian\n20081030",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Chaos Giant (Title)",
					text = L"- Прааг (Тир 4) и Эатан (тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: То, что внутри.\n- КООРДИНАТЫ: 57к, 10к (Прааг) и 20к, 18к (Эатан).\n- ОПИСАНИЕ: убиваем монстра на берегу реки в Прааге, получаем предмет для анлока, несём его в Эатан к Линселлии.\n- НАГРАДА: титул Стойкий.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nChaos Giant (Geral the Dim) - What's Inside is What Counts\nThe Unflinching\n\nPRAAG @ 58, 10. Kill this giant to receive Mote of Zzangal'Nes Nalanel, and then talk to Aialye Lynsellia in Eataine.\n\nBy Drunklight & Tyrilian\n20081030",
				},

				{-- Order version
					--realm = TTitan.Data.Realm.ORDER,
					name = L"• Chaos Mutant (Token) - Ордеровский (баг)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Проводник для заблудших.\n- КООРДИНАТЫ: 16к, 13к или 15к, 18к.\n- ОПИСАНИЕ: Убить именного зомби (обычно бродит около разбитой телеги слева от дороги).\n- НАГРАДА: знак зверя.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nChaos Mutant (Calvin Lankdorf) - Leader of the Lost\nBEAST TOKEN\n\nPRAAG @ 16, 13 or 15, 9 or 18,15. Kill this L38 mob. The Mutated Peasant is a place holder for him if he is not up on that spot.\n\nBy Thovargh, Teran, Josemite, skyrunner620, Benji & Drollgar\n20081124",
				},
				
				{
					name = L"• Chaos Spawn (Acc) - Сет Жубы Ваааагха",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Эмпирическое докозательство.\n- КООРДИНАТЫ: 13к, 1к или 12к, 3к.\n- ОПИСАНИЕ: В зоне действия паблика 17-й главы хаоса Врата Праага за зданием найти книгу Третий дневник Ллиана и активировать ее.\n- НАГРАДА: Один клык (Сет Жубы Ваааагха 1/4).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Spawn - Empirical Proof\nJEWELRY ITEM\n\nPRAAG @ 13, 1 or 12.5, 3. A book called Third Journal of Lilean spawns near the first (D) PQ Gates of Praag. Find it E of town where Sasha Michailov is. Grants the unlock: One Tusk. NOTE: The Third Journal might also spawn on the opposite side of Sasha (near his most E spawn point, near the Griffon couriers) behind a building.\n\nBy Supaplex, Blodsalv, Flawed, Dirg & omega1\n20081110",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Hound (Tactic)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Придирчивый едок.\n- КООРДИНАТЫ: 44к, 47к.\n- ОПИСАНИЕ: В Лагере 19-й главы Хаоса найти Голодного пса, который даст вам Пустую сумку для еды и попросит наполнить ее. Найти по близости лагеря Лошадь и убить ее для заполнения мешка мясом. Отдать мешок Голодному псу.\n- НАГРАДА: Фрагмент Тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHound - Picky Eater\nBESTIAL TOME TACTIC\n\nPRAAG. Talk to the to the guard with a hound near him at chaos Ch19 to receive an empty bag. Kill horses until you obtain a Fresh Meat, then return to the guard to receive the reward.\n\nBy Fura\n20081021",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Hound (Tactic)",
					text = L"- Прааг (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Придирчивый едок.\n- КООРДИНАТЫ: 13к, 56к и 8к, 64к.\n- ОПИСАНИЕ: Поговорить с голодным псом, стоящим сзади палатки (первые координаты) и получить пустой мешок. Отправиться к тоннелям (вторые координаты), убить крысолюда, получить свежее мясо. Затем вернуться к голодному псу.\n- НАГРАДА: фрагмент тактики зверей.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nHound - Picky Eater\nBESTIAL TOME TACTIC\n\nPRAAG. Talk to the Hungry Hound behind the tent with the rally master in Ch17. He gives you an empty bag. Kill Warpwind Night Runners near the tunnel @ 8.5, 64 to get fresh meat. Return to the hound afterward to give them their 'lovely' meal.\n\nBy India\n20081021",
				},

				{
					name = L"• Skaven (Pocket)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: В яблочко.\n- КООРДИНАТЫ: 8к, 62к.\n- ОПИСАНИЕ: Внутри тонеля крысолюдов найти большую комнату где бродит Ташрак Кровоглаз (побед. 35 ур.) и убить его.\n- НАГРАДА: Колючий хлыст (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————Skaven (Thashtak Bloodeye) - Dead Eye\nPOCKET ITEM UNLOCK\n\nPRAAG @ 8, 62k. This L35 champ is found inside the Skaven tunnels and gives the item: Barbed Whip.\n\nBy rollen, Eridanos & JodouTR",
				},

				{
					name = L"• Tuskgor (Pocket)",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Жесткое мясо.\n- КООРДИНАТЫ: 18к, 15к.\n- ОПИСАНИЕ: На холме найти и убить кабана Серпозуба (37 ур.).\nили\n- КООРДИНАТЫ: 5, 20.\n- ОПИСАНИЕ: Найти и убить Кровавая буря (побед. 37 ур.).\n- НАГРАДА: Гордуккен (карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTuskgor (Bloodcharge) (Scythetusk) - Tough Meat\nPOCKET ITEM\n\nPRAAG @ 5, 20. Kill L37 champ Bloodcharge.\n-OR-\nPRAAG @ 18, 15. Kill L38 Scythetusk.\n\nBy Ness, Blodsalv, Teran & omega1\n20081110",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Wolf (Cloak)",
					text = L"- Прааг, Пустоши Хаоса (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Собирая куски.\n- КООРДИНАТЫ: 19к, 2к.\n- ОПИСАНИЕ: на границе между Праагом и Пустошами убить 60 волков (или бирюков) и сдать задание регистратору убийств в 20 главе Империи в Пустошах Хаоса.\n- НАГРАДА: Шкура мастера-охотника (плащ).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nWolf (Winter Wolf) - Picking Up The Pieces\nCLOAK ITEM\n\nPRAAG. Kill these mobs and max out the Kill Collector in the Ch20 (O) camp in Chaos Waste. Best place to kill wolves is actually on the road on the border between Praag/Chaos along the (O) road. Unlocks: Hunter Master's Pelt\n\nBy fjir\n20081031",
				},

				{
					name = L"• Wraith (Acc) - Сет Разорванные кольца",
					text = L"- ПРААГ (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Ловец душ.\n- КООРДИНАТЫ: 22к, 53к.\n- ОПИСАНИЕ: Внизу под водопадом стоит Покров Тьмы (39 ур.), которого нужно убить.\n- НАГРАДА: Фозерайн (Сет Разорванные кольца 1/2).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWraith (Gloomveil) - Soul Catcher\nJEWELRY ITEM\n\nPRAAG @ 22, 53. There is a mob under the waterfall. Kill it to unlock the ring: Foserain.\n\nBy rollen\n20081021",
				},
			},
		},
		
		{
			name = L"ЗАПАД ПРААГА • West Praag",
			entries =
			{
				{
					name = L"• Centigor (Title)",
					text = L"- ЗАПАД ПРААГА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Хорош до последней капли.\n- КООРДИНАТЫ: 44к, 45к.\n- ОПИСАНИЕ: Возле водопада в лагере Горхов найти в кустах боченок Пагубного пива. Выпить пиво до истечении 22:00 для получения титула.\n- НАГРАДА: Титул Необузданный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nCentigor - Good to the Last Drop\nThe Unruly\n\nWEST PRAAG @ 43.5, 45. Click the Noxious Ale and use it in your inventory to get the title. NOTE: May have a long respawn time. Show up around 22:00 in-game time, in case that turns out to be relevant. It's by the big rock across the river, on the ground. Tip: Make sure you have inventory space for it as it will disappear if you do not. I learned this the hard way by Ulgwar.\n\nBy Drunklight, Flawed & TorqueVolkmar\n20081110",
				},

				{	-- Destruction version
					--realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Daemonvine (Pocket)", -- Chaos Spawn
					text = L"- ЗАПАД ПРААГА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Обрывая связи.\n- КООРДИНАТЫ: 46к, 6к (растение) 14к, 17к (куча).\n- ОПИСАНИЕ: С северной стороны открытого паблика 17-й главы по тропинке есть старый дом за которым найдите и выполите странное растение Гордость Завиана (36 ур.).\n- ПРИМЕЧАНИЕ: Это 2-я часть анлока из Пустошей Хаоса по нахождению Жертвенного криса, который валяеться в куче грязи, но из за ошибки выполняется и без него.\n- НАГРАДА: Смрадный цветок (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Spawn, Daemonvine (Xavian's Pride) - Cutting Bonds\nPOCKET ITEM UNLOCK\n\nWEST PRAAG @ 46k, 6k. Go N of the Chasing Shadows PQ (N border of map). The mob is near a house. Killing it gave the unlock, although the tome unlock says you need a certain weapon. If I'm not mistaken this is also what the NPC in Chaos Wastes talks about for the Hide and Seek unlocks. Gives item: (Putrid Flower.) NOTE: You find a (Sacrifical Kris) in a mound of dirt in Chaos Wastes. It says (Gerrad Farmstead, West Praag) on it. That must be the weapon, but I guess you don't need it. And yeah, it is related to the Hide and Seek NPCs (Anya Gerrad for D and Xavian Gerrad for O).\n\nBy Supaplex, Thanat0s & Xilanle\n20081007",
				},
				
				{
					name = L"• Great Cat (Acc) - Сет Нарушенный завет", -- небыло в ТТ
					text = L"- ЗАПАД ПРААГА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Отличный вкус.\n- КООРДИНАТЫ: 52к, 3к.\n- ОПИСАНИЕ: Найти кошек Снежных рыкунов и выбить с них Отличный мех. После чего отнести его торговцу в 16-ю главу (Рейкланд для Ордера, Пустоши хаоса для Дестракта).\n- ПРИМЕЧАНИЕ: Шанс выпадения очень мал. Подходя к торговцу нужно иметь в инвентаре только один мех.\n- НАГРАДА: Верши предков (Сет Нарушенный завет 1/2).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGreat Cat - Refined Tastes\nThe Ascendant Verses\n\nHead to west praag 52k/3k kill Frost Growler until it drops a Fine Fur Pelt. After receiving 1 (and only one) go speak to the merchant NPC in the C16 hub (Reikland for Order/Chaos Wastes for Destro).",
					},

				{
					name = L"• Hound (Tactic)",
					text = L"- ЗАПАД ПРААГА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Сумашедший пес.\n- КООРДИНАТЫ: 31к, 21к.\n- ОПИСАНИЕ: Возле самых гор найти Потерянную собаку (33 ур.) и убить ее.\n- НАГРАДА: Фрагмент Тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHound (Lost Hound) - Dog Gone Crazy\nBESTIAL TOME TACTIC\n\nWEST PRAGG @ 31, 21. There are three 'Lost Hounds' wandering here. Killing one gives the unlock.\n\nBy Mapples & Iranium\n20081020",
				},
				
				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"?• Wolf (Tactic) - не исследован", -- небыло в базе
					text = L"- ЗАПАД ПРААГА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Модные меха.\n- КООРДИНАТЫ: 42к, 11к или 51к, 55к.\n- ОПИСАНИЕ: Соберите 5 Тонких Шкур Волка и отнесите их Охотнику в лагерь Хаоса 17-й главы.\n- ПРИМЕЧАНИЕ: Шанс выпадения шкур очень мал и выпадают они только из Рейкских Волков и Линскийских Волков, а из Рейкландских Бирюков непадает.\n- НАГРАДА: Фрагмент тактики зверей (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWolf (Reik Wolf/Lynsk Wolf) - Fall Fur Fashions\nBESTIAL TOME TACTIC\n\nWEST PRAGG. Kill Reik Wolves and collect 5 Fine Wolf Pelt.\nNote: Only a Reik Wolf or Lynsk Wolf will drop it, Reik Howlers will not.\nNote. Has a drop rate of about 0.4%, lower for Lynsk wolves. Extremely low drop rate.",
				},
			},
		},

		{
			name = L"ПУСТОШИ ХАОСА • Chaos Wastes",
			entries =
			{
				{
					name = L"• Chaos (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Завершить сценарий Хаоса.\n- ОПИСАНИЕ: Уавствовать в сценарии Утроба безумия. Сценарий доступен когда зона находиться в спорном конфликте.\n- НАГРАДА: Титул Защитник Разума.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos - Complete a Chaos Scenario\nThe Sentinel of Sanity\n\nCHAOS WASTES. Win a Maw of Madness Scenario.",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Beastman, Bestigor (Tactic)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Страшная ночь.\n- КООРДИНАТЫ: 56к, 20к.\n- ОПИСАНИЕ: убить 60 горхов и получить награду у регистратора убийств в 22 главе Империи.\n- НАГРАДА: фрагмент тактики Хаоса.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nBeastman, Bestigor (Dreadhorn Bestigors) - Nacht to Dread\nCHAOS TOME TACTIC UNLOCK\n\nCHAOS WASTES @ 56, 20k. Max out Kill Collector Harald Nacht at (O) Chp 22 Camp by killng the mob at the Fall of Night PQ.\n\nBy Foggye\n20081017",
				},

				{
					name = L"• Beastmen, Bray Shaman (Token)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Чем громче рев, тем лучше день.\n- КООРДИНАТЫ: 57к. 28к или 61к, 25к или 54к, 15к.\n- ОПИСАНИЕ: Найти и убить Краткар или Ревущий шаман (40 ур.).\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBeastmen, Bray Shaman (Kratkar/Dreadhorn Shaman) - Bigger Bray to Make Your Day\nBEASTIAL TOKEN\n\nCHAOS WASTES @ 56, 25 or 61, 25 or 54, 15. Kill this L40-41 non-champ. He's a few feet off the W side of the road. If he's not up, there may be a Dreadhorn Shaman L40 non-champ there instead. I got the unlock for killing Kratkor, a friend got the unlock for killing the shaman later that day. NOTE: Five minute respawn. FURTHER: Mob also pops up on the side of the cliff E of the road, so look all around the Gor areas, not just the W side of the road.\n\nBy Valekrin, Sandkat, JodouTR, Magdain & Dissent\n20081021",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• B. of Khorne (Token)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Ключ к успеху.\n- КООРДИНАТЫ: 39к, 13к (ключ) 60к, 33к (кусты).\n- ОПИСАНИЕ: Поговорить с Михаилом Нечестивым, чтобы получить Ключ-талисман. Найти в кустах Потертый сейф и взять из него Усыпанный самоцветами череп. Вызвать черепом демона Ситум (побед. 39 ур.) и убить его.\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBloodletter of Khorne (Cithym) - The Key to Success\nBEAST TOKEN\n\nCHAOS WASTES\n1: @ 38.5, 12.5. Hail Mihkail for the Talisman Key.\n2: @ 60, 33 there is a Battered Strongbox at the Reaping Field PQ E of the Empire Warcamp. It may be hard to see initially, somewhat hidden behind a bush at the back end of the PQ near the hills. Clicking on the Strongbox will spawn a L39 Champ. Kill it.\n\nBy fjir, Iranium & Benji\n20081124",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• B. of Khorne (Token)",
					text = L"- Пустоши Хаоса (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Поехали!\n- КООРДИНАТЫ: 38к, 12к и 60к, 33к.\n- ОПИСАНИЕ: Убейте Михаила Нечестивого, и получите ключ-талисман (автоматически помещается в инвентарь), затем откройте Потёртый сейф (вторые координаты) и достаньте Усыпанный самоцветами череп. Используйте его. Убейте появившегося монстра Ситума (победоносец, 37 ур.).\n- НАГРАДА: знак зверя.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nBloodletter of Khorne (Cithym) - The Key to Success\nBEAST TOKEN\n\nCHAOS WASTES\n1: @ 38.5, 12.5 kill Mikhail the Profane from the Battered but not Forgotten unlock. When you kill him you will receive a Talisman Key. It will automatically go to your inventory when you kill him.\n2: @ 60, 33 there is a Battered Strongbox at the Reaping Field PQ E of the Empire Warcamp. It may be hard to see initially, somewhat hidden behind a bush at the back end of the PQ near the hills. Clicking on the Strongbox will spawn a L39 Champ. Kill it.\n\nBy fjir, Iranium & Benji\n20081124",
				},

				{
					name = L"• B. of Khorne (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Разбит но не забыт.\n- КООРДИНАТЫ: 39к, 13к.\n- ОПИСАНИЕ: Для стороны РАЗРУШЕНИЯ поговорить с Михаилом Нечестивым. для стороны ПОРЯДКА убить Михаила Нечестивого.\n- НАГРАДА: Титул Кровавый.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nBloodletter of Khorne (Mikhail The Profane) - Battered But Not Forgotten\nThe Sanguinary\n\nCHAOS WASTES @ 38.5, 12.5. Speak to(D) or kill(O) Mikhail the Profane, depending on your side.\n\nBy Iranium, Sandkat, Socran & Benji\n20081112",
				},

				{
					name = L"• Daemonvine (Pocket)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Обрывая связи.\n- КООРДИНАТЫ: 14к, 17к или 41к, 16к (куча) 46к, 8к (растение).\n- ОПИСАНИЕ: Взять из кучи грязи древний меч Жертвенный крис. Отправиться в ЗАПАДНЫЙ ПРААГ и за домиком найти единственное живое растение Гордость завиана (36 ур.) и убить его имея Жертвенный крис у себя в инвентаре.\n- ПРИМЕЧАНИЕ: По некоторым сведениям анлок засчитываеться даже если пропустить 1-ю часть.\n- НАГРАДА: Смрадный цветок (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Spawn, Daemonvine - Cutting Bonds\nPOCKET ITEM\n\nThis unlock has two parts.\n1. CHAOS WASTES @ 11, 14 to 22, 20 or 14, 17. Along the road, W side, there is a pile of dirt that's interactable, and gives you an item called Sacrificial Kris.\n2. WEST PRAAG @ 46, 8. Near the farm shack there is a single Demonvine spawn. Kill it with the Kris in your inventory and you receive the unlock for: Putrid Flower.\nNOTE: Confirmed to work by skipping step one and just killing the damned flower.\n\nBy WNxInviso, JodouTR & poll12\n20081029",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Daemonvine (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Прятки.\n- КООРДИНАТЫ: 6к, 10к.\n- ОПИСАНИЕ: Найти спрятавшуюся Анья Герарт в кустах и послушать ее историю.\n- НАГРАДА: Титул Траводер.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nChaos Spawn, Daemonvine - Hide and Seek\nThe Weed Whacker\n\nCHAOS WASTES @ 5.5, 10. Listen to the sad story of Anya Gerrad. Head down the road, past the Wight area till the road curves and you see some Daemonettes, Chaosfuries and Plaguebearers to your left. She's hiding beside the road in the bushes. Stay close and listen for the unlock.\n\nBy Blodsalv, Thanat0s, Nukeitall, Houyoko & India\n20081104",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Daemonvine (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Прятки.\n- КООРДИНАТЫ: 50к, 61к.\n- ОПИСАНИЕ: Поговорить с Ксавьеном Геррадом (выслушать всю его речь). Он стоит на горе, рядом со входом в пещеру.\n- НАГРАДА: титул Траводёр.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nChaos Spawn, Daemonvine - Hide and Seek\nThe Weed Whacker\n\nCHAOS WASTES @ 50, 61 Find Xavian Gerrad. Listen to his chatter and you get the unlock.\nNOTE: Confirmed to work for some and not for others. May be bugged, or need an initial first trigger.\n\nBy Blodsalv, Thanat0s, Nukeitall, Houyoko & India\n20081104",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• D. of Slaanesh (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Песнь и танец.\n- КООРДИНАТЫ: 16к, 31к или 16к, 40к.\n- ОПИСАНИЕ: Найти и убить победоносца Эйди Хватайка (побед. 33 ур.), при этом получить критический удар.\n- НАГРАДА: Титул Непорочный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDaemonette of Slaanesh (Eidih the Helator) (Bloodsong) - Song and Dance\nThe Chaste\n\nCHAOS WASTES @ 15.6, 31 or 15.5, 40. Kill it and achieve the killing blow.\n\nBy Sandkat, Nukeitall, Bass, TorqueVolkmar & Kriegzwerg\n20081105",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• D. of Slaanesh (Title)",
					text = L"- Пустоши Хаоса (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Песнь и танец.\n- КООРДИНАТЫ: 24к, 55к.\n- ОПИСАНИЕ: убить Пляшущий Клинок.\n- НАГРАДА: титул Непорочный.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nDaemonette of Slaanesh (Eidih the Helator) (Bloodsong) - Song and Dance\nThe Chaste\n\nCHAOS WASTES @ 54.5, 46. Accept the Breaking Point quest from Hewitt. Turn this quest in to Karl the Chaste @ 61, 53 and accept the next part. After killing 15 Daemonettes of Slaanesh turn this part in and receive a quest to kill Bloodsong. The mob spawns near the bottom of the hill and runs up to you. Kill it.\n\nBy Sandkat, Nukeitall, Bass, TorqueVolkmar & Kriegzwerg\n20081105",
				},

				{
					name = L"• F. of Tzeentch (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Безумие мембраны.\n- КООРДИНАТЫ: 22к, 52к.\n- ОПИСАНИЕ: Спустится в ущелье восточнее Башни пробуждения и убить Порочное исчадие (побед. 39 ур.), чтобы получить Искрящуюся чешую. Активировать чешую 3 раза при этом она клонируеться.\n- НАГРАДА: Титул Безумный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nFirewyrm of Tzeentch (Corrupt Spawn) - Insane in the Membrane\nThe Insane\n\nCHAOS WASTES @ 22, 52k. Kill the L39 champ. You have to go down into a ravine just E of the Tower of Awakening PQ. After killing it you get a message that you pull a scale out of the beast. Look in your regular inventory for the scale, has the icon of a talisman. Right-click it a few times, after the first time a new one popped up and after the 2nd click a 3rd scale popped up so just keep clicking till you get the unlock.\n\nBy Supaplex\n20081007",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• F. of Tzeentch (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Невозможно отказаться.\n- КООРДИНАТЫ: 48к, 7к.\n- ОПИСАНИЕ: На вершине холма найти Пламень Духа и поговорить с ним.\n- НАГРАДА: Титул Экзорцист.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nFlamer of Tzeentch (Soulflame) - Hot Deal\nThe Banisher\n\nCHAOS WASTES @ 48, 6.5. Talk to the L41 named mob.\n\nBy Dissent, rally & JCBN\n20081103",
				},

				{
					name = L"• Giant Bat (Acc)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Нет ни в одном магазине.\n- КООРДИНАТЫ: 45к, 3к.\n- ОПИСАНИЕ: Внутри пешеры ужаса найти и убить Смертокрыла (побед. 40 ур.), который летает под самым сводом пешеры. Добудте Клыки смертекрыла и активируйте их.\n- ПРИМЕЧАНИЕ: Шанс дропа примерно 30%, а релог монстра 5 минут.\n- НАГРАДА: Несчастливая монетка (сет аксесуаров (1/4).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGiant Bat (Deathwing) - Not Available in Any Stores\nSET ITEM\n\nCHAOS WASTES @ 45, 3. Kill this L40 champ. It flies up near the ceiling at the back of the Caverns of Terror. 15min repop. Drops Deathwing's Fangs, rt click for: Hapless Ha'penny. NOTE: Drop rate is not 100%.\n\nBy Rathenau & Barandox\n20081021",
				},

				{
					name = L"• J. of Khorne (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Между камнем и... .\n- КООРДИНАТЫ: 40к, 15к или 42к, 16к.\n- ОПИСАНИЕ: В развалинах недалеко от Освобожденного мастодонта найти Груду камушков.\n- НАГРАДА: Титул Ни шагу с места.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nJuggernaut of Khorne - Between a Rock and a...\nThe Immoveable Object\n\nCHAOS WASTES @ 40, 15 or 42, 16. Face the friendly Rhinox: Unbound Mastodon. Stand on the mob and face west. Their is a pile of rubble in the corner behind the wall. Click on it.\n\nBy Sandkat, Thanat0s, Socran & Yvarma\n20081112",
				},

				{
					name = L"• L. of Change (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Я иду искать.\n- КООРДИНАТЫ: 64к, 29к (око) 62к, 37к (холмы).\n- ОПИСАНИЕ: На верху горы осмотреть Око перемен, похоже на голубой глаз торчащей из земли.\n- ПРИМЕЧАНИЕ: Чтобы забраться нужно пройти на снежные холмы, а от туда уже спуститься к Оку.\n- НАГРАДА: Титул Росток Анархии.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nLord of Change (Eye of Change) - I Spy With My Eye\nScion of Anarchy\n\nCHAOS WASTES @ 64, 29. Up on the side of the mt is the Eye of Change (looks like a blue gem on the ground). Click it for the unlock. To get to it, go around the S of the mt range @ 62, 36.5 and then up the backside to drop down on it.\n\nBy Dissent. TorqueVolkmar & Crucifix\n20081022",
				},

				{
					name = L"• Nurgling (Tactic)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Нет конца выделению.\n- КООРДИНАТЫ: 11к, 55к или 18к, 47к или 19к, 38к или 18к, 40к.\n- ОПИСАНИЕ: Найти места земли покрытые зеленой тиной и убить там Толстых нурглингов (33 ур.), чтобы собрать 20 Образцов слизи нурглинга.\n- НАГРАДА: Фрагмент Тактики демонов (1/22).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nNurgling (Corpulent Nurglings) - No End to the Oozing\nDAEMONIC TOME TACTIC\n\nCHAOS WASTES @ 17.5, 46.5 or 18.5, 38 or 18.5, 40. Kill Nurglings to loot Nurgling Ooze Sample. Collect 20 and you'll get the unlock. NOTE: Look for green bile on the floor, it show up on your mini-map.\n\nBy Supaplex, Thanat0s, Houyoko & Kriegzwerg\n20081104",
				},

				{
					name = L"• Plaguebearer (Acc)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Потрошитель.\n- КООРДИНАТЫ: 49к, 53к.\n- ОПИСАНИЕ: Убить чумоносцев и добыть у них Зараженный орган.\n- ПРИМЕЧАНИЕ: Орган дропаеться с вероятностью примерно 5%.\n- НАГРАДА: Кайтерил (сет аксесуаров 1/2).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nPlaguebearer (Grotesque Plaguebearer/Plaguebringer) - Organ Grinder\nSET ITEM\n\nCHAOS WASTES @ 43, 55. Kill the mob and there is a chance you will receive an item called Pestilent Organ. Inspect this to get the unlock for the set item: Cytheril. \nNOTE: Mob is slightly to the SE of given coords.\n\nBy Zoe9906, JodouTR, Benji & Jette Black\n20081117",
				},

				{
					name = L"• S. of Tzeentch (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Море вопросов.\n- КООРДИНАТЫ: 12к, 56к.\n- ОПИСАНИЕ: Найти область обитания крикунов рядом с пешерой.\n- НАГРАДА: Титул Крушитель крикунов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nScreamer - Puddles of Points\nThe Screamer Seeker\n\nCHAOS WASTES @ 12, 55.5k. Unlocked it after stepping into an area filled with Screamers.\n\nBy Blodsalv\n20081007",
				},

				{
					name = L"• S. of Tzeentch (Cloak)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Ткни добычу!\n- КООРДИНАТЫ: 5к, 25к.\n- ОПИСАНИЕ: Найти и убить крикуна Мерзкочрева (побед. 33 ур.), летающего недалеко от Одинокой башни.\n- НАГРАДА: Накидка крикуна (плащь).\n\nTEZARIUS © 2009\n\n————————————————————————————————Screamer of Tzeentch (Vilemaw) - Poke'm If You Got'em\nCLOAK ITEM\n\nCHAOS WASTES @ 5, 25. Kill this L33 champ who roams the water just SE of the Lonely Tower PQ. Unlocks the cloak: Cape of the Screamer.\n\nBy Supaplex\n20081104",
				},

				{
					name = L"• Spirit Host (Tactic)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Широта души.\n- КООРДИНАТЫ: 41к, 58к.\n- ОПИСАНИЕ: На холме найти и убить Душемерзость (побед. 38 ур.).\n- НАГРАДА: Фрагмент Тактики нежети (1/12).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpirit Host (Vilesoul) - Higher Caliber of Soul\nUNDEAD TOME TACTIC UNLOCK\n\nCHAOS WASTES @ 41, 57.5k Kill this L38 champ spirit host.\n\nBi Itsari & Iranium\n20081008",
				},

				{
					name = L"• Wight (Title)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Мертвый груз.\n- КООРДИНАТЫ: 8к, 2к или 6к, 1к.\n- ОПИСАНИЕ: На границе найти и убить Бримдалль Иггдрейдар (1 ур.).\n- АЛЬТЕРНАТИВА: Задание в ПИК СМЕРТИ.\n- НАГРАДА: Титул Убийца Короля.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWight (Brimdall Yggdreidar) - Dead Weight\nKing Slayer\n\nCHAOS WASTES @ 8, 2 or 6.5, 1.5. NE of Deathchill, Chaos Ch15. Kill this L1 mob.\nALTERNATE METHOD: See entry in DEATH PEAK below.\n\nBy Bastion & Phrost\n20081104",
				},
				
				{   -- небыло в базе найден на ВИКИ
					name = L"• Wight (Title)",
					text = L"- ПУСТОШИ ХАОСА (Ти 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Некоторые тайны стоят смерти.\n- КООРДИНАТЫ: 42к, 64к.\n- ОПИСАНИЕ: На верху над Извилистой пещерой найти Мрачную скалу. Получив Дневник Ма-Линга возле скалы появиться морок Ма-Линг (39 ур.) и начнет вас атаковать. После расправы с ним прочитайте дневник в инвентаре.\n- ПРИМЕЧАНИЕ: взобраться на гору можно по краю с РвР зоной.\n- НАГРАДА: Титул Своевольный.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWight - The Best Secrets Are Worth Dying For\nThe Intractable\n\nChaos Wastes: 42, 63.5. Click the Obscure Rock on the ground, and a named Wraith will spawn. Kill it, then inspect Ma'leeng's Journal for the unlock. NOTE: You will need to do some mountain climbing to get there.",
				},

				{
					name = L"• Zombie (Tactic)",
					text = L"- ПУСТОШИ ХАОСА (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Умерли в один день.\n- КООРДИНАТЫ: 16к, 58к.\n- ОПИСАНИЕ: Недалеко от дороги на холме найти развалены где бродят 4 зомби Томас Сполфинкр, Аннабель Спольфинк, Папа Спольфинкр, Кретч Спольфинкр (33 ур.). Убить их всех.\n- НАГРАДА: Фрагмент тактики Нежити (1/12).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nZombie - The Family That Dies Together\nUNDEAD TOME TACTIC\n\nCHAOS WASTES: SW of the Tower of Awakening PQ, you'll reach a broken down house with named creatures inside: Tomas Spolfinkr, Croetch Spolfinkr, Papa Spolfinkr and Annabelle Spolfinkr. Killing them all gave the unlock.\n\nBy Supaplex & Thanat0s\n20081029",
				},
				
				
			},
		},

		{
			name = L"РЕЙКЛАНД • Reikland",
			entries =
			{
				{
					name = L"• Spirit Host (Tactic)",
					text = L"- РЕЙКЛАНД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Стоит того, чтобы ждать.\n- КООРДИНАТЫ: 12к, 44к.\n- ОПИСАНИЕ: На пристане найти служанку Майбл Рисс (побед. 31 ур.). Приблизившись она превратиться с Злобного духа, которого надо убить.\n- ПРИМЕЧАНИЕ: Найти ее можно только ночью с 9 вечера до 9 утра.\n- НАГРАДА: Фрагмент Тактики Нежити (1/12).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nSpirit Host - Worth the Waiting For\nUndead Tactic Fragment\n\nREIKLAND @ 12, 44. An NPC named Mabel Riess spawns at this loc. Attack her, she turns into a Spirit Host. Kill it for the unlock.\nProbably only spawns at night in-game, IE 9pm-9am or so. - Mob Roams: Docks, to point shown, to 10k, 46k. Sits down at each point to whine about Alexi Gaertner standing her up, and then moves along.\n\nBy Wabbajack + Benji",
				},

				{
					name = L"• Empire (Title)",
					text = L"- РЕЙКЛАНД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Завершить сценарий Империи.\n- ОПИСАНИЕ: Принять участие в сценарии Рейкландские холмы. Сценарий доступен когда зона находиться в спорном конфликте.\n- НАГРАДА: Титул Охотник на людей.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nEmpire - Complete an Empire Scenario\nMan Hunter\n\nREIKLAND. Win a Reikland Hills Scenario. Scenario unlocks when the zone becomes contested.\n\nBy Ulgwar\n20081110",
				},

				{   
					name = L"• Giant (Tactic)",
					text = L"- РЕЙКЛАНД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Кольцо в носу.\n- КООРДИНАТЫ: 45к, 27к.\n- ОПИСАНИЕ: Найти под мостом и убить Углика (побед. 39 ур.) и получить кольцо в нос. Использовать в инвентаре.\n- АЛЬТЕРНАТИВА: Задание в Долине Кадрид.\n- НАГРАДА: фрагмент тактики великанов (1/15).\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nGiant (Uglik) - Brutal Nose Ring\nGIANT TOME TACTIC\n\n\REIKLAND @ 45, 27. Just NE of the Chaos Warcamp there is a short path leading below the bridge to Chaos Ch. 20. Take the path under the bridge and you'll see this L39 Champ. Examine the Giant Nosering for the unlock.\nAlternate method in KADRIN VALLEY.\n\nBy Schwarzwald\n20081110",
				},

				{
					name = L"• Great Cat (Tactic)",
					text = L"- РЕЙКЛАНД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Теперь - не так уж и велик.\n- КООРДИНАТЫ: 2к, 19к.\n- ОПИСАНИЕ: Найти и убить рысь Гриня (38 ур.).\n- НАГРАДА: Фрагмент тактики Зверя (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGreat Cat (Grinya) (Bloodmane) - Not So Great Any More\nBESTIAL TOME TACTIC\n\nREIKLAND @ 2, 18. Kill L38 Grinya W of the road on the mt.\n-OR-\nREIKLAND @ 59, 19. Kill L38 Bloodmane.\n\nBy logoth, JodouTR & Phrost\n20081110",
				},

				{
					name = L"• Hound (Pocket)",
					text = L"- РЕЙКЛАНД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Ловец собак.\n- КООРДИНАТЫ: 46к, 37к.\n- ОПИСАНИЕ: Найти возле камня Ловушку для питомцев и активировать ее.\n- НАГРАДА: Собачий свисток (Карман).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHound - Dog Catcher\nPOCKET ITEM\n\nREIKLAND @ 46, 37. There is a Pet Trap just S of Chaos Ch21 camp. Use it for the unlock. Gives Dog Whistle. TIP: Look a bit to the W when standing at the pet trap and you'll see a chest that you can use to get a little hidden quest.\n\nBy Supaplex\n20081110",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"?• Wolf (Tactic) - не исследован",
					text = L"- РЕЙКЛАНД (Тир 4).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Модные меха.\n- КООРДИНАТЫ: 9к, 29к или 6к, 51к.\n- ОПИСАНИЕ: Соберите 5 Тонких Шкур Волка и отнесите их Мултан Охотнику в имперский лагерь 15-й главы.\n- ПРИМЕЧАНИЕ: Шанс выпадения шкур очень мал и выпадают они только из Рейкских Волков и Линскийских Волков, а из Рейкландских Бирюков непадает.\n- НАГРАДА: Фрагмент тактики зверей (1/30).\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWolf (Reik Wolf/Lynsk Wolf) - Fall Fur Fashions\nBESTIAL TOME TACTIC\n\nREIKLAND. Collect five Fine Wolf Pelts to finish the kill request from Multan the Huntsman in the (O) Ch15 town. NOTE: Reik Wolf drop rates are INCREDIBLY RARE at about .65% (1 in 150). Lynsk drops are near .11% (1 in 900).\n\nBy Socran & Kellithe\n20081110",
				},
			},
		},

		{
			name = L"<данж> СТУПЕНИ БАСТИОНА • Bastion Stair",
			entries =
			{
				{
					name = L"• Beastman, Gor (Acc.)",
					text = L"- СТУПЕНИ БАСТИОНА (данж).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Логово Скотины.\n- КООРДИНАТЫ: левое крыло.\n- ОПИСАНИЕ: Убивать Горхов, чтобы получить Череп ревущего шамана. Использовать его в инвентаре.\n- НАГРАДА: Сочащаяся личника (сет аксесуара 1/4).\n\nЧейз Рэйнхолд и TEZARIUS © 2009\n\n————————————————————————————————\n\nBeastman, Gor (Bloodherd Gor) - Lair of the Brute\nSET ITEM\n\nBASTION STAIR. In the first wing kill the Gor to get the Skull of a Bray-shaman. This item is used to unlock The Seeping Larva for The Divided set.\n\nBy Naukan & Risingashes\n20081104",
				},
				
				{
					name = L"?• Daemon Prince (Title)",
					text = L"- СТУПЕНИ БАСТИОНА (данж).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: таковы дары Хаоса.\n- КООРДИНАТЫ: ...\n- ОПИСАНИЕ: ...\n- НАГРАДА: Титул ???.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nDaemon Prince - Such are the Gifts of Chaos",
				},

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• F.Hound of Khorne (Token)",  -- не завершон
					text = L"- СТУПЕНИ БАСТИОНА (данж).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Слабости плоти.\n- КООРДИНАТЫ: 47к, 44к (нпс).\n- КООРДИНАТЫ: центральное крыло.\n- ОПИСАНИЕ: Найти в среднем крыле бастиона Меднобоких Кроволюбов и выбить с них Разорительно острый клык. После Отнесите его в ЗАПАДНЫЙ ПРААГ шаману Краглорн Созидатель.\n- ПРИМЕЧАНИЕ: Возможно нужно собрать 20 штук для выполнения следующего этапа.\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nFlesh Hound of Khorne - Hunters of Blood\nBEAST TOKEN\n\nBASTION STAIR\n1. Get a Razor Sharp Fang from the Ruin (middle) wing of Bastion Stair.\n2. WEST PRAAG @ 46.5, 44. Deliver to Kraglorn the Maker. NOTE: Confirmed delivery of a stack of 20 Razor Sharp Fang. 20 may not be necessary though. 1 fang does not work.\n\nBy Chewen, Nukeitall & Ryfar\n20081106",
				},

				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• F.Hound of Khorne (Token)",
					text = L"- СТУПЕНИ БАСТИОНА (данж).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Слабости плоти.\n- КООРДИНАТЫ: 20к, 20к (нпс).\n- КООРДИНАТЫ: центральное крыло.\n- ОПИСАНИЕ: Найти в среднем крыле бастиона Меднобоких Кроволюбов и выбить с них Разорительно острый клык. После отнести его в Эатан нпсу Aialye Lynsellia.\n- ПРИМЕЧАНИЕ: Возможно нужно собрать 20 штук для выполнения следующего этапа.\n- НАГРАДА: Знак зверя.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nFlesh Hound of Khorne - Hunters of Blood\n\nBEAST TOKEN\n\nBASTION STAIR\n1. Get a Razor Sharp Fang from the Ruin (middle) wing of Bastion Stair.\n2. Deliver 20 fangs to... (???)\nUNCONFIRMED. Negative on the turn in to: Aialye Lynsellia in EATAINE.\n\nBy Chewen, Nukeitall & Ryfar\n20081106",
				},

				{
					name = L"• Tuskgor (Title)",
					text = L"- СТУПЕНИ БАСТИОНА (данж).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Не пытаясь спрятаться.\n- ОПИСАНИЕ: Найти бивнегорхов в левом проходе и выбить с них Прочную кожу бивнегорха и осмотреть ее.\n- НАГРАДА: Титул Охотник Бивнегорхов.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nTuskgor (Bloodherd Tuskgor) - Not Trying To Hide\nThe Tuskgor Hunter\n\nBASTION STAIR. Loot and inspect the drop Thick Tuskgor Hide.\n\nBy Rhuobhe\n20081029",
				},
			},
		},

		
		
		{	-- Divider, won't expand when clicked
			name = L"••• форт •••",
			divider = true,
			entries = {},
		},
		
		{
			name = L"УТРОБА • The Maw",
			entries =
			{
				{-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"?• Chaos (Title) - не найден",
					text = L"- УТРОБА (Форт).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Победить Лорда хаоситской крепости.\n- ОПИСАНИЕ: Убить лорда в форте Хаоса.\n- НАГРАДА: Титул Чемпион света.\n\n————————————————————————————————\n\Chaos  - Vanquish the Chaos Fortress Lord\nChampion of the Light\n\n(D) REIKWALD. Be in the vicinity of a Fortress Lord kill.",
				},

			},
		},
		
		{
			name = L"РЕЙКВАЛЬД • Reikwald",
			entries =
			{
				{-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Empire (Title)",
					text = L"- РЕЙКВАЛЬД (Форт).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Убить властителя крепости Империи.\n- ОПИСАНИЕ: Убить лорда имперского форта.\n- НАГРАДА: Титул Длань Хаоса.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nEmpire - Vanquish the Empire Fortress Lord\nThe Hand of Chaos\n\n(D) REIKWALD. Be in the vicinity of a Fortress Lord kill.\n\nBy Risingashes\n20081216",
				},


			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• сталица •••",
			divider = true,
			entries = {},
		},
		
		{
			name = L"ГОРОД НЕИЗБЕЖНОСТИ • The Inevitable City",
			entries =
			{

				{	-- Destruction version
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"• Крушитель бочек (Title)",
					text = L"- ГОРОД НЕИЗБЕЖНОСТИ (Сталица).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Уничтожена таинственная бочка в городе.\n- ОПИСАНИЕ: В городе возле стен найти Подозрительную бочку и разбить ее.\n- НАГРАДА: Титул Крушитель бочек.\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},

			},
		},

		{
			name = L"АЛЬТДОРФ • Altdorf",
			entries =
			{
				{	-- Order version
					realm = TTitan.Data.Realm.ORDER,
					name = L"• Skaven (Tactic)",
					text = L"- АЛЬТДОРФ (Сталица).\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Обеззараживание.\n- КООРДИНАТЫ: 24к, 27к.\n- ОПИСАНИЕ: Убить в доме трёх крысолюдов, затем убить крысолюда-героя (15 ур.).\n- НАГРАДА: фрагмент тактики крысолюдов.\n\nЧейз Рэйнхолд © 2009\n\n————————————————————————————————\n\nSkaven (Master Moulder Vitchek) - Septic Sanitation\nSKAVEN TOME TACTIC\n\n(O) ALTDORF @ 24, 27. Kill 3 L15 Skaven in a house. There is a woman outside the house who cries about beasties in her house. This will spawn a L19 Hero Skaven. Kill this Skaven for the unlock. NOTE: Must achieve killing blow.\nNOTE: (O) only. The same mob appears in the sewers and may also give the unlock.\n\nBy Bethryn, JodouTR & Frantic\n20081027",
				},
			},
		},
		
		{	-- Divider, won't expand when clicked
			name = L"••• пустыня •••",
			divider = true,
			entries = {},
		},
		
		{
			name = L"ЗЕМЛЯ МЕРТВЫХ • Land of the Dead",
			entries =
			{
				
				{	
					name = L"• Asp Bone Construct (Trophy)", -- Костяной Голем-Гадюка
					text = L"- ЗЕМЛЯ МЕРТВЫХ.\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Величайшая из костяных гадюк-големов да падет!.\n- КООРДИНАТЫ: 9.2к 13.8к.\n- ОПИСАНИЕ: Выполняя открытоя задание Амя Асафа, дойти до 3-го этапа и убить костяную гадюку Ибехме Гнев Асаф (герой 41 лвл.).\n- ПРИМЕЧАНИЕ: Анлок засчитываеться только тому кто нанес ПОСЛЕДНИЙ удар убив героя.\n- НАГРАДА: Костяной голем-гадюка (трофей).\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{	
					name = L"• Carrion (Trophy)", -- Костяной Голем-Падальщик
					text = L"- ЗЕМЛЯ МЕРТВЫХ.\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Не разбив яйца, яичницу не сделаеш.\n- КООРДИНАТЫ: 43к 20к или 54к 40к.\n- ОПИСАНИЕ: В гнездах падальщиков найти Блестящии безделушки 5 кусков и отнести их в город.\n     (D) Ксалине дурной глаз в Пылючую Сушь - 56.4к 10.5к.\n     (O) Вандену легкому ветру в Златохолм.\n- НАГРАДА: Падальщик (трофей).\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{	
					name = L"• Scarab bone construct (Trophy)", -- Костяной Голем-Скарабей
					text = L"- ЗЕМЛЯ МЕРТВЫХ.\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Око за око.\n- КООРДИНАТЫ: 18к 26к.\n- ОПИСАНИЕ: Найти Скарабеев и выбить у них 10 Глаз скарабея-конструкта. После чего отнести их в город.\n     (D) Дреггу Дурному Глазу в Пылючую Сушь - 57.9к 8.1к.\n     (O) Кальвану Теорину в Златохолм.\n- ПРИМЕЧАНИЕ: Чаще всего глаза можно получить с Костяных воинов бродячего паблика Поход Некхера.\n- НАГРАДА: Костяной голем-скарабей (трофей).\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{	
					name = L"• Tomb Swarm (Trophy)", -- Могильный Рой
					text = L"- ЗЕМЛЯ МЕРТВЫХ.\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Могильный рой сметает все на своем пути.\n- КООРДИНАТЫ: 17.6к, 23.7к.\n- ОПИСАНИЕ: Спустившись по ступеням осмотреть Кучу костей (пару раз) и убить появившейся Могильный рой (40 лвл.).\n- НАГРАДА: Могильный рой (трофей).\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{	
					name = L"• Ushitabi (Trophy)", -- Ушабти
					text = L"- ЗЕМЛЯ МЕРТВЫХ.\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Ушабти порой само очарование.\n- КООРДИНАТЫ: 21к, 46к.\n- ОПИСАНИЕ: Найти монстров типа Ушабти и выбить у них 10 Амулетов Ушабти. После чего отнести их в город.\n     (D) Кавине Буревестнице в Пылючую Сушь - 56.8к 9.2к.\n     (O) Грейвену Златохолму в Златохолм.\n- ПРИМЕЧАНИЕ: Чаще всего амулеты падают с Образ Джафа в ОЗ Библеотека Зандри.\n- НАГРАДА: Ушабти (трофей).\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{	
					name = L"• Обнаружить дерижабль (Trophy)", -- Достижение ЗМ
					text = L"- ЗЕМЛЯ МЕРТВЫХ.\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Обнаружить дерижабль (Достижения).\n- КООРДИНАТЫ: (D) 60к 43к, (O) 55к 6к.\n- ОПИСАНИЕ: Выполнить квест по уничтожению Дерижабля.\n- НАГРАДА: Медаль за разрушение дерижабля (трофей).\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{	
					name = L"• Гробницы (Trophy)", -- Достижение ЗМ
					text = L"- ЗЕМЛЯ МЕРТВЫХ.\n- ЗАПИСЬ В КНИГЕ ЗНАНИЯ: Все ключевые противники из логов убиты (Достижения).\n- ОПИСАНИЕ: Посетить все 4 гробницы (Звезды, Неба, Солнце, Луну) и победить их обитателей.\n- НАГРАДА: Погребальный сосуд костяного голема-гадюки (трофей).\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{	
					name = L"• Карманные безделушки (Pocket)", -- Достижение ЗМ
					text = L"- ЗЕМЛЯ МЕРТВЫХ.\n————————————————————————————————\n- КООРДИНАТЫ: 14.3к, 3.3к.\n- ОПИСАНИЕ: Убить Саа Хасеф (герой 43 лвл.) в гробнице Солнца.\n- НАГРАДА: Посох Сияния (карман) (Вашу атаку нельзя парировать или блокировать в течении 15 сек).\n————————————————————————————————\n- КООРДИНАТЫ: 10.3к, 48.9к.\n- ОПИСАНИЕ: Убить Хапу Шебикеф (герой 43 лвл.) в гробнице Луны.\n- НАГРАДА: Золотой Анкхра (карман) (Поглощает до 3515 магического урона за 20 сек).\n————————————————————————————————\n- КООРДИНАТЫ: 59.3к, 25.3к.\n- ОПИСАНИЕ: Убить Тсекани Хайафа (герой 43 лвл.) в гробнице Звезд.\n- НАГРАДА: Золотой глаз Рах-Нутта (карман) (Поглощает до 3515 урона в ближнем бою за 20 сек).\n————————————————————————————————\n- КООРДИНАТЫ: 60.3к, 32.1к.\n- ОПИСАНИЕ: Убить (герой 43 лвл.) в гробнице Неба.\n- НАГРАДА: (карман).\n————————————————————————————————\n\nTEZARIUS © 2009",
				},
				
			},
		},

		

	},
}
