TTitan.Data.HistoryHEvDE = {}

function TTitan.Data.HistoryHEvDE.GetData()
	return TTitan.Data.HistoryHEvDE.RawData
end

function TTitan.Data.HistoryHEvDE.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.HistoryHEvDE.RawData)
end

TTitan.Data.HistoryHEvDE.RawData =
{
	header = L"История и знания - Высшие эльфы против Темных эльфов", -- History & Lore Unlocks - High Elves vs Dark Elves
	categories =
	{
		{
			name = L"The Blighted Isle",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Shining Guard",
					text =
[[
You get this by talking to Eliryel Sorrowblade the career trainer near Prince Eldrion at 43000, 8500.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Eltharin, Language of the High Elves",
					text =
[[
Starting area. Just go past the tower and to your right. Book is right there. I wasn't able to unlock this until I had the quest to look at the book, Tomes of Knowledge I.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Benevolent Protectors",
					text =
[[
You get this by participating / exploring PQs just outside of Adunei.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Masters of Sword, Bow and Spear",
					text =
[[
House Arkaneth PQ. As you face the large building, there is an armoire tucked away on the left, co-ordinates (52070,8525). Click it.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Ancient History",
					text =
[[
During the quest Tears of Lileath (co-ordinates 56065,13980), where you collect the tears there is a bookshelf. Click on it.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The War of the Beard",
					text =
[[
Enter Lacorith from the south and then turn left. Go to the table at co-ordinates (37400,37860) and click on the scroll.
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Beastmasters of Karond Kar",
					text =
[[
This unlock is achieved by examining a chain, labeled "Manacles of Wrought Iron," in the ruins north of Chapter 3 camp. The chains are located on the right-hand of the ruins obscured by a twisted tree, it's behind, well hidden. Location is (14530, 47785).
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"House Uthorin",
					text =
[[
This unlock is achieved by examining a banner outside the south side (facing the lake) of the tent containing Kerryn Darkwater, in the Mistwood camp, in Chapter 2 of the Dark Elf storyline. The location is approximately (12808, 32731).
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Witch King",
					text =
[[
This unlock is achieved by examining a banner (Dark Elf Gonfalon) on the east side of a big tent by the sea in Eranneth, on the far side in high elf territory. This one is one of the hardest to get as you have find a way to drop down from the cliffs and avoiding the order guards nearby while getting to the other side. The location is approximately (59597, 41011)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Vile Perversions",
					text =
[[
This unlock is achieved by examining a "Defiled Body" in the Northeast corner of Mistwood past the Mistwood Grove PQ (Chapter 2), on a peninsula surrounded by cliffs and guarded by Griffins called Dreadtalon Guardians. The body is near one of the nests hugging the edges of the cliff and the location is approximately (33854, 27488) Note: Beware of the Champions (Rank 4) in the area for those who want to get it early on.
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Dark Elf Treachery",
					text =
[[
This unlock is achieved by examining "A Discarded Message" on the ground on the opposite of the Spires of Narthain PQ (Chatper 1). Find it next to on the west side of a series of worn short walls and burnt trees, right next to a destroyed bolt thrower close to Isha's Garden. It is in the same area that the parchment quest is, which can make location difficult. (8371, 20506)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"The Black Arks of Naggaroth",
					text =
[[
This unlock is achieved by standing on the ledge, overlooking the Black Ark, to the east of the Black Ark Landing camp. The location is approximately (10336, 8913)). It is only available while on the quest to get this particular unlock.
]],
				},
			},
		},

		{
			name = L"Chrace",
			entries =
			{
				{
					name = L"The Shadow Warriors",
					text =
[[
Near the entrance to the Shadowlands, there is a camp of shadow warriors. They surround a large blighted tree to the North of the Shadowlands entrance. Examine the body of a tortured dark elf. (3495,51205)
]],
				},

				{
					name = L"Manbane",
					text =
[[
On a hill near Thanuil's Retreat. Among some trees there are some flowers. Click on the flowers to get this unlock. (31744, 41779)
]],
				},

				{
					name = L"Thanuil's Retreat",
					text =
[[
This is a proximity unlock in the major landmark Thanuil's Retreat. Go to the top of the hill where the building and Thanuil are to unlock it. (37000, 40000)
]],
				},

				{
					name = L"The White Lions of Chrace",
					text =
[[
Immediately west of the first public quest for dark elves in this zone is a forest with a camp of White Lions in it. This entry is a proximity unlock in the middle of the camp of White Lions. (8164, 9887)
]],
				},

				{
					name = L"Aenarion",
					text =
[[
On a bluff overlooking a high elf city below. Click on "A memorial to Aenarion", which is a small block of stone with some elvish script on it. (55808, 19251)
]],
				},

				{
					name = L"Khaine",
					text =
[[
Examine the body of a Blade Sacrifice next to a blood cauldron in Lyrianna's Mansion. (63181, 42906)
]],
				},

				{
					name = L"The Menhir Stones",
					text =
[[
Proximity unlock, around Stone of Imrathir. (60006, 53965) May unlock behind the camp at around 57447, 56398 or at 60293,48234 also.
]],
				},

				{
					name = L"The Vortex",
					text =
[[
Proximity unlock, near a Menhir Stone in the Illboughs. (8294, 42598)
]],
				},
			},
		},

		{
			name = L"The Shadowlands",
			entries =
			{
				{
					name = L"Nagarythe",
					text =
[[
Proximity unlock, in small camp near the road around 42k, 31k.
]],
				},

				{
					name = L"A People Divided",
					text =
[[
Proximity unlock, on the western coast near 3k, 28k look on a hilltop for a statue overlooking the sea.
]],
				},

				{
					name = L"Dark Magic",
					text =
[[
A "Worn Cage" near 54k, 3.8k
]],
				},

				{
					name = L"The Noble Houses of Naggaroth",
					text =
[[
"Weathered ScrolL"on ground between two trees overlooking a Dark Elf NPC camp at 52604, 31894.
]],
				},

				{
					name = L"Rise to Power",
					text =
[[
Click on "Forgotten Tablet" found at 62,915 39,933 near Forellion Lightbringer.
]],
				},

				{
					name = L"Land of Chill",
					text =
[[
Click on the tent General Issue Tent at 12k, 45k. May find it at 19,923 32,244 if not at original location.
]],
				},

				{
					name = L"Intrigues of the Dark Elves",
					text =
[[
An Emptied Coffer on top of a hill around 25600, 14000, located at the base of a tree - West of Empty Weapons rack)
]],
				},

				{
					name = L"The Shadow Warriors' Vigil",
					text =
[[
"Recent Campfire" in Shattered Basin, Dark Elf Chapter 6, southwest of Sundered Strand. (1498, 14605)
]],
				},

				{
					name = L"The Price of Vengeance",
					text =
[[
An "Emptied Weapon Rack" on the south side of a large spire at about 29127, 14564
]],
				},

				{
					name = L"The Outer Kingdoms",
					text =
[[
A scroll (Outer Kingdoms Map) at the entrance to a small tent in the Shattered Basin, close to Dark Elf Champion NPC Ardelon Nisothar. (15565, 15155)
]],
				},
			},
		},

		{
			name = L"Ellyrion",
			entries =
			{
				{
					name = L"The Steeds of Ellyrion",
					text =
[[
"Dead Ellyrion Steed" beside the upright wagon at 49k, 22k
]],
				},

				{
					name = L"High Magic",
					text =
[[
Small tablet, "Mage Stone", on a ledge with lions, around 18600,19800 also found near 17900,20400
]],
				},

				{
					name = L"Ithilmar",
					text =
[[
At approx. 40K, 41K, behind the tower, a piece of damaged armor. Right up against the mountain range.
]],
				},

				{
					name = L"The Silver Helms",
					text =
[[
Walk near where the roads meet at 26k, 30k.
]],
				},

				{
					name = L"The Seers of Ghrond",
					text =
[[
Kill the "Seer of Ghrond" overlooking camp at 61k, 6.5k
]],
				},

				{
					name = L"Vile Perversions",
					text =
[[
There is a Ellyrian Reaver shackled to the ground with Prisoner Shackles. Examine the shackles. 51302,59290
]],
				},

				{
					name = L"Corsair Raids",
					text =
[[
Along the eastern beach, near 63000,51000.
]],
				},

				{
					name = L"Har Ganeth Executioners",
					text =
[[
Weapon Rack at 32k,46k, HE side of the gate.
]],
				},

				{
					name = L"Balance and Discipline",
					text =
[[
Broken High Elf Bust at about 8909, 50850 (NOTE: it is small and looks like a colon)
]],
				},

				{
					name = L"The Lessons of the Past",
					text =
[[
Examine the neglected brazier at 6k,45k.
]],
				},
			},
		},

		{
			name = L"Avelorn",
			entries =
			{
				{
					name = L"The Temple of Khaine",
					text =
[[
"A tree trunk" at 9100, 4600
]],
				},

				{
					name = L"Shrine of Solace?",
					text =
[[
"Neglected Altar" inside ruined temple at 6000, 62000 NOTE: Coordinates are correct but unlock Twilight of the High Elves for Saphery
]],
				},

				{
					name = L"Death Night",
					text =
[[
Denied Reveler body at 13440, 28186.
]],
				},

				{
					name = L"Assassins",
					text =
[[
There is a hanging corpse "A Hung Peasant" in 38011,29983 use it to get the unlock.
]],
				},

				{
					name = L"The Cauldrons of Blood",
					text =
[[
IN Saphery!!! Likely a bug but still able to obtain it. Click on A Weathered Cauldron at 52728, 59469.
]],
				},

				{
					name = L"Bitter Rivals",
					text =
[[
Click on the corpse A Tortured Rival at 33180, 20597.
]],
				},

				{
					name = L"Witchbrew",
					text =
[[
Click on the bottles Assorted Concoctions at 13407, 47486 inside the tent.
]],
				},

				{
					name = L"Lethal Poisons",
					text =
[[
In Saphery, 30800k 63600k
]],
				},

				{
					name = L"Isha",
					text =
[[
Click on "An Elder Root" at roughly 15000, 61000
]],
				},

				{
					name = L"Astarielle, the First Everqueen",
					text =
[[
A book named "A History of Elves" located at 3994,52712 close to the water
]],
				},

				{
					name = L"Land of Eternal Summer",
					text =
[[
Go south of the Jade Coast (Chapter 12 DE)It's right off the west side of the road, next to a tree, there's a Bone-Dry stein on the ground. This is also nearby a camp of High Elves for some of the quests given at Chapter 12 along with a group trolls. Exact location to it are (7995, 51435)
]],
				},

				{
					name = L"The Inner Kingdoms",
					text =
[[
Click on high elf "Heart stricken warrior", 20535, 1573.
]],
				},

				{
					name = L"The Maiden Guard",
					text =
[[
Click on the shield Maiden's Guard on the ground behind the tower on the east side of Grimwater 61460, 9131.
]],
				},

				{
					name = L"Malekith and Morathi",
					text =
[[
Click on the skull on the table titled A Clear Message in a tent at 41868, 23218.
]],
				},

				{
					name = L"Seafaring Tradition",
					text =
[[
Approach the ship at 1507,59376.
]],
				},
			},
		},

		{
			name = L"Saphery",
			entries =
			{
				{
					name = L"The Crimson Shroud",
					text =
[[
Click on the Basin of Skulls at 57597, 16553.
]],
				},

				{
					name = L"The Cornerstone",
					text =
[[
"Dedication plaque" on the ground, below a statue at 18940,50659
]],
				},

				{
					name = L"Conclave of the Winds",
					text =
[[
"Broken mage staff" among the tree cluster at 10420, 57082
]],
				},

				{
					name = L"Dragon Mages",
					text =
[[
Click on Fire Pit at 61716, 32880.
]],
				},

				{
					name = L"Twilight of the High Elves",
					text =
[[
Click on the Neglected Altar in the ruined building at 5833,62652 NOTE: Coordinates are correct its in Avelorn though.
]],
				},

				{
					name = L"The Siege of Silverglass Stand",
					text =
[[
Click on the Broken Rock Lobber outside orc Stronghold near 39097, 42392.
]],
				},

				{
					name = L"Noble Privilege",
					text =
[[
Click the tent @ 37.5k, 17k. It's up on the cliff, above the nests.
]],
				},

				{
					name = L"The Glade of Reflection",
					text =
[[
Approach the isolated well near 32k, 20k.
]],
				},

				{
					name = L"The Skyrage Griffons",
					text =
[[
Walk near the griffon nests at around 40295, 34378.
]],
				},

				{
					name = L"Dark Pact",
					text =
[[
Approach the fountain near 49k, 28k. NOTE: You have to approach through the "gate" inbetween the two flags/colons for it to work.
]],
				},

				{
					name = L"First Among Equals",
					text =
[[
Banner at River behind tree 24117,48609 
]],
				},

				{
					name = L"Stoic Defenders",
					text =
[[
Sorceress Cauldron at 27648, 54144
]],
				},

				{
					name = L"Highborn",
					text =
[[
???
]],
				},

				{
					name = L"Cold One Knights",
					text =
[[
Walk near the pen with Cold Ones in it. I actually got this in Avelorn at approximately 57.3 32.8. Unsure if it is a bug or not. NOTE: This is on the second page of the Saphery unlocks. (more information is needed)
]],
				},

				{
					name = L"The Cauldrons of Blood",
					text =
[[
IN Saphery!!! Likely a bug but still able to obtain it. Click on A Weathered Cauldron at 52728, 59469.
]],
				},

				{
					name = L"Lethal Poisons",
					text =
[[
In Saphery, 30800k 63600k
]],
				},
			},
		},

		{
			name = L"ПРОБУЖДЕНИЕ ДРАКОНА (Dragonwake)",
			entries =
			{
				{
					name = L"• Воул (Vaul)",
					text = L"27.9к, 53.4к.\nПройти между 2-мя статуями.\n\n————————————————————————————————\n\n27.5, 53k Proximity by the statue(eastern one). Order Side.",
				},

				{
					name = L"• Слепые кузнецы (The Blind Smiths)",
					text = L"22.3к, 47.5к.\nНа горе найти и прочесть книгу Песнь о слепых кузнецах.\n\n————————————————————————————————\n\n22.51k, 47.69k click on the book 'The Lay of the Blind Smith'",
				},

				{
					name = L"• Драконий хребет (The Dragon Spine Mountians)",
					text = L"20.1к, 62.5к.\nПосетить область горы.\n\n————————————————————————————————\n\nClimb the mountain to the south of a Dark Elf encampment until you reach around 20k,63.5k.",
				},

				{
					name = L"The Taming of Bracchus",
					text =
[[
————————————————————————————————
Click on 'A Beastmaster's Tome' that sits on a crate by a campfire at 10k, 21k
]],
				},

				{
					name = L"• Дрессировщики Каронд Кара (Beastmasters of Karond Kar)",
					text = L"23.4к, 53.8к.\nПосетить область.\n\n————————————————————————————————\n\nWalk near the cages at around 22994, 54077.",
				},

				{
					name = L"The Sacred Flame",
					text =
[[
???
]],
				},

				{
					name = L"The Phoenix Guard",
					text =
[[
————————————————————————————————
Click on "A Simple Headstone" at 13200,31000. 
]],
				},

				{
					name = L"• Паломничество (The Pilgrimage)",
					text = L"61.8к, 11.3к.\nВозле горы найти и осмотреть Старую часовню.\n\n————————————————————————————————\n\nClick on an Aged Wayshrine at 61866, 11385.",
				},

				{
					name = L"The Black Heart Coven",
					text =
[[
————————————————————————————————
38.5k, 40k Click on the "pile of skulls" 
]],
				},

				{
					name = L"The Dragonwake Sentinels",
					text =
[[
???
]],
				},

				{
					name = L"• Сосуд Кейна (A Vessel of Khaine)",
					text = L"55.4к, 59к.\nВозле водопада найдите и осмотрите Потерянный мешок.\nСпуститься к водопаду можно сойдя с дороги в 64к, 53к.\n\n————————————————————————————————\n\nClick on a 'Worn Sack' at 55400,59000. This is in the area below the waterfall and the cliffs, so you should approach from the road around 64000,53000.",
				},

				{
					name = L"Monument to the Ancient Fallen",
					text =
[[
————————————————————————————————
Click the small stone on ground labeled "Monument to the Ancient Fallen" at 20k, 53k. It is at the top of the peninsula just west of the Ritual of Light above the giant statue. (Isle of the Dead) 
]],
				},

				{
					name = L"Downward Spiral",
					text =
[[
————————————————————————————————
Approach the end of the beach at 39k, 5.8k next to Insane Sorcerer (Isle of the Dead).
]],
				},

				{
					name = L"Timeless",
					text =
[[
————————————————————————————————
Walk near 40445, 28386 in the Isle of the Dead zone.
]],
				},

				{
					name = L"Aenarion and the Daemons",
					text =
[[
————————————————————————————————
Approach the Obelisk at 40k, 35k (Isle of the Dead).
]],
				},
			},
		},

		{
			name = L"Caledor",
			entries =
			{
				{
					name = L"Beastmasters of Karond Kar",
					text =
[[
Empty Goblet on top of a stone at 52685, 4455
]],
				},

				{
					name = L"Caledor Dragontamer",
					text =
[[
Dragontamer Monument in the forest at 1.25, 19.22
]],
				},

				{
					name = L"The Dragon Princes",
					text =
[[
64k,40k Dragon Prince Horse, there are two located side by side between two trees near the cliff.

Lucrin
]],
				},

				{
					name = L"Truesteel ",
					text =
[[
'Crate of Truesteel' at 45k, 43k
]],
				},

				{
					name = L"A Memory of Fire",
					text =
[[
30k, 53k Volcanic Rock in the middle of the river.

Lucrin
]],
				},

				{
					name = L"Reaper Bolt Thrower",
					text =
[[
Use the Reaper Bolt Thrower at 64.4k, 12.8k 
]],
				},

				{
					name = L"Lost Legacy",
					text =
[[
Walk under the stone arch near the PQ and the road at 47078, 16666.
]],
				},

				{
					name = L"A First Time for Everything",
					text =
[[
Next to Kelysian's Landing at 24500, 4600 just walk near the water, near the large dwarven vessel
]],
				},

				{
					name = L"Caledor the Conqueror",
					text =
[[
???
]],
				},

				{
					name = L"The Price of Disloyalty",
					text =
[[
Cage at the side of the road at 10945, 12255
]],
				},

				{
					name = L"Prideful Arrogance",
					text =
[[
Walk under the little building with chairs and a harp at 16819, 3072.
]],
				},

				{
					name = L"The Drakefall Defenders",
					text =
[[
Click on the Sun Bleached bone near a giant ribcage next to the road at 43,600 49,000
]],
				},

				{
					name = L"Tethlis the Slayer",
					text =
[[
Click on a pile of books on top of small stairs on the back of the PQ tower at 8602, 3149.
]],
				},

				{
					name = L"The Trail of Darian Stormspear",
					text =
[[
Notably Worn Bedroll at 64k 58k. South of the cage holding the Dark Steeds (which is at the end of a path that leads up to the top of the hill). Jump down to the ledge below. The bedroll is behind a tree at the south end of the ledge.

Lucrin
]],
				},

				{
					name = L"Fickle Favor",
					text =
[[
Walk near the altar at 4344, 33480.
]],
				},
			},
		},

		{
			name = L"Eataine",
			entries =
			{
				{
					name = L"Asuryan",
					text =
[[
Inspect the Shrine of Asuryan at 21500,62300
]],
				},

				{
					name = L"The Lothern Seaguard",
					text =
[[
A Seaguard Banner on lower dock at 62.4k, 6.6k. Destruction can approach from the water to click on it. 
]],
				},

				{
					name = L"High Elf Colonies",
					text =
[[
Small campfire remains. 1k, 11k
]],
				},

				{
					name = L"Eagle's Claw Bolt Thrower",
					text =
[[
Bolt Thrower at 13k, 53k
]],
				},

				{
					name = L"The Straits of Lothern",
					text =
[[
Cornerstone Plague, on ground outside tower. 54k, 21k
]],
				},

				{
					name = L"The Third Gate",
					text =
[[
Click on "A Stored Dinghy" underneath the docks at 64000,47000. 
]],
				},

				{
					name = L"Lileath",
					text =
[[
A gem adorned tome behind a shadow warrior tent at 39.7K 15.2K
]],
				},

				{
					name = L"Orcification",
					text =
[[
Orc landing banner, infront of the orc barge. 6k, 5.5k
]],
				},

				{
					name = L"The Underworld Sea",
					text =
[[
Locked chest, at beach. 2k, 9k
]],
				},

				{
					name = L"Repeater Crossbow",
					text =
[[
Roadside Memorial, at 25k, 49k

Lucrin
]],
				},

				{
					name = L"Tiranoc Chariot",
					text =
[[
Chariot in front of tower. Proximity unlock at 14k,54k

Lucrin
]],
				},

				{
					name = L"Cold One Chariot",
					text =
[[
Proximity unlock by stepping close to the Chariot at 15.7k, 23.9k 
]],
				},

				{
					name = L"A Dark Day (Enemies in the Inner Sea)",
					text =
[[
Run by the Orc "ship" 31.7k 2.9k
]],
				},

				{
					name = L"Stronger Bonds",
					text =
[[
Click on the 'Empty Jug' at 29k, 15k
]],
				},

				{
					name = L"Cold One Knights",
					text =
[[
Gnawed Bones, outside the fort walls. 47k, 13k
]],
				},
			},
		},
	},
}
