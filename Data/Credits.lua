TTitan.Data.Credits = {}

function TTitan.Data.Credits.GetData()
	return TTitan.Data.Credits.RawData
end

function TTitan.Data.Credits.FilterData()
end

TTitan.Data.Credits.RawData =
{
	header = L"FAQ & Credits",
	categories =
	{
		{
			name = L"FAQ",
			entries =
			{
				{
					name = L"About this Add-on",
					text =
[[
Welcome to Tome Titan! The purpose of this add-on is to take information posted in various important Warhammer Alliance threads and list them here in-game in an easy to read and find format.

In short: To keep you from having to alt-tab.


We have information on Bestiary, History & Lore, and Noteworthy Persons unlocks as well as lists of Lairs, Explorations, and Pursuits. We also have Apothecary, Cultivating, and Talisman Making information. The add-on will also track if unlocks have been completed and can either hide them altogther, or mark them as completed, accordingly.


To add a macro for easy access to Tome Titan, open the Main Menu, select Macros, and use this as your macro text:

/script TTitan.UI.ToggleShowing()

You can also make a macro for going to the current zone:

/script TTitan.UI.ExpandCurrentZone(); TTitan.UI.Show()


Please use "/ttitan help" for a full list of commands. If you have any issues, don't hesitate to comment on the project's page on Curse.com or submit a ticket in our issue tracker at CurseForge.

http://war.curse.com/downloads/war-addons/details/ttitan.aspx
http://war.curseforge.com/projects/ttitan/tickets/
]],
				},

				{
					name = L"How often is it updated?",
					text =
[[
This add-on WILL need to be updated about once a week to keep the information, well, up to date. It is, however, possible that the add-on will be updated daily - and sometimes even several times a day!

You can do this by visiting this link:
http://war.curse.com/downloads/war-addons/details/ttitan.aspx

Or by using the Curse Client to automatically update this and all your other add-ons:
http://www.curse.com/client/

We will do our best to make sure the data files are updated at LEAST once a week, if not faster.

In the event that the add-on will malfunction or stop working altogether because of changes to the game, it will be updated as fast as possible.
]],
				},

				{
					name = L"Can you add...",
					text =
[[
Post a ticket with your feature request on curseforge here:
http://war.curseforge.com/projects/ttitan/tickets/

And we'll get back to you as soon as possible if it's possible or not. You can also see other feature requests that are pending or have been implemented.

If you think we should have a page for another type of information, please contact one of us or post a ticket to the CurseForge project!
]],
				},

				{
					name = L"Can I use the code?",
					text =
[[
Please contact us first as we haven't yet released the code publicly.
]],
				},
			},
		},

		{
			name = L"Credits",
			entries =
			{
				{
					name = L"The Tome Titan Team",
					text =
[[
That's a lot of T's! But seriously, Tome Titan is updated and mainted by a number of people. I will list them here along with their roles.

Aenathel -- Project Manager, Main Coder, Data Updates
   Aenathel has really improved the add-on a lot since joining the Tome Titan Team - he is the main coder and the guy working the magic of the latest updates. Nearly the entire add-on has been rewritten to be easier to use, and provide much more functionality thanks to him. All major code changes go through him. He's also helped out on cleaning up a lot of the various data files.

Oscarr -- Bestiary and Lair Locations Updates, Minor Coding
   Oscarr originally started this project as Bestiary Helper. It was based off of the Killerguides add-on (with their permission) used for displaying data in a nice two-paned window - the same window used by the in-game Help. Oscarr and Squigy got together later on and decided to rename the project as Tome Titan and started including more data. Oscarr wrote the TTBar directory that handles modules for the popular Board/Bar add-ons. He also attempts to assist Aenathel with coding when and where possible.

SquigyStardust -- History & Lore and Noteworthy Persons, Minor Coding
   Squigy handles updating History & Lore and Noteworthy Persons unlocks data. He also added a button for each unlock section.

Other -- Other pages/authors
   Cultivation Page -- Kavel of Wolfenburg handles the Cultivation Page.
   Apothecary Page -- To be announced.
   Talismans Page -- To be announced.
]],
				},

				{
					name = L"The Community",
					text =
[[
Huge thanks to the community and posters at WarhammerAlliance.com and various other sites who have submitted all the information. Without them, this wouldn't be possible. Keep up the great work and team-spirit guys and gals.

If you think we should have a page for another type of information, please contact one of us or post a ticket on the CurseForge project here:
http://war.curseforge.com/projects/ttitan/tickets/
]],
				},

				{
					name = L"Thread Maintainers",
					text =
[[
Shrewd - Bestiary Unlocks Thread
    Shrewd is the man who maintains the WarhammerAlliance thread here: http://www.warhammeralliance.com/forums/showthread.php?t=121526
He's also assisted me a lot with the add-on early on, testing it, and providing feedback and suggestions on the data formats. Big thanks to him!

History and Lore Unlocks Thread // HammerWiki
    burfo is the man who maintains the Warhammer Alliance thread here: http://www.warhammeralliance.com/forums/showthread.php?t=149022
A lot of the information also comes from HammerWiki: http://warhammeronline.wikia.com/

Exploration and Pursuit Unlocks Thread


The Lair Locations Thread
    magos is the man who maintains the Warhammer Alliance thread here: http://www.warhammeralliance.com/forums/showthread.php?t=132580
]],
				},
			},
		},

		{
			name = L"Changes",
			entries =
			{
				{
					name = L"Version 2.1.22",
					text =
[[

Updated a few entries.

Released on 18.04.2009.
]]
				},
				
				{
					name = L"Version 2.1.21",
					text =
[[
CODE

Hopefully fixed most isses with the code caused by coding errors in earlier versions.

DATA

Updated a few entries.

Released on January 23, 2009.
]]
				},

				{
					name = L"Version 2.1.17",
					text =
[[
DATA

Updated History Section, Cleared version notes (Pruned a few to keep it neat).

Returned to version 2.1.15 main code to avoid ALL the problems the button fix did. This should hopefully fix all the problesm but unfortunately will make the button problem stay.

]]
				},

				{
				name = L"Version 2.1.16",
				text =
[[
DATA

Updated Noteworthy and History Sections, Cleared version notes (Pruned a few to keep it neat).

Fixed Button Problem. (It comes up with an error but it stops the problem, until i can isolate the error you'll have to live with it for now.)

]]
				},

				{
					name = L"Version 2.1.15",
					text =
[[
DATA

Squigy released changes for the History and Noteworthy sections.

Released on November 28, 2008.
]],
				},

				{
					name = L"Version 2.1.14",
					text =
[[
ERROR:
Curseforge.com servers were full, and thus borked the 2.1.13 release. Have to re-release again under a new name, so it's going to be 2.1.14. Read 2.1.13 notes for changes.

Released on November 28, 2008.
]],
				},

				{
					name = L"Version 2.1.13",
					text =
[[
CODE CHANGES

Saving your settings: The main button showing (or not) and hide completed entries (or not) settings will now be saved and loaded each session.

Fixed the "FAQ and Credits" button on the main Tome Titan window properly collapsing all rows before opening to the About page.

Fixed the default Tome Titan button not showing up for people not using bar/board add-ons.

Removed an old, unneeded file, that was no longer in use from the TomeTitan directory.

Released on November 28, 2008.
]],
				},

				{
					name = L"Version 2.1.12",
					text =
[[
DATA CHANGES

Bestiary - Dwarf vs. Greenskins

    Mount Bloodhorn
    - Updated the Order version information for the unlock for Spite (Tainted Grove Spite) - A Defiled Life-Force

Released on November 26, 2008.
]],
				},

				{
					name = L"Version 2.1.11",
					text =
[[
DATA CHANGES

History/Lore - High Elf vs. Dark Elf

    Eataine
    - Corrected the name of A Dark Day (Enemies in the Inner Sea)

Bestiary - Dwarf vs. Greenskins

    Black Crag
    - Corrected the name of Let's See What This Does

CODE CHANGES

Changed the Layer parameter of TTBar - It will no longer appear ontop of the world map.

Released on November 26, 2008.
]],
				},

				{
					name = L"Version 2.1.10",
					text =
[[
CODE CHANGES

Fixed a problem where Tome Titan wouldn't work unless LibSurveyor was installed. Thanks to Test0r for pointing this out.

Released on November 26, 2008.
]],
				},

				{
					name = L"Version 2.1.9",
					text =
[[
CODE CHANGES

TTBar - A new pop-out bar (based off DevModBar) for going to any TomeTitan Page -- the bar is moveable via the layout editor. As a result of this, all the old buttons have been removed, save the FAQ button.

Achievements and Pursuits will now correctly 'check off' on completion!


DATA CHANGES

Exploration/Pursuit Unlocks - Updated to latest thread information 2008.11.26.

Bestiary Unlocks - Dwarfs vs Greenskins

    Barak Varr
    - Corrected the name of Off With His ...

Released on November 26, 2008.
]],
				},
			},
		},
	},
}
