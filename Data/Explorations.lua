TTitan.Data.Explorations = {}

function TTitan.Data.Explorations.GetData()
	return TTitan.Data.Explorations.RawData
end

function TTitan.Data.Explorations.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.Explorations.RawData)
end

TTitan.Data.Explorations.RawData =
{
	header = L"Достижения Исследования", -- Exploration Achievements
	categories =
	{
		{
			name = L"Описание страниц", -- About This Page
			entries =
			{
				{
					name = L"Исследования", -- Explorations
					text =
[[
These are Pursuit Unlocks found in the Achievements section of your ToK.
Use "/ttitan explorations" to open the Explorations page.
]],
				},

				{
					name = L"Пояснение", -- Credit
					text =
[[
The information found here is directly from the Warhammer Alliance Forums "The Unofficial Exploration/Pursuit Achievement Unlock Thread". You can find it here:
http://www.warhammeralliance.com/forums/showthread.php?t=180277

The thread is maintained by Socran of the Red Eye Mountain server.
]],
				},
			},
		},

		{	-- Divider, won't expand when clicked
			name = L"---------",
			divider = true,
			entries = {},
		},

		{
			name = L"Cold One Hunters (1)",
			entries =
			{
				{
					name = L"Cold One Hunters",
					text =
[[
Cold One Hunters (Cold One Hunters) - BLIGHTED ISLE. Both factions, possibly different triggers. Destruction unlocks at the Cold One Corral at Isha's Garden. Order unlocks around 50K, 48K, near where the bears spawn. Approach the Cold Ones near a tree bent at a 45 degree angle.
By Bilious and Gargol.
20081122
]],
				},
			},
		},

		{
			name = L"Darkhelm Resistance (1)",
			entries =
			{
				{
					name = L"Darkhelm Residence",
					text =
[[
Darkhelm Residence (Darkhelm Residence) - BARAK VARR @ 47, 19. Both factions. Enter Longbeard's Strand (the cave just East of the RvR lake) and keep going until you get to the bridge, cross it and turn left. You'll see a door which is not clickable (just a piece of wall texture), walk up to it to get the unlock. (Better directions for Destruction would probably be helpful.)
By rzeuss.
]],
				},
			},
		},

		{
			name = L"Dead in the Water (3)",
			entries =
			{
				{
					name = L"Dead in the Water",
					text =
[[
Dead in the Water (Dead in the Water) - BARAK VARR. Both factions. In the RvR lake, west along the beach from the Ironclad objective, is a sea creature's skeleton that gives a History/Lore unlock when approached. Swimming far north of this place, you'll eventually see a red sea mine in the distance. Right-click it to get this unlock, as well as a mine fragment.
By Socran.
]],
				},

				{
				realm = TTitan.Data.Realm.ORDER,
				name = L"Settin' Things Straight",
					text =
[[
Dead in the Water (Settin' Things Straight) - BARAKK VARR. (O) only. After clicking on the mine, you can take the piece of it to a dwarven Siege Engineer in Thane's Defense (dwarf chapter 7) which gives the unlock.
By SlasherZet and Terrok.
]],
				},

				{
				realm = TTitan.Data.Realm.DESTRUCTION,
				name = L"Wot's Dis Den?",
					text =
[[
Dead in the Water (Wot's Dis Den?) - (D) only. After clicking on the mine, you can bring the piece the to the Greenskin Chapter camp north of it and there will be a goblin metalhead (a random goblin with a large helmet on) and right click him for this tome unlock. Afterwards, the goblin will promptly explode dead.
By Terrok.
]],
				},
			},
		},

		{
			name = L"Everlastink Nob Toppa (1)",
			entries =
			{
				{
					name = L"Everlastink Nob Toppa",
					text =
[[
Everlastink Nob Toppa (Everlastink Nob Toppa) - MOUNT BLOODHORN @ 18, 37. Both factions. Examine Ranklot's Pile, a tiny piece of trash next to a much larger pile, in Da Gobbo Camp. It despawned after clicking on it, so it may not always be there.
By Cowhugger Mike and Kynnae.
20081123
]],
				},
			},
		},

		{
			name = L"Gettin' Slopped (1)",
			entries =
			{
				{
					name = L"Gettin' Sloped",
					text =
[[
Gettin' Slopped (Gettin' Sloped) - MOUNT BLOODHORN. (O), probably (D). There's a small Slop Bucket in Ranklot's hut in Da Gobbo Camp. Click on it for the unlock. It has several spawn points inside the hut. Ranklot has a fast respawn timer, so be prepared to fight your way out.
By Zenae.
20081123
]],
				},
			},
		},

		

		{
			name = L"Marsh Choppa (1)",
			entries =
			{
				{
					name = L"Marsh Choppa",
					text =
[[
Marsh Choppa (Marsh Choppa) - MARSHES OF MADNESS @ 28, 7. Both factions. Inside a cave is a Choppa in a web-covered ditch. Right-click it for the unlock.
By Nirmalti, additional info by rzeuss.
]],
				},
			},
		},

		{
			name = L"Missions of Mercy (4)",
			entries =
			{
				{
					name = L"Lady in Distress",
					text =
[[
Missions of Mercy (Lady in Distress) - NORDLAND. Both factions, risky for (D). In the Empire Chapter 2 camp, go behind the Grey Lady Inn and kill some rats for this unlock.
By Socran, additional info by I dO VoOdOo.
]],
				},

				{
				realm = TTitan.Data.Realm.ORDER,
				name = L"Waterbearer",
					text =
[[
Missions of Mercy (Waterbearer) - NORDLAND. Probably (O) only. In the Empire Chapter 2 camp, find the bucket of water near the well. Take the road back toward the previous camp and give the water to one of the Wounded Griffons along the road.
By Nahario.
]],
				},

				{
				realm = TTitan.Data.Realm.ORDER,
				name = L"Tending the Sick",
					text =
[[
Missions of Mercy (Tending the Sick) - NORDLAND. Probably (O) only. In the Empire Chapter 3 camp, look around for a crate of supplies that you can interact with. It should give you some food. Head west from the camp to the tent where the doctor is tending sick patients, and talk to one of the patients to give him your food.
By Socran.
]],
				},

				{
				realm = TTitan.Data.Realm.ORDER,
				name = L"Missions of Mercy",
					text =
[[
Missions of Mercy (Missions of Mercy) - NORDLAND. Probably (O) only. Complete the previous three unlocks.
By Socran, Nahario, and Bilious.
]],
				},
			},
		},

		{
			name = L"Raiders (1)",
			entries =
			{
				{
					name = L"Northern Raiding Route",
					text =
[[
Raiders (Northern Raiding Route) - CHRACE @ 13, 23. Both factions. In the back of the Emlyria Caverns, walk through an inconspicuous pool of water along the wall. (NOTE: Contrary to the map's appearance, you enter the caverns from the north, not the south.)
By Assuran.
]],
				},
			},
		},

		{
			name = L"Ranks of the Damned (4)",
			entries =
			{
				{
					name = L"Ranks of the Damned, Kalarthay the Mad",
					text =
[[
Ranks of the Damned (Ranks of the Damned, Kalarthay the Mad) - INEVITABLE CITY. (D), possibly (O) during a city siege. Examine the pile of books, Ranks of the Damned, in the room at the end of the left hall of the Lyceum (library) in Inevitable City. The door requires city rank 4 to open, and the exact unlock seems to differ between characters.
By Kellithe.
20081123
]],
				},

				{
					name = L"Ranks of the Damned, Ileon Blackwing",
					text =
[[
Ranks of the Damned (Ranks of the Damned, Ileon Blackwing) - INEVITABLE CITY. (D), possibly (O) during a city siege. Examine the pile of books, Ranks of the Damned, in the room at the end of the left hall of the Lyceum (library) in Inevitable City. The door requires city rank 4 to open, and the exact unlock seems to differ between characters. (NOTE: Ileon him(?)self has been reported as spawning after clicking on the "Font of Souls" located elsewhere in the Inevitable City. However, the font cannot be clicked under most circumstances.)
20081125
By rhadamanthos and I dO VoOdOo.
]],
				},

				{
					name = L"Ranks of the Damned, Maesius Blackwing",
					text =
[[
Ranks of the Damned (Ranks of the Damned, Maesius Blackwing) - INEVITABLE CITY. (D), possibly (O) during a city siege. Examine the pile of books, Ranks of the Damned, in the room at the end of the left hall of the Lyceum (library) in Inevitable City. The door requires city rank 4 to open, and the exact unlock seems to differ between characters.
By Assuran.
20081123
]],
				},

				{
					name = L"Ranks of the Damned, Darul Kingslayer",
					text =
[[
Ranks of the Damned (Ranks of the Damned, Darul Kingslayer) - INEVITABLE CITY. (D), possibly (O) during a city siege. Examine the pile of books, Ranks of the Damned, in the room at the end of the left hall of the Lyceum (library) in Inevitable City. The door requires city rank 4 to open, and the exact unlock seems to differ between characters.
By Kynnae.
20081123
]],
				},
			},
		},

		{
			name = L"Riphorn Challenge (2)",
			entries =
			{
				{
					name = L"Riphorn Challenge",
					text =
[[
Riphorn Challenge (Riphorn Challenge) - TROLL COUNTRY @ 63.5, 48.5. Both factions. Just go near the torches and the bunch of bones laying on the ground at the coordinates to get the unlock.
By quotifon.
]],
				},

				{
					name = L"Blood Rite",
					text =
[[
Riphorn Challenge (Blood Rite) - TROLL COUNTRY @ 63.5, 48.5. Kill Urghu Redhorn at the location of the previous unlock. Loot his skull and right-click it. May be on a long respawn timer or have other requirements to spawn.
Info by quotifon and Bowl the zealot.
20081125
]],
				},
			},
		},

		{
			name = L"Таинственный М.Г. (7)", -- The Mysterious M.B.
			entries =
			{
				{
					name = L"• Драгоценная фляга (ОСКВЕРНЕННЫЙ ОСТРОВ)", -- Precious Flask
					text = L"ОСКВЕРНЕННЫЙ ОСТРОВ (39.4к, 1.8к).\nВ самом начале лагеря 1 главы высших эльфов найти около деревьев Ценную флягу, которая поместиться в ваше инвентраь. Кликнув на нее откроеться запись.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nThe Mysterious M.B. (Precious Flask) - BLIGHTED ISLE. Both factions, incredibly risky for (D). In the High Elf starting camp, find and interact with a flask on a bench. Activate the item in your inventory for the unlock. (Needs specification, may be mixed up with other flasks in the same category.) (NOTE: Can also be handed off to other players and used multiple times. This is handy for Destruction players who don't want to get their hands dirty.)\nBy Socran, additional info by I dO VoOdOo.",
				},

				{
					name = L"Rare Flask",
					text =
[[
The Mysterious M.B. (Rare Flask) - EKRUND. (O), possibly (D) at great risk. In the Dwarf Chapter 2 camp, in the bar, find and interact with a flask on some crates. Activate the item in your inventory for the unlock. (NOTE: Can also be handed off to other players and used multiple times. This is handy for Destruction players who don't want to get their hands dirty.)
By Socran, additional info by I dO VoOdOo and Kynnae.
]],
				},

				{
					name = L"Unique Flask",
					text =
[[
The Mysterious M.B. (Unique Flask) - NORDLAND. Both factions, incredibly risky for (D). In the Empire Chapter 3 camp, find and interact with a flask on some crates. Activate the item in your inventory for the unlock. (NOTE: Can also be handed off to other players and used multiple times. This is handy for Destruction players who don't want to get their hands dirty.)
By Socran, additional info by I dO VoOdOo and Kynnae.
]],
				},

				{
					name = L"The Mysterious M.B.",
					text =
[[
The Mysterious M.B. (The Mysterious M.B.) - N/A. Possibly (O) only, unless the next three are (D) equivalents. Earned when you complete the first three achievements in this category.
By Socran.
]],
				},

				{
					name = L"Less than Meets the Eye",
					text =
[[
The Mysterious M.B. (Less than Meets the Eye) - MARSHES OF MADNESS. (O), possibly (D). There's a small mine in Oathhold full of scorpions. Click on the Dust Covered Flasks in the back of the cave. Seems to require the previous four unlocks first.
By Zenae.
20081123
]],
				},

				{
					name = L"Part 6 ???",
					text =
[[
The Mysterious M.B. (Part 6) - Unknown. Possibly a Destruction equivalent to the others?
]],
				},

				{
					name = L"Part 7 ???",
					text =
[[
The Mysterious M.B. (Part 7) - Unknown. Possibly a Destruction equivalent to the others?
]],
				},
			},
		},

		{
			name = L"Visions (2)",
			entries =
			{
				{
					name = L"Visions of Danger",
					text =
[[
Visions (Visions of Danger) - CHRACE @ 47, 49. (O), probably (D). Swim through the small pond just south of Lyriana's Repose.
By rzeuss, and Sildroni since I'm an idiot.
]],
				},

				{
					name = L"Part 2 ???",
					text =
[[
Visions (Part 2) - Unknown. Judging by the previous unlock, likely involves two elves meeting in front of a lone tree on a low-lying hill.
]],
				},
			},
		},

		{
			name = L"Will of the Assured (3)",
			entries =
			{
				{
					name = L"The Rune of Cython",
					text =
[[
Will of the Assured (The Rune of Cython) - BLIGHTED ISLE @ 59, 12. Both factions. Examine the white stone named "Rune of Cython" at the edge of the cliff. If you use it at night time (type /time to get the in-game time, the night begins after 6 p.m.) an Imp will spawns and lead you to the second rune. The Imp will wander right through an Order base with lvl 55 champions guarding it, so don't follow it as Destruction player.
By quotifon.
]],
				},

				{
					name = L"The Asking",
					text =
[[
Will of the Assured (The Asking) - BLIGHTED ISLE @ 48, 39. Both factions. If you follow the Imp he will lead you to the second rune. As a Destruction player go directly to the rune without following the Imp. Examine the "Rune of Cython" found on the hill in the middle of all the scorpions. Use it at night time (/time after 6p.m.) to unlock the Achievement. If you do it right a bunch of Imps will spawn around you.
By quotifon.
]],
				},

				{
					name = L"The Testing",
					text =
[[
Will of the Assured (The Testing) - CHRACE @ 13, 27.5. Both factions. Examine the "Rune of Cython" found at the coordinates between the trees on the small rise west of the bridge leading to the Dark Elves Chapter 4 camp at night time (/time after 6p.m.). This will spawn an Imp that will lead you to the fourth "Rune of Cython" (CHRACE: 11000, 47500) which you need to examine for the unlock. If you do it right a bunch of Imps will spawn around you.
By quotifon.
]],
				},
			},
		},

		{
			name = L"Вольфенбург (1)", -- Wolfenburg
			entries =
			{
				{
					name = L"• Вольфенбург (ОСТЛАНД)", -- Woldfenburg
					text = L"ОСТЛАНД (43.5к, 60к).\nНайдите город Вольфенбург дойдя о пересечения улиц, где стоит фургон и валяються трупы.\n\nОдно время Вольфенбург был блистательной столицей Остланда... до пришествия Хаоса.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nWolfenburg (Wolfenburg) - OSTLAND. Both factions. Simply walk into Wolfenburg in SE Ostland for the unlock.\nBy Socran.",
				},
			},
			
		},

		{
			name = L"Могила неизвестного послушника (1)", -- Grave of the Unknown Acolyte
			entries =
			{
				{
					name = L"• Могила неизвестного послушника (ОСТЛАНД)", -- Grave of the Unknown Acolyte
					text = L"ОСТЛАНД (62.5к, 45.8к).\nНедалеко от Мрачного моностыря найти Могильную плиту, спрятанную в кустах.\n\nТот, кто посвятил жизнь служению Империи, не всегда находит последнее пристанище в родном городе.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nGrave of the Unknown Acolyte (Grave of the Unknown Acolyte) - OSTLAND @ 62, 46. (O), probably (D). In the cemetery north of the cathedral east of Wolfenburg, click on a tombstone behind some bushes.\nBy Bilious.\n20081123",
				},
			},
		},
		
		{
			name = L"Древний плач (1)", -- Ancient Lament
			entries =
			{
				{
					name = L"• Древний плач (ОСКВЕРНЕННЫЙ ОСТРОВ)", -- Ancient Lament
					text = L"ОСКВЕРНЕННЫЙ ОСТРОВ (44.8к, 24к).\nПройдя через эльфов светлой и темной стороны, которые боряться друг с другом, найти на лавке Древний свиток и прочесть его.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nThe Tradeskills: Ancient Lament achievement category contains only one achievement.\nClick on an 'Ancient Scroll' on a bench at 44800,24000 in The Blighted Isle. Destruction Players will be flagged attempting to get this one as it lies on the ledge below the Order camp of Adunei where High Elf and Dark Elf NPCs are engaged in battle.",
				},
			},
		},
		
		{
			name = L"Земля Мертвых (5)", -- Land of the Dead
			entries =
			{
				{
					name = L"• Стела Ахары (МЕРТВЫЙ ГОРОД ЗАНДРИ)", 
					text = L"МЕРТВЫЙ ГОРОД ЗАНДРИ (58.9к, 38.7к).\nВ конце небольшой грабницы, охраняемой бандитами, осмотреть руну Ахары.\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{
					name = L"• Стела Ахафета (МЕРТВЫЙ ГОРОД ЗАНДРИ)", 
					text = L"МЕРТВЫЙ ГОРОД ЗАНДРИ (16к, 63к).\nНа самом краю карты среди песчаных бурь на вершине бархана найти руну Ахафета.\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{
					name = L"• Стела Ахатаха (МЕРТВЫЙ ГОРОД ЗАНДРИ)", 
					text = L"МЕРТВЫЙ ГОРОД ЗАНДРИ (16.8к, 35.6к).\nВозле высокого пика найти руну Ахатаха и активировать ее.\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{
					name = L"• Стела Ахазебаха (МЕРТВЫЙ ГОРОД ЗАНДРИ)", 
					text = L"МЕРТВЫЙ ГОРОД ЗАНДРИ (14.9к, 17.9к).\nВ углу небольших развалин, где прячиться одиночный рой скарабеев, найти и осмотреть руну Ахазебаха.\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
				{
					name = L"• Обнаружить дирижабль (МЕРТВЫЙ ГОРОД ЗАНДРИ)", 
					text = L"МЕРТВЫЙ ГОРОД ЗАНДРИ (59к, 44к).\nВыполнить сопровождающий квест по уничтожению вражеского Дирижабля.\n\nTEZARIUS © 2009\n\n————————————————————————————————",
				},
				
			},
		},
		
		
	},
}
