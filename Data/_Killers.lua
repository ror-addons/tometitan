TTitan.Data.Killers = {}

function TTitan.Data.Killers.GetData()
	return TTitan.Data.Killers.RawData
end

function TTitan.Data.Killers.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.Killers.RawData)
end

TTitan.Data.Killers.RawData =
{
	header = L"УБИЙСТВА", 
	categories =
	{
		{
			name = L"Описание страниц",
			entries =
			{
				{
					name = L"Описание",
					text =
[[
Информацию можно дополнить своими открытиями на форуме
http://forum.electronicarts.ru/showthread.php?t=10102

The information found here is directly from the Warhammer Alliance Forums "The Lair Thread". You can find it here:
http://www.warhammeralliance.com/forums/showthread.php?t=132580

The thread is maintained by Maglight of the guild Curbstomp on the Volkmar server.
]],
				},

--[[				{
					name = L"Формат",
					text =
[[

]],
				},
--]]				
			},
		},

		{	-- Divider, won't expand when clicked
			name = L"---------",
			divider = true,
			entries = {},
		},

		{
			name = L"Убийство 1,000 монстров",
			entries =
			{
				{
					name = L"Beasts",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Basilisk - Bestial Token

Bat, Giant - Bestial Token

Bear - Bestial Token

Boar - Bestial Token

Great Cat - Bestial Token

Hound - ??

Rhinox - Bestial Tactic Fragment

Wolf - Bestial Token
]],
				},

				{
					name = L"Birds",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Great Eagle - Bestial Tactic Fragment

Vulture - Bestial Token

Warhawk - Bestial Token
]],
				},

				{
					name = L"Insects & Arachnids",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Scorpion, Giant - Bestial Token

Spider, Giant - Bestial Token
]],
				},

				{
					name = L"Reptiles",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Cold One - Bestial Tactic Fragment

Lizard, Giant - Bestial Token
]],
				},

				{
					name = L"Daemons of Khorne",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Bloodbeast of Khorne - ??

Bloodletter of Khorne - Daemonic Tactic Fragment

Bloodthirster of Khorne - ??

Flesh Hound of Khorne - ??

Juggernaut of Khorne - ??
]],
				},

				{
					name = L"Daemons of Nurgle",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Great Unclean One - ??

Nurgling - Bestial Token

Plaguebearer - Bestial Token

Plaguebeast of Nurgle - ??

Slimehound - Daemonic Tactic Fragment
]],
				},

				{
					name = L"Daemons of Slaanesh",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Daemonette of Slaanesh - Daemonic Tactic Fragment

Fiend of Slaanesh - ?? 

Keeper of Secrets - ?? 
]],
				},

				{
					name = L"Daemons of Tzeentch",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Firewyrm of Tzeentch - Daemonic Tactic Fragment

Flamer of Tzeentch - (Chaos?) Tactic Fragment

Horror of Tzeentch - Bestial Token 

Lord of Change - ?? 

Screamer of Tzeentch - Daemonic Tactic Fragment 

Watcher - ?? 
]],
				},

				{
					name = L"Unmarked Daemons",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Chaos Fury - Daemonic Tactic Fragment

Chaos Hound - Bestial Tactic Fragment 

Chaos Spawn - Daemonic Tactic Fragment 

Chaos Spawn, Daemonvine - Daemonic Tactic Fragment 

Daemon Prince - ?? 

Walker - Undead Tactic Fragment
]],
				},

				{
					name = L"Beastmen",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Beastman, Bestigor - Bestial Token

Beastman, Bray Shaman - Chaos Tactic Fragment 

Beastman, Gor - Bestial Token 

Beastman, Ungor - Bestial Token 

Doombull - Chaos Tome Tactic
]],
				},

				{
					name = L"Dwarfs",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Dwarf - Dwarven Destroyer, One Pip (Trophy Item) 

Dwarf, Engineer - ?? 

Dwarf, Ironbreaker - ?? 

Dwarf, Runepriest - ?? 

Dwarf, Slayer - Bestial Token 

(Dwarf, Hammerer)? 
]],
				},

				{
					name = L"Elves",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Dark Elf - Dark Elf Destroyer, One Pip (Trophy Item)

Dark Elf, Blackguard - ?? 

Dark Elf, Disciple of Khaine - ?? 

Dark Elf, Sorceror - "Spellbinder" Title 

Dark Elf, Witch Elves - "The Deft" Title 

High Elf - High Elf Eliminator, One Pip (Trophy Item)

High Elf, Archmage - ?? 

High Elf, Shadow Warrior - ?? 

High Elf, Swordmaster - ?? 

High Elf, White Lion - ?? 
]],
				},

				{
					name = L"Greenskins",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Gnoblar - Bestial Token 

Goblin - Greenskin Grappler, One Pip (Trophy Item)

Goblin, Night Goblin - Man Tactic Fragment 

Goblin, Shaman - ?? 

Goblin, Squig Herder - ?? 

Orc - Greenskin Grappler, One Pip (Trophy Item)

Orc, Black Orc - ?? 

Orc, Choppa - ?? 

Orc, Savage Orc - Greenskin Tactic Fragment 

Snotling - Bestial Token 

Squig - "The Squig Squisher" Title 
]],
				},

				{
					name = L"Humans",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Bandit - Bestial Token 

Chaos - Chaos Cleanser, One Pip (Trophy Item)

Chaos, Chosen - ?? 

Chaos, Magus - ?? 

Chaos, Marauder - ?? 

Chaos, Zealot - ?? 

Drakk Cultist - Bestial Token  

Empire - Empire Eradicator, One Pip (Trophy Item)

Empire, Bright Wizard - ?? 

Empire, Knight of the Blazing Sun - ?? 

Empire, Warrior Priest - ?? 

Empire, Witch Hunter - ?? 

Ghoul - Bestial Token 

Plague Victim - ?? 
]],
				},

				{
					name = L"Ogres",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Gorger - Giant Tactic Fragment 

Ogre, Bull - Bestial Token 

Ogre, Tyrant - Giant Tome Tactic 

Yhetee - Tactic Fragment 
]],
				},

				{
					name = L"Skaven",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Rat Ogre - Skaven Tactic Fragment 

Skaven - Bestial Token 
]],
				},

				{
					name = L"Chaos Breeds",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Centigor - Pocket Item: Ale Barrel Lid Shield 

Chaos Mutant - Chaos Tactic Fragment 

Dragon Ogre - Chaos Tactic Fragment 

Flayerkin - Chaos Tactic Fragment 

Harpy - Bestial Token  

Maggot - Chaos Tactic Fragment 

Tuskgor - Tuskgor Saddle (Pocket Item) 
]],
				},

				{
					name = L"Dragonoids",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Dragon - Cloak (Cloak of the Flameborne?)

Hydra - ?? 

Wyvern - Mythical Tactic Fragment
]],
				},

				{
					name = L"Giants",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Giant - ?? 

Giant, Chaos - Giant Tome Tactic 
]],
				},

				{
					name = L"Magical Beasts",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Cockatrice - Mythical Tactic Fragment 

Griffon - Giant Tactic Fragment 

Imp - Mythical Tactic Fragment 

Manticore - Mythical Tactic Fragment 

Pegasus - Cloak of the Pegasus 

Unicorn - Mythical Tactic Fragment 
]],
				},

				{
					name = L"Trolls",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Troll - Bestial Token 

Troll, Chaos - Giant Tactic Fragment 

Troll, River - Bestial Token 

Troll, Stone - Bestial Token 
]],
				},

				{
					name = L"Forest Spirits",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Dryad - Bestial Token 

Spite - Bestial Token 

Tree Kin - Mythical Tactic Fragment 

Treemen - ??  
]],
				},

				{
					name = L"Constructs",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Bone Giant - Undead Tactic Fragment 

Living Armor - Man Tactic Fragment 

Winged Nightmare - ?? 
]],
				},

				{
					name = L"Greater Undead",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Liche - ?? 

Vampire - ??  
]],
				},

				{
					name = L"Skeletons",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Skeleton - Bestial Token 
]],
				},

				{
					name = L"Spirits",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Banshee - Undead Tactic Fragment 

Spirit Host - Bestial Token 

Wraith - Undead Tactic Fragment 
]],
				},

				{
					name = L"Wights",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Wight - Undead Tactic Fragment 
]],
				},

				{
					name = L"Zombies",
					text =
[[
These are non-zone-specific unlocks completed by killing 1,000 of a certain type of mob.

Zombie - Bestial Token 
]],
				},

			},
		},

		{
			name = L"Убийство 10,000 монстров",
			entries =
			{
				{
					name = L"Beasts",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Basilisk - ??

Bat, Giant - ??

Bear - ??

Boar - Title: "Scout"

Great Cat - Title: "The Stealth"

Hound - ??

Rhinox - ??

Wolf - Title: "Master Wolf Tracker"
]],
				},

				{
					name = L"Birds",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Great Eagle - ??

Vulture - ??

Warhawk - ??
]],
				},

				{
					name = L"Insects & Arachnids",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Scorpion, Giant - ??

Spider, Giant - ??
]],
				},

				{
					name = L"Reptiles",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Cold One - ??

Lizard, Giant - ??
]],
				},

				{
					name = L"Daemons of Khorne",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Bloodbeast of Khorne - ??

Bloodletter of Khorne - ??

Bloodthirster of Khorne - ??

Flesh Hound of Khorne - ??

Juggernaut of Khorne - ??
]],
				},

				{
					name = L"Daemons of Nurgle",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Great Unclean One - ??

Nurgling - Title: "Plaguepests' Foe"

Plaguebearer - ??

Plaguebeast of Nurgle - ??

Slimehound - ??
]],
				},

				{
					name = L"Daemons of Slaanesh",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Daemonette of Slaanesh - ??

Fiend of Slaanesh - ?? 

Keeper of Secrets - ?? 
]],
				},

				{
					name = L"Daemons of Tzeentch",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Firewyrm of Tzeentch - ??

Flamer of Tzeentch - ??

Horror of Tzeentch - Title: "Aethyr Custodian"

Lord of Change - ?? 

Screamer of Tzeentch - ?? 

Watcher - ?? 
]],
				},

				{
					name = L"Unmarked Daemons",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Chaos Fury - ??

Chaos Hound - ?? 

Chaos Spawn - ?? 

Chaos Spawn, Daemonvine - ?? 

Daemon Prince - ?? 

Walker - ??
]],
				},

				{
					name = L"Beastmen",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Beastman, Bestigor - ??

Beastman, Bray Shaman - ?? 

Beastman, Gor - ??

Beastman, Ungor - ??

Doombull - ??
]],
				},

				{
					name = L"Dwarfs",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Dwarf - Dwarven Destroyer, Two Pips (Trophy Item) 

Dwarf, Engineer - ?? 

Dwarf, Ironbreaker - ?? 

Dwarf, Runepriest - ?? 

Dwarf, Slayer - ?? 

(Dwarf, Hammerer)? 
]],
				},

				{
					name = L"Elves",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Dark Elf - Dark Elf Destroyer, Two Pips (Trophy Item)

Dark Elf, Blackguard - ?? 

Dark Elf, Disciple of Khaine - ?? 

Dark Elf, Sorceror - ?? 

Dark Elf, Witch Elves - ?? 

High Elf - High Elf Eliminator, Two Pips (Trophy Item)

High Elf, Archmage - ?? 

High Elf, Shadow Warrior - ?? 

High Elf, Swordmaster - ?? 

High Elf, White Lion - ?? 
]],
				},

				{
					name = L"Greenskins",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Gnoblar - ?? 

Goblin - Greenskin Grappler, Two Pips (Trophy Item)

Goblin, Night Goblin - ?? 

Goblin, Shaman - ?? 

Goblin, Squig Herder - ?? 

Orc - Greenskin Grappler, Two Pips (Trophy Item)

Orc, Black Orc - ?? 

Orc, Choppa - ?? 

Orc, Savage Orc - ??

Snotling - Title: "The Wierd"

Squig - Bestial Tactic Fragment
]],
				},

				{
					name = L"Humans",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Bandit - Title: "The Man Hunter" 

Chaos - Chaos Cleanser, Two Pips (Trophy Item)

Chaos, Chosen - ?? 

Chaos, Magus - ?? 

Chaos, Marauder - ?? 

Chaos, Zealot - ?? 

Drakk Cultist - ??  

Empire - Empire Eradicator, Two Pips (Trophy Item)

Empire, Bright Wizard - ?? 

Empire, Knight of the Blazing Sun - ?? 

Empire, Warrior Priest - ?? 

Empire, Witch Hunter - ?? 

Ghoul - ??

Plague Victim - ?? 
]],
				},

				{
					name = L"Ogres",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Gorger - ?? 

Ogre, Bull - ?? 

Ogre, Tyrant - ??

Yhetee - ?? 
]],
				},

				{
					name = L"Skaven",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Rat Ogre - ?? 

Skaven - ??
]],
				},

				{
					name = L"Chaos Breeds",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.
,
Centigor - ?? 

Chaos Mutant - ?? 

Dragon Ogre - ?? 

Flayerkin - ?? 

Harpy - ?? 

Maggot - ?? 

Tuskgor - ??
]],
				},

				{
					name = L"Dragonoids",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Dragon - ??

Hydra - ?? 

Wyvern - Title: "Master Wyvern Slayer"
]],
				},

				{
					name = L"Giants",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Giant - ?? 

Giant, Chaos - ??
]],
				},

				{
					name = L"Magical Beasts",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Cockatrice - ??

Griffon - ??

Imp - Title: "Master Imp Killer"

Manticore - ??

Pegasus - ??

Unicorn - ??
]],
				},

				{
					name = L"Trolls",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Troll - ??

Troll, Chaos - ??

Troll, River - ??

Troll, Stone - ??
]],
				},

				{
					name = L"Forest Spirits",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Dryad - ??

Spite - ??

Tree Kin - ??

Treemen - ??  
]],
				},

				{
					name = L"Constructs",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Bone Giant - ??

Living Armor - ??

Winged Nightmare - ?? 
]],
				},

				{
					name = L"Greater Undead",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Liche - ?? 

Vampire - ??  
]],
				},

				{
					name = L"Skeletons",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Skeleton - ??
]],
				},

				{
					name = L"Spirits",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Banshee - ??

Spirit Host - ??

Wraith - ??
]],
				},

				{
					name = L"Wights",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Wight - ??
]],
				},

				{
					name = L"Zombies",
					text =
[[
These are non-zone-specific unlocks completed by killing 10,000 of a certain type of mob.

Zombie - Title: "of the Dead"
]],
				},

			},
		},
		
		
		{
			name = L"Убийство 100,000 монстров",
			entries =
			{
				{
					name = L"Beasts",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Basilisk - ??

Bat, Giant - ??

Bear - ??

Boar - Title: "Predator"

Great Cat - ??

Hound - ??

Rhinox - ??

Wolf - ??
]],
				},

				{
					name = L"Birds",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Great Eagle - ??

Vulture - ??

Warhawk - ??
]],
				},

				{
					name = L"Insects & Arachnids",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Scorpion, Giant - ??

Spider, Giant - ??
]],
				},

				{
					name = L"Reptiles",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Cold One - ??

Lizard, Giant - ??
]],
				},

				{
					name = L"Daemons of Khorne",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Bloodbeast of Khorne - ??

Bloodletter of Khorne - ??

Bloodthirster of Khorne - ??

Flesh Hound of Khorne - ??

Juggernaut of Khorne - ??
]],
				},

				{
					name = L"Daemons of Nurgle",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Great Unclean One - ??

Nurgling - ??

Plaguebearer - ??

Plaguebeast of Nurgle - ??

Slimehound - ??
]],
				},

				{
					name = L"Daemons of Slaanesh",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Daemonette of Slaanesh - ??

Fiend of Slaanesh - ?? 

Keeper of Secrets - ?? 
]],
				},

				{
					name = L"Daemons of Tzeentch",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Firewyrm of Tzeentch - ??

Flamer of Tzeentch - ??

Horror of Tzeentch - ??

Lord of Change - ?? 

Screamer of Tzeentch - ?? 

Watcher - ?? 
]],
				},

				{
					name = L"Unmarked Daemons",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Chaos Fury - ??

Chaos Hound - ?? 

Chaos Spawn - ?? 

Chaos Spawn, Daemonvine - ?? 

Daemon Prince - ?? 

Walker - ??
]],
				},

				{
					name = L"Beastmen",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Beastman, Bestigor - ??

Beastman, Bray Shaman - ?? 

Beastman, Gor - ??

Beastman, Ungor - ??

Doombull - ??
]],
				},

				{
					name = L"Dwarfs",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Dwarf - Dwarven Destroyer, Three Pips (Trophy Item) 

Dwarf, Engineer - ?? 

Dwarf, Ironbreaker - ?? 

Dwarf, Runepriest - ?? 

Dwarf, Slayer - ?? 

(Dwarf, Hammerer)? 
]],
				},

				{
					name = L"Elves",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Dark Elf - Dark Elf Destroyer, Three Pips (Trophy Item)

Dark Elf, Blackguard - ?? 

Dark Elf, Disciple of Khaine - ?? 

Dark Elf, Sorceror - ?? 

Dark Elf, Witch Elves - ?? 

High Elf - High Elf Eliminator, Three Pips (Trophy Item)

High Elf, Archmage - ?? 

High Elf, Shadow Warrior - ?? 

High Elf, Swordmaster - ?? 

High Elf, White Lion - ?? 
]],
				},

				{
					name = L"Greenskins",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Gnoblar - ?? 

Goblin - Greenskin Grappler, Three Pips (Trophy Item)

Goblin, Night Goblin - ?? 

Goblin, Shaman - ?? 

Goblin, Squig Herder - ?? 

Orc - Greenskin Grappler, Three Pips (Trophy Item)

Orc, Black Orc - ?? 

Orc, Choppa - ?? 

Orc, Savage Orc - ??

Snotling - ??

Squig - ??
]],
				},

				{
					name = L"Humans",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Bandit - ??

Chaos - Chaos Cleanser, Three Pips (Trophy Item)

Chaos, Chosen - ?? 

Chaos, Magus - ?? 

Chaos, Marauder - ?? 

Chaos, Zealot - ?? 

Drakk Cultist - ??  

Empire - Empire Eradicator, Three Pips (Trophy Item)

Empire, Bright Wizard - ?? 

Empire, Knight of the Blazing Sun - ?? 

Empire, Warrior Priest - ?? 

Empire, Witch Hunter - ?? 

Ghoul - ??

Plague Victim - ?? 
]],
				},

				{
					name = L"Ogres",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Gorger - ?? 

Ogre, Bull - ?? 

Ogre, Tyrant - ??

Yhetee - ?? 
]],
				},

				{
					name = L"Skaven",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Rat Ogre - ?? 

Skaven - ??
]],
				},

				{
					name = L"Chaos Breeds",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Centigor - ?? 

Chaos Mutant - ?? 

Dragon Ogre - ?? 

Flayerkin - ?? 

Harpy - ?? 

Maggot - ?? 

Tuskgor - ??
]],
				},

				{
					name = L"Dragonoids",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Dragon - ??

Hydra - ?? 

Wyvern - ??
]],
				},

				{
					name = L"Giants",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Giant - ?? 

Giant, Chaos - ??
]],
				},

				{
					name = L"Magical Beasts",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Cockatrice - ??

Griffon - ??

Imp - ??

Manticore - ??

Pegasus - ??

Unicorn - ??
]],
				},

				{
					name = L"Trolls",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Troll - ??

Troll, Chaos - ??

Troll, River - ??

Troll, Stone - ??
]],
				},

				{
					name = L"Forest Spirits",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Dryad - ??

Spite - ??

Tree Kin - ??

Treemen - ??  
]],
				},

				{
					name = L"Constructs",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Bone Giant - ??

Living Armor - ??

Winged Nightmare - ?? 
]],
				},

				{
					name = L"Greater Undead",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Liche - ?? 

Vampire - ??  
]],
				},

				{
					name = L"Skeletons",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Skeleton - ??
]],
				},

				{
					name = L"Spirits",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Banshee - ??

Spirit Host - ??

Wraith - ??
]],
				},

				{
					name = L"Wights",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Wight - ??
]],
				},

				{
					name = L"Zombies",
					text =
[[
These are non-zone-specific unlocks completed by killing 100,000 of a certain type of mob.

Zombie - ??
]],
				},

			},
		},
	

	
	},
}
