TTitan.Data.IBF = {}

function TTitan.Data.IBF.GetData()
	return TTitan.Data.IBF.RawData
end

function TTitan.Data.IBF.FilterData()
end

TTitan.Data.IBF.RawData =
{
	header = L"Стратегия - Описание тактик на Босах", -- Instanced Boss Fights
	categories =
	{
		{
			name = L"About This Page",
			entries =
			{
				{
					name = L"Information",
					text =
[[
The information in this page was made to aid you in boss fights, it is NOT a definitive way of killing a boss as the circumstances in each fight will vary. The information here was obtained using my own experinces and some previous knowledge of instances and how boss fights are designed.

Squigy
]],
				},

				{
					name = L"Format",
					text =
[[
Each Boss fight will have certain rules and attacks that you will have to defeat. I will cover the main objective and the skills of the boss and then rolling through the minor changes you have to make depending on what type of class make-up you have.

- 2DPS, 2Tanks, 2 Healers
- 3DPS, 1Tank, 2 Healers
- 3DPS, 2Tanks, 1 Healer
- 5DPS, 1Tank, 1 Healer

These are the main groups that a party might be constructed from. As a general rule, the most balanced and effective party will be the 2DPS, 2 Tanks and 2 Healers group, each group moving downwards will have a lower chance on sucess.
]],
				},
			},
		},

		{	-- Divider, won't expand when clicked
			name = L"---------",
			divider = true,
		},

		 {
         name=L"Bastion Stair",
			entries =
			{
				{
				name = L"Thar'lgnan",
				text =
[[
Thar'lgnan is the first instance in Bastion Stair, this particular boss requires any of the 2 healer parties, unless you healer happens to have an obscenely high amount of willpower (800+). It is recommended that one of your DPS classes can do a decent amount of AoE damage, this is however not required to complete the instance, but it is recommended.

OBJECTIVES:

The objective of this fight is to first, lure the dogs in the middle of the stadium. Make sure all the dogs around are all lured into one spot. With this done, the DPS assigned to AoE will quickly kill the dogs off the tank. The healers aim in this whole instance is to keep EVERYBODY up. When someone dies in this boss, he regenerates around 30 - 50% of his health back. This could mean a huge tide turning in this bosses favor, as this boss, as with any other instanced boss has a hidden rage ticker that will pop when you take too long to kill him. Besides the abilities mentioned below, this fight is a plain tank and spank. Melee DPS can focus on smashing the boss from behind while the tank keeps aggro off of them, the same applies to Ranges DPS, which should try and stay as far away as possible while still in range of the healers and the boss.

ABILITIES:

- He has a root ability, that will root you on the groud for a set peroid of time, this should not really effect the party at all as this targets all around him and the heals in this game are all ranged. Melee DPS in this root can just throw axes to wait it out.

- He throws people randomly to the corpses on the ground. When you first enter it is good to know that there are attackable corpses on the ground. These are the targets of where he will throw you.

- He has an AoE dot that hits anyone near him. Healers should try and stay at max range to prevent interruptions to heals and excessive damage.

- He will randomly charge and attack someone for around 900 damage to a light armour DPS. This should be quickly healed as he may do this 2 - 3 times in a row and sometimes even sneak off a normal hit which does around 2k.

- The Regenration Passive allows him to heal instantly 30 - 50% of his health when any of your party members or their pets die from his attacks.

NOTES:

- Corpses make good morale growers, if you really want, make your whole party farm a corpse until everyone has max morale. (The corpses can't die so you all can attack )

- As this boss regenertates HP upon killing anything, all pet based classes should have their pets de-summoned and their tactics changed to one supporting their damage without a pet (If applicable)

]],
				},

								{
				name = L"Lord Slaurith",
				text =
[[
Lord Slaurith is the second instance in Bastion Stair, this boss requires better co-ordination between the 6 people in the party and you MUST have a 2/2/2 group. 1 tank might suffice if they have a high amount of wounds and a blanace of toughness.

OBJECTIVES:

For this instance, you have to spread out as much as possible so that the ability he uses will only affect1 party member at a time. This avoids certain complications that might occur in the process of more then 1 member taking excessive damage.

ABILITIES:

- Lord Salurith has a skill that targets the ground. It then ticks on the target it was cast on 3 ticks of around 1k each give or take. Be sure to have more then 5.8k health for every member of your party.

- Another ability he has is a full party dot. This does around 600 damage to each member of the party and you have to live through it.

NOTES:

- The AoE has two options to survive it. One option is to take the damage. This will unfortunately make Lord Slaurith target the person who took it. This can be used to your advantage to move the tank into the dot so that he can get the aggro. The other option is to move when he casts it on you to avoid the damage. This will get him to cast the whole party dot so if you have good healers you can probably heal through the damage. 

- A AoE specced healers will help alot if you so choose to run out of the point based AoE ability.
]],
				},

			},
			
        },
		
		
		{
         name=L"Прохождение Гробниц Земли Мертвых",
			entries =
			{
				{
				name = L"Гробница Звёзд",
				text =
[[
Состав пати:
Танк / 2 ДД /1 МДД / 2 хила
5 малых вардов

После входа в инст, спускаемся до ворот, там рычаг открывающий решетку. Собираем всю пати перед воротами и только после этого дергаем рычаг. Из начала локации начинает спускаться смертельный рой, бежим от него по коридору, не останавливаясь. Во время забега старайтесь не попадать в песок, который сыпется с потолка. На тех кто в него попал вешается дот, который не снимается (есть вероятность что дот может перевесится на хила, если тот будет хилить умирающего, так что лучше не хилить). 
Если все пробежали чисто, переходим к босу, если нет, то:
Не хилим тех кто попал в песок, все на ком дот должны умереть. После того как умрет последний "обдотаный" будет возможен рес. Не отчаивайтесь если умрут хилы, просто ресните одного из них флагом.
Герой:
Тут все просто, танчится в дальнем углу комнаты, иногда выпускает скарабеев, которые отлечивают Героя когда наносят дамаг по кому-то, по этому сливать их надо быстро (тут и нужен брайт). Через несколько минут (примерно 4-5) дверь открывается и в помещение врывается смертельный рой (хорошо хоть двигается очень медленно, он ваншотит так что будьте осторожнее.
# В теории, если героя не успели добить до появления роя, его можно кайтить.

Ну на счет дота - его дамаг ГВАРДИТЬСя, тоесть если кинуть гвард на хила, его он жрать будет в 2 раза меньше, и легко отхилиться, а танку особо не больно - дот бьет физом, часто уходит в блок и пари. Меня бьет по 140, и оч часты пари и блоки.
]],
				},

				{
				name = L"Гробница Неба",
				text =
[[
Босс стоит в центре небольшой комнаты. Постоянно и в большом количестве появляются могильные рои. Вдоль стен двигается смерч - персонаж попавший в него получает около 2 к дамага/сек + замедляется (выбраться из него живым трудно) Когда смерчь исчезает - появляются бомбы. Взрываются ~20 сек спустя. Каждая наносит 1900-2500 урона каждому члену группы. Дамаг фиксирован в зависимости от резистов твоего персонажа. 

Тактика проста. 

Пати стоит в центре комнаты вплотную к боссу. Обязателен АОЕ дамагер, убивающий мелких мобов. Группа должна следить за появлением смерча и быстро отходить с его пути. Двигается он медленно, так что главное не проспать появление. Смерчь исчезает, когда доходит до угла комнаты (чаще всего проходит 2 стены короткую и длинную, после этого исчезает. Иногда проходит только длинную). После исчезновения наступает черед бомб, появляющихся в углах. Бить их можно ТОЛЬКО МИЛИ атаками. Все Рендж абилки НЕ НАНОСЯТ урон. На уничтожение бомб у вас есть ~20 сек. Помочь в этом могут боепопы. Врыв 1-2 бомб для пати несмертелен и легко отхиливается, главное не уронить танка.

Upd от Прометеус: могильные рои вешают на цель стакающийся дебафф на броню, т.е. танку лучше не использовать масс-таунт и вообще каким-либо образом привлекать к себе внимание мелочи.

Хинт от Прометеус: если нет желания после вайпа бежать в гробницу из варкампа, можно оставить одного хила в коридоре за пределами комнаты, чтобы тот сэвейдил босса.

Идеальный состав - 1 танк, 2 АОЕ дамагера, 1 мили дамагер, 2 хила.
]],
				},
				
				{
				name = L"Гробница Луны",
				text =
[[
На данный момент считается самым сложным данжем.

Босс стоит в центре комнаты. Внизу респятся 2 адда победоносца с аурами (черная и золотая). 


Тактика 
Танк держит босса. 2-й танк/хил(upd. предпочтительнее) держит адда с золотой аурой, хил держит адда с черной аурой. Мейн танк обязан завести босса в черную (Upd. Была золотая - столкнулись с багом при первых траях) ауру (не попадая в нее сам) - по боссу пойдет урон отличный от 1. ДД должны встать в золотую (Upd. Так же поправил с черной) ауру - исходящий дамаг увеличиться на 250%. 

Победоносцам нельзя подойти к боссу/стакнуть на нем ауры - приводит к вайпу. Убивать победоносцев - нельзя, приводит к вайпу. АОЕ необходимо использовать с макимальной осторожностью или не использовать вовсе.

Многие члены группы не видят аур - очень досадный баг.
Идеальный состав: 1 танка, 3 РДД, 2 хила

Upd. Обязательно читаем пост с тактикой от Jennahaze 

Цитата:
Сообщение от Jennahaze 
Все правильно но только с точностью до наоборот) Босс должен стоять в черной ауре, все остальные - в золотой. Черная аура дает 250% входящего дамага, золотая - 250% исходящего.

Наша Тактика:
Танк бежит и таунтит босса, хилеры стоят слева и справа от босса, у лестниц. Когда снизу набегают ады - хилы дотают/бьют их чтобы сагрить на себя. Дпс и танк адов не дотрагиваются. Ады вламывают примерно по 1-1.5к, так что каждый хил, если напряжется, может без проблем держать по 1 аду на себе, при сильном проседании используя детаунт (скорее всего, к архимагам не применимо, т.к. их детаунт снижает аггро). Групп хил НЕ ИСПОЛЬЗУЕТСЯ чтобы хилеры не переагривали адов друг с друга. Если адд побежал от одного хила к другому - это вайп. В крайнем случае, 1 хилер модет поставить тактику на минус аггро, чтобы время от времени кидать групповой хил. Итак, ады поднимаются, хилеры их агрят на себя и оттаскивают каждый в свой угол. Дальше начинается их расстановка. Задача - поставить адов так, чтобы босс попадал в черную ауру, а все остальные, в т.ч. и танк - в золотую. Например вот так: http://img291.imageshack.us/i/165t.jpg/ Как это сделать? Наш танк под моралькой (100% блок) заводил босса в черную ауру, разворачивался там, и выходил из нее так, чтобы босс остался в ней. Далее, хилер, который держит ада с золотой аурой - потихоньку подводил ада к танку, как только танк попадает в золотую ауру - он может нормально дамагать и держать аггро. Вслед за ним из золотой ауры подключаются и остальные дпсеры. Расставились - и вливаете в босса до его смерти.

Одним из самых сложных моментов здесь является удержание хилами аггро от адов. Мы делали так: я (ВП) постоянно дамагал своего ада небесным ударом, карой и небесной атакой. У меня стояла тактика на минус аггро. Лечил я себя двумя хотами, проками с молитвы на отхил (+тактика "молюсь за тебя") и при сильном проседании танка от босса - юзал групповой хил. Т.е. я набирал аггро для себя дамагая ада, хиля только себя от его дамага. Наш второй хилер (Жрец рун) тактику на деагро не ставил, ада хватал дотами и лечил под детаунтом себя и танка прямыми хилами и хотами. Он набрал изначально аггро дотом при пуле его на себя и держал его стабильно леча себя от его дамага. 

Отличия/дополнения в нашем прохождении: 
1) Танк и босс стоят на 1 месте. Танк агрит босса всем что есть. Не юзает только масс таунт
2) Дамагеры стоят и скучают до появления золотой ауры на танке.
3) Хилы агрят мобов и внимательно подводят из поближе - чтобы золото было на танке, черное на боссе.
4) Ключевым моментом в удержании аддов является детаунт. Без него дамаг трудно отхилить и хилы могут падать.
5) Танк гвардит самого дамажного дамагера, иначе происходит срыв босса на него.

Upd. Подумал и таки решил написать.

Можно пройти вдоль стенок внутрь и занять удобные позиции. С гробов можно запрыгнуть на столбики, вне досягаемости атак мобов. 

Хилы спокойно агрят на себя аддов. Получаются 2 статичные ауры, 2 небъющих и неполучающих дамаг победоносца. 
К сожалению в определенный момент они начинают нервничать, дергаться/эвейдится. Из-за этого сделать данж самым простым из всех не удалось.
]],
				},
				
				{
				name = L"Гробница Солнца",
				text =
[[
Босс - РДД. В 30 футах от него - аура на самохил от дамага. (Upd. от Keplian: включается после предупреждающего крика "я выпью из тебя жизнь" и следующего за ним каста) Стоять в ней строго противопоказано. Вся площадь ниже разрушенных колонн, площадки перед входом и парапетов по периметру залита черным туманом - ~500 дпс. Босс атакует шарами. По 1 на каждого члена пати. ~1800 хп за удар, главная цель получает примерно в 2 раза больше урона. После удара шар остается на месте и взрывается через 2 секунды. Не успел отойти - отбрасывает (чаще всего в туман). Если кто то находиться в тумане, то урон по всей группе увеличивается вдвое, криты по мейн цели становятся очень злыми. (Возможно идет бафф +100% к дамагу за каждого, находящегося в тумане)

Тактика - все члены группы стоят на удобных для перебегания/перепрыгивания местах. Нет времени для долгих кастов. Максимум 3 секунды. Танк гвардит основного дамагера и юзает таунт по кулдауну. Остальные используют детаунт по кулдауну. На шары распространаяется несрабатывание. Урон от шаров считается мили дамагом (действуют щиты хилов).
Вайпнуть группу могут скучающие в тумане))


UPDATE Если вся группа спускается в туман - босс ПЕРЕСТАЕТ дамажить. Урон от тумана фиксированный - 750/сек. Хил не сбивает. Данный инст становится элементарным.
===================
Теперь нужно милишникам бить его в мили до тех пор,пока он не крикнет свою коронную - и быстро отпрыгивать в момент произнесения(иначе он сильно отхилится).После этого он рандомно ТПшится и снова милишники идут его бить.Мешают в этом деле шары и дурацкий рельеф местности.

]],
				},

			},
			
        },

}
}
