TTitan.Data.HistoryDvG = {}

function TTitan.Data.HistoryDvG.GetData()
	return TTitan.Data.HistoryDvG.RawData
end

function TTitan.Data.HistoryDvG.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.HistoryDvG.RawData)
end

TTitan.Data.HistoryDvG.RawData =
{
	header = L"История и знания - Гномы против Зеленокожих", -- History & Lore Unlocks - Dwarfs vs Greenskins
	categories =
	{
		{
			name = L"Ekrund",
			entries =
			{
				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Gold",
					text =
[[
Three mining tunnels exit the location in the dwarven starting area where the squigs are attacking. In the center tunnel there is a gold nugget on the ground as you enter it. Examine this gold for the unlock.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Reclaiming Ekrund",
					text =
[[
In the dwarven location where the squigs are, there is a plaque on the center wall of the landing where the kill collector is.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Helga",
					text =
[[
Walk near the cannon Helga, in dwarven starting area.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Oathbearers",
					text =
[[
Talk to an NPC with name starting with Oathbearer in dwarven starting area.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"Grudges",
					text =
[[
Within the hall where newly created dwarfs spawn and the cannons can be used to fire at orcs, there is a pillar with 3 benches surrounding it. On one of the benches is a book of grudges. Examine the book for an unlock.
]],
				},

				{	-- Order only
					realm = TTitan.Data.Realm.ORDER,
					name = L"The Pick & Goggles",
					text =
[[
Go into the tavern named Picks & Goggles in the dwarven starting area
]],
				},

				{
					name = L"Rune Magic",
					text =
[[
This unlock is achieved by examining a book in a captured dwarven tower, in the Ancestor's Deep area. It is south of and high above the Goldfist Hole Recovery PQ (Chapter 4) within the area of the Broketoof Camp PQ (Chapter 4). The book (Book of Runes) to examine is near a dwarf skeleton on the first landing going up within the tower. (53740,49589)
]],
				},

				{
					name = L"Cannons",
					text =
[[
Tharik Redaxe, Mordin's Anvil (start area) is NPC for first quest in the series. Regardless of home realm (Empire, Elf, Dwarf) everyone must complete his first request before he will give you cannonball crate quest. Once you have this, you will get tome unlock on your first crate click. As of 10-4-08, unreachable by Destruction characters.
]],
				},

				{
					name = L"Ancestor Worship",
					text =
[[
There is a giant dwarven head statue in front of the Orc camp for the first Public Quest for the first Dwarf Chapter. It's slightly concealed by a wooden barricade right of the entrance into the orc camp. Destruction chars can only reach this by jumping down into the Dwarf area from a mountain around 35k, 34k.[1].
]],
				},

				{
					name = L"Durak Bitterstone",
					text =
[[
This tome unlock is completed if you run to the top of the stairs and touch the door in the PQ area next to Redhammer Brewery. Proximity activated unlock. The greenskin version is straight north of redhammer brewery walk between the giant door and the greenskin war machine thing. (45318, 21234)
]],
				},
			},
		},

		{
			name = L"Mount Bloodhorn",
			entries =
			{
				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Waaagh!",
					text =
[[
This unlock is discovered by entering the hut with big bones on top behind a shaman (Gubnash Facebiter) at the base of Lobber Hill for the Greenskin starting area. Location is (36320, 49690).
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Simple Minds",
					text =
[[
This unlock is discovered by using the Observation Point (a telescope) accessible from the area players are thrown to by the orc lobbers on Lobber Hill. The path to the sighting glass is an opening for a downward dirt path on the right-hand side of the Ironmane's Lock area (Same side where you unlock Ironmane's Lock). The location is (47078, 57440).
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Bloody Sun Boyz",
					text =
[[
This unlock is discovered by using the banner (41000, 52000) behind Skarzag, the Orc on the Wyvern that greets the player as they begin a Greenskin character. This unlock can only be recieved while on the quest to find it. It cannot be discovered otherwise.
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Choppas",
					text =
[[
This unlock is discovered by using a dead dwarf corpse, "Stunty Chopped Gud." The corpse is found in the woods northeast of Snout's Pens in the Mount Bloodhorn area. (32047, 47874)
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Ironmane's Lock",
					text =
[[
This unlock is discovered by reading the Memorial plaque in the Stunty Mountain area, the stuntie-killin' area accessed by the character using a lobber on Lobber Hill. The plaque can be found on a lower right-hand side of the northern wall with the marooned ship's brow facing toward it. The location is approximately (40820, 61042).
]],
				},

				{	-- Destruction only
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Greenskin Technology",
					text =
[[
This unlock is discovered by the player entering the area containing two siege towers directly west of "Da War Maka." (12000, 52000) This unlock occurs by proximity to a location.
]],
				},

				{
					name = L"Da War Maka",
					text =
[[
This unlock is discovered by examining a body below the north-facing blade of "Da War Maka". Seems broken for Order as of Oct. 26 '08
]],
				},

				{
					name = L"Rock Lobbers",
					text =
[[
This unlock is discovered by entering the area containing the Rock Lobbers in front of the gate of the Ironclaw Camp. This unlock is triggered by proximity to a location. (15000,40000)
]],
				},

				{
					name = L"Greenskin Food",
					text =
[[
This unlock is found in the Felridge area of the Mount Bloodhorn map. The item to look for is called Dinner Scraps inside in a hut, on the top of a small hill just after you cross the bridge. The location is approximately (38872, 38947).
]],
				},

				{
					name = L"Dragonbone Pass",
					text =
[[
It is on the dwarf side and located at the entrance to one of their towns, So when you get there you will see a Giant Skeleton of a dragon that a path runs through you will need to get close enough to it for you to unlock it but beware of the high level order guards that are around. The location is approximately (42842, 20672).
]],
				},
			},
		},

		{
			name = L"Barak Varr",
			entries =
			{
				{
					name = L"Long Drong and his Slayer Pirates",
					text =
[[
This unlock can be found by examining a barrel of ale, labeled "Rarest Ale", washed up on the beaches by The Seas Anvil. (6440,45760)
]],
				},

				{
					name = L"Sacred Oaths",
					text =
[[
North East between Thene's Defense and Foultooth's Warcamp is a Destroyed Dwarf Tower with Greenskins walking about, just enter the tower to get the unlock. Coords aprox : 36k 44k.
]],
				},

				{
					name = L"Waaagh! Magic",
					text =
[[
This unlock can be discovered by examining some orc remains in Barak Weald. (42336,51184)
]],
				},

				{
					name = L"Size Matters",
					text =
[[
Walk near the statue at 5767,27787.
]],
				},

				{
					name = L"Greenskin Battle Barges",
					text =
[[
Approach the docked barge at 16226, 27034. Also confirmed to unlock by swimming toward the floating barge E of the Greenskin Ch. 5 camp. (10752, 28832)
]],
				},

				{
					name = L"One Tusk Tribe",
					text =
[[
Walk near the sign post just west of Nogaz's Boyz at 4569,30334
]],
				},

				{
					name = L"Overlook Bridge",
					text =
[[
Walk over the large Bridge above the underground Port of Barak Varr (inside the mountain) or swim under it at 46437, 25091.
]],
				},

				{
					name = L"Tribal Assimilation",
					text =
[[
Use the fallen banner at 33014, 18678.
]],
				},

				{
					name = L"Ironclads",
					text =
[[
Reach the sunken Ironclad at 27850, 29082.
]],
				},

				{
					name = L"Leviathan",
					text =
[[
Walk near big bones on the beach in RvR zone near Ironclad Battlefield Objective circa 34315, 35954.
]],
				},
			},
		},

		{
			name = L"Marshes of Madness",
			entries =
			{
				{
					name = L"Oathgold",
					text =
[[
Examine chest at 51k, 8k north of Chapter 8 camp.
]],
				},

				{
					name = L"The Mourkain",
					text =
[[
Walk up to an arch at 9k,28k.
]],
				},

				{
					name = L"Who's Da Boss?",
					text =
[[
Walk up to a Watch tower at 14000, 21000.
]],
				},

				{
					name = L"The World's Best Miners",
					text =
[[
Interact with the Mining Tools just on the other side of a pile of logs outside the entrance to Falcon's Tomb at [62k,52k]. The tomb is at the most South-east explorable corner of the Marshes of Madness
]],
				},

				{
					name = L"Nebhorest, the Vampire Lord",
					text =
[[
Walk down the path (not on map) near 28.6k, 20.8k, just south of Dragoneye Cavern. Unlocked by running around the crucifixed skeleton next to where the path starts.
]],
				},

				{
					name = L"The Marsh Road",
					text =
[[
Walk up to two dead Dwarven miners at 13133,7296
]],
				},

				{
					name = L"The Black Skulls",
					text =
[[
Hargruk camp, the Greenskins are Black Skulls, discovered when looking for quest Shellshock. Walk near the left side of the entrance at around 27563, 12583.
]],
				},

				{
					name = L"Vital Supplies",
					text =
[[
Examine supply crates by a large rock in the middle of the swamp near 32k, 57k
]],
				},

				{
					name = L"The Price of Knowledge",
					text =
[[
Dawr galaz grung inside the cave you find a body underneath a muck scorpion 3146,31719.
]],
				},

				{
					name = L"Greenskin Stupidity",
					text =
[[
At 56k , 53k it is a barrel 'name' Really Empty Barrel
]],
				},
			},
		},

		{
			name = L"The Badlands",
			entries =
			{
				{
					name = L"Animosity",
					text =
[[
Ride under/across the bridge NE of Muggar's Choppaz at 52504, 39172.
]],
				},

				{
					name = L"The Cauldron",
					text =
[[
Up in the hills in the The Cauldron public quest area is a cauldron. Go near it to get the unlock at around 4000, 28000.
]],
				},

				{
					name = L"Gork and Mork",
					text =
[[
At about 43K, 25K at the Skullbreak Ridge Dwarf chapter 13 PQ there is a totem. Just approach the totem for an update
]],
				},

				{
					name = L"Blue Face Orcs",
					text =
[[
Around 5000, 39000 is a wooden hut with a red roof. Approach it to get the unlock.
]],
				},

				{
					name = L"Tribal Culture",
					text =
[[
Walk near the Orc camp at around 5100, 18000 and you will get the unlock.
]],
				},

				{
					name = L"Moonfang Tribe",
					text =
[[
Walk into the middle of the goblin camp at around 5000, 6000 to get the unlock.
]],
				},

				{
					name = L"Da Little Waaagh!",
					text =
[[
Click on a "Goblin Map" that can be found on a table next to a hut in the Night Goblin village outside Mount Gunbad (position: 10550, 4400)
]],
				},

				{
					name = L"Dragon's Gut",
					text =
[[
Click on the Dwarf corpse at 22426, 54886
]],
				},

				{
					name = L"Warpstone",
					text =
[[
Click on the Skaven (ratman) corpse on the ground at around 4680, 31000 to get the unlock.
]],
				},

				{
					name = L"Warpaint",
					text =
[[
Click on the Table of Supplies inside of a hut around 3.5k, 37k.
]],
				},

				{
					name = L"Phineas Fireforge",
					text =
[[
Click on a Rusty Scrollcase on the ground behind a wall at 4000, 28770 for unlock. The scrollcase is very small and half hidden in the ground. If you jump down from the higher plateau of The Cauldron PQ, it's the first wall on the right hand side.
]],
				},

				{
					name = L"Bloodspike Mesa",
					text =
[[
Walk near the dead dwarf by the big rock at around 6000, 22000 and you will get the unlock.
]],
				},
			},
		},

		{
			name = L"Black Fire Pass",
			entries =
			{
				{
					name = L"Beer",
					text =
[[
Dwarf town of 'Kaltlaup' 48k 58k.For destro players:face building, watch for pat of griffon champ,pull 3 oathbearers on cliff-face,then walk around to corner of building,barrel of beer is just on the corner.
]],
				},

				{
					name = L"Battle of Black Fire Pass",
					text =
[[
Approach the tower at Ancient's Reach 44k, 20k
]],
				},

				{
					name = L"The Oath between Men and Dwarfs",
					text =
[[
5.9k 22.2k Plunger at the end of the cave
]],
				},

				{
					name = L"Bugman's Brewery",
					text =
[[
Go to Bugman's Brewery in the RvR zone 25k 19k and click on Long forgotten tools, they are behind the building next to a ramp
]],
				},

				{
					name = L"Dwarf Craftsmanship",
					text =
[[
Click on the anvil at Public Quest Tharadrik Smashin' behind one of the buildings 5.5k 57k
]],
				},

				{
					name = L"Sisters of Shallya",
					text =
[[
Click on the medicine crate at around 55000, 57000 to get the unlock.
]],
				},

				{
					name = L"Dwarf Trains",
					text =
[[
Click on the book Manual for New Engineers on top of the tower at 23069, 42093.
]],
				},

				{
					name = L"Ghal Maraz",
					text =
[[
Approach the giant statue that is part of the mountainside at 40k, 5.7k
]],
				},

				{
					name = L"Worlds Edge Mountains",
					text =
[[
Approach the gate in the RvR zone at 1223, 4631
]],
				},

				{
					name = L"The Time of Woes",
					text =
[[
Approach the dwarven statue at 46400, 13500
]],
				},

				{
					name = L"The Path of Sigmar - Part One",
					text =
[[
Pass the Sigmarite Temple (14.5k,40k) on the left side. Follow the road starting in back. Read the plaque at 14k, 44k Near a statue of Sigmar)
]],
				},

				{
					name = L"The Path of Sigmar - Part Two",
					text =
[[
Continue the road from part 1. A few corners later in front of an angel statue at 10.3k, 41.7k
]],
				},

				{
					name = L"The Path of Sigmar - Part Three",
					text =
[[
Keep following the road from part 2. On your right side slightly upwards near another Statue of Sigmar, this one is not in a wooden shrine. 4.3k, 47k
]],
				},

				{
					name = L"The Path of Sigmar - Part Four",
					text =
[[
Follow the road from part 3. you'll come to an open spot. You'll see wooden fence on yer left. It's on the right side near some stones. 2k, 34.5k
]],
				},

				{
					name = L"The Path of Sigmar - Conclusion",
					text =
[[
Follow the road from part 4 all the way up the mountain. At the top right in front of the big Statue of Sigmar and 2 angel statues. 1.5k, 29k
]],
				},
			},
		},

		{
			name = L"Thunder Mountain",
			entries =
			{
				{
					name = L"Hurgan's Vent",
					text =
[[
Ride over bridge at 10621,18350 to unlock.
]],
				},

				{
					name = L"The Final Grudge of Drangin Ironboots",
					text =
[[
???
]],
				},

				{
					name = L"The Long Defeat",
					text =
[[
???
]],
				},

				{
					name = L"Not Much Grows Here",
					text =
[[
???
]],
				},

				{
					name = L"Last Stand at Karak Palik",
					text =
[[
Walk near the crumbling wall at 12358, 23069.
]],
				},

				{
					name = L"Und-a-Runki Book of Grudges",
					text =
[[
Inspect Book of Grudges inside the Und-a-Runki PQ at about 3150,62700
]],
				},

				{
					name = L"Ash Crawler Victims",
					text =
[[
Click on the Pile of Skulls at 15279, 55050.
]],
				},

				{
					name = L"Runelords",
					text =
[[
Approach a dwarven statue on the path at 53700, 16500 
]],
				},

				{
					name = L"Sons of Valaya",
					text =
[[
Approach the ruined building at the far east of the Sons Of Valaya PQ at around 62000,6500
]],
				},

				{
					name = L"The Golden Lumps",
					text =
[[
Examine tiny Golden Lumps near a lava pond at 17700,49200
]],
				},

				{
					name = L"Reichart's Raiders",
					text =
[[
Four possible methods of unlocking, verification needed:

   1. Proximity unlock upon entering the camp.
   2. Clicking on Riechart's Tent and burning it (2 in the camp).
   3. Click on the Raider's Chest just outside of his tent.
   4. Click on the chest of valuables (3 in the camp). 

Note: Please verify and update, editors. 
]],
				},

				{
					name = L"The Throng of Karaz-a-Karak",
					text =
[[
Examine Remnant of Battle at 48k, 14k on top of a broken piece of wall. 
]],
				},

				{
					name = L"View from the Top",
					text =
[[
Click on the Congratulatory Plaque at 25800,1500 for the Unlock. This is at the top of a mountain which can be reached by climbing east from 11000,0 (right at the border with Kadrin Valley).
]],
				},

				{
					name = L"The Bone Field",
					text =
[[
Approach the obelisk at 49800,9350 for the unlock
]],
				},

				{
					name = L"Built to Last",
					text =
[[
Click on the mushroom brew barrel at 60593, 62390 or 58645, 58645. (Has multiple spawn locations) OR it may trigger by walking near the cave at 59844, 62615. I got the unlock right as I did both.
]],
				},
			},
		},

		{
			name = L"Kadrin Valley",
			entries =
			{
				{
					name = L"Karak Ankor",
					text =
[[
In the hut just to the west of the unlock for Ghavrin's Brace is a Gold Chest. Click on it for this unlock. Position is 58200,12100.
]],
				},

				{
					name = L"Grimnir",
					text =
[[
Walk in or near the shrine at 26739, 10411.
]],
				},

				{
					name = L"Valaya",
					text =
[[
Examine the Prayer Book at 5k, 44k (Inside the house directly west of Dwarf Engineers update)
]],
				},

				{
					name = L"Grungni",
					text =
[[
Examine the Mine Barrow at 43.5k, 60k
]],
				},

				{
					name = L"The King's Flying Corps",
					text =
[[
Approach the Gyrocopter Pad at location 44500 58500.
]],
				},

				{
					name = L"Dwarf Engineers",
					text =
[[
Examine the Dwarf Corpse at 7.7k, 44.4k (next to the broken Gyrocopter)
]],
				},

				{
					name = L"The Slayer Oath",
					text =
[[
???
]],
				},

				{
					name = L"Ghavrin's Brace",
					text =
[[
Approach the statue at 58, 10. You will need to climb the mountain from the south to get into the town.
]],
				},

				{
					name = L"Karak Kadrin",
					text =
[[
Approach Slayer Keep at around 54300,25800
]],
				},

				{
					name = L"Flame Cannon",
					text =
[[
At 35,32 is a flame cannon. destroy it to get the unlock - Cerri 
]],
				},

				{
					name = L"Peak Pass",
					text =
[[
Enter mine at 56900,3800 for the unlock
]],
				},

				{
					name = L"Gyrocopters",
					text =
[[
Approach the Gyrocopter in the Baradum PQ at about 58825,42850
]],
				},

				{
					name = L"The Slayer Heirarchy",
					text =
[[
Click on the corpse of Brugi at 46.5k, 36k
]],
				},

				{
					name = L"Dogs of War",
					text =
[[
Click on a "Chest" in a building in the Kazad Urbar PQ (for Destruction) around 60900,59200.
]],
				},

				{
					name = L"Organ Guns",
					text =
[[
Examine the Deceased Gunner corpse at about 34400,41000
]],
				},
			},
		},

		{
			name = L"Black Crag",
			entries =
			{
				{
					name = L"Morkfang and Grimgork",
					text =
[[
10k, 27k, at PQ, walk to the end of the platform down from hut
]],
				},

				{
					name = L"Squigger",
					text =
[[
20.3k, 36.7, just click on the banner outside the orc camp of chap 15 
]],
				},

				{
					name = L"Frenzy",
					text =
[[
"Orc Corpse" on side of road South of Dwarf Chapter 20, around 50.8k, 12.7k 
]],
				},

				{
					name = L"Vandalism",
					text =
[[
Approach a vandalised statue at 30000,26900.
]],
				},

				{
					name = L"Effigies and Totems",
					text =
[[
12k, 13k, walk between the rocks toward the canyon edge
]],
				},

				{
					name = L"The Throngmortar of Sturlin Thunderhelm",
					text =
[[
Charred skull, 20185,18962,area of echo bluff in rvr area.
]],
				},

				{
					name = L"The Greenskin Muck pen",
					text =
[[
Walk in or near the Muck Pen PQ at around 33704, 24642.
]],
				},

				{
					name = L"Ur'kar's Lair",
					text =
[[
???
]],
				},

				{
					name = L"The Brother's Damgrundsson",
					text =
[[
In an ammo crate behind a boulder outside of Gudmud's Strong Huts Warcamp, around 19k, 57k 
]],
				},

				{
					name = L"Keebsta's Dead Giant-Killin' Gits",
					text =
[[
Click on the goblin tools at 12433,41494.
]],
				},

				{
					name = L"Morkfang Treasure Chest",
					text =
[[
9k, 27k at PQ, go to hut on platform, click on chest
]],
				},

				{
					name = L"Echo Bluff Watchpoint",
					text =
[[
Walk to the northern edge of the plateau at 24800,24600 for the unlock.
]],
				},

				{
					name = L"King's Bluff",
					text =
[[
Walk through area 33467,40545, in RvR area.
]],
				},

				{
					name = L"Redfang Camp",
					text =
[[
Unlocked wjhile traveling along the path at 50631, 57971.
]],
				},

				{
					name = L"Shine Trinkets",
					text =
[[
Walk near the entrance to the pit fight at around 58421, 28012.
]],
				},
			},
		},
	},
}
