TTitan.Data.Pursuits = {}

function TTitan.Data.Pursuits.GetData()
	return TTitan.Data.Pursuits.RawData
end

function TTitan.Data.Pursuits.FilterData()
	TTitan.Data.FilterByRealm(TTitan.Data.Pursuits.RawData)
end

TTitan.Data.Pursuits.RawData =
{
	header = L"Достижения Поиски", -- Pursuit Achievements
	categories =
	{
		{
			name = L"Описание страниц", -- About This Page
			entries =
			{
				{
					name = L"Поиски", -- Pursuits
					text =
[[
These are Exploration Unlocks found in the Achievements section of your ToK.
Use "/ttitan pursuits" to open the Pursuits page.
]],
				},

				{
					name = L"Пояснение", -- Credit
					text =
[[
The information found here is directly from the Warhammer Alliance Forums "The Unofficial Exploration/Pursuit Achievement Unlock Thread". You can find it here:
http://www.warhammeralliance.com/forums/showthread.php?t=180277

The thread is maintained by Socran of the Red Eye Mountain server.
]],
				},
			},
		},

		{	-- Divider, won't expand when clicked
			name = L"---------",
			divider = true,
			entries = {},
		},


		{
			name = L"Ale Run",
			entries =
			{
				{
					realm = TTitan.Data.Realm.ORDER,
					name = L"Ale: The Thirst Quencher",
					text =
[[
Ale Run (Ale: the Thirst Quencher) - MARSHES OF MADNESS. Probably (O) only. In the back of the Dwarf Chapter 6 camp is a small cave. Speak to Durgan Grimminsson inside the cave for this unlock.
By Socran.
]],
				},

				{
					realm = TTitan.Data.Realm.ORDER,
					name = L"Alebearer",
					text =
[[
Ale Run (Alebearer) - MARSHES OF MADNESS. Probably (O) only. Just south of the Dwarf Chapter 8 camp, a group of dwarfs are seen guarding and working on a machine of some sort, grumbling about lack of ale. Talking to one of them gives you this achievement.
By Socran.
]],
				},
			},
		},

		{
			name = L"Cannonball!",
			entries =
			{
				{
					name = L"Cannonball!",
					text =
[[
Cannonball! (Cannonball!) - BARAK VARR @ 10.5, 47. Both factions. Examine the Crate of Cannonballs next to one of the cannons in The Sea's Anvil.
By Kynnae.
20081123
]],
				},
			},
		},

		{
			name = L"Come One, Come All",
			entries =
			{
				{
					name = L"Wandering Rock",
					text =
[[
Come One, Come All (Wandering Rock) - TROLL COUNTRY. Probably (O) only. Speak to Raving Waldo at the Empire Chapter 5 camp. He appears to give different unlocks to different people. More info is needed.
By Zenae.
20081122
]],
				},

				{
					name = L"Part 2",
					text =
[[
Come One, Come All (Part 2) - TROLL COUNTRY. Probably (O) only. Speak to Raving Waldo at the Empire Chapter 5 camp. He appears to give different unlocks to different people. More info is needed.
By Zenae.
20081122
]],
				},

				{
					name = L"Part 3 ???",
					text =
[[
Come One, Come All (Part 3) - Unknown.
]],
				},

				{
					name = L"Part 4 ???",
					text =
[[
Come One, Come All (Part 4) - Unknown.
]],
				},

				{
					name = L"Part 5 ???",
					text =
[[
Come One, Come All (Part 5) - Unknown.
]],
				},

				{
					name = L"One For All",
					text =
[[
Come One, Come All (One For all) - TROLL COUNTRY. Probably (O) only. Speak to Raving Waldo at the Empire Chapter 5 camp. He appears to give different unlocks to different people. More info is needed.
By Kommandant, additional info by Zenae.
20081122
]],
				},
			},
		},

		{
			name = L"Da Bigga 'Un",
			entries =
			{
				{
					name = L"Da Bigga 'Un",
					text =
[[
Da Bigga 'Un (Da Bigga 'Un) - MARSHES OF MADNESS @ 27, 13. (O), almost definitely (D). Kill a level 1 Champion Orc named Da Bigga 'Un patrolling the road outside of Hargruk's Camp.
By Socran.
]],
				},

				{
					name = L"Part 2 ???",
					text =
[[
Da Bigga 'Un (Part 2) - Unknown.
]],
				},

				{
					name = L"Part 3 ???",
					text =
[[
Da Bigga 'Un (Part 3) - Unknown.
]],
				},
			},
		},

		{
			name = L"Dirty Dealings",
			entries =
			{
				{
					name = L"Enemy of my Enemy",
					text =
[[
Dirty Dealings (Enemy of my Enemy) - MOUNT BLOODHORN. Both factions, risky as (D). Approach the ruined tower inside the Dwarf Chapter 4 camp.
By Socran, Destruction confirmed by rhadamanthos
]],
				},

				{
					name = L"Part 2 ???",
					text =
[[
Dirty Dealings (Part 2) - Unknown.
]],
				},

				{
					name = L"Part 3 ???",
					text =
[[
Dirty Dealings (Part 3) - Unknown.
]],
				},
			},
		},

		{
			name = L"Durak's Journal",
			entries =
			{
				{
					name = L"Durak's Journal",
					text =
[[
Durak's Journal (Durak's Journal) - MOUNT BLOODHORN @ 26, 32. Both factions. Near the middle of the map, there's a cave with a Dwarf sitting out front called Forgotten Hold. Inside the cave you'll find a sarcophagus with a journal sitting next to it. Activate the journal for the unlock. (NOTE: Can also be found at the bottom of Glon Barak next to a Hero spider.)
By Socran, additional info by Bilious and Ullen.
]],
				},
			},
		},

		{
			name = L"Grebo's Pile",
			entries =
			{
				{
					name = L"Grebo's Pile",
					text =
[[
Grebo's Pile (Grebo's Pile) - BARAKK VARR @ 7, 35. (O), probably (D). There's a tower just south of the Greenskin Chapter 5 camp. Enter it for the unlock.
By rzeuss.
20081123
]],
				},

				{
					name = L"Part 2 ???",
					text =
[[

]],
				},
			},
		},

		{
			name = L"Суровое наказание", -- Harsh Punishment (Достижение/Поиски)
			entries =
			{
				{
					name = L"• Суровое наказание (ОСТЛАНД)",  -- Harsh Punishment
					text = L"ОСТЛАНД (28050,16900).\nВ зоне паблика Круг жнеца найти Указательный столб и осмотреть его.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nHarsh Punishment (Harsh Punishment) - OSTLAND @ 27.5, 17. Both factions. Examine the signpost in the center of the Destruction PQ Reaper's Circle.\nBy quotifon.",
				},
			},
		},

		{
			name = L"Illiteracy",
			entries =
			{
				{
					realm = TTitan.Data.Realm.ORDER,
					name = L"Illiteracy",
					text =
[[
Illiteracy (Illiteracy) - NORDLAND. Probably (O) only. In the north end of the Empire starting camp, some soldiers are standing in front of a table asking people to sign up for the military. Examine the papers on the table for the unlock.
By Socran.
]],
				},

				{
					realm = TTitan.Data.Realm.ORDER,
					name = L"Wolfram's Letter",
					text =
[[

Illiteracy (Wolfram's Letter) - ALTDORF @ 18, 21 & NORDLAND. Probably (O) only. Find the courier's building in Altdorf and talk to the man behind the desk. He'll give you a letter to deliver to Agatha Becker in Nordland. Return to the Empire starting camp and deliver the letter to her.
By Socran, specification by rzeuss.

]],
				},
			},
		},

		{
			name = L"Random Sacrifice",
			entries =
			{
				{
					name = L"Ritual of Tzeentch",
					text =
[[
Random Sacrifice (Ritual of Tzeentch) - NORSCA @ 56, 33 (multiple spawns). Both factions. Find the Altar of the Raven and lure an enemy to it. Kill them atop the altar to complete this unlock. Altar has multiple spawn points, so it may not be at the listed location.
By greggus.
]],
				},

				{
					name = L"Ritual of Khorne",
					text =
[[
Random Sacrifice (Ritual of Khorne) - NORSCA @ 48, 60 (multiple spawns). Both factions. Find the Altar of Bloodshed and lure an enemy to it. Kill them atop the altar to complete this unlock. Altar has multiple spawn points, so it may not be at the listed location.
By Assuran.
]],
				},

				{
					name = L"Ritual of Nurgle",
					text =
[[
Random Sacrifice (Ritual of Nurgle) - NORSCA @ 45, 53; 44, 34 (multiple spawns). Both factions. Find the Altar of Decay and lure an enemy to it. Kill them atop the altar to complete this unlock. Altar has multiple spawn points, so it may not be at the listed location.
By Bilious, additional info by Assuran.
]],
				},

				{
					name = L"Ritual of Slaanesh",
					text =
[[
Random Sacrifice (Ritual of Slaanesh) - NORSCA @ 38, 40. Both factions. Find the Altar of Pleasure and lure an enemy to it. Kill them atop the altar to complete this unlock. Altar has multiple spawn points, so it may not be at the listed location.
By Assuran.
]],
				},
			},
		},

		{
			name = L"Sign of Change",
			entries =
			{
				{
					name = L"Sign of Change",
					text =
[[
Sign of Change (Sign of Change) - NORSCA. Both factions. Chaos' first public quest Ruinous Powers @ 39.5k,20k; right in the middle of it, where the boss of the last stage spawns. Right-click the Eye of Change on the ground.
By ThePedant.
]],
				},
			},
		},

		{
			name = L"Stirring up Trouble",
			entries =
			{
				{
					realm = TTitan.Data.Realm.ORDER,
					name = L"Stirring up Trouble",
					text =
[[
Stirring up Trouble (Stirring up Trouble) - NORDLAND. Probably (O) only. In the Empire starting camp, find and talk to a Frightened Peasant. They appear to only be present when the witch hunting posse is searching the town. (Specification needed.)
By Socran.
]],
				},

				{
					realm = TTitan.Data.Realm.ORDER,
					name = L"Stirring up More Trouble",
					text =
[[
Stirring up Trouble (Stirring up More Trouble) - NORDLAND. Probably (O) only. In the Empire Chapter 2 camp, talk to a Frightened Citizen. They appear only to be present when the witch hunting posse is in town, which happens very rarely and very briefly (stayed for one minute after waiting hours).
By Gudrulf.
]],
				},

				{
					realm = TTitan.Data.Realm.ORDER,
					name = L"Troubling Indeed",
					text =
[[
Stirring up Trouble (Troubling Indeed) - NORDLAND. Probably (O) only. In the Empire Chapter 3 camp, talk to a Frightened Citizen. They appear to only be present when the witch hunting posse is searching the town. (Specification needed.)
By Socran.
]],
				},
			},
		},

		{
			name = L"Subversion",
			entries =
			{
				{
					name = L"Part 1",
					text =
[[
Subversion (Part 1) - NORDLAND. Both factions, skip to bottom for (O). At approximately 52k,53k in Salzenmund there is a blue tent. It's right next to the other end of Smugglers' Tunnel (this end is not shown on the map). Within it, Luthor Vogt and Dirk Eisenhauer have a short conversation concerning a mysterious 'Master'. Luthor Vogt says: "I can only give you the key, Eisenhauer. The rest is up to you." The conversation is more or less done now; Vogt will leave the tent, and a few seconds later, Eisenhauer follows. Kill Eisenhauer, get Wizard's Key, and use it. It will teleport you inside a tower, unlocking the first part Subversion. (If no conversation is going on inside the tent, Eisenhauer is wandering about Salzenmund and you may have to track him down first.) (NOTE: Eisenhauer is friendly to Order and can't be attacked by them. You must reach the tower on foot by using the rocks outside the NW exit to Smuggler's Tunnel to get up the mountainside.)
By ThePedant, Order method by Assuran.
]],
				},

				{
					realm = TTitan.Data.Realm.DESTRUCTION,
					name = L"Brotherhood of Nuln",
					text =
[[
Subversion (Brotherhood of Nuln) - NORDLAND. Apparently (D) only. After unlocking the previous part, you will be in a tower. The tower you are releported to does not appear to have any exits. However, almost at the top of the tower there is a rank 9 Hero named Hieronymus of Nuln. He will be near a portal device. Killing him unlocks the second part, Brotherhood of Nuln. After killing him the portal device can be used; it teleports you outside. (NOTE: If you came to the tower on foot, you cannot open the door to get inside. This means Order apparently cannot complete this unlock.)
By ThePedant.
]],
				},
			},
		},

		{
			name = L"Врата к всеведению", -- The Gateway to Ultimate Knowledge (поиски)
			entries =
			{
				{
					name = L"• Таинственные врата", -- Mysterious Gateway
					text = L"ОСКВЕРНЕННЫЙ ОСТРОВ (35.3к, 15.5к).\nВозле логова Гортлака найти и осмотреть Порченный кристал, который стоит у самой воды.\n\nTEZARIUS © 2009\n\n————————————————————————————————\n\nThe Gateway to Ultimate Knowledge (Mysterious Gateway) - BLIGHTED ISLE. Both factions. Near the Lair of Gorthlak, around 35.3, 15.5 (on the west side of the lake), you'll find a crystal. Activating it spawns another crystal and gives this unlock.\nBy Socran, better coords by Mimer.",
				},

				{
					name = L"Chasing Wisdom",
					text =
[[
The Gateway to Ultimate Knowledge (Chasing Wisdom) - BLIGHTED ISLE. Both factions. Near the Lair of Gorthlak, around 35.3, 15.5 (on the west side of the lake), you'll find a crystal. Activating it spawns another crystal and gives an unlock. The second crystal spawns a third and gives this unlock.
By Socran, better coords by Mimer.
]],
				},

				{
					name = L"Pursuit of Knowledge",
					text =
[[
The Gateway to Ultimate Knowledge (Pursuit of Knowledge) - BLIGHTED ISLE. Both factions. Near the Lair of Gorthlak, around 35.3, 15.5 (on the west side of the lake), you'll find a crystal. Activating it spawns another crystal and gives an unlock. The second crystal spawns a third and gives an unlock. The third gives this unlock. (NOTE: There may be conditions to spawning the third. More information is needed. The entry for the previous mentions "a mind free of desire or ambition", possibly indicating doing nothing for a moment.) (Also, Destruction cannot interact with the stone.)
By Socran, specification by Mimer and Bilious.
]],
				},

				{
					name = L"Deep in Study",
					text =
[[
The Gateway to Ultimate Knowledge (Deep in Study) - BLIGHTED ISLE. Both factions. Somehow, in interactions with the previous set of crystals, this one may spawn. More information is needed. (Destruction cannot interact with the stone.)
By Xanthian.
]],
				},

				{
					name = L"Teach Them Well",
					text =
[[
The Gateway to Ultimate Knowledge (Teach Them Well) - BLIGHTED ISLE. Both factions. Somehow, in interactions with the previous set of crystals, this one may spawn. More information is needed. (Destruction cannot interact with the stone.)
By Socran.
]],
				},

				{
					name = L"The Gateway to Ultimate Knowledge",
					text =
[[
The Gateway to Ultimate Knowledge (The Gateway to Ultimate Knowledge) - BLIGHTED ISLE. Both factions. After getting all four Stones of Wisdom to spawn, click the Redeemed Crystal in the center for the final unlock.
By Xanthian.
]],
				},
			},
		},

		{
			name = L"The Herald",
			entries =
			{
				{
					name = L"The Herald",
					text =
[[
The Herald (The Herald) - TROLL COUNTRY. Both factions. Kill this level 17 champion zealot at around 22, 11. He is friendly to Destruction, but talking to him gives the unlock and causes him to become aggressive.
By StopDropROFL, additional info by Kynnae.
]],
				},

				{
					name = L"Wandering Alone",
					text =
[[
The Herald (Wandering Alone) - TROLL COUNTRY. Both factions. After killing the Herald, a Horror will spawn. Killing it should grant the unlock.
By StopDropROFL.
]],
				},

				{
					name = L"Carrying His Word",
					text =
[[
The Herald (Carrying His Word) - TROLL COUNTRY. Both factions. Loot the "Profane Writ" off of the Herald and right-click it for this unlock. Drop is not 100%. (NOTE: This is bugged in the Tome, causing the text to run off screen. The contents of this letter remain a mystery.)
By Socran.
]],
				},
			},
		},

		{
			name = L"",
			entries =
			{
				{
					name = L"",
					text =
[[

]],
				},
			},
		},
	},
}
