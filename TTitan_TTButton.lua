TTitan.TTButton = {}

function TTitan.TTButton.Initialize()
	ButtonSetTextColor("TTitanTTButton", Button.ButtonState.NORMAL, DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b)
	ButtonSetText("TTitanTTButton", L"TT")

	WindowSetShowing("TTitanTTButtonWindow", TTitan.Settings["showbutton"])
	LayoutEditor.RegisterWindow("TTitanTTButtonWindow", L"Tome Titan Button", L"Tome Titan Button", false, false, true, nil)
end

function TTitan.TTButton.OnLClick()
	TTitan.UI.ToggleShowing()
end

function TTitan.TTButton.OnRClick()
	TTitan.UI.ExpandCurrentZone()
	TTitan.UI.Show()
end

function TTitan.TTButton.OnMouseover()
if (deb) then DEBUG(L"OnMouseoverWorldMapBtn") end
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )
	Tooltips.SetTooltipColor( 1, 1, 130, 180, 255 )
	Tooltips.SetTooltipText( 1, 1, L"             Tome Titan" )
	Tooltips.SetTooltipText( 3, 1, L"Часто задаваемые вопросы" )
	Tooltips.SetTooltipText( 4, 1, L"перевод от - TEZARIUS" )
	Tooltips.Finalize()
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_LEFT)
end
