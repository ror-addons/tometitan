TTitan.Data = {}

TTitan.Data.MapPins = {}

TTitan.Data.Realm =
{
	ORDER = GameData.Realm.ORDER,
	DESTRUCTION = GameData.Realm.DESTRUCTION,
}

function TTitan.Data.Load()
	TTitan.Data.FilterAll()
	TTitan.Data.SynchronizeAll()
end

function TTitan.Data.FilterAll()
	for _, dataSource in ipairs({ TTitan.Data.BestiaryDvG, TTitan.Data.BestiaryEvC, TTitan.Data.BestiaryHEvDE,  TTitan.Data.HistoryDvG, TTitan.Data.HistoryEvC, TTitan.Data.HistoryHEvDE, TTitan.Data.NoteworthyDvG, TTitan.Data.NoteworthyEvC, TTitan.Data.NoteworthyHEvDE, TTitan.Data.Explorations, TTitan.Data.Pursuits, TTitan.Data.Plants }) do
		dataSource.FilterData()
	end
end

-- converts to string, trims, and lowercases
function TTitan.Data.CleanString(str)
	str = tostring(str)
	str = string.gsub(str, "^%s*(.-)%s*$", "%1")
	str = str.lower(str)

	return str
end

function TTitan.Data.FilterByRealm(data)
	for categoryIndex, category in ipairs(data.categories) do
		local entries = {}

		for entryIndex, entry in ipairs(category.entries) do
			if not entry.realm or entry.realm == GameData.Player.realm then
				table.insert(entries, entry)
			end
		end

		category.entries = entries
	end
end

function TTitan.Data.SynchronizeAll()
	TTitan.Data.SynchronizeAchievements()
	TTitan.Data.SynchronizeBestiary()
	TTitan.Data.SynchronizeHistoryAndLore()
	TTitan.Data.SynchronizeNoteworthyPersons()
end

function TTitan.Data.ScanAchievements()
	local entries = {}

	for _, typeData in ipairs(TomeGetAchievementsTOC()) do
		for _, subTypeData in ipairs(typeData.subtypes) do
			if subTypeData.isUnlocked then
				currentSubTypeData = TomeGetAchievementsSubTypeData(subTypeData.id)

				for _, entryData in ipairs(currentSubTypeData.entries) do
					if entryData.isUnlocked then
						local name = TTitan.Data.CleanString(entryData.name)
						entries[name] = true
					end
				end
			end
		end
	end

	return entries
end

function TTitan.Data.SynchronizeAchievements()
	local achievements = TTitan.Data.ScanAchievements()
	for _, dataSource in ipairs({ TTitan.Data.Explorations, TTitan.Data.Pursuits, TTitan.Data.Lairs }) do
		local data = dataSource.RawData

		for _, category in ipairs(data.categories) do
			category.completed = true

			for _, entry in ipairs(category.entries) do
				local name = TTitan.Data.CleanString(entry.name)
				entry.completed = name ~= nil and achievements[name] ~= nil
				category.completed = category.completed and entry.completed
			end
		end

		dataSource.RawData = data
	end
end

function TTitan.Data.ScanBestiary()
	local entries = {}

	for _, typeData in ipairs(TomeGetBestiaryTOC()) do
		for _, subtypeData in ipairs(typeData.subtypes) do

		  	if(TomeIsBestiarySubTypeUnlocked(subtypeData.id) == true) then
				local currentSubtypeData = TomeGetBestiarySubTypeData(subtypeData.id)

				for _, speciesData in ipairs(currentSubtypeData.species) do
				  	if(TomeIsBestiarySpeciesUnlocked(speciesData.id) == true) then
						local currentSpeciesData = TomeGetBestiarySpeciesData(speciesData.id)

						for _, taskData in ipairs(currentSpeciesData.tasks) do
							if taskData.isComplete then
								local name = TTitan.Data.CleanString(taskData.text)
								entries[name] = true
							end
						end
					end
				end
			end
		end
	end

	return entries
end

function TTitan.Data.SynchronizeBestiary()
	local bestiary = TTitan.Data.ScanBestiary()
	for _, dataSource in ipairs({ TTitan.Data.BestiaryDvG, TTitan.Data.BestiaryEvC, TTitan.Data.BestiaryHEvDE }) do
		local data = dataSource.RawData

		for _, category in ipairs(data.categories) do
			category.completed = true

			for _, entry in ipairs(category.entries) do
				local name = TTitan.Data.CleanString(tostring(entry.text):match("[^-]+- (.-)\n"));
				entry.completed = name ~= nil and bestiary[name] ~= nil
				category.completed = category.completed and entry.completed
			end
		end
	end
end

function TTitan.Data.ScanHistoryAndLore()
	local entries = {}

	for _, pairingData in ipairs(TomeGetHistoryAndLoreTOC()) do
		for _, tierData in ipairs(pairingData.tiers) do
			for _, zoneData in ipairs(tierData.zones) do
				local currentZoneData = TomeGetHistoryAndLoreZoneData(zoneData.id)

				for _, entryData in ipairs(currentZoneData.entries) do
					if entryData.isUnlocked then
						local name = TTitan.Data.CleanString(entryData.name)
						entries[name] = true
					end
				end
			end
		end
	end

	return entries
end

function TTitan.Data.SynchronizeHistoryAndLore()
	local historyAndLore = TTitan.Data.ScanHistoryAndLore()
	for _, dataSource in ipairs({ TTitan.Data.HistoryDvG, TTitan.Data.HistoryEvC, TTitan.Data.HistoryHEvDE }) do
		local data = dataSource.RawData

		for _, category in ipairs(data.categories) do
			category.completed = true

			for _, entry in ipairs(category.entries) do
				local name = TTitan.Data.CleanString(entry.name)
				entry.completed = name ~= nil and historyAndLore[name] ~= nil
				category.completed = category.completed and entry.completed
			end
		end
	end
end

function TTitan.Data.ScanNoteworthyPersons()
	local entries = {}

	for _, pairingData in ipairs(TomeGetNoteworthyPersonsTOC()) do
		for _, tierData in ipairs(pairingData.tiers) do
			for _, zoneData in ipairs(tierData.zones) do
				local currentZoneData = TomeGetNoteworthyPersonsZoneData(zoneData.id)

				for _, entryData in ipairs(currentZoneData.entries) do
					if entryData.isUnlocked then
						local name = TTitan.Data.CleanString(entryData.name)
						entries[name] = true
					end
				end
			end
		end
	end

	return entries
end

function TTitan.Data.SynchronizeNoteworthyPersons()
	local noteworthyPersons = TTitan.Data.ScanNoteworthyPersons()
	for _, dataSource in ipairs({ TTitan.Data.NoteworthyDvG, TTitan.Data.NoteworthyEvC, TTitan.Data.NoteworthyHEvDE }) do
		local data = dataSource.RawData

		for _, category in ipairs(data.categories) do
			category.completed = true

			for _, entry in ipairs(category.entries) do
				local name = TTitan.Data.CleanString(entry.name)
				entry.completed = name ~= nil and noteworthyPersons[name] ~= nil
				category.completed = category.completed and entry.completed
			end
		end

		dataSource.RawData = data
	end
end

function TTitan.Data.OnTomeAlertAdded()
	local alert = DataUtils.GetTomeAlerts()[1]
	if not alert then return end

	if alert.section == GameData.Tome.SECTION_BESTIARY then
		TTitan.Data.SynchronizeBestiary()
		TTitan.UI.PrepareData()
	elseif alert.section == GameData.Tome.SECTION_HISTORY_AND_LORE then
		TTitan.Data.SynchronizeHistoryAndLore()
		TTitan.UI.PrepareData()
	elseif alert.section == GameData.Tome.SECTION_NOTEWORTHY_PERSONS then
		TTitan.Data.SynchronizeNoteworthyPersons()
		TTitan.UI.PrepareData()
	end

	TTitan.UI.TomeClearNewEntriesCheckForNew()
end
