<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">    
  <UiMod name="Tome Titan" version="v2.1.23" date="05/07/2009">        
    <Author name="Oscar,Squigy,Aenathel,TEZARIUS" email="oscarr_d24@yahoo.com / ngkaijie@hotmai.com / aenathel@dogsofwarguild.eu" />        
    <Description text="Tome Titan is the compiled information of Bestiary, History &amp; Lore, and Noteworthy Persons sections of your Tome of Knowledge." />    
    <VersionSettings gameVersion="1.9.9" />		
    <Dependencies>			
      <Dependency name="LibSlash" />        
    </Dependencies>		
    <Files>			
      <File name="TTitan.lua" />			
      <File name="TTitan_UI.lua" />			
      <File name="TTitan_UI.xml" />			
      <File name="TTitan_TTButton.lua" />			
      <File name="TTitan_TTButton.xml" />			
      <File name="TTitan_Data.lua" />			 			
      <File name="TTBar\TTmodules.lua" />			
      <File name="Data\Credits.lua" />			
      <File name="Data\Bestiary_DvG.lua" />			
      <File name="Data\Bestiary_EvC.lua" />			
      <File name="Data\Bestiary_HEvDE.lua" />			 			
      <File name="Data\Killers.lua" />			
      <File name="Data\History_DvG.lua" />			
      <File name="Data\History_EvC.lua" />			
      <File name="Data\History_HEvDE.lua" />			
      <File name="Data\Noteworthy_DvG.lua" />			
      <File name="Data\Noteworthy_EvC.lua" />			
      <File name="Data\Noteworthy_HEvDE.lua" />			
      <File name="Data\Plants.lua" />			
      <File name="Data\Talismans.lua" />			
      <File name="Data\Lairs.lua" />			
      <File name="Data\Explorations.lua" />			
      <File name="Data\Pursuits.lua" />			
      <File name="Data\Bosses.lua" />		
    </Files>        
    <OnInitialize>			
      <CreateWindow name="TTitanUI" show="false" />			
      <CreateWindow name="TTitanTTButtonWindow" show="true" />			
      <CallFunction name="TTitan.Init" />			
      <CallFunction name="TTitan.UI.Init" />			
      <CallFunction name="TTmodules.OnInitialize" />        
    </OnInitialize>		
    <OnUpdate>			
      <CallFunction name="TTitan.UI.OnUpdate" />		
    </OnUpdate>		 		
    <SavedVariables>			
      <SavedVariable name="TTitan.Settings" />		
    </SavedVariables>    
  </UiMod>
</ModuleFile>