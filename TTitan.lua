TTitan = {}
TTitan.ShowButton = true
if not TTitan.Settings then TTitan.Settings = {} end

-- setup default settings, don't forget to add to TTitan.CheckSettings() function below
TTitan.DefaultSettings = {
	["showbutton"] = true,
	["hidecompleted"] = true,
}

TTitan.Type =
{
	BESTIARY = 1,
	HISTORY = 2,
	NOTEWORTHY = 3,
}

function TTitan.Init()
	LibSlash.RegisterSlashCmd("ttitan", function(input)  TTitan.SlashCmd(input) end)
	LibSlash.RegisterSlashCmd("tometitan", function(input)  TTitan.SlashCmd(input) end)

	RegisterEventHandler(SystemData.Events.LOADING_END, "TTitan.Data.Load")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "TTitan.Data.Load")
	RegisterEventHandler(SystemData.Events.TOME_ALERT_ADDED, "TTitan.Data.OnTomeAlertAdded")
	
	TTitan.CheckSettings() 
end

-- This is for future TT versions if you add any new settings which need to be saved/loaded, they needed to be added in here and to the DefaultSettings table above.
-- This will insure the default setting is loaded since obviously a new setting won't have a value stored in the SavedVariables.lua (thus they will == nil)
function TTitan.CheckSettings()
	if TTitan.Settings["showbutton"] == nil then TTitan.Settings["showbutton"] = TTitan.DefaultSettings["showbutton"] end
	if TTitan.Settings["hidecompleted"] == nil then TTitan.Settings["hidecompleted"] = TTitan.DefaultSettings["hidecompleted"] end
end

function TTitan.Print(txt)
    EA_ChatWindow.Print(towstring(tostring(txt)));
end

function TTitan.SlashCmd(input) -- Handles the /ttitan commands
	if input == "" then
		TTitan.UI.ToggleShowing()

	elseif input == "current" then
		TTitan.UI.ExpandCurrentZone()
		TTitan.UI.Show()

	elseif input == "showcompleted" then
		TTitan.UI.ToggleHideCompleted()

	elseif input == "button" then
		TTitan.UI.ToggleButton()
		
	elseif input == "help" then
		-- due to the ingame font, the "lines" will appear different for different people
		TTitan.Print("-----------------------------------------------------")
		TTitan.Print("----Tome Titan ToK Unlock Helper----")
		TTitan.Print("-------------Main Commands---------------")
		TTitan.Print([[Use "/ttitan" to toggle the helper window on or off.]])
		TTitan.Print([[Use "/ttitan current" open to the current zone.]])
		TTitan.Print([[Use "/ttitan showcompleted" to show entries for completed unlocks. Use again to hide entries for completed unlocks.]])
		TTitan.Print([[Use "/ttitan button" to toggle the button showing on or off.]])

		elseif input == "debug" then 		-- use this for debugging
		debugvar1 = towstring(GameData.Player.zone)
		debugvar2 = GetZoneName(GameData.Player.zone)
		EA_ChatWindow.Print(L"Zone ID: "..debugvar1..L"  -  Zone Name: "..debugvar2)

	elseif input == "zonelist" then -- for internal use, not meant for normal public use
		function zonelistprint(zoneid)
			testvar1 = GetZoneName(zoneid)
			if testvar1 == "" then testvar1 = "None" end
			if testvar1 == GetZoneName(GameData.Player.zone) then EA_ChatWindow.Print(L"Hey this is where you are!") end
			EA_ChatWindow.Print(L"Zone id number: "..zoneid..L" - Zone name: "..testvar1)
		end
		for i=1,200,1 do -- list zone id's 1-200 (change this to get whatever zones you need -- do not change the last ,1)
			zonelistprint(i)
		end

	else
		TTitan.Print("/ttitan "..input.." is not a valid command, use /ttitan help")
	end
end
