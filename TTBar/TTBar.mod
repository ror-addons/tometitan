<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="TTBar" version="1.0" date="25/11/2008" >		
    <Author name="Oscarr" email="oscarr_d24@yahoo.com" />		
    <Description text="Slide out bar of buttons to select Tome Titan pages." />		
    <Dependencies>         			
      <Dependency name="EATemplate_DefaultWindowSkin" />			
      <Dependency name="EASystem_WindowUtils" />			
      <Dependency name="EA_LegacyTemplates" />			
      <Dependency name="EA_UiDebugTools" />			
      <Dependency name="Tome Titan" />		
    </Dependencies>		
    <Files>         			
      <File name="TTBar.xml" />			
      <File name="TTBar_Setup.lua" />		
    </Files>		 		
    <OnInitialize>			
      <CreateWindow name="TTBarActivator" show="true" />			
      <CallFunction name="TTBar.SetupPages" />		
    </OnInitialize>              		 	
  </UiMod>
</ModuleFile>    