-------------- Setup for TTBar--------------
--------------------------------------------------

--[[  Some good icon numbers:
59 - looks like a ToK book,  
113-120 - shield icons with various crests, 
141 - quill pen + numbers thingy, 
205 - book with rune on the cover, 
219 - potion icon,  
300 - candle, 
327 - open book,  
565 - cool lookin book (closed), 
577 - book with a skull on it, 
583 - book with a dragon, 
615 - banner,  
680 - cold one head, 
699 - roses,  
907/908 - scroll with skull,  
928 - sealed letter, 
934 - standard bearer icon,  
2579 - bar of soap(lol?), 
2585 - greenskin choking dwarf,  
2603 - crazy looking squig, 
2613 - arrows busting throw a wood shield, 
5137 - 2 dragons facing each other, 
5234, chaos monument, 
5262 - spell book(?) dripping blue fire,  
8018 - man breathing fire
8028 - heart on fire with icon on heart, 
10524-10527 - different books, 
10951 - scroll,  
11784 - nearly naked DE torso, 
20065 - 20128 - alphabet, 
20409 - seed, 
20449 - voodoo doll,  
Tactic Icons: 22650-22737,  
23057 - deck of cards(?), 

Apothecary page --> 219 probably
605 - seeds (like repeatable cultivation quests)
--]]

-- Create the Icons/Buttons for the Pages --
-- the order they're listed in is the order they appear on the bar, from left to right 
function TTBar.SetupPages()
	-- FAQ/Credits Page
	TTBar:RegisterPage (L"Tome Titan", L"FAQ, Описание и Пояснения", TTBar.Credits, 20084)
	-- Bestiary Pages
	TTBar:RegisterPage (L"Бестиарий", L"Гномы против Зеленокожих", TTBar.BestiaryDvGButton, 00278)
	TTBar:RegisterPage (L"Бестиарий", L"Империя против Хаоса", TTBar.BestiaryEvCButton, 00278)
	TTBar:RegisterPage (L"Бестиарий", L"Высшие эльфы против Темных эльфов", TTBar.BestiaryHEvDEButton, 00278)
	-- Bestiary Killers
	--TTBar:RegisterPage (L"Бестиарий", L"Убийства монстров", TTBar.KillersButton, 08028)
	TTBar:RegisterPage (L"Бестиарий", L"Убийства монстров", TTBar.PlantsButton, 23065)
	-- History/Lore Pages
	TTBar:RegisterPage (L"История и знания", L"Гномы против Зеленокожих", TTBar.HistoryDvGButton, 00327) 
	TTBar:RegisterPage (L"История и знания", L"Империя против Хаоса", TTBar.HistoryEvCButton, 00327)
	TTBar:RegisterPage (L"История и знания", L"Высшие эльфы против Темных эльфов", TTBar.HistoryHEvDEButton, 00327)
	-- Noteworthy Persons Pages
	TTBar:RegisterPage (L"Известные личности", L"Гномы против Зеленокожих", TTBar.NoteworthyDvGButton, 10951)
	TTBar:RegisterPage (L"Известные личности", L"Империя против Хаоса", TTBar.NoteworthyEvCButton, 10951)
	TTBar:RegisterPage (L"Известные личности", L"Высшие эльфы против Темных эльфов", TTBar.NoteworthyHEvDEButton, 10951)
	-- Extras Pages
	TTBar:RegisterPage (L"Достижения", L"Исследования", TTBar.ExplorationsButton, 00732) -- old 05234 (00732)
	TTBar:RegisterPage (L"Достижения", L"Поиски", TTBar.PursuitsButton, 00761) -- old 08028 (00761)
	TTBar:RegisterPage (L"Достижения", L"Элитные Логова", TTBar.LairsButton, 05137)
	--
	--TTBar:RegisterPage (L"Производство", L"Cultivation Information", TTBar.PlantsButton, 20409)
	TTBar:RegisterPage (L"Производство", L"Талисмановодство", TTBar.TalismansButton, 00285)
	TTBar:RegisterPage (L"Стратегия", L"Описание тактик на Босах", TTBar.IBFButton, 02448)
	--TTBar:RegisterPage (L"+++", L"------------", TTBar.IBFButton, 11000)
end

-- 00283, 00289, 08028, 23065


-- Set the function for each Page --
function TTBar.Credits()
	for _, category in ipairs(TTitan.UI.Data.categories) do
		category.expanded = false
	end
	TTitan.UI.SetDataSource(TTitan.Data.Credits)
	TTitan.UI.Show()
end

function TTBar.BestiaryDvGButton()
	TTitan.UI.Type = TTitan.Type.BESTIARY
	TTitan.UI.SetDataSource(TTitan.Data.BestiaryDvG)
	TTitan.UI.Show()
end

function TTBar.HistoryDvGButton()
	TTitan.UI.Type = TTitan.Type.HISTORY
	TTitan.UI.SetDataSource(TTitan.Data.HistoryDvG)
	TTitan.UI.Show()
end

function TTBar.NoteworthyDvGButton()
	TTitan.UI.Type = TTitan.Type.NOTEWORTHY
	TTitan.UI.SetDataSource(TTitan.Data.NoteworthyDvG)
	TTitan.UI.Show()
end

function TTBar.BestiaryEvCButton()
	TTitan.UI.Type = TTitan.Type.BESTIARY
	TTitan.UI.SetDataSource(TTitan.Data.BestiaryEvC)
	TTitan.UI.Show()
end

function TTBar.HistoryEvCButton()
	TTitan.UI.Type = TTitan.Type.HISTORY
	TTitan.UI.SetDataSource(TTitan.Data.HistoryEvC)
	TTitan.UI.Show()
end

function TTBar.NoteworthyEvCButton()
	TTitan.UI.Type = TTitan.Type.NOTEWORTHY
	TTitan.UI.SetDataSource(TTitan.Data.NoteworthyEvC)
	TTitan.UI.Show()
end

function TTBar.BestiaryHEvDEButton()
	TTitan.UI.Type = TTitan.Type.BESTIARY
	TTitan.UI.SetDataSource(TTitan.Data.BestiaryHEvDE)
	TTitan.UI.Show()
end

function TTBar.HistoryHEvDEButton()
	TTitan.UI.Type = TTitan.Type.HISTORY
	TTitan.UI.SetDataSource(TTitan.Data.HistoryHEvDE)
	TTitan.UI.Show()
end

function TTBar.NoteworthyHEvDEButton()
	TTitan.UI.Type = TTitan.Type.NOTEWORTHY
	TTitan.UI.SetDataSource(TTitan.Data.NoteworthyHEvDE)
	TTitan.UI.Show()
end

function TTBar.KillersButton()
	TTitan.UI.SetDataSource(TTitan.Data.Killers)
	TTitan.UI.Show()
end

function TTBar.ExplorationsButton()
	TTitan.UI.SetDataSource(TTitan.Data.Explorations)
	TTitan.UI.Show()
end

function TTBar.PursuitsButton()
	TTitan.UI.SetDataSource(TTitan.Data.Pursuits)
	TTitan.UI.Show()
end

function TTBar.LairsButton()
	TTitan.UI.SetDataSource(TTitan.Data.Lairs)
	TTitan.UI.Show()
end


function TTBar.PlantsButton()
	TTitan.UI.Type = TTitan.Type.BESTIARY
	TTitan.UI.SetDataSource(TTitan.Data.Plants)
	TTitan.UI.Show()
end

function TTBar.TalismansButton()
	TTitan.UI.SetDataSource(TTitan.Data.Talismans)
	TTitan.UI.Show()
end

function TTBar.IBFButton()
	TTitan.UI.SetDataSource(TTitan.Data.IBF)
	TTitan.UI.Show()
end
