-- About this Add-on:
-- The code here is from the DevBarMod add-on with functions renamed so it'll be compatible, also registered the window with the Layout Editor so you can move it around

----------------------------------------------------------------------------------------
-- TTBarData : Wraps access to name, description, activation, and icon information.
----------------------------------------------------------------------------------------

TTBarData = {}
TTBarData.__index = TTBarData
setmetatable (TTBarData, TTBarData)

function TTBarData:Create (name, description, activatorFunction, iconNum)
    local ttBarData = setmetatable ({}, TTBarData)
    
    if (ttBarData)
    then
        ttBarData.m_Icon           = iconNum
        ttBarData.m_Name           = name
        ttBarData.m_Description    = description
        ttBarData.m_Activator      = activatorFunction
    end
    
    return ttBarData
end

function TTBarData:GetPageIcon ()
    return self.m_Icon
end

function TTBarData:GetPageName ()
    return self.m_Name
end

function TTBarData:GetPageDescription ()
    return self.m_Description
end

function TTBarData:CallActivator ()
    if (type (self.m_Activator) == "string")
    then
        WindowSetShowing (self.m_Activator, not WindowGetShowing (self.m_Activator))
    elseif (type (self.m_Activator) == "function")
    then
        self.m_Activator ()
    else
        DEBUG (L"Unable to call activator for TTBar: ["..self:GetPageName ()..L"]");
    end
end

----------------------------------------------------------------------------------------
-- TTBarButton : Wraps a button that activates a page
----------------------------------------------------------------------------------------

TTBarButton = Frame:Subclass ("TTBarButton")

function TTBarButton:Create (windowName, parentName, ttBarData)
    local newTTBarButton = self:CreateFromTemplate (windowName, parentName)
    
    if (newTTBarButton)
    then
        newTTBarButton.m_TTBarData = ttBarData
        
        local texture, x, y, disabledTexture = GetIconData (ttBarData:GetPageIcon ())
        DynamicImageSetTexture (windowName.."IconBase", texture, x, y)
        
        newTTBarButton:Show (true)
    end
    
    return newTTBarButton
end

function TTBarButton.MouseOver ()
    local TTBarButton = FrameManager:GetMouseOverWindow ()
    
    if (TTBarButton) 
    then
        local ttBarData           = TTBarButton.m_TTBarData
        local TTBarButtonName  = TTBarButton:GetName ()
        
        Tooltips.CreateTextOnlyTooltip (TTBarButtonName)
        Tooltips.SetTooltipText (1, 1, ttBarData:GetPageName ())
        Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_HEADING)
        Tooltips.SetTooltipText (2, 1, ttBarData:GetPageDescription ())
        Tooltips.Finalize();
        Tooltips.AnchorTooltip( { Point = "topleft",  RelativeTo = TTBarButtonName, RelativePoint = "bottomleft",   XOffset = 0, YOffset = -10 } )
    end
end

function TTBarButton.LButtonUp ()
    local TTBarButton = FrameManager:GetMouseOverWindow ()
    
    if (TTBarButton)
    then
        TTBarButton.m_TTBarData:CallActivator ()
    end 
end

----------------------------------------------------------------------------------------
-- TTBar : Ties the whole mess together, allows pages to register themselves for
--          easy activation
------------------------------------------------------------------------------------------

local c_DEFAULT_HIDE_TIMER = 1

TTBar = 
{
    m_HideCountdown     = c_DEFAULT_HIDE_TIMER,
    m_IsShowing         = false,
}

function TTBar.Initialize ()
    CreateWindow ("TTBar", false)
    WindowSetMovable("TTBarActivator", true)

	LayoutEditor.RegisterWindow("TTBarActivator", L"Tome Titan Bar", L"Tome Titan Bar", false, false, true, nil)
end

function TTBar.Shutdown ()

end

function TTBar.Show ()
    WindowSetShowing ("TTBar", true)
    TTBar.m_IsShowing      = true
    TTBar.m_HideCountdown  = c_DEFAULT_HIDE_TIMER
end

function TTBar.Hide ()
    WindowSetShowing ("TTBar", false)
    TTBar.m_IsShowing      = false;
    TTBar.m_HideCountdown  = c_DEFAULT_HIDE_TIMER
end

function TTBar.Update (elapsedTime)
    if (TTBar.m_IsShowing and TTBar.m_HideCountdown > 0)
    then
        TTBar.m_HideCountdown = TTBar.m_HideCountdown - elapsedTime
        
        if (TTBar.m_HideCountdown <= 0)
        then
            local windowName        = SystemData.MouseOverWindow.name
            local overTTBarOrChild = false
            
            -- If the mouse over window is not part of the TTBar
            -- then hide the TTBar  
            -- Otherwise just reset
            
            while (windowName ~= "Root")
            do
                if (windowName == "TTBar" or WindowGetParent (windowName) == "TTBar")
                then
                    overTTBarOrChild = true
                    break
                end
                
                windowName = WindowGetParent (windowName)
            end
            
            if (overTTBarOrChild == false)
            then
                TTBar.Hide ()
            else
                TTBar.m_HideCountdown = c_DEFAULT_HIDE_TIMER
            end
        end
    end
end

function TTBar:RegisterPage (name, description, activationFunction, icon)
    local buttonId = 0
    
    if (self.m_RegisteredPages == nil)
    then
        self.m_RegisteredPages = {}
        buttonId = 1
    else
        buttonId = #(self.m_RegisteredPages) + 1
    end
    
    local ttBarData   = TTBarData:Create (name, description, activationFunction, icon)
    local ttBarButton = TTBarButton:Create ("TTBarButton"..buttonId, "TTBar", ttBarData)
    
    -- Simply anchoring buttons left to right.
    if (ttBarButton)
    then
        if (buttonId == 1)
        then
            ttBarButton:SetAnchor ({Point = "topleft", RelativeTo = "TTBar", RelativePoint = "topleft", XOffset = 5, YOffset = 5 })
        else
            ttBarButton:SetAnchor ({Point = "right", RelativeTo = "TTBarButton"..(buttonId - 1), RelativePoint = "left", XOffset = 2, YOffset = 0 })
        end

        self.m_RegisteredPages[buttonId] = ttBarButton
        self:Resize ()    
    end
end

function TTBar:Resize ()
    local width         = 0
    local height        = 0
    
    local numColumns    = 1
    
    if (self.m_RegisteredPages ~= nil)
    then
        numColumns = #(self.m_RegisteredPages)
    end
    
    local numRows       = 1
    local border        = 5 * 2
    local horizontalPad = 2 * (numColumns - 1)
    local verticalPad   = 2 * 0
    local buttonWidth   = 55
    local buttonHeight  = 55
    local fudgeHeight   = 5
    
    if (self.m_RegisteredPages)
    then
        local button = self.m_RegisteredPages[1]
        
        if (button)
        then
            buttonWidth, buttonHeight = WindowGetDimensions (button:GetName ()) 
        end
    end
        
    width   = (numColumns   * buttonWidth)  + horizontalPad + border
    height  = (numRows      * buttonHeight) + verticalPad   + border + fudgeHeight
    
    WindowSetDimensions ("TTBar", width, height)
end