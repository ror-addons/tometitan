-- Author: Oscarr // Date: 2008.11.16
-- This handles primarily checking for any of the popular Bar/Board addons, and then checking if they have old versions of Tome Titan modules installed, then disabling the old versions if they are installed/enabled
-- Currently Supports SNT_Bar, WaaaghBarShockBar and WarBoard.


if not TTmodules then TTmodules = {} end

-- Register events for initial loading and reload ui
--    We check reload_interface incase someone turns on/off one of the boards (like me, where I sometimes have to disable SNT_BAR and enable WaaaghBar then do a /reload)
function TTmodules.OnInitialize()
	RegisterEventHandler( SystemData.Events.LOADING_END, "TTmodules.OnLoad" )
	RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE, "TTmodules.OnLoad" )
end

-- check to see if any of the 3 more popular Board/Bar mods are installed
function TTmodules.OnLoad()
	TTmodules.Check_SNT_BAR()
	TTmodules.Check_WaaaghBar()
	TTmodules.CheckWarBoard()
end

--- Check for SNT_BAR
function TTmodules.Check_SNT_BAR()
	d("Searching for SNT_BAR...")
	local mods = ModulesGetData()
	for k,v in ipairs(mods) do
		if v.name == "SNT_BAR" and v.isEnabled then
			if not v.isLoaded then
				ModuleInitialize(v.name);
			end
			d("  SNT_BAR Found! Checking for outdated module versions...")
			TTmodules.CheckForOld_SNT_BAR_module()
			return
		end
	end
	d("  SNT_BAR was not found.")
end

-- if SNT_BAR was found, then this is called
function TTmodules.CheckForOld_SNT_BAR_module()
	local mods = ModulesGetData()
	for k,v in ipairs(mods) do
		if v.name == "SNT_BAR-Tome Titan" and v.isEnabled then -- check for old SNT_BAR Tome Titan addon
			d("    Outdated module found! Disabling!")
			DialogManager.MakeOneButtonDialog(L"Old version of SNT_BAR Tome Titan found. Disabling it! -- Please uninstall it, as it is built into TomeTitan now.",L"Ok", nil)
			EA_ChatWindow.Print(L"!!PLEASE NOTE: You have an out dated version of SNT_BAR Tome Titan installed -- please uninstall it, as it is built into TomeTitan now. (Disabling outdated version.)")
			ModuleSetEnabled("SNT_BAR-Tome Titan", false) -- unload the old / outdated version
			return
		end
	end
	d("    None Found!")
end

-- Check for WaaaghBar
function TTmodules.Check_WaaaghBar()
	d("Searching for WaaaghBar...")
	local mods = ModulesGetData()
	for k,v in ipairs(mods) do
		if v.name == "WaaaghBar" and v.isEnabled then
			if not v.isLoaded then
				ModuleInitialize(v.name);
			end
			d("  WaaaghBar Found! Checking for outdated module versions...")
			TTmodules.CheckForOld_WaaaghBar_module()
			return
		end
	end
	d("  WaaaghBar was not found.")
end

-- if WaaaghBar was found, then this is called
function TTmodules.CheckForOld_WaaaghBar_module()
	local mods = ModulesGetData()
	for k,v in ipairs(mods) do
		if v.name == "WaaaghBar_TomeTitan" and v.isEnabled then -- check for old WaaaghBar Tome Titan addon
			d("    Outdated module found! Disabling!")
			DialogManager.MakeOneButtonDialog(L"Old version of WaaaghBar Tome Titan found. Disabling it! -- Please uninstall it, as it is built into TomeTitan now.",L"Ok", nil)
			EA_ChatWindow.Print(L"!!PLEASE NOTE: You have an out dated version of WaaaghBar Tome Titan installed -- please uninstall it, as it is built into TomeTitan now. (Disabling old version.)")
			ModuleSetEnabled("WaaaghBar_TomeTitan", false) -- unload the old / outdated version
			return
		end
	end
	d("    None Found!")
end

-- Check for WarBoard (For no reason... ?)
function TTmodules.CheckWarBoard()
	d("Searching for WarBoard...")
	local mods = ModulesGetData()
	for k,v in ipairs(mods) do
		if v.name == "WarBoard" and v.isEnabled then
			if not v.isLoaded then
				ModuleInitialize(v.name);
			end
			d("  WarBoard Found!")
			-- There is no Old WarBoard module, this is new, so no CheckForOld needs to be done.
			return
		end
	end
	d("  WarBoard was not found.")
end



