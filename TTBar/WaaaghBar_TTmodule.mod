<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="WaaaghBar_TTmodule" version="1.0" date="11/17/2008">		
    <Author name="Oscarr" email="oscarr_d24@yahoo.com" />		
    <Description text="Tome Titan module for WaaaghBar" />		
    <Dependencies>			
      <Dependency name="WaaaghBar" />			
      <Dependency name="Tome Titan" />		
    </Dependencies>		
    <Files>			
      <File name="WaaaghBar_TTmodule.lua" />		
    </Files>		
    <OnInitialize>			
      <CallFunction name="WTTitan.Initialize" />		
    </OnInitialize>	
  </UiMod>
</ModuleFile>