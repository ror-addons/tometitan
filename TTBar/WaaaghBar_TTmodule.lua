-- WaaaghBar module for Tome Titan
-- Supports both ShockBeta and LongLing's rewrites/continuations of WaaaghBar
-- author: Oscarr <oscarr_d24@yahoo.com>
-- Feel free to use this for anything you wish.


WTTitan = {}

function WTTitan.Initialize()
	WTTitan.Waaagh = WaaaghBar.NewPlugin("WaaaghBarTT")
	WTTitan.Button = WTTitan.Waaagh:NewElement("Button", 25, L"WaaaghBar Tome Titan", { r = 255, g = 55, b = 55 }, 00100)
	WTTitan.Button:SetText(towstring("TT"))
	-- Register mouse Events
	WindowRegisterCoreEventHandler(WTTitan.Button.name, "OnMouseOver", "WTTitan.OnMouseOver")
	WindowRegisterCoreEventHandler(WTTitan.Button.name, "OnLButtonDown", "WTTitan.OnLButtonDown")
	WindowRegisterCoreEventHandler(WTTitan.Button.name, "OnRButtonDown", "WTTitan.OnRButtonDown")
	WindowRegisterCoreEventHandler(WTTitan.Button.name, "OnMButtonDown", "WTTitan.OnMButtonDown")
	WindowSetShowing("TTitanTTButtonWindow", false) -- Hide the TomeTitan button by default
	TTitan.ShowButton = false
	EA_ChatWindow.Print(L"Tome Titan WaaaghBar Module Loaded!")
end


function WTTitan.OnMouseOver()
    Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )
    if WaaaghBar.GetTooltipsAnchor() then 						-- Check for function from LongLing's WaaaghBar for determining bar location
		Tooltips.AnchorTooltip( WaaaghBar.GetTooltipsAnchor() ) -- and use it if it exists...
	else														-- otherwise...
		Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_BOTTOM )	-- just anchor it at the bottom.
	end
	Tooltips.SetTooltipColor( 1, 1, 255, 0, 0 )
    Tooltips.SetTooltipText( 1, 1, L"Tome Titan WaaaghBar Module" )
	Tooltips.SetTooltipText( 2, 1, L"Left click to toggle Tome Titan" )
	Tooltips.SetTooltipText( 3, 1, L"Right click to open to current zone" )
	Tooltips.SetTooltipText( 4, 1, L"Middle click to toggle TT button" )
	Tooltips.Finalize()
end

function WTTitan.OnLButtonDown()
	TTitan.UI.ToggleShowing()
end

function WTTitan.OnRButtonDown()
	TTitan.UI.ExpandCurrentZone()
	TTitan.UI.Show()
end

function WTTitan.OnMButtonDown()
	TTitan.UI.ToggleButton()
end

WaaaghBar.WTTitan = WTTitan

